<?php

    require_once("../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      //echo $database->session_user_id;

      if(!isset($_GET["rs_id"])) {

          header("Location: ../manager.php");

      } else {


            if(isset($_POST["submit_reserve"])) {
                      $rs_category = $_POST["rs_category"];
                      $rs_id = $_POST["rs_id"];
                      $rs_name = $_POST["rs_name"];
                      $rs_location = $_POST["rs_location"];
                      $rs_description = $_POST["rs_description"];
                      $rs_capacity = $_POST["rs_capacity"];
                      $rs_price = $_POST["rs_price"];
                      $rs_start_date = $_POST["rs_start_date"];
                      $rs_start_time = $_POST["rs_start_time"];
                          $rs_start_time_format =date("H:i:s",strtotime($rs_start_time));
                      $rs_stop_date = $_POST["rs_stop_date"];
                      $rs_stop_time = $_POST["rs_stop_time"];
                          $rs_stop_time_format =date("H:i:s",strtotime($rs_stop_time));

    /*                  $upload->set_photo_name($_FILES["file_upload"]["name"]);
                      $upload->set_file_type($_FILES["file_upload"]["type"]);
                      $upload->set_tmp_loc($_FILES["file_upload"]["tmp_name"]);
                      $upload->set_file_error($_FILES["file_upload"]["error"]);

                      if($upload->validate_image()) { 
                            $database->set_rs_photo_path($upload->Object_path); 
                      }*/
                      
                        //$automation->generate_rs_id();
                        
                       $database->set_rs_category($rs_category);
                        $database->set_rs_id($rs_id);
                        $database->set_user_id($database->session_user_id);
                        $database->set_rs_name($rs_name);
                        $database->set_rs_description($rs_description);
                        $database->set_rs_stock($rs_capacity);
                        $database->set_rs_price($rs_price);
                        $database->set_rs_location($rs_location);
                        $database->set_rs_start_date($rs_start_date);
                        $database->set_rs_start_time($rs_start_time_format);
                        $database->set_rs_stop_date($rs_stop_date);
                        $database->set_rs_stop_time($rs_stop_time_format);
                        
                        if($database->update_reserve_table($database->session_user_id)) {
                          $alert->message("Reserve Updated","Success");
                        } else {
                          $alert->message("Failed to Update Reserve","Fail");
                        }


              }

        }

      
    } else {

        header("Location: ../index.php");
  
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="../icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
     <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/styling/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="../Front End/e-shama.css" />-->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine| Staatliches|Montserrat|Raleway|Merriweather|Anton|EB Garamond|Varela Round|Josefin Sans|Abel|Roboto Slab|Fahkwang|Staatliches|Hammersmith One|Orbitron|IBM Plex Sans|Rationale|Meera Inimai|Mallanna|Sarpanch">
    <link rel="stylesheet" type="text/css" media="screen" href="Authentication/styles/css/font-awesome.min.css" />
    <link rel="stylesheet" href="../Front End/node_modules/material-design-lite/material.min.css">
    <script src="../Front End/node_modules/material-design-lite/material.min.js"></script>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="../Front End/admin.css" rel="stylesheet">

</head>
<body >
     
      <div class="navbar navbar-expand-lg bg-dark navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="../manager.php" class="pull-left"><img class="img-responsive2"       
                    src="../Front End/Images/pop-in.png" style="margin-top:1.2rem;"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar">

                <a class="nav-item menuItem" href="../manager.php" style="text-decoration: none; font-family: 'Josefin Slab'; color : #fff; font-size: 40px; padding: 30px;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

      </div>

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

        <h2 style="text-align: center; font-family:'Varela Round'; color : #96980f;  ">MANAGER <?php echo strtoupper($database->session_username); ?></h2>

          <div class="my-flex-item managementContent">

            <?php

                $reserve_id = $_GET["rs_id"];

                $database->fetch_specific_reserve_data($reserve_id);


            ?>


              <div id="editReservePanel">
                <h3 style=" color : #000; font-family: 'Varela Round';">UPDATE EVENT</h3>

                  <form id="editReserveForm" action="editReserve.php?rs_id=<?php echo $reserve_id; ?>" method="POST" enctype="multipart/form-data">
                        <p><select name="rs_category" class="form-control">
                          <option>Music</option>
                          <option>Movies</option>
                          <option>Sports</option>
                        </select></p>

                        <p><input class="form-control" type="text" name="rs_name" placeholder="Reserve Name" value="<?php echo $database->fetched_rs_name; ?>" required></p>
                      
                        <p><input class="form-control" type="date" name="rs_start_date" placeholder="Starting Date Of Reserve" 
                          value="<?php echo $database->fetched_rs_start_date; ?>" required></p>
                   
                        <p><input class="form-control" type="time"  name="rs_start_time" placeholder="Starting Time Of Reserve" 
                          value="<?php echo $database->fetched_rs_start_time; ?>" required></p>

                        <p><input class="form-control" type="date" name="rs_stop_date" placeholder="Stopping Date Of Reserve" 
                          value="<?php echo $database->fetched_rs_stop_date; ?>" required></p>
                   
                        <p><input class="form-control" type="time"  name="rs_stop_time" placeholder="Stopping Time Of Reserve" 
                          value="<?php echo $database->fetched_rs_stop_time; ?>" required></p>
                 
                       <!--  <p><input class="input-group" type="file" name="file_upload" accept="image/*" 
                          value="<?php /*echo $database->fetched_rs_photo_path; */?>" required></p> -->

                        <p><input class="form-control" type="text" name="rs_location" placeholder="Enter Location" 
                          value="<?php echo $database->fetched_rs_location; ?>" required></p>

                        <p><input class="form-control" type="hidden" name="rs_id" 
                          value="<?php echo $database->fetched_rs_id; ?>" required></p>

                        <p><input class="form-control" type="text" name="rs_capacity" placeholder="Enter reserve capacity eg Unlimited or 20" 
                          value="<?php echo $database->fetched_rs_stock; ?>" required></p>
               
                        <p><input class="form-control" type="text" name="rs_price" placeholder="Enter Price" 
                          value="<?php echo $database->fetched_rs_price; ?>" required></p>

                        <p><textarea class="form-control" rows="4" cols="20" name="rs_description" placeholder="description">
                          <?php echo trim($database->fetched_rs_description); ?></textarea></p>

                        <p><input type="submit" value="Upload Reserve" name="submit_reserve" class="btn btn-primary" style="background: #96980f;"></p>

                      </form>


              </div><!-- end of editReservePanel div-->

             
            </div>


          </div>

    </div>



<footer class="bg-dark" id="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Pop-In</h2>
                  <hr>
                  <p>Pop-in to buy advance tickets to upcoming reserves</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Contact us</h2>
                  <hr>
                 <ul>
                     <li style="list-style: none;"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Navigation</h2>
                  <hr>
                 <ul>
                     <a href="login.php"><li style="list-style: none;">Home</li></a>
                     <a href="login.php"><li style="list-style: none;">Login</li></a>
                     <a href="register.php"><li style="list-style: none;">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p>&copy copyright Pop-In all Rights Reserved: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="../Front End/styling/script.js"></script>
    <script src="../Front End/scripting/e-shama.js"></script>
    <script src="../Front End/ShamaScript.js"></script>

    </script>


  <!--side bars-->
      
</body>
</html>




