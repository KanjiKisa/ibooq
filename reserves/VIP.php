<?php

  require_once("../Customers/includes/initialise.php");

  if($session->is_logged) {

      $database->get_session_details($session->session_id); 

        $OwnerId = $database->session_user_id;
        
        if(isset($_POST['delete'])) {

            $reserve_id = $_POST["reserve_id"];

            if($database->delete_reserve($reserve_id)) {
              $alert->message("Reserve Deleted","Success");
            } else {
              $alert->message("Failed to delete Reserve","Fail");
            }
          
        }

      } else {

          header("Location: ../../index.php");

      }


?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="../icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela Round|Oswald|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style2.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/styling/style.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/e-shama.css" />
    <link rel="stylesheet" href="../Front End/node_modules/material-design-lite/material.min.css">
    <script src="../Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="../Front End/custom.css" rel="stylesheet">

</head>
<body >
     
      <div class="navbar navbar-expand-lg bg-dark navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="../manager.php" class="pull-left"><img class="img-responsive2"       
                    src="../Front End/Images/pop-in.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar">

                <a class="nav-item menuItem" href="index.php" style="text-decoration: none; font-family: 'Varela Round'; color : #df3d82; font-size: 1.2rem; padding: 0.8rem 0;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

      </div>

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

        <h1 style=" color: #96980f; font-family: 'Oswald'; ">MANAGER <?php echo strtoupper($database->session_username); ?></h1>

          <div class="my-flex-item managementContent">



              <div id="sidebar">
                <ul id="managementTab">
                  <li  class="specificManagementTab" onclick="show_specific_reserve_panel()">VIP Table Reserves</li>
                  <li onclick="show_specific_teller_panel()">Accounting</li>
                </ul><!-- end of managementTab ul -->
              </div><!-- end of sidebar div-->


              <div id="specificReservePanel">


                  <div class="mdl-grid" style="justify-content: center; ">
              

                    <?php 
                  
                        $sql = $database->fetch_reserve_by_owner_category($OwnerId,"VIP Table");


                        foreach($sql as $row) {

                          $category = $row["Category"];
                          $reserveId = $row["ReserveId"];
                          $ownerId = $row["OwnerId"];
                          $reserveName = $row["ReserveName"];
                          $reserveDesc = $row["ReserveDesc"];
                          $picPath = $row["PicPath"]; 
                          $reserveStock = $row["ReserveStock"];
                          $reservePrice = $row["ReservePrice"];
                          $reserveLocation = $row["ReserveLocation"];
                          $Date = $row["ReserveDate"];
                          $Time = $row["ReserveTime"];

                    ?>

                        <div class="card mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone product-card" style="margin-left: 10px; ">

                          <img class="card-img-top product" src="<?php echo "../".$picPath; ?>" alt="" >
                          <div class="card-body">

                            <p class="card-text"><?php echo $reserveName; ?></p>
                            <p class="card-text">

                              <form action='VIP.php' method='POST'>
                                <input type='hidden' value="<?php echo $reserveName; ?>" name="reserve_name">
                                <input type='hidden' value="<?php echo $reservePrice; ?>" name="reserve_price">
                                <input type='hidden' value="<?php echo $reserveId; ?>" name="reserve_id">
                                <input type='hidden' value="<?php echo $category; ?>" name="reserve_cat">
                                <input type='submit' class="form-control" value='Del' name='delete' style="background: #96980f; width: 30%; float:left; margin-left: 4px;">
                                <a role='button' class="btn btn-primary"  href="editReserve.php?rs_id=<?php echo $reserveId; ?>" style="background: #96980f; width: 30%; float:left; margin-left: 4px;">Edit</a>																<a href=".modal" data-toggle="modal" data-book-id="<?php echo $reserveId; ?>" class="btn btn-primary">Make Online</a>
                              </form>

                           </p>

                          </div>

                        </div><!-- close of card class div-->

                    <?php 


                       }


                    ?>


                </div><!-- close of mdl-grid class div--> 

            </div><!-- end of productPanel div -->

              <div id="specificTellerPanel">


                 <div class="my-flex-item managerAccounting">

                   <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">Data</th>
                        </tr>
                      </thead>

                      <tbody>

                        <?php

                            $totalReserves = $payment->count_reserves_by_owner_cat($OwnerId,"VIP Table");
                            $totalAmount = $payment->sum_of_reserves_total_by_owner_cat($OwnerId,"VIP Table");

                            $percantage_total = $totalAmount * 0.06;

                            $final_pay = $totalAmount - $percantage_total;

                        ?>

                        <tr>
                            <th>Total Reserves</th>
                            <td><?php echo $totalReserves; ?></td>
                        </tr>

                        <tr>
                            <th>Total Amount</th>
                            <td><?php echo $totalAmount; ?></td>
                        </tr>

                        <tr>
                            <th>% Pay Per Reserve</th>
                            <td><b>6%</b></td>
                        </tr>

                        <tr>
                            <th>% Pay Total</th>
                            <td><?php echo $percantage_total; ?></td>
                        </tr>

                        <tr>
                            <th>Your Amount After % Pay</th>
                            <td><?php echo $final_pay; ?></td>
                        </tr>

                      </tbody>

                    </table>

                  </div>

                  <div class="my-flex-item managerAccounting" >

                    <input type="text" class="form-control" id="searchPay" onkeyup="searchPayments()" placeholder="Search by Reserve Name / Transaction Code / Payer Phone.." >

                  <table class="table table-striped" id="payTable">
                      <thead>
                        <tr>
                          <th scope="col">Reserve Name</th>
                          <th scope="col">Transaction Code</th>
                          <th scope="col">Payer Phone</th>
                          <th scope="col">Total</th>
                        </tr>
                      </thead>

                      <tbody>

                          <?php

                              $data = $payment->fetch_reserve_payment_details_cat($OwnerId,"VIP Table");

                              $total = 0;

                              foreach($data as $row) {


                                  $reserve_name = $row["ReserveName"];
                                  $trans_code = $row["TransactionReference"];
                                  $payer_phone = $row["PayerPhone"];
                                  $amount = $row["AmountPaid"];


                          ?>

                        <tr>
                          <td><?php echo $reserve_name; ?></td>
                          <td><?php echo $trans_code; ?></td>
                          <td><?php echo $payer_phone; ?></td>
                          <td><?php echo $amount; ?></td>

                          </td>
                        </tr>

                        <?php

                            }

                        ?>

<!--                         <tr><td><?php //echo $total; ?></td></tr> -->

                      </tbody>
                  
                   </table>

                 </div>

              </div><!-- end of tellerPanel div -->


          </div>

    </div>
    <div class="modal" tabindex="-1" role="dialog" id="payModal" >      <div class="modal-dialog" role="document">        <div class="modal-content" >          <div class="modal-header">            <h5 class="modal-title" style="text-align : center; color : #ff3c4e; font-family: 'Josefin Slab';">PAY FOR EVENT</h5>            <button type="button" class="close" data-dismiss="modal" aria-label="Close">              <span aria-hidden="true">&times;</span>            </button>          </div>          <div class="modal-body" >                     <ul id="m-pesa_guide">                         <li>Make the table available</li>                       </ul>                      <form action="VIP.php" method="post" >                         <div class="form-group">                            <input class="form-control" type="number" name="rs_capacity" placeholder="Enter Table capacity" required>                         </div>						                          <div class="form-group">                           <button type="submit" class="btn btn-primary" id="subMpesa" name="subMpesa">Complete</button>                         </div>                     </form>          </div>          <div class="modal-footer">          </div>        </div>      </div>    </div>
<footer class="bg-dark" id="footer" style="background:#000;">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Pop-In</h2>
                  <hr style="border: 1px solid #fff;">
                  <p style="font-family:'Raleway';">Pop-in to buy advance tickets to upcoming events</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Contact us</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <li style="list-style: none; font-family:'Raleway';"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;font-family:'Raleway';"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Navigation</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <a href="login.php"><li style="list-style: none;font-family:'Raleway';">Login</li></a>
                     <a href="register.php"><li style="list-style: none;font-family:'Raleway';">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;font-family:'Raleway';">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p style="font-family:'Raleway';">&copy copyright Pop-In all Rights Reserved: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i style="font-size:40px; color:#3b5998" class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i style="font-size:40px; color:#c32aa3"class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>
<!--<div class="mdl-grid" id="footer">

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Tickets</h2>
      <p><a href="../index.php" style="text-align : center; color : #fff; text-decoration: none; ">Confirm Ticket</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Contact Us</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>
 

  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Our Products</h2>

  </div>

</div>-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="../Front End/styling/script.js"></script>
    <script src="../Front End/scripting/e-shama.js"></script>
    <script src="../Front End/ShamaScript.js"></script>

    <script>

       function searchPayments() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchPay");
        filter = input.value.toUpperCase();
        table = document.getElementById("payTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          td2 = tr[i].getElementsByTagName("td")[1];
          td3 = tr[i].getElementsByTagName("td")[2];
          if (td || td2 || td3) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
      }


    </script>		<script>         $(document).ready(function(){          $('.modal').on('show.bs.modal', function(e) {            var payId = $(e.relatedTarget).data('book-id');            $(e.currentTarget).find('input[name="reserve_id"]').val(payId);          });        });    </script>

  <!--side bars-->
      
</body>
</html>




