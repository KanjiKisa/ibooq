<?php

	require_once("initialise.php");

	class ticket extends paymentAPI {

		public $ticketsTable = "poppintickets_res";
		public $ticketsLaterTable = "poppin_later_tickets_res";
		public $receiptTable = "poppindrinksreceipt";
		public $reserveTable = "poppinreserve";
		public $paymentTable = "poppinreservepayment";
		public $paymentLaterTable = "poppinreservelater";
		public $drinksPaymentTable = "poppindrinkspayment";

		public $ticket_id,$pay_id,$reserve_id,$payer_phone,$ticket_url,$ticket_status,$ticket_date,$ticket_time;
		public $receipt_id,$drink_id,$receipt_url,$receipt_status,$receipt_date,$receipt_time;
		public $reserve_name,$ticket_no,$ticket_capacity,$payer_name,$payer_email,$pay_date,$pay_time,$ending_time,$amount_paid,$category;

		public function set_ticket_id($ticketId)
		{
			$this->ticket_id = $ticketId;
		}
		
		public function set_pay_id($payId)
		{
			$this->pay_id = $payId;
		}

		public function set_reserve_id($reserveId)
		{
			$this->reserve_id = $reserveId;
		}

		public function set_payer_phone($payerPhone)
		{
			$this->payer_phone = $payerPhone;
		}

		public function set_ticket_url($ticketUrl)
		{
			$this->ticket_url = $ticketUrl;
		}

		public function set_ticket_status($status)
		{
			$this->ticket_status = $status;
		}
		
		public function set_receipt_id($receiptId)
		{
			$this->receipt_id = $receiptId;
		}

		public function set_drink_id($drinkId)
		{
			$this->drink_id = $drinkId;
		}
		
		public function set_receipt_url($receiptUrl)
		{
			$this->receipt_url = $receiptUrl;
		}

		public function set_receipt_status($status)
		{
			$this->receipt_status = $status;
		}

		public function insert_into_tickets_table()
		{

			$sql = $this->dbase->query("SELECT PaymentId FROM $this->ticketsTable WHERE PaymentId='$this->pay_id' ");

				//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

			if($sql->rowCount() > 0 ) {

				$this->stmt = $this->dbase->prepare("UPDATE $this->ticketsTable SET TicketId=?,PaymentId=?,ReserveId=?,PayerPhone=?,TicketUrl=?,TicketStatus=?,TicketDate=now(),TicketTime=now() 
					WHERE PaymentId='$this->pay_id' ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			} else {

				
				$this->stmt = $this->dbase->prepare("INSERT INTO $this->ticketsTable(TicketId,PaymentId,ReserveId,PayerPhone,TicketUrl,TicketStatus,TicketDate,TicketTime) 
								VALUES(?,?,?,?,?,?,now(),now() ) ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

		}

		public function insert_into_unpaid_tickets_table()
		{

			$sql = $this->dbase->query("SELECT PaymentId FROM $this->ticketsLaterTable WHERE PaymentId='$this->pay_id' ");

				//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

			if($sql->rowCount() > 0 ) {

				$this->stmt = $this->dbase->prepare("UPDATE $this->ticketsLaterTable SET TicketId=?,PaymentId=?,ReserveId=?,PayerPhone=?,TicketUrl=?,TicketStatus=?,TicketDate=now(),TicketTime=now() 
					WHERE PaymentId='$this->pay_id' ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			} else {

				
				$this->stmt = $this->dbase->prepare("INSERT INTO $this->ticketsLaterTable(TicketId,PaymentId,ReserveId,PayerPhone,TicketUrl,TicketStatus,TicketDate,TicketTime) 
								VALUES(?,?,?,?,?,?,now(),now() ) ");

				$this->stmt->bindParam(1,$this->ticket_id);
				$this->stmt->bindParam(2,$this->pay_id);
				$this->stmt->bindParam(3,$this->reserve_id);
				$this->stmt->bindParam(4,$this->payer_phone);
				$this->stmt->bindParam(5,$this->ticket_url);
				$this->stmt->bindParam(6,$this->ticket_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

		}
		
		public function insert_into_receipt_table()
		{

			$sql = $this->dbase->query("SELECT DrinkId,PayerPhone FROM $this->receiptTable WHERE DrinkId='$this->drink_id' AND PayerPhone='$this->payer_phone' ");

				//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

			if($sql->rowCount() > 0 ) {

				$this->stmt = $this->dbase->prepare("UPDATE $this->receiptTable SET ReceiptId=?,DrinkId=?,PayerPhone=?,ReceiptUrl=?,ReceiptStatus=?,ReceiptDate=now(),ReceiptTime=now() 
					WHERE DrinkId='$this->drink_id' AND PayerPhone='$this->payer_phone' ");

				$this->stmt->bindParam(1,$this->receipt_id);
				$this->stmt->bindParam(2,$this->drink_id);
				$this->stmt->bindParam(3,$this->payer_phone);
				$this->stmt->bindParam(4,$this->receipt_url);
				$this->stmt->bindParam(5,$this->receipt_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			} else {

				
				$this->stmt = $this->dbase->prepare("INSERT INTO $this->receiptTable(ReceiptId,DrinkId,PayerPhone,ReceiptUrl,ReceiptStatus,ReceiptDate,ReceiptTime) 
								VALUES(?,?,?,?,?,now(),now() ) ");

				$this->stmt->bindParam(1,$this->receipt_id);
				$this->stmt->bindParam(2,$this->drink_id);
				$this->stmt->bindParam(3,$this->payer_phone);
				$this->stmt->bindParam(4,$this->receipt_url);
				$this->stmt->bindParam(5,$this->receipt_status);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

		}

		public function update_ticket_status($ticketId)
		{

			$this->stmt = $this->dbase->prepare("UPDATE $this->ticketsTable SET TicketStatus='1' WHERE TicketId=? ");

			$this->stmt->bindParam(1,$ticketId);

			if($this->stmt->execute()) {
				return true;
			} else {
				return false;
			}

		}
		
		public function check_host_of_reserve_receipt($OwnerId,$ticketId)
		{
		    $sql = $this->dbase->query("SELECT b.ReserveId,b.TicketId,c.OwnerId,c.ReserveId FROM $this->ticketsTable b,$this->paymentTable c 
				WHERE b.ReserveId=c.ReserveId AND c.OwnerId='$OwnerId' AND b.TicketId='$ticketId'");
		    
		    if($sql->rowCount() > 0 ) {
		        return true;
		    } else {
		        return false;
		    }
		    
		}
		
		public function check_if_reserve_receipt_confirmed($ticketId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->ticketsTable WHERE TicketId='$ticketId' AND TicketStatus=1 ");
		    
		    if($sql->rowCount() > 0 ) {
		        return false;
		    } else {
		        return true;
		    }
		    
		}
		
		public function update_reserve_receipt_status_mobile($ticketId)
		{
		    $sql = $this->dbase->query("SELECT * FROM $this->ticketsTable WHERE TicketId='$ticketId' ");
		    
		    if($sql->rowCount() > 0 ) {

    			$this->stmt = $this->dbase->prepare("UPDATE $this->ticketsTable SET TicketStatus=1 WHERE TicketId=? ");
    
    			$this->stmt->bindParam(1,$ticketId);
    
    			if($this->stmt->execute()) {
    				return true;
    			} else {
    				return false;
    			}
    			
		    } else {
		        return false;
		    }

		}
		
		public function update_receipt_status($ticketId)
		{

			$this->stmt = $this->dbase->prepare("UPDATE $this->receiptTable SET ReceiptStatus='1' WHERE ReceiptId=? ");

			$this->stmt->bindParam(1,$ticketId);

			if($this->stmt->execute()) {
				return true;
			} else {
				return false;
			}

		}
		
	    public function check_host_of_receipt($OwnerId,$ticketId)
		{
		    $sql = $this->dbase->query("SELECT b.DrinkId,b.ReceiptId,c.OwnerId,c.PaymentId FROM $this->receiptTable b,$this->drinksPaymentTable c 
				WHERE c.PaymentId=b.DrinkId AND c.OwnerId='$OwnerId' AND b.ReceiptId='$ticketId'");
		    
		    if($sql->rowCount() > 0 ) {
		        return true;
		    } else {
		        return false;
		    }
		    
		}
		
		public function check_if_receipt_confirmed($ticketId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->receiptTable WHERE ReceiptId='$ticketId' AND ReceiptStatus=1 ");
		    
		    if($sql->rowCount() > 0 ) {
		        return false;
		    } else {
		        return true;
		    }
		    
		}
		
		public function update_receipt_status_mobile($ticketId)
		{
		    $sql = $this->dbase->query("SELECT * FROM $this->receiptTable WHERE ReceiptId='$ticketId' ");
		    
		    if($sql->rowCount() > 0 ) {

    			$this->stmt = $this->dbase->prepare("UPDATE $this->receiptTable SET ReceiptStatus=1 WHERE ReceiptId=? ");
    
    			$this->stmt->bindParam(1,$ticketId);
    
    			if($this->stmt->execute()) {
    				return true;
    			} else {
    				return false;
    			}
    			
		    } else {
		        return false;
		    }

		}
		
		public function fetch_drinks_bought($ReceiptId)
		{
		    $sql = $this->dbase->query("SELECT a.PayerName,a.PayerEmail,a.PayerPhone,a.PayDate,a.PayTime,a.DrinksId,a.AmountPaid,a.PaymentId,b.DrinkId,b.ReceiptId FROM 
		                                $this->drinksPaymentTable a,$this->receiptTable b WHERE a.PaymentId=b.DrinkId AND b.ReceiptId='$ReceiptId' ");
		                                
		      return $sql;
		}
		
		public function fetch_tables_reserved($TicketId)
		{
		    $sql = $this->dbase->query("SELECT a.PayerName,a.PayerEmail,a.PayerPhone,a.PayDate,a.PayTime,a.TableCapacity,a.ReserveId,a.AmountPaid,a.PaymentId,b.ReserveId,b.TicketId,c.ReserveId,c.ReserveName,c.Category,c.ReserveLocation FROM 
		                                $this->paymentTable a,$this->ticketsTable b,$this->reserveTable c WHERE a.ReserveId=b.ReserveId AND b.ReserveId=c.ReserveId AND b.TicketId='$TicketId' ");
		                                
		      return $sql;
		}
		
		public function get_ticket_details($OwnerId)
		{

			$sql = $this->dbase->query("SELECT DISTINCT b.TicketId,a.ReserveId,a.OwnerId,a.ReserveName,b.ReserveId,b.TicketStatus,b.PayerPhone,c.PayerPhone,c.PayerName,c.TableCapacity,c.PayerEmail,c.PayDate,c.PayTime FROM $this->reserveTable a,$this->ticketsTable b,$this->paymentTable c 
				WHERE a.ReserveId=b.ReserveId AND b.PayerPhone=c.PayerPhone AND a.OwnerId='$OwnerId' AND b.TicketStatus='0' ");

			return $sql;

		}

		public function get_ticket_id_details($PaymentId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a,$this->ticketsTable b,$this->paymentTable c 
				WHERE c.PaymentId='$PaymentId' AND c.ReserveId=b.ReserveId AND a.ReserveId=b.ReserveId  ");

			foreach ($sql as $key => $row) {
				# code...
				$this->reserve_name = $row["ReserveName"];
	              $this->payer_phone = $row["PayerPhone"];
	              //$this->ticket_no = $row["TicketId"];
	              $this->category = $row["Category"];
          			$this->amount_paid = $row["AmountPaid"];
	              $this->ticket_capacity = $row["TableCapacity"];
	              $this->payer_name = $row["PayerName"];
	              $this->payer_email = $row["PayerEmail"];
	              $this->pay_date = $row["ArrivalDate"];
	              $this->pay_time = $row["ArrivalTime"];				
			}

		}

		public function get_unpaid_ticket_id_details($PaymentId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a,$this->ticketsLaterTable b,$this->paymentLaterTable c 
				WHERE c.PaymentId='$PaymentId' AND c.ReserveId=b.ReserveId AND a.ReserveId=b.ReserveId  ");

			foreach ($sql as $key => $row) {
				# code...
				$this->reserve_name = $row["ReserveName"];
	              $this->payer_phone = $row["PayerPhone"];
	              $this->ticket_no = $row["TicketId"];
	              $this->category = $row["Category"];
          			$this->amount_paid = $row["AmountPaid"];
	              $this->ticket_capacity = $row["TableCapacity"];
	              $this->payer_name = $row["PayerName"];
	              $this->payer_email = $row["PayerEmail"];
	              $this->pay_date = $row["ArrivalDate"];
				  $this->pay_time = $row["ArrivalTime"];
				  $this->ending_time = $row["EndingTime"];				
			}

		}

		public function get_distinct_ticket_id($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.PaymentId,c.OwnerId,c.PaymentId,b.TicketStatus,b.TicketId FROM $this->ticketsTable b,$this->paymentTable c 
				WHERE b.PaymentId=c.PaymentId AND c.OwnerId='$OwnerId' AND b.TicketStatus='0' ");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}

		public function get_unpaid_distinct_ticket_id($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.PaymentId,c.OwnerId,c.PaymentId,b.TicketStatus,b.TicketId FROM $this->ticketsLaterTable b,$this->paymentLaterTable c 
				WHERE b.PaymentId=c.PaymentId AND c.OwnerId='$OwnerId' AND b.TicketStatus='0' ");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		
		public function get_distinct_confirmed_ticket_id($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.PaymentId,c.OwnerId,c.PaymentId,b.TicketStatus,b.TicketId FROM $this->ticketsTable b,$this->paymentTable c 
				WHERE b.PaymentId=c.PaymentId AND c.OwnerId='$OwnerId' AND b.TicketStatus='1' ");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		
		public function get_ticket_details_mobile($OwnerId)
		{

			$sql = $this->dbase->query("SELECT a.ReserveId,a.OwnerId,a.ReserveName,a.Category,b.ReserveId,b.TicketId,b.TicketStatus,b.PayerPhone,c.PayerPhone,c.PayerName,c.AmountPaid,c.TableCapacity,c.PayerEmail,c.PayDate,c.PayTime FROM $this->reserveTable a,$this->ticketsTable b,$this->paymentTable c 
				WHERE a.ReserveId=b.ReserveId AND b.PayerPhone=c.PayerPhone AND a.OwnerId='$OwnerId' AND b.TicketStatus='0' GROUP BY(b.TicketId)");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		
		public function get_receipt_details_mobile($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.DrinkId,b.ReceiptId,b.ReceiptStatus,b.ReceiptUrl,b.PayerPhone,c.PayerPhone,c.PayerName,c.PaymentId,c.OwnerId,c.PayerEmail,c.PayDate,c.PayTime FROM $this->receiptTable b,$this->drinksPaymentTable c 
				WHERE c.PaymentId=b.DrinkId AND b.PayerPhone=c.PayerPhone AND c.OwnerId='$OwnerId' AND b.ReceiptStatus='0' GROUP BY(b.ReceiptId) ASC");

			if($sql->rowCount() > 0) {
			    return $sql;
			} else {
			    return false;
			}

		}
		
		
		public function get_receipt_details($OwnerId)
		{

			$sql = $this->dbase->query("SELECT b.DrinkId,b.ReceiptId,b.ReceiptStatus,b.ReceiptUrl,b.PayerPhone,c.PayerPhone,c.PayerName,c.PaymentId,c.OwnerId,c.PayerEmail,c.PayDate,c.PayTime FROM $this->receiptTable b,$this->drinksPaymentTable c 
				WHERE b.DrinkId=c.PaymentId AND b.PayerPhone=c.PayerPhone AND c.OwnerId='$OwnerId' AND b.ReceiptStatus='0' GROUP BY(b.ReceiptId) ASC");

			return $sql;

		}


		
	}

	$ticket = new ticket();

?>