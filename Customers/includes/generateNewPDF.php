<?php

require_once("initialise.php");

	class generateNewPDF extends FPDF
	{
	    public $receipt_id;
	    public $append_path;
	    public $file = "Tickets/QR/TKT321138PSDKS.png";

		/*public function __construct($orientation = 'P', $unit = 'pt', $format = 'A4', $margin= 40)
		{

			$this->FDF($orientation,$unit,$format);
			$this->SetTopMargin($margin);
			$this->SetLeftMargin($margin);
			$this->SetRightMarging($margin);
			$this->SetAutoPageBreak(true,$margin);

		}
*/

		//$this->get_total_meetings();

		/*public function set_ticket_poster_pic($url) 
		{

			$this->Image('Customers/Front End/'.$url,60,23,80,50);

		}*/
		
		public function set_res_header_image($receipt_id)
		{
		    return $this->append_path.$receipt_id;
		}
		
		public function set_qr_code($receipt_id)
		{
		    $this->receipt_id = $receipt_id;
		}
		
		public function set_append_path($path)
		{
		    $this->append_path = $path;
		}

		public function Header()
		{
//
			$this->AddFont('Calligrapher','','calligra.php');
			$this->Image($this->set_res_header_image($this->receipt_id).'.png',10,10,50,50);
			$this->SetFont('Arial','',20);
			$this->SetFillColor(255);
			$this->SetTextColor(0);
			$this->SetXY(140,13);
			//$this->SetY(20);
			$this->Cell(60,10,$this->receipt_id,0,0,"L",true);
			$this->Ln(20);

		}

		public function create_ticket($reserve_owner,$capacity,$category,$table_name,$price,$name,$phone,$email,$arr_date,$arr_time)
		{
		    
		    $db = new database();
		    
		    $db->fetch_reservation_user_reg_info($reserve_owner);

			$this->SetMargins(10,10,10);
			$this->AddPage();

			$this->AddFont('Calligrapher','','calligra.php');

			// addresses
			$this->Ln(20);
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);


			$this->Ln();
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->username,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->city.", ".$db->country,0,"","L",true);

			$this->Ln(20);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,"BILLED TO",0,0,"L",true);
			
			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Name of buyer",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$name,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Phone Number ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$phone,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Email Address ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$email,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Arrival Date ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$arr_date,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Arrival Time ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$arr_time,0,"","L",true);


			$this->Ln(20);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(49,8,"Service Name",0,0,"C",true);
			$this->Cell(47,8,"Service Category",0,0,"C",true);
			$this->Cell(47,8,"No of clients",0,0,"C",true);
			$this->Cell(47,8,"Total",0,0,"C",true);


				# code...
				$this->Ln();
				$this->setFont("Arial","",12);
				$this->SetTextColor(0);
				$this->SetFillColor(255);
				$this->Cell(49,15,$table_name,0,0,"C",true);
				$this->Cell(47,15,$category,0,0,"C",true);
				$this->Cell(47,15,$capacity,0,0,"C",true);
				$this->Cell(47,15,$price*$capacity,0,0,"C",true);

			

			$this->Ln(25);
			$this->SetX(107);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,8,"Total",0,0,"C",true);
			$this->SetTextColor(0);
			$this->Cell(47,8,$price*$capacity,0,"","C",true);

			$this->SetAutoPageBreak(true,2);
		    
		}
		
		public function create_receipt($name,$phone,$email)
		{
		     $db = new database();
		    
		    $db->fetch_reservation_user_reg_info($_SESSION['owner']['id']);

			$this->SetMargins(10,10,10);
			$this->AddPage();

			$this->AddFont('Calligrapher','','calligra.php');

			// addresses
			$this->Ln(20);
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);


			$this->Ln();
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->username,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->city.", ".$db->country,0,"","L",true);

			$this->Ln(20);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,"BILLED TO",0,0,"L",true);
			
			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Name of buyer",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$name,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Phone Number ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$phone,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Email Address ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$email,0,"","L",true);



			/*$this->Ln(20);
			$this->setFont("Calligrapher","",12);
			$this->SetTextColor(9,157,44);
			$this->SetFillColor(54,54,51);
			$this->Cell(190,10,"DRINKS",1,0,"C",true);*/

			$this->Ln(20);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(49,8,"Description",0,0,"C",true);
			$this->Cell(47,8,"Qty",0,0,"C",true);
			$this->Cell(47,8,"Price",0,0,"C",true);
			$this->Cell(47,8,"Total",0,0,"C",true);

			$query = $db->fetch_bought_drinks(); 
			

            foreach($query as $row) {

            	$quantity = $_SESSION['carts'][$row['DrinkId']]['quantity'];

				# code...
				$this->Ln();
				$this->setFont("Arial","",12);
				$this->SetTextColor(0);
				$this->SetFillColor(255);
				$this->Cell(49,15,"(".$row['DrinkName']."), ".$row['DrinkDesc'],0,0,"C",true);
				$this->Cell(47,15,$quantity,0,0,"C",true);
				$this->Cell(47,15,$row['DrinkPrice'],0,0,"C",true);
				$this->Cell(47,15,$quantity*$row['DrinkPrice'],0,0,"C",true);

			}

			$this->Ln(25);
			$this->SetX(107);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,8,"Total",0,0,"C",true);
			$this->SetTextColor(0);
			$this->Cell(47,8,$_SESSION['total']['amount'],0,"","C",true);

			$this->SetAutoPageBreak(true,2);

		}

		public function Footer()
		{

			$this->SetFont('Arial','',20);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->SetXY(10,-15);
			$this->Cell(0,15,"www.ibooq.co.ke",'T',0,'C',true);

		}


		/*public function print_minutes()
		{

			
		}*/

	}

	$pdf = new generateNewPDF('P','mm','A4');

?>