<?php

	require_once("initialise.php");

	class drinksPaymentAPI extends database {

		public $pay_id,$pay_drk_id,$pay_owner_id,$pay_user_email,$pay_amount;

		public $drk_name,$drk_id;

		private $drinksPaymentTable = "poppindrinkspayment";
		public $drinksTable = "poppindrinks";
		
		public $oid,$phone,$email,$sid;
		
		    public function set_order_id($oid)
            {
                $this->oid = $oid;
            }
            
            
            public function set_phone($phone)
            {
                $this->phone = $phone;
            }
            
            public function set_email($email)
            {
                $this->email = $email;
            }
            
            public function set_sid($sid)
            {
                $this->sid = $sid;
            }

			public function set_service_name($attr) 
			{
					$this->service_name = $attr;
			}

			public function set_business_no($attr) 
			{
					$this->business_no = $attr;
			}

			public function set_transaction_ref($attr) 
			{
					$this->transaction_ref = $attr;
			}

			public function set_internal_transId($attr) 
			{
					$this->internal_transId = $attr;
			}

			public function set_trans_timestamp($attr) 
			{
					$this->trans_timestamp = $attr;
			}

			public function set_trans_type($attr) 
			{
					$this->trans_type = $attr;
			}

			public function set_account_no($attr) 
			{
					$this->account_no = $attr;
			}
			
			public function set_table_capacity($tkt_capacity)
			{
				$this->table_capacity = $tkt_capacity;
			}

			public function set_sender_phone($attr) 
			{
					$this->sender_phone = $attr;
			}

			public function set_first_name($attr) 
			{
					$this->first_name = $attr;
			}

			public function set_middle_name($attr) 
			{
					$this->middle_name = $attr;
			}

			public function set_last_name($attr) 
			{
					$this->last_name = $attr;
			}
			
			public function set_full_name($f,$l) 
			{
					$this->full_name = $f." ".$l;
			}

			public function set_amount($attr) 
			{
					$this->amount = $attr;
			}

			public function set_currency($attr) 
			{
					$this->currency = $attr;
			}

			public function set_acc_deposit($acc)
			{
				$this->acc_deposit = $acc;
			}

			public function set_signature($attr) 
			{
					$this->signature = $attr;
			}

			public function set_pay_id($pay_id)
			{
				$this->pay_id = $pay_id;
			}
			
			public function set_pay_drk_id($drk_id)
			{
				$this->pay_drk_id = $drk_id;
			}

			public function set_pay_owner_id($owner_id)
			{
				$this->pay_owner_id = $owner_id;
			}
			
			public function set_pay_user_email($user_email)
			{
				$this->pay_user_email = $user_email;
			}
			
			public function set_pay_amount($pay_amount)
			{
				$this->pay_amount = $pay_amount;
			}

			public function initiator_request()
            {
                
                // Make API call to iPay
                $url 		= 'https://apis.ipayafrica.com/payments/v2/transact';
        
                $live       = 1;
                $oid        = $this->oid; 
                $inv        = $this->oid;
                $amount 	= $this->amount;
                $tel        = $this->phone;
                $eml        = $this->email;
                $vid        = "pop";   
                $curr       = "KES"; 
                $p1         = "";
                $p2         = "";
                $p3         = "";
                $p4         = "";
                $cbk        = "http://popin.co.ke/mpesa_details.php"; // Points to the callback file
                $cst        = 1;
                $crl        = 0;
                // Check hash
                $key 		= "5fkjed65oj85epfe";
                $datastring = $live.$oid.$inv.$amount.$tel.$eml.$vid.$curr.$p1.$p2.$p3.$p4.$cst.$cbk;
        
                $generated_hash = hash_hmac('sha256', $datastring, $key);
        
                $fields = array(
                    'amount'=>$amount,
                    'oid'	=>$oid,
                    'inv'	=>$inv,
                    'vid'	=>$vid,
                    'curr'	=>$curr,
                    'live'	=>$live,
                    'tel'	=>$tel,
                    'eml'	=>$eml,
                    'p1'	=>$p1,
                    'p2'	=>$p2,
                    'p3'	=>$p3,
                    'p4'	=>$p4,
                    'cbk'	=>$cbk,
                    'crl'	=>$crl,
                    'cst'	=>$cst,
                    'hash'	=>$generated_hash
                );
                
        
                // Open connection
                $ch = curl_init($url);
        
                // Set the url, number of POST vars, POST data
        		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
                
        
                // Execute post
                $result = curl_exec($ch);
        
        
               // $si = json_decode($result)->data->sid;
                
                return $result;
                
                /* if($si){
                    header("Location: get_ipay.php?sid=$si");
                }*/
                
            }
            
            public function process_initiator_stk()
            {
        
                $vid = "pop";
                
                $url1 		= 'https://apis.ipayafrica.com/payments/v2/transact/push/mpesa';
                
                $dstring = $this->phone.$vid.$this->sid;
                
                 $key = "5fkjed65oj85epfe";
                
                $g_hash = hash_hmac('sha256',$dstring , $key);
                
                $f = [
                    'phone'=>$this->phone,
                    'sid'	=>$this->sid,
                    'vid'	=>$vid,
                    'hash'	=>$g_hash
                    ];
                    
                $ch1 = curl_init($url1);
        
                // Set the url, number of POST vars, POST data
        		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch1,CURLOPT_POSTFIELDS, $f);
        
                // Execute post
                $result1 = curl_exec($ch1);
                
                return $result1;
                
            }
            
            public function search_pay_orders($oid)
            {
                
                $vid = "pop";
                
                $url1	= 'https://apis.ipayafrica.com/payments/v2/transaction/search';
                
                $dstring = $oid.$vid;
                
                $key 		= "5fkjed65oj85epfe";
        
                $g_hash = hash_hmac('sha256',$dstring , $key);
        
                $f = [
                    'vid'	=>$vid,
                    'hash'	=>$g_hash,
                    'oid'   =>$oid
                    ];
            
                $ch1 = curl_init($url1);

                // Set the url, number of POST vars, POST data
        		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch1,CURLOPT_POSTFIELDS, $f);
                
        
                // Execute post
                $result1 = curl_exec($ch1);
                
                return $result1;

                
            }

			public function insert_to_mpesa_table()
			{

				$this->stmt = $this->dbase->prepare
				("INSERT INTO $this->mpesaTable(ServiceName,BusinessNumber,TransactionReference,InternalTransactionId,TransactionTimestamp,
					TransactionType,AccountNumber,SenderPhone,FirstName,MiddleName,LastName,Amount,Currency,Signature,PesaDate,PesaTime) 
					VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now()) ");

				$this->stmt->bindParam(1,$this->service_name);
				$this->stmt->bindParam(2,$this->business_no);
				$this->stmt->bindParam(3,$this->transaction_ref);
				$this->stmt->bindParam(4,$this->internal_transId);
				$this->stmt->bindParam(5,$this->trans_timestamp);
				$this->stmt->bindParam(6,$this->trans_type);
				$this->stmt->bindParam(7,$this->account_no);
				$this->stmt->bindParam(8,$this->sender_phone);
				$this->stmt->bindParam(9,$this->first_name);
				$this->stmt->bindParam(10,$this->middle_name);
				$this->stmt->bindParam(11,$this->last_name);
				$this->stmt->bindParam(12,$this->amount);
				$this->stmt->bindParam(13,$this->currency);
				$this->stmt->bindParam(14,$this->signature);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}


			}

			
			public function insert_into_drinks_payment_table($transaction_ref,$sender_phone)
			{

				/*$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' AND SenderPhone='$sender_phone' ");

				if($sql->rowCount() > 0 ) {*/


					$this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->drinksPaymentTable(TransactionReference,PaymentId,DrinksId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,AccDeposit,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,now(),now())");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_drk_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->acc_deposit);

					if($this->stmt->execute()) {

						//echo "<script> alert('Successful pay'); </script>";
						return true;
					} else {
						//echo "<script> alert('Successful pay'); </script>";
						return false;
					}

			/*	} else {
					return false;
					echo "<script> alert('Incorrect Transaction Number or Phone Number or Both'); </script>";
				}*/

			}

			public function fetch_specific_mpesa_details($transaction_ref)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' ");

				return $sql;

			}
			
			public function fetch_specific_drinks_mpesa_details($drinks_id)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->drinksPaymentTable WHERE DrinksId='$drinks_id' ");

				return $sql;

			}

			public function get_total_drinks_amount_per_payer($drk_id,$payer_phone)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->drinksPaymentTable WHERE DrinksId='$drk_id' AND PayerPhone='$payer_phone' ");

				return $sql;

			}

			public function insert_to_balance_table($drk_id,$user_phone,$balance)
			{

					$sql = $this->dbase->query("SELECT * FROM $this->balanceTable WHERE DrinksId='$drk_id' AND PayerPhone='$user_phone' ");

					$date = date("Y-d-m");
					$time = date("hh:mm:s");

					//if($balance != 0 ) // if the balance status of the buyer is 0 for any drinks, delete that record 

						if($sql->rowCount() > 0 ) {


								$this->stmt = $this->dbase->prepare(
												"UPDATE $this->balanceTable SET DrinksId=? ,PayerPhone=?,AmountBalance=?,UpdateDate=now(),UpdateTime=now() WHERE DrinksId=? AND PayerPhone=? ");
								
								$this->stmt->bindParam(1,$drk_id);
								$this->stmt->bindParam(2,$user_phone);
								$this->stmt->bindParam(3,$balance);
								$this->stmt->bindParam(4,$drk_id);
								$this->stmt->bindParam(5,$user_phone);

								if($this->stmt->execute()) {
									return true;
								} else {
									return false;
								}

						} else {

							$this->stmt = $this->dbase->prepare("INSERT INTO $this->balanceTable(DrinksId,PayerPhone,AmountBalance,UpdateDate,UpdateTime) 
																VALUES(?,?,?,now(),now())");
							$this->stmt->bindParam(1,$drk_id);
							$this->stmt->bindParam(2,$user_phone);
							$this->stmt->bindParam(3,$balance);

							if($this->stmt->execute()) {
								return true;
							} else {
								return false;
							}

						}

			}


			public function fetch_by_payer_from_balance_table($payer_phone,$DrinksId)
			{


				$sql = $this->dbase->query("SELECT * FROM $this->balanceTable WHERE PayerPhone='$payer_phone' AND DrinksId='$DrinksId' ");

				if($sql->rowCount() > 0 ) {
					
					foreach($sql as $row) {

						$this->fetched_bal_drinks_id = $row["DrinksId"];
						$this->fetched_bal_payer_phone = $row["PayerPhone"];
						$this->fetched_bal_payer_balance = $row["AmountBalance"];
						$this->fetched_bal_update_date = $row["UpdateDate"];
						$this->fetched_bal_update_time = $row["UpdateTime"];

						return true;

					}
					
				} else {
					return false;
				}

			}

			public function fetch_user_payment_details($P_Phone,$DrinksId)
			{

				$sql = $this->dbase->query("SELECT DISTINCT DrinksId,PayerPhone,PayerEmail FROM $this->drinksPaymentTable WHERE PayerPhone='$P_Phone' AND DrinksId='$DrinksId' ");

				foreach($sql as $row) {

					return $row["PayerEmail"];

				}

			}


			public function fetch_drinks_payment_details($OwnerId)
			{

				$sql = $this->dbase->query("SELECT a.DrinksId,a.AmountPaid,a.AccDeposit,a.TransactionReference,a.PayerPhone,a.PayerName,b.DrinkId,b.DrinkName 
					FROM $this->drinksPaymentTable a,$this->drinksTable b WHERE b.OwnerId='$OwnerId' AND a.AccDeposit='0' ");

				return $sql;

			}

			public function fetch_all_drinks_payment_details()
			{

				$sql = $this->dbase->query("SELECT a.DrinksId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.DrinksId,b.DrinksName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->drinksPaymentTable a,$this->drinksTable b,$this->regTable c WHERE b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_specific_drinks_all_details($drinks_id)
			{

				$sql = $this->dbase->query("SELECT a.DrinksId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.DrinksId,b.Category,b.DrinksLocation,b.DrinksPrice,b.PicPath,b.StartDate,b.StartTime,b.StopTime,b.StopDate,b.DrinksName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->drinksPaymentTable a,$this->drinksTable b,$this->regTable c WHERE b.DrinksId='$drinks_id' AND b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_drinks_payment_details_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT a.DrinksId,a.AmountPaid,a.TransactionReference,a.PayerPhone,a.PayerName,b.Category,b.DrinksId,b.DrinksName 
					FROM $this->drinksPaymentTable a,$this->drinksTable b WHERE b.OwnerId='$OwnerId' AND b.Category='$Category' ");

				return $sql;

			}

			public function count_drinks_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalDrinks FROM $this->drinksTable WHERE OwnerId='$OwnerId' ");

				foreach($sql as $row) {
					return $row["TotalDrinks"];
				}

			}

			public function sum_of_drinks_total_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->drinksPaymentTable WHERE OwnerId='$OwnerId' AND AccDeposit<2 ");

				foreach($sql as $row) {
					return $row["TotalAmount"];
				}

			}

			public function sum_of_specific_drinks_total($DrinksId)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT * FROM $this->drinksPaymentTable WHERE DrinksId='$DrinksId' AND AccDeposit<2 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function count_all_drinks()
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalDrinks FROM $this->drinksTable  ");

				foreach($sql as $row) {
					return $row["TotalDrinks"];
				}

			}

			public function sum_of_all_drinks_total()
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->drinksPaymentTable WHERE AccDeposit<2 ");

				foreach($sql as $row) {
					return $row["TotalAmount"];
				}

			}

			public function count_drinks_by_owner_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalDrinks FROM $this->drinksTable WHERE OwnerId='$OwnerId' AND Category='$Category' ");

				foreach($sql as $row) {
					return $row["TotalDrinks"];
				}

			}

			public function sum_of_drinks_total_by_owner_cat($OwnerId,$Category)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT a.OwnerId,a.DrinksId,a.AccDeposit,b.DrinksId,b.Category,a.AmountPaid FROM $this->drinksPaymentTable a,$this->drinksTable b WHERE b.Category='$Category' 
					AND a.DrinksId=b.DrinksId AND a.OwnerId='$OwnerId' AND  a.AccDeposit=1 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function fetch_of_specific_drinks_payment_details($DrinksId)
			{
				$total = 0;

				$sql = $this->dbase->query("SELECT DISTINCT a.DrinksId,a.AmountPaid,b.DrinkId,b.DrinkName,a.AccDeposit 
					FROM $this->drinksPaymentTable a,$this->drinksTable b WHERE a.DrinksId=b.DrinkId AND a.DrinksId='$DrinksId' 
					AND a.AccDeposit='0'");

				foreach($sql as $row) {
					$this->drk_name = $row["DrinksName"];
					$this->drk_id = $row["DrinksId"];
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function update_pay_withdrawal_status($OwnerId)
			{

				$this->stmt = $this->dbase->prepare("UPDATE $this->drinksPaymentTable SET AccDeposit='1',PayDate=now(),PayTime=now() WHERE OwnerId=? AND AccDeposit='0'");

				$this->stmt->bindParam(1,$OwnerId);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function update_cashout_withdrawal_status($DrinksId)
			{

				$this->stmt = $this->dbase->prepare("UPDATE $this->drinksPaymentTable SET AccDeposit='2',PayDate=now(),PayTime=now() WHERE DrinksId=? AND AccDeposit='1'");

				$this->stmt->bindParam(1,$DrinksId);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function fetch_withdrawal_status_checkout()
			{

				$sql = $this->dbase->query("SELECT DISTINCT a.OwnerId,a.PayDate,a.PayTime,b.DrinksName,a.AccDeposit,
					c.UserId,c.Username,c.Email,c.PhoneNo 
					FROM $this->drinksPaymentTable a,$this->drinksTable b,$this->regTable c 
					WHERE a.OwnerId=c.UserId AND a.AccDeposit='1'");

				return $sql;

			}
			
			public function fetch_total_tables_booked($drinksId)
			{
				
				$sql = $this->dbase->query("SELECT SUM(TableCapacity) as TotalBookedTables FROM $this->drinksPaymentTable WHERE DrinksId='$drinksId' ");
				
				foreach($sql as $row){
					return $row['TotalBookedTables'];
				}
				
			}


	}

	$drinks_payment = new drinksPaymentAPI();


?>