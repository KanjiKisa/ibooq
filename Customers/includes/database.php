<?php

require_once("initialise.php");

class database extends alerts{
		public $dbase;
		public $user_email,
				$admin_email,
				$admin_code,
				$user_id,
				$username,
				$phone_no,
				$usertype,
				$profile_pic,
				$cover_pic,
				$front_id_pic,
				$back_id_pic,
				$IDStatus,
				$business_licence_pic,
				$business_licence_status,
				$settlement_account,
				$settlement_account_status,
				$open_days,
				$open_time,
				$close_time,
				$happy_hour_start,
				$happy_hour_stop,
				$address,
				$city,
				$country,
				$password,$raw_password,$old_password,$pass,$approval_status,
				$fetched_email,
				$fetched_user_id,
				$fetched_user_type,
				$regDate,
				$regTime;
		private $regEventsTable = "poppinreg";
		private $regTable = "poppinreg_res";
		private $BOregSPTable = "ibooq_bo_sp";
		private $regAdminTable = "poppin_reg_admin";
		private $userRegTable = "poppinuser";
		private $loginTable = "poppinlogin";
		private $reserveTable = "poppinreserve";
		private $drinksTable = "poppindrinks";
		private $cartTable = "poppincart";
		private $poppinMpesaTable = "poppinmpesa";
		private $paymentTable = "poppinreservepayment";
		private $balanceTable = "poppinpaymentbalances_res";
		public $stmt;
		public $session_email,$session_user_id,$session_username,$session_user_type,$session_phone,$session_regDate,$session_regTime;

		public $rs_category,$rs_name,$rs_description,$rs_stock,$rs_no_tables,$rs_id,$rs_price,$rs_location,$rs_vacancy,$rs_qty,$rs_date
				,$rs_time,$rs_stop_date,$rs_stop_time,$rs_photo_path,$cart_id,$cart_status,$total;
				
		public $drk_category,$drk_name,$drk_description,$drk_price,$drk_size,$drk_id,$drk_happy_hour,$now_date_only,$now_time_only;

		public $fetched_rs_category,$fetched_rs_name,$fetched_rs_description,$fetched_rs_stock,$fetched_rs_tables,$fetched_rs_id,$fetched_rs_price,$fetched_rs_location,
				$fetched_rs_qty,$fetched_rs_date,$fetched_rs_time,$fetched_rs_stop_date,$fetched_rs_stop_time,$fetched_rs_photo_path;
		
		public $fetched_search_array;

		public $post_id,$post_title,$post_data,$post_photo_path;
		public $regUserId,$regUsername,$regUserEmail,$regUserPhone,$full_name;
		public $fetchedPayId,$fetchedPayUserId,$fetchedPayAmount,$fetchedPayMethod,$fetchedPayStat,$fetchedPayDate,$fetchedPayTime;

		public function __construct()
		{
			$this->connect();
			$this->fetched_search_array = array();
		}


		public function connect()
		{

			try {

				$this->dbase = new PDO("mysql:host=".server.";dbname=".database,user,pass);

				

			}catch(PDOException $e) {

				echo "Failed to connect to db";

			}

			$this->dbase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			date_default_timezone_set('Africa/Nairobi');
			
			$this->now_date_only = date("Y-m-d");
			
			$this->now_time_only = date("H:i:s");

		}
		public function set_email($email)
		{
			$this->user_email = $email;
		}
		
		public function set_admin_email($admin_email)
		{
			$this->admin_email = $admin_email;
		}
		
		public function set_admin_code($admin_code)
		{
			$this->admin_code = $admin_code;
		}

		public function set_user_id($userId)
		{
			$this->user_id = $userId;
		}

		public function set_user_city($city){
			$this->city =$city;
		}

		public function set_user_country($country){
			$this->country =$country;
		}

		public function set_user_address($address){
			$this->address = $address;
		}

		public function set_username($username)
		{
			$this->username = $username;
		}

		public function set_full_name($fname,$lname)
		{
			$this->full_name = $fname." ".$lname;
		}

		public function set_phone_no($phoneNo)
		{
			$this->phone_no = $phoneNo;
		}

		public function set_user_type($usertype)
		{
			$this->usertype = $usertype;
		}

		public function set_business_licence_pic($business_licence)
		{
			$this->business_licence_pic = $business_licence;
		}

		public function set_business_licence_status($business_licence_status)
		{
			$this->business_licence_status = $business_licence_status;
		}

		public function set_front_id_pic($front_pic)
		{
			$this->front_id_pic = $front_pic;
		}

		public function set_back_id_pic($back_pic)
		{
			$this->back_id_pic = $back_pic;
		}

		public function set_user_id_status($IDStatus)
		{
			$this->IDStatus = $IDStatus;
		}

		public function set_settlement_account($settlement_account)
		{
			$this->settlement_account = $settlement_account;
		}

		public function set_settlement_account_status($settlement_account_status)
		{
			$this->settlement_account_status = $settlement_account_status;
		}
		
		public function set_profile_pic($profile_pic)
		{
			$this->profile_pic = $profile_pic;
		}
		
		public function set_cover_pic($cover_pic)
		{
			$this->cover_pic = $cover_pic;
		}
		
		public function set_open_days($open_days)
		{
			$this->open_days = $open_days;
		}
		
		public function set_open_time($open_time)
		{
			$this->open_time = $open_time;
		}
		
		public function set_close_time($close_time)
		{
			$this->close_time = $close_time;
		}
		
		public function set_happy_hour_start($open_time)
		{
			$this->happy_hour_start = $open_time;
		}
		
		public function set_happy_hour_stop($close_time)
		{
			$this->happy_hour_stop = $close_time;
		}

		public function set_address($address)
		{
			$this->address = $address;
		}

		public function set_city($city)
		{
			$this->city = $city;
		}

		public function set_country($country)
		{
			$this->country = $country;
		}

		public function set_account_approval($approval)
		{
			$this->approval_status = $approval;
		}

		public function set_raw_password($pass)
		{
			$this->raw_password = $pass;
		}
		
		public function set_old_password($pass)
		{
			$this->old_password = $pass;
		}

		public function set_password($pass)
		{
			$this->password = password_hash($pass, PASSWORD_BCRYPT);
		}

		public function set_photo_id($photoid)
		{
			$this->photo_id = $photoid;
		}

		public function set_photo_name($photoname)
		{
			$this->photo_name = $photoname;
		}

		public function set_photo_desc($photodesc)
		{
			$this->photo_desc = $photodesc;
		}

		public function set_photo_path($photopath)
		{
			$this->photo_path = $photopath;
		}

		public function set_post_id($postid) 
		{
			$this->post_id = $postid;
		}

		public function set_post_title($title)
		{
			$this->post_title = $title;
		}

		public function set_post_data($postdata)
		{
			$this->post_data = $postdata;
		}

		public function set_post_photo_path($photopath)
		{
			$this->post_photo_path = $photopath;
		}
		
		public function set_prod_id($prod_id)
		{
			$this->prod_id = $prod_id;
		}
		
		public function set_cart_id($cartId)
		{
			$this->cart_id = $cartId;
		}
		
		public function set_cart_status($cartStatus)
		{
			$this->cart_status = $cartStatus;
		}

		public function set_rs_id($rs_id)
		{
			$this->rs_id = $rs_id;
		}
		
		public function set_rs_category($category)
		{
			$this->rs_category = $category;
		}
		
		public function set_rs_name($name)
		{
			$this->rs_name = $name;
		}
		
		public function set_rs_description($rs_desc)
		{
			$this->rs_description = $rs_desc;
		}
		
		public function set_rs_stock($rs_stock)
		{
			$this->rs_stock = $rs_stock;
		}
		
		public function set_rs_no_tables($rs_no_tables)
		{
			$this->rs_no_tables = $rs_no_tables;
		}
		
		public function set_rs_qty($rs_qty)
		{
			$this->rs_qty = $rs_qty;
		}
		
		public function set_rs_photo_path($rs_photo_path)
		{
			$this->rs_photo_path = $rs_photo_path;
		}
		
		public function set_rs_price($rs_price)
		{
			$this->rs_price = $rs_price;
		}

		public function set_rs_location($rs_location)
		{
			$this->rs_location = $rs_location;
		}
		
		public function set_rs_vacancy($rs_vacancy)
		{
			$this->rs_vacancy = $rs_vacancy;
		}

		public function set_rs_date($rs_start)
		{
			$this->rs_date = $rs_start;
		}

		public function set_rs_time($rs_start)
		{
			$this->rs_time = $rs_start;
		}

		public function set_rs_stop_date($rs_stop)
		{
			$this->rs_stop_date = $rs_stop;
		}

		public function set_rs_stop_time($rs_stop)
		{
			$this->rs_stop_time = $rs_stop;
		}
		
		public function set_drk_id($drk_id)
		{
			$this->drk_id = $drk_id;
		}
		
		public function set_drk_category($category)
		{
			$this->drk_category = $category;
		}
		
		public function set_drk_name($name)
		{
			$this->drk_name = $name;
		}
		
		public function set_drk_size($size)
		{
			$this->drk_size = $size;
		}
		
		public function set_drk_price($price)
		{
			$this->drk_price = $price;
		}
		
		public function set_drk_happy_hour($happy_hour)
		{
			$this->drk_happy_hour = $happy_hour;
		}
		
		public function set_drk_description($drk_desc)
		{
			$this->drk_description = $drk_desc;
		}
		

		public function insert_to_reserve_table()
		{
			$this->stmt = $this->dbase->prepare("INSERT INTO $this->reserveTable(Category,ReserveId,OwnerId,ReserveName,ReserveDesc,PicPath,ReserveStock,ReserveTablesTotal,ReservePrice,ReserveLocation,ReserveVacancy,ReserveDate,ReserveTime) 
			VALUES(?,?,?,?,?,?,?,?,?,?,?,now(),now() )");

			$this->stmt->bindParam(1,$this->rs_category);
			$this->stmt->bindParam(2,$this->rs_id);
			$this->stmt->bindParam(3,$this->user_id);
			$this->stmt->bindParam(4,$this->rs_name);
			$this->stmt->bindParam(5,$this->rs_description);
			$this->stmt->bindParam(6,$this->rs_photo_path);
			$this->stmt->bindParam(7,$this->rs_stock);
			$this->stmt->bindParam(8,$this->rs_no_tables);
			$this->stmt->bindParam(9,$this->rs_price);
			$this->stmt->bindParam(10,$this->rs_location);
			$this->stmt->bindParam(11,$this->rs_vacancy);
			/* $this->stmt->bindParam(10,date("Y-m-d",'now'));
			$this->stmt->bindParam(11,date("H:i:s",'now')); */
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
		}

		public function get_all_services($category)
		{
			$sql = $this->dbase->query("SELECT * FROM ibooq_services WHERE category='$category'");

			if($sql->rowCount() > 0) {
				return $sql;
			} else {
				return false;
			}
		}

		public function update_profile_pics($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET ProfilePic=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->profile_pic);
			$this->stmt->bindParam(2,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}

		public function update_cover_pics($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET CoverPic=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->cover_pic);
			$this->stmt->bindParam(2,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}
		
		public function insert_to_drinks_table()
		{
			$this->stmt = $this->dbase->prepare("INSERT INTO $this->drinksTable(Category,DrinkId,OwnerId,DrinkName,DrinkSize,DrinkPrice,DrinkDesc,HappyHour) 
			VALUES(?,?,?,?,?,?,?,?)");

			$this->stmt->bindParam(1,$this->drk_category);
			$this->stmt->bindParam(2,$this->drk_id);
			$this->stmt->bindParam(3,$this->user_id);
			$this->stmt->bindParam(4,$this->drk_name);
			$this->stmt->bindParam(5,$this->drk_size);
			$this->stmt->bindParam(6,$this->drk_price);
			$this->stmt->bindParam(7,$this->drk_description);
			$this->stmt->bindParam(8,$this->drk_happy_hour);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
		}

		public function update_reserve_table($UserId)
		{
			$this->stmt = $this->dbase->prepare
			("UPDATE $this->reserveTable SET Category=?,ReserveId=?,OwnerId=?,ReserveName=?,ReserveDesc=?,ReserveStock=?,ReservePrice=?,ReserveLocation=?,ReserveVacancy=?,
				ReserveDate=now(),ReserveTime=now() WHERE ReserveId=? AND OwnerId='$UserId' ");

			$this->stmt->bindParam(1,$this->rs_category);
			$this->stmt->bindParam(2,$this->rs_id);
			$this->stmt->bindParam(3,$this->user_id);
			$this->stmt->bindParam(4,$this->rs_name);
			$this->stmt->bindParam(5,$this->rs_description);
			$this->stmt->bindParam(6,$this->rs_stock);
			$this->stmt->bindParam(7,$this->rs_price);
			$this->stmt->bindParam(8,$this->rs_location);
			$this->stmt->bindParam(9,$this->rs_vacancy);
			$this->stmt->bindParam(10,$this->rs_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
		}
		
		public function update_drinks_table($UserId)
		{
			$this->stmt = $this->dbase->prepare
			("UPDATE $this->drinksTable SET Category=?,DrinkId=?,OwnerId=?,DrinkName=?,DrinkSize=?,DrinkPrice=?,DrinkDesc=? WHERE DrinkId=? AND OwnerId='$UserId' ");

			$this->stmt->bindParam(1,$this->drk_category);
			$this->stmt->bindParam(2,$this->drk_id);
			$this->stmt->bindParam(3,$this->user_id);
			$this->stmt->bindParam(4,$this->drk_name);
			$this->stmt->bindParam(5,$this->drk_size);
			$this->stmt->bindParam(6,$this->drk_price);
			$this->stmt->bindParam(7,$this->drk_description);
			$this->stmt->bindParam(8,$this->drk_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
			
		}
		
		public function update_rs_vacancy_status($reserve_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->reserveTable SET ReserveVacancy='Not Vacant' WHERE ReserveId=? ");
			
			$this->stmt->bindParam(1,$reserve_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}
		
		public function search_owners($keyword)
		{

			$this->stmt = $this->dbase->prepare("SELECT * FROM $this->regTable WHERE Username LIKE ?");
				
			$this->stmt->execute(array("%".$keyword."%"));

			if($this->stmt->rowCount() > 0) {
				return $this->stmt;
			} else {
				return false;
			}
			

		}

		public function search_services($keyword)
		{

			$this->stmt = $this->dbase->prepare("SELECT * FROM $this->reserveTable WHERE Category LIKE ? OR ReserveName LIKE ?  ");
				
			$this->stmt->execute(array("%".$keyword."%","%".$keyword."%"));

			if($this->stmt->rowCount() > 0) {
				return $this->stmt;
			} else {
				return false;
			}

		}
		
		public function search_reserves_category($keyword)
		{

			$this->stmt = $this->dbase->prepare("SELECT * FROM $this->reserveTable WHERE Category LIKE ? AND ReserveVacancy='Vacant' ");

			$this->stmt->execute(array("%".$keyword."%"));

			return $this->stmt;

		}

		public function delete_reserve($reserve_id)
		{

			$this->stmt = $this->dbase->prepare("DELETE FROM $this->reserveTable WHERE ReserveId=? ");

			$this->stmt->bindParam(1,$reserve_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}

		}
		
		public function delete_drink($drink_id)
		{

			$this->stmt = $this->dbase->prepare("DELETE FROM $this->drinksTable WHERE DrinkId=? ");

			$this->stmt->bindParam(1,$drink_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}

		}
		
		public function fetch_from_reserve_table_by_category_index($Cat)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE Category='$Cat' ORDER BY ReserveDate ASC LIMIT 4");
			
			return $sql;
		}
		
		public function fetch_from_reserve_table_by_category($Cat)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE Category='$Cat' ORDER BY ReserveDate");
			
			return $sql;
		}
		
		public function fetch_from_reserve_table()
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveVacancy='Vacant' ORDER BY ReserveDate LIMIT 12");
			
			return $sql;
		}
		
		public function fetch_from_reserve_table_by_club($ownerId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveVacancy='Vacant' AND OwnerId='$ownerId' ORDER BY ReserveDate LIMIT 12");
			
			return $sql;
		}

		public function fetch_from_reserve_table_by_club_mobile($ownerId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveVacancy='Vacant' AND OwnerId='$ownerId' ORDER BY ReserveDate");
			
			if($sql->rowCount() > 0 ) {
		    	return $sql;
			} else {
			    return false;
			}
		}

		public function fetch_reserve_by_owner_category($OwnerId,$Cat)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE OwnerId='$OwnerId' AND Category='$Cat'  ");

			return $sql;

		}
		
		public function fetch_reserve_by_owner($OwnerId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE OwnerId='$OwnerId' ");

			return $sql;

		}
		
		public function fetch_drinks_by_owner_mobile($OwnerId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->drinksTable WHERE OwnerId='$OwnerId' ");

			if($sql->rowCount() > 0 ) {
			    return $sql;
			} else {
			    return false;
			}

		}

		public function fetch_specific_reserve_data($reserve_id)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveId='$reserve_id' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {


							$this->fetched_rs_category = $row["Category"];
							$this->fetched_rs_id = $row["ReserveId"];
							$this->fetched_user_id = $row["OwnerId"];
							$this->fetched_rs_name = $row["ReserveName"];
							$this->fetched_rs_description = $row["ReserveDesc"];
							$this->fetched_rs_photo_path = $row["PicPath"];
							$this->fetched_rs_stock = $row["ReserveStock"];
							$this->fetched_rs_tables = $row["ReserveTablesTotal"];
							$this->fetched_rs_price = $row["ReservePrice"];
							$this->fetched_rs_location = $row["ReserveLocation"];
							$this->fetched_rs_date = $row["ReserveDate"];
							$this->fetched_rs_time = $row["ReserveTime"];

				}

			} else {

				echo "Invalid ReserveId";

			}

		}

		/*public function fetch_all_reserve_specific_data()
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a,$this->")

		}*/

		public function fetch_from_reserve_table_ID($ReserveId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveId='$ReserveId' ");
			
			return $sql;
		}
		
		public function fetch_specific_reserve_details_mobile($ReserveId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable WHERE ReserveId='$ReserveId' ");
			
			return $sql;
		}

		public function fetch_specific_reserve_table_by_club_mobile($ReserveId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a,$this->regTable b WHERE a.ReserveId='$ReserveId' AND a.ReserveVacancy='Vacant' AND a.OwnerId=b.UserId ");
			
			if($sql->rowCount() > 0) {	
		        return $sql;
		    } else {
		        return false;
		    }
		}
		
		public function fetch_specific_drinks_details_mobile($DrinkId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->drinksTable WHERE DrinkId='$DrinkId' ");
			
			if($sql->rowCount() > 0) {	
		        return $sql;
		    } else {
		        return false;
		    }
		    
		}
		
		public function fetch_from_reg_by_owner_id($UserId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' ");
			
		    if($sql->rowCount() > 0) {	
		        return $sql;
		    } else {
		        return false;
		    }
		}

		public function fetch_all_reserves_by_owners($OwnerId)
		{
			$sql = $this->dbase->query("SELECT DISTINCT a.ReserveId,b.ReserveId,a.OwnerId FROM $this->reserveTable a,$this->paymentTable b 
				WHERE a.ReserveId=b.ReserveId AND a.OwnerId='$OwnerId' ");

			return $sql;
		}
		
		public function insert_to_cart_table()
		{
			$this->stmt = $this->dbase->prepare("INSERT INTO $this->cartTable(CustId,CartId,CartStatus,Category,ProdId,ProdName,ProdQty,ProdPrice,PaymentId,CartDate,CartTime) 
																	VALUES(:custId,:cartId,:cartStatus,:cat,:prodid,:pname,:pQty,:price,:payid,now(),now())");

			$this->stmt->bindParam(":custId",$this->user_id);
			$this->stmt->bindParam(":cartId",$this->cart_id);
			$this->stmt->bindParam(":cartStatus",$this->cart_status);
			$this->stmt->bindParam(":cat",$this->prod_category);
			$this->stmt->bindParam(":prodid",$this->prod_id);
			$this->stmt->bindParam(":pname",$this->prod_name);
			$this->stmt->bindParam(":pQty",$this->prod_qty);
			$this->stmt->bindParam(":price",$this->prod_price);
			$this->stmt->bindParam(":payid",$this->pay_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
		}

		public function fetch_from_cart_table($session_id)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->cartTable WHERE CustId='$session_id' AND CartStatus='Cart' ");
			
			return $sql;
		}

		public function fetch_all_from_pay_table()
		{
			$sql = $this->dbase->query("SELECT * FROM $this->paymentTable a,$this->cartTable b WHERE a.PaymentId = b.PaymentId AND a.DeliveryStat='Not Delivered' ");
			
			return $sql;
		}
		
		public function fetch_all_from_reg_table($user_id)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->regTable  WHERE UserId='$user_id' ");
			
			foreach($sql as $row) {
				$this->regUserId = $row["UserId"];
				$this->regUsername = $row["Username"];
				$this->regUserEmail = $row["Email"];
				$this->regUserPhone = $row["PhoneNo"];
			}
		}
		
		public function count_reserved_seats($ReserveId)
		{
		    
		    $sql = $this->dbase->query("SELECT b.TableCapacity FROM $this->reserveTable a,$this->paymentTable b WHERE a.ReserveId=b.ReserveId AND a.ReserveId='$ReserveId' ");
		    
		    if($sql->rowCount() > 0) {
				return $sql;
			} else {
				return false;
			}
		    
		}
		
		public function update_cart_table($session_id,$pay_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->cartTable SET CartStatus='offload', PaymentId='$pay_id' WHERE CustId='$session_id' ");
			$this->stmt->execute();
			
			if($this->stmt) {
				return true;
			} else {
				return false;
			}
			
		}

		public function update_manager_pay_table($prod_id,$cart_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable a,$this->cartTable b SET a.DeliveryStat='Delivery'
			 WHERE a.PaymentId= b.PaymentId AND b.CartId='$cart_id' AND b.ProdId='$prod_id' ");
			$this->stmt->execute();
			
			if($this->stmt) {
				return true;
			} else {
				return false;
			}
			
		}
		
		public function insert_to_payment_table()
		{
			$this->stmt = $this->dbase->prepare("INSERT INTO $this->paymentTable(CustId,PaymentId,PaymentMethod,Amount,DeliveryStat,PurchaseDate,PurchaseTime) 
																	VALUES(:custId,:payId,:payMethod,:amt,:delivery,now(),now())");

			$this->stmt->bindParam(":custId",$this->user_id);
			$this->stmt->bindParam(":payId",$this->pay_id);
			$this->stmt->bindParam(":payMethod",$this->pay_method);
			$this->stmt->bindParam(":amt",$this->pay_amount);
			$this->stmt->bindParam(":delivery",$this->delivery_stat);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}
		}
		
		public function fetch_cart_pay($session_id)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->cartTable WHERE CustId='$session_id' ");
			
			return $sql;
		}

		public function fetch_purchases($session_id,$pay_id)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE CustId='$session_id' AND PaymentId='$pay_id' ");
			
			foreach($sql as $row) {
				$this->fetchedPayUserId = $row["CustId"];
				$this->fetchedPayId = $row["PaymentId"];
				$this->fetchedPayMethod = $row["PaymentMethod"];
				$this->fetchedPayAmount = $row["Amount"];
				$this->fetchedPayStat = $row["DeliveryStat"];
				$this->fetchedPayDate = $row["PurchaseDate"];
				$this->fetchedPayTime= $row["PurchaseTime"];
			}
		}
		
		public function fetch_all_drinks_by_owners_category($owner_id,$category)
		{
		     $sql =$this->dbase->query("SELECT * FROM poppindrinks WHERE OwnerId='$owner_id' AND Category='$category' ORDER BY Category ASC"); 
		     
		     return $sql;
		}

		public function fetch_all_drinks_by_owner($owner_id)
		{
		     $sql =$this->dbase->query("SELECT * FROM poppindrinks WHERE OwnerId='$owner_id' ORDER BY Category ASC"); 
		     
		     return $sql;
		}
		
		public function fetch_club_happy_hour_time($owner_id)
		{
		    $sql =$this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$owner_id' ");
		    
		    foreach($sql as $row) {
		        $this->happy_hour_start = $row["HappyHourStart"];
		        $this->happy_hour_stop = $row["HappyHourStop"];
		    }
		}
		
		public function fetch_all_drinks_by_owners_happy_hour($owner_id)
		{
		     $sql =$this->dbase->query("SELECT * FROM poppindrinks WHERE OwnerId='$owner_id' AND HappyHour='1' ORDER BY Category ASC"); 
		     
		     return $sql;
		}
		
		public function fetch_bought_drinks()
		{
		    
		    /* $sql= $this->dbase->query("SELECT * FROM $this->paymentTable"); 
		     
                        
            foreach($sql as $row) {
                
                $drinks = explode(",",$row['ReserveId']);
                
            }
            
            for($i=0; $i<sizeof($drinks);$i++){
            
                $sql_s=$this->dbase->query("SELECT * FROM poppindrinks WHERE DrinkId={$drinks[$i]}"); 
                
               return $sql_s; 
            
            }*/
            
            $sql="SELECT * FROM poppindrinks WHERE DrinkId IN ("; 
                          
                        foreach($_SESSION['carts'] as $id => $value) { 
                            $sql.=$id.","; 
                        } 
                          
                        $sql=substr($sql, 0, -1).") ORDER BY category ASC"; 
                        
                        return $this->dbase->query($sql); 
            
          //  return $drinks;
		    
		}
		
		public function fetch_drinks_data($id)
		{
		    
		     $sql = $this->dbase->query("SELECT * FROM poppindrinks WHERE DrinkId='$id' ");
		     
		     return $sql;
		    
		}
		
		public function fetch_cart_items()
		{
		    
		    $d_id = array();

            foreach($_SESSION['carts'] as $id => $value) {
                
                $d_id1 = array_push($d_id,$id);
                
                
                $u = implode(",",$d_id);
            
            }
		    
		       $sql="SELECT * FROM poppindrinks WHERE DrinkId IN '$u' "; 
                    
                return $this->dbase->query($sql);
		    
		}
		
		public function insert_to_events_reg_table()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regEventsTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database

								return false;

						} else {

								$this->stmt = $this->dbase->prepare("INSERT INTO $this->regEventsTable(UserId,Username,UserType,Email,PhoneNo,Address,City,Country,Password,RegDate,RegTime) 
																	VALUES(:userid,:username,:usertype,:email,:phoneno,:address,:city,:country,:pass,now(),now())");

								$this->stmt->bindParam(":userid",$this->user_id);
								$this->stmt->bindParam(":username",$this->username);
								$this->stmt->bindParam(":usertype",$this->usertype);
								$this->stmt->bindParam(":email",$this->user_email);
								$this->stmt->bindParam(":phoneno",$this->phone_no);
								$this->stmt->bindParam(":address",$this->address);
								$this->stmt->bindParam(":city",$this->city);
								$this->stmt->bindParam(":country",$this->country);
								$this->stmt->bindParam(":pass",$this->password);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

						}

		}

		public function insert_to_reg_table($id_status,$licence_status,$acc_status)
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
						
								return false;		
								

						} else {
                                
                                $this->stmt = $this->dbase->prepare("INSERT INTO $this->regTable(UserId,BOId,Username,UserType,ProfilePic,CoverPic,OpeningDays,OpeningTime,ClosingTime,IDStatus,BusinessLicenceStatus,SettlementAccountStatus,Email,PhoneNo,Address,City,Country,Password,Approval,RegDate,RegTime) 
																	VALUES(:userid,:boid,:username,:usertype,:p_pic,:c_pic,:open_days,:open_time,:close_time,:IDStatus,:BusinessLicenceStatus,:SettlementAccountStatus,:email,:phoneno,:address,:city,:country,:pass,:approve,now(),now())");

								$this->stmt->bindParam(":userid",$this->user_id);
								$this->stmt->bindParam(":boid",$this->user_id);
								$this->stmt->bindParam(":username",$this->username);
								$this->stmt->bindParam(":usertype",$this->usertype);
								$this->stmt->bindParam(":p_pic",$this->profile_pic);
								$this->stmt->bindParam(":c_pic",$this->cover_pic);
								$this->stmt->bindParam(":open_days",$this->open_days);
								$this->stmt->bindParam(":open_time",$this->open_time);
								$this->stmt->bindParam(":close_time",$this->close_time);
								$this->stmt->bindParam(":IDStatus",$id_status);
								$this->stmt->bindParam(":BusinessLicenceStatus",$licence_status);
								$this->stmt->bindParam(":SettlementAccountStatus",$acc_status);
								$this->stmt->bindParam(":email",$this->user_email);
								$this->stmt->bindParam(":phoneno",$this->phone_no);
								$this->stmt->bindParam(":address",$this->address);
								$this->stmt->bindParam(":city",$this->city);
								$this->stmt->bindParam(":country",$this->country);
								$this->stmt->bindParam(":pass",$this->password);
								$this->stmt->bindParam(":approve",$this->approval_status);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

                        
						}

		}

		public function BO_to_reg_SP_table($bo_id)
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
						
								return false;		
								

						} else {
                                
                                $this->stmt = $this->dbase->prepare("INSERT INTO $this->regTable(UserId,BOId,Username,UserType,ProfilePic,CoverPic,OpeningDays,OpeningTime,ClosingTime,IDStatus,BusinessLicenceStatus,SettlementAccountStatus,Email,PhoneNo,Address,City,Country,Password,Approval,RegDate,RegTime) 
																	VALUES(:userid,:boid,:username,:usertype,:p_pic,:c_pic,:open_days,:open_time,:close_time,:IDStatus,:BusinessLicenceStatus,:SettlementAccountStatus,:email,:phoneno,:address,:city,:country,:pass,:approve,now(),now())");

                                $id_status = 0;$licence_status = 0;$acc_status = 0; 
								$this->stmt->bindParam(":userid",$this->user_id);
								$this->stmt->bindParam(":boid",$bo_id);
								$this->stmt->bindParam(":username",$this->username);
								$this->stmt->bindParam(":usertype",$this->usertype);
								$this->stmt->bindParam(":p_pic",$this->profile_pic);
								$this->stmt->bindParam(":c_pic",$this->cover_pic);
								$this->stmt->bindParam(":open_days",$this->open_days);
								$this->stmt->bindParam(":open_time",$this->open_time);
								$this->stmt->bindParam(":close_time",$this->close_time);
								$this->stmt->bindParam(":IDStatus",$id_status);
								$this->stmt->bindParam(":BusinessLicenceStatus",$licence_status);
								$this->stmt->bindParam(":SettlementAccountStatus",$acc_status);
								$this->stmt->bindParam(":email",$this->user_email);
								$this->stmt->bindParam(":phoneno",$this->phone_no);
								$this->stmt->bindParam(":address",$this->address);
								$this->stmt->bindParam(":city",$this->city);
								$this->stmt->bindParam(":country",$this->country);
								$this->stmt->bindParam(":pass",$this->password);
								$this->stmt->bindParam(":approve",$this->approval_status);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

                        
						}

		}

		public function insert_to_SP_to_BO_relation_table($sp_id,$username,$bo_id,$approval_status,$admin_approval_status) {
			$this->stmt = $this->dbase->prepare("INSERT INTO $this->BOregSPTable(BOId,UserId,Username,Approval,SuperadminApproval,RegDate,RegTime) 
			VALUES(:boid,:spid,:username,:approve,:adminapprove,now(),now())");

					$id_status = 0;$licence_status = 0;$acc_status = 0; 
					$this->stmt->bindParam(":spid",$sp_id);
					$this->stmt->bindParam(":boid",$bo_id);
					$this->stmt->bindParam(":username",$username);
					$this->stmt->bindParam(":approve",$approval_status);
					$this->stmt->bindParam(":adminapprove",$admin_approval_status);
					$this->stmt->execute();

					if($this->stmt) {
						return true;
					} else {
						return false;
					}
		}

		public function delete_BO_registered_SP($user_id,$bo_id)
		{

			$this->stmt = $this->dbase->prepare("DELETE FROM $this->BOregSPTable WHERE UserId=? AND BOId=? ");

			$this->stmt->bindParam(1,$user_id);
			$this->stmt->bindParam(2,$bo_id);
			$this->stmt->execute();

			if($this->stmt) {
				return true;
			} else {
				return false;
			}

		}
		
		public function update_reg_event_table()
		{
			$this->stmt = $this->dbase->prepare
			("UPDATE $this->regEventsTable SET UserId=?,Username=?,Email=?,PhoneNo=?,Address=?,City=? WHERE UserId=? ");

			                    $this->stmt->bindParam(1,$this->user_id);
								$this->stmt->bindParam(2,$this->username);
								$this->stmt->bindParam(3,$this->user_email);
								$this->stmt->bindParam(4,$this->phone_no);
								$this->stmt->bindParam(5,$this->address);
								$this->stmt->bindParam(6,$this->city);
								$this->stmt->bindParam(7,$this->user_id);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

		}
		
		public function update_reg_table()
		{
			$this->stmt = $this->dbase->prepare
			("UPDATE $this->regTable SET UserId=?,Username=?,Email=?,PhoneNo=?,Address=?,City=?,ProfilePic=?,CoverPic=?,OpeningDays=?,OpeningTime=?,ClosingTime=? WHERE UserId=? ");

			                    $this->stmt->bindParam(1,$this->user_id);
								$this->stmt->bindParam(2,$this->username);
								$this->stmt->bindParam(3,$this->user_email);
								$this->stmt->bindParam(4,$this->phone_no);
								$this->stmt->bindParam(5,$this->address);
								$this->stmt->bindParam(6,$this->city);
								$this->stmt->bindParam(7,$this->profile_pic);
		                        $this->stmt->bindParam(8,$this->cover_pic);
		                        $this->stmt->bindParam(9,$this->open_days);
                    			$this->stmt->bindParam(10,$this->open_time);
                    			$this->stmt->bindParam(11,$this->close_time);
								$this->stmt->bindParam(12,$this->user_id);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

		}
		
		
		public function fetch_reservation_user_reg_info($owner)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$owner' ");
		    
		    foreach($sql as $row){
		        
		        $this->username = $row["Username"];
		        $this->open_days = $row["OpeningDays"];
		        $this->open_time = $row["OpeningTime"];
		        $this->close_time = $row["ClosingTime"];
		        $this->user_email = $row["Email"];
		        $this->phone_no = $row["PhoneNo"];
		        $this->city = $row["City"];
		        $this->country = $row["Country"];
		        
		    }
		    
		}

		public function fetch_from_specific_club_data_mobile($owner)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$owner' ");
		    
		    if($sql->rowCount() > 0) {
			    return $sql;
		    } else {
		        return false;
		    }
		    
		}

		public function fetch_from_club_data_mobile()
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable ");
		    
		    if($sql->rowCount() > 0) {
			    return $sql;
		    } else {
		        return false;
		    }
		    
		}

		public function fetch_approval_status_from_reg_data($UserId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' ");
		    
		    if($sql->rowCount() > 0) {
			    foreach ($sql as $key => $row) {
			    	return $row["Approval"];
			    }
		    } else {
		        return false;
		    }
		    
		}
		
		public function fetch_from_reg_data_mobile($UserId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' ");
		    
		    if($sql->rowCount() > 0) {
			    return $sql;
		    } else {
		        return false;
		    }
		    
		}
		
		public function update_user_id_pics($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET FrontIDPic=?,BackIDPic=?,IDStatus=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->front_id_pic);
			$this->stmt->bindParam(2,$this->back_id_pic);
			$this->stmt->bindParam(3,$this->IDStatus);
			$this->stmt->bindParam(4,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}


		public function is_uploaded_id_status_true($UserId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' AND IDStatus='1' ");
		    
		    if($sql->rowCount() > 0) {
			    return true;
		    } else {
		        return false;
		    }
		    
		}

		public function update_user_business_licence_pics($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET BusinessLicencePic=?,BusinessLicenceStatus=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->business_licence_pic);
			$this->stmt->bindParam(2,$this->business_licence_status);
			$this->stmt->bindParam(3,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}

		public function is_uploaded_business_licence_status_true($UserId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' AND BusinessLicenceStatus='1' ");
		    
		    if($sql->rowCount() > 0) {
			    return true;
		    } else {
		        return false;
		    }
		    
		}

		public function update_user_settlement_account($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET SettlementAccount=?,SettlementAccountStatus=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->settlement_account);
			$this->stmt->bindParam(2,$this->settlement_account_status);
			$this->stmt->bindParam(3,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}

		public function update_user_approval_status($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET Approval='1' WHERE UserId=? ");

			$this->stmt->bindParam(1,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}

		public function is_uploaded_settlement_account_status_true($UserId)
		{
		    
		    $sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$UserId' AND SettlementAccountStatus='1' ");
		    
		    if($sql->rowCount() > 0) {
			    return true;
		    } else {
		        return false;
		    }
		    
		}
		
		public function update_club_time($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET OpeningDays=?,OpeningTime=?,ClosingTime=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->open_days);
			$this->stmt->bindParam(2,$this->open_time);
			$this->stmt->bindParam(3,$this->close_time);
			$this->stmt->bindParam(4,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}
		
		public function update_happy_hour_time($user_id)
		{
			
			$this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET HappyHourStart=?,HappyHourStop=? WHERE UserId=? ");
			
			$this->stmt->bindParam(1,$this->happy_hour_start);
			$this->stmt->bindParam(2,$this->happy_hour_stop);
			$this->stmt->bindParam(3,$user_id);
			
			$this->stmt->execute(); 

			if($this->stmt) {
				return true;
			} else {
				return false;
			} 
			
		}
		
		public function insert_to_admin_reg_table()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regAdminTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database

								$this->message("Use an unregistered e-mail.","Fail");

						} else {

								$this->stmt = $this->dbase->prepare("INSERT INTO $this->regAdminTable(Email,Unique_Code,RegDate,RegTime) 
																	VALUES(?,?,now(),now())");

								$this->stmt->bindParam(1,$this->admin_email);
								$this->stmt->bindParam(2,$this->admin_code);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

						}

		}

		public function insert_to_user_reg_table()
		{

			$sql = $this->dbase->query("SELECT * FROM $this->poppinMpesaTable WHERE SenderPhone='$this->phone_no'");

			if($sql->rowCount() < 0) {

				$this->stmt = $this->dbase->prepare("INSERT INTO $this->poppinuser(Name,PhoneNo,Email,Password,RegDate,RegTime) VALUES(?,?,?,?,now(),now()) ");

				$this->stmt->bindParam(1,$this->full_name);
				$this->stmt->bindParam(2,$this->phone_no);
				$this->stmt->bindParam(3,$this->user_email);
				$this->stmt->bindParam(4,$this->password);
				$this->execute();

				if($this->stmt) {
					return true;
				} else {
					return false;
				}

			}

		}

		public function insert_to_login_table()
		{

			//see whether the entered email matches that in the EmailLihub table
			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE Email='$this->user_email' ");

			//if yes, fetch all the details that match the entered email and store in variables
			if($sql->rowCount() > 0) {

				foreach($sql as $row) {
					$this->fetched_email = $row["Email"];
					$this->fetched_user_id = $row["UserId"];
					$this->fetched_user_type = $row["UserType"];
				    $this->password = $row["Password"];
				}

				//confirm password

				//if above condition is met, enter that data into the Login table
				if(password_verify($this->raw_password,$this->password)) {

					$this->stmt = $this->dbase->prepare("INSERT INTO $this->loginTable(UserId,LoginDate,LoginTime) VALUES(:userid,now(),now())");

					$this->stmt->bindParam(":userid",$this->fetched_user_id);
					$this->stmt->execute();

					if($this->stmt) {
						return true;
					} else {
						return false;
					}

				}

			}


		}
		
		public function change_password()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$this->user_id' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
        						foreach($sql2 as $row) {
                					$this->pass = $row["Password"];
                				}
        						
						  if(password_verify($this->old_password,$this->pass)) {
						
						    $this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET Password=? WHERE UserId=?");

								$this->stmt->bindParam(1,$this->password);
								$this->stmt->bindParam(2,$this->user_id);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

					    	} /*else {
					    	    return false;
					    	}*/
						
						} else {
						    
								return false;

						}

		}
		
		public function change_events_password()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM $this->regEventsTable WHERE UserId='$this->user_id' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
        						foreach($sql2 as $row) {
                					$this->pass = $row["Password"];
                				}
        						
						  //if(password_verify($this->old_password,$this->pass)) {
						
						    $this->stmt = $this->dbase->prepare("UPDATE $this->regEventsTable SET Password=? WHERE UserId=?");

								$this->stmt->bindParam(1,$this->password);
								$this->stmt->bindParam(2,$this->user_id);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

					    	/*} else {
					    	    return false;
					    	}*/
						
						} else {
						    
								return false;

						}

		}
		
		public function forgot_password_events()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regEventsTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
						    $this->stmt = $this->dbase->prepare("UPDATE $this->regEventsTable SET Password=? WHERE Email=?");

								$this->stmt->bindParam(1,$this->password);
								$this->stmt->bindParam(2,$this->user_email);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

							
						} else {
						    
						        return false;
								$this->message("Use an unregistered e-mail.","Fail");

						}

		}
		
		public function forgot_password_reservations()
		{

			//sql query to determine if the UserId is already registered
			$sql2 = $this->dbase->query("SELECT * FROM ".$this->regTable." WHERE Email='$this->user_email' ");


						if($sql2->rowCount() > 0) { // if it does not exist in database
						
						    $this->stmt = $this->dbase->prepare("UPDATE $this->regTable SET Password=? WHERE Email=?");

								$this->stmt->bindParam(1,$this->password);
								$this->stmt->bindParam(2,$this->user_email);
								$this->stmt->execute();

								if($this->stmt) {
									return true;
								} else {
									return false;
								}

							
						} else {
						    
						        return false;
								$this->message("Use an unregistered e-mail.","Fail");

						}

		}

		public function get_session_details($session_id)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$session_id' ");

			foreach($sql as $row) {
				$this->session_email = $row["Email"];
				$this->session_user_id = $row["UserId"];
				$this->session_username = $row["Username"];
				$this->session_user_type = $row["UserType"];
				$this->session_phone = $row["PhoneNo"];
				$this->session_regDate = $row["RegDate"];
				$this->session_regTime = $row["RegTime"];
			}

		}

		public function get_all_SP()
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserType='SP'");
			return $sql;

		}

		public function get_all_BO_SP($BOId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->BOregSPTable WHERE BOId='$BOId' AND SuperadminApproval='1'");

			$sql1 = $this->dbase->query("SELECT * FROM $this->regTable WHERE BOId='$BOId' ");
			
			if($sql->rowCount() > 0) { 
				return $sql; 
			} else {
				return $sql1;
			}

		}

		public function get_all_registered_SP_to_BO($BOId)
		{

			$sql = $this->dbase->query("SELECT DISTINCT UserId FROM $this->BOregSPTable WHERE BOId='$BOId' ");
			
			if($sql->rowCount() > 0) { 
				return $sql; 
			} else {
				return false;
			}

		}

		public function get_all_registered_BO_to_SP($UserId,$approval)
		{

			$sql = $this->dbase->query("SELECT DISTINCT BOId FROM $this->BOregSPTable WHERE UserId='$UserId' AND Approval='$approval' ");
			
			if($sql->rowCount() > 0) { 
				return $sql; 
			} else {
				return false;
			}

		}


		public function get_all_BO()
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserType='BO' ");
			return $sql;

		}

		public function get_specific_BO($user_id)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserType='BO' AND UserId='$user_id' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {
					$this->username = $row["Username"];
					$this->open_days = $row["OpeningDays"];
					$this->open_time = $row["OpeningTime"];
					$this->close_time = $row["ClosingTime"];
					$this->profilePic = $row["ProfilePic"];
					$this->user_email = $row["Email"];
					$this->phone_no = $row["PhoneNo"];
					$this->city = $row["City"];
					$this->country = $row["Country"];
				}

				return true;

			} else {

				return false;

			}

		}

		public function get_specific_SP($user_id)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserType='SP' AND UserId='$user_id' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {
					$this->username = $row["Username"];
					$this->open_days = $row["OpeningDays"];
					$this->open_time = $row["OpeningTime"];
					$this->close_time = $row["ClosingTime"];
					$this->profilePic = $row["ProfilePic"];
					$this->user_email = $row["Email"];
					$this->phone_no = $row["PhoneNo"];
					$this->city = $row["City"];
					$this->country = $row["Country"];
				}

				return true;

			} else {

				return false;

			}

		}

		public function get_specific_SP_AND_BO($user_id)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE UserId='$user_id' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {
					$this->username = $row["Username"];
					$this->open_days = $row["OpeningDays"];
					$this->open_time = $row["OpeningTime"];
					$this->close_time = $row["ClosingTime"];
					$this->profilePic = $row["ProfilePic"];
					$this->user_email = $row["Email"];
					$this->phone_no = $row["PhoneNo"];
					$this->city = $row["City"];
					$this->country = $row["Country"];
				}

				return true;

			} else {

				return false;

			}

		}

		public function get_searched_specific_BO($reserve_name)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a, $this->regTable b WHERE a.OwnerId=b.UserId AND b.UserType='BO' AND ReserveName='$reserve_name' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {


							$this->fetched_rs_category = $row["Category"];
							$this->fetched_rs_id = $row["ReserveId"];
							$this->fetched_user_id = $row["OwnerId"];
							$this->fetched_rs_name = $row["ReserveName"];
							$this->fetched_rs_description = $row["ReserveDesc"];
							$this->fetched_rs_photo_path = $row["PicPath"];
							$this->fetched_rs_stock = $row["ReserveStock"];
							$this->fetched_rs_tables = $row["ReserveTablesTotal"];
							$this->fetched_rs_price = $row["ReservePrice"];
							$this->fetched_rs_location = $row["ReserveLocation"];
							$this->fetched_rs_date = $row["ReserveDate"];
							$this->fetched_rs_time = $row["ReserveTime"];

							array_push($this->fetched_search_array,$this->fetched_user_id);

				}

				return true;

			} else {

				return false;

			}

		}

		public function get_searched_specific_SP($reserve_name)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a, $this->regTable b WHERE a.OwnerId=b.UserId AND b.UserType='SP' AND ReserveName='$reserve_name' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {


							$this->fetched_rs_category = $row["Category"];
							$this->fetched_rs_id = $row["ReserveId"];
							$this->fetched_user_id = $row["OwnerId"];
							$this->fetched_rs_name = $row["ReserveName"];
							$this->fetched_rs_description = $row["ReserveDesc"];
							$this->fetched_rs_photo_path = $row["PicPath"];
							$this->fetched_rs_stock = $row["ReserveStock"];
							$this->fetched_rs_tables = $row["ReserveTablesTotal"];
							$this->fetched_rs_price = $row["ReservePrice"];
							$this->fetched_rs_location = $row["ReserveLocation"];
							$this->fetched_rs_date = $row["ReserveDate"];
							$this->fetched_rs_time = $row["ReserveTime"];

							array_push($this->fetched_search_array,$this->fetched_user_id);

				}

				return true;

			} else {

				return false;

			}

		}

		public function get_searched_specific_SP_AND_BO($reserve_name)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->reserveTable a, $this->regTable b WHERE a.OwnerId=b.UserId AND ReserveName='$reserve_name' ");
			
			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {


							$this->fetched_rs_category = $row["Category"];
							$this->fetched_rs_id = $row["ReserveId"];
							$this->fetched_user_id = $row["OwnerId"];
							$this->fetched_rs_name = $row["ReserveName"];
							$this->fetched_rs_description = $row["ReserveDesc"];
							$this->fetched_rs_photo_path = $row["PicPath"];
							$this->fetched_rs_stock = $row["ReserveStock"];
							$this->fetched_rs_tables = $row["ReserveTablesTotal"];
							$this->fetched_rs_price = $row["ReservePrice"];
							$this->fetched_rs_location = $row["ReserveLocation"];
							$this->fetched_rs_date = $row["ReserveDate"];
							$this->fetched_rs_time = $row["ReserveTime"];

							array_push($this->fetched_search_array,$this->fetched_user_id);

				}

				return true;

			} else {

				return false;

			}

		}

		public function get_all_unapproved_users()
		{

			$sql = $this->dbase->query("SELECT * FROM $this->regTable WHERE Approval='0' ");
			return $sql;

		}

		public function delete_user($userid)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->emailTable WHERE UserId='$userid' ");

			if($sql->rowCount() > 0) {

				$this->stmt = $this->dbase->prepare("DELETE $this->emailTable,$this->regTable FROM $this->emailTable INNER JOIN $this->regTable 
													WHERE $this->emailTable.UserId=$this->regTable.UserId AND $this->emailTable.UserId=:userid ");

				$this->stmt->bindParam(":userid",$userid);
				$this->stmt->execute();

				if($this->stmt) {
					echo "Deletion success";
				} else {
					echo "Deletion failed";
				}

			} else {
				echo "User not registered";
			}

		}
		public function test(){
			return "Db Connect";
		}

		public function delete_from_balance_table($PayerPhone)
		{

			$sql = $this->dbase->query("SELECT PayerPhone,AmountBalance FROM $this->balanceTable WHERE AmountBalance ='0' AND PayerPhone='$PayerPhone' ");

				//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

			if($sql->rowCount() > 0 ) {


				$this->stmt = $this->dbase->prepare("DELETE FROM $this->balanceTable WHERE AmountBalance='0' AND PayerPhone='$PayerPhone'");

				$this->stmt->execute();

				if($this->stmt) {
					return true;
				} else {
					return false;
				}
;

			} 

		}


	}
	$database = new database();

?>