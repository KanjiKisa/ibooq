<?php

	require_once("initialise.php");

	class paymentAPI extends database {

		public $service_name,$business_no,$transaction_ref,$internal_transId,$trans_timestamp,
				$trans_type,$account_no,$sender_phone,$first_name,$middle_name,$last_name,$full_name,$amount,$table_capacity,$currency,$acc_deposit,$signature;
		public $pay_id,$pay_rs_id,$pay_drk_id,$pay_owner_id,$pay_user_email,$pay_amount;

		public $rs_name,$rs_id;

		public  $fetched_service_name,
				$fetched_business_no,
				$fetched_transaction_ref,
				$fetched_internal_transId,
				$fetched_trans_timestamp,
				$fetched_trans_type,
				$fetched_account_no,
				$fetched_sender_phone,
				$fetched_first_name,
				$fetched_middle_name,
				$fetched_last_name,
				$fetched_amount,
				$fetched_currency,
				$fetched_signature,
				$fetched_pesa_date,
				$fetched_pesa_time;

		public $fetched_bal_reserve_id,
				$fetched_bal_payer_phone,
				$fetched_bal_payer_balance,
				$fetched_bal_update_date,
				$fetched_bal_update_time;

		public $account_balance;

		private $mpesaTable = "poppinmpesa";
		private $paymentTable = "poppinreservepayment";
		public $reserveTable = "poppinreserve";
		public $drinksTable = "poppindrinks";
		private $drinksPaymentTable = "poppindrinkspayment";
		private $regTable = "poppinreg_res";
		private $balanceTable = "poppinpaymentbalances_res";
		
		public $oid,$phone,$email,$sid;
		
		    public function set_order_id($oid)
            {
                $this->oid = $oid;
            }
            
            
            public function set_phone($phone)
            {
                $this->phone = $phone;
            }
            
            public function set_email($email)
            {
                $this->email = $email;
            }
            
            public function set_sid($sid)
            {
                $this->sid = $sid;
            }

			public function set_service_name($attr) 
			{
					$this->service_name = $attr;
			}

			public function set_business_no($attr) 
			{
					$this->business_no = $attr;
			}

			public function set_transaction_ref($attr) 
			{
					$this->transaction_ref = $attr;
			}

			public function set_internal_transId($attr) 
			{
					$this->internal_transId = $attr;
			}

			public function set_trans_timestamp($attr) 
			{
					$this->trans_timestamp = $attr;
			}

			public function set_trans_type($attr) 
			{
					$this->trans_type = $attr;
			}

			public function set_account_no($attr) 
			{
					$this->account_no = $attr;
			}
			
			public function set_table_capacity($tkt_capacity)
			{
				$this->table_capacity = $tkt_capacity;
			}

			public function set_sender_phone($attr) 
			{
					$this->sender_phone = $attr;
			}

			public function set_first_name($attr) 
			{
					$this->first_name = $attr;
			}

			public function set_middle_name($attr) 
			{
					$this->middle_name = $attr;
			}

			public function set_last_name($attr) 
			{
					$this->last_name = $attr;
			}
			
			public function set_full_name($f,$l) 
			{
					$this->full_name = $f." ".$l;
			}

			public function set_amount($attr) 
			{
					$this->amount = $attr;
			}

			public function set_currency($attr) 
			{
					$this->currency = $attr;
			}

			public function set_acc_deposit($acc)
			{
				$this->acc_deposit = $acc;
			}

			public function set_signature($attr) 
			{
					$this->signature = $attr;
			}

			public function set_pay_id($pay_id)
			{
				$this->pay_id = $pay_id;
			}

			public function set_pay_rs_id($rs_id)
			{
				$this->pay_rs_id = $rs_id;
			}
			
			public function set_pay_drk_id($drk_id)
			{
				$this->pay_drk_id = $drk_id;
			}

			public function set_pay_owner_id($owner_id)
			{
				$this->pay_owner_id = $owner_id;
			}
			
			public function set_pay_user_email($user_email)
			{
				$this->pay_user_email = $user_email;
			}
			
			public function set_pay_amount($pay_amount)
			{
				$this->pay_amount = $pay_amount;
			}

			public function initiator_request()
            {
                
                // Make API call to iPay
                $url 		= 'https://apis.ipayafrica.com/payments/v2/transact';
        
                $live       = 1;
                $oid        = $this->oid; 
                $inv        = $this->oid;
                $amount 	= $this->amount;
                $tel        = $this->phone;
                $eml        = $this->email;
                $vid        = "pop";   
                $curr       = "KES"; 
                $p1         = "";
                $p2         = "";
                $p3         = "";
                $p4         = "";
                $cbk        = "http://popin.co.ke/mpesa_details.php"; // Points to the callback file
                $cst        = 1;
                $crl        = 0;
                // Check hash
                $key 		= "5fkjed65oj85epfe";
                $datastring = $live.$oid.$inv.$amount.$tel.$eml.$vid.$curr.$p1.$p2.$p3.$p4.$cst.$cbk;
        
                $generated_hash = hash_hmac('sha256', $datastring, $key);
        
                $fields = array(
                    'amount'=>$amount,
                    'oid'	=>$oid,
                    'inv'	=>$inv,
                    'vid'	=>$vid,
                    'curr'	=>$curr,
                    'live'	=>$live,
                    'tel'	=>$tel,
                    'eml'	=>$eml,
                    'p1'	=>$p1,
                    'p2'	=>$p2,
                    'p3'	=>$p3,
                    'p4'	=>$p4,
                    'cbk'	=>$cbk,
                    'crl'	=>$crl,
                    'cst'	=>$cst,
                    'hash'	=>$generated_hash
                );
                
        
                // Open connection
                $ch = curl_init($url);
        
                // Set the url, number of POST vars, POST data
        		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
                
        
                // Execute post
                $result = curl_exec($ch);
        
        
               // $si = json_decode($result)->data->sid;
                
                return $result;
                
                /* if($si){
                    header("Location: get_ipay.php?sid=$si");
                }*/
                
            }
            
            public function process_initiator_stk()
            {
        
                $vid = "pop";
                
                $url1 		= 'https://apis.ipayafrica.com/payments/v2/transact/push/mpesa';
                
                $dstring = $this->phone.$vid.$this->sid;
                
                 $key = "5fkjed65oj85epfe";
                
                $g_hash = hash_hmac('sha256',$dstring , $key);
                
                $f = [
                    'phone'=>$this->phone,
                    'sid'	=>$this->sid,
                    'vid'	=>$vid,
                    'hash'	=>$g_hash
                    ];
                    
                $ch1 = curl_init($url1);
        
                // Set the url, number of POST vars, POST data
        		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch1,CURLOPT_POSTFIELDS, $f);
        
                // Execute post
                $result1 = curl_exec($ch1);
                
                return $result1;
                
            }
            
            public function search_pay_orders($oid)
            {
                
                $vid = "pop";
                
                $url1	= 'https://apis.ipayafrica.com/payments/v2/transaction/search';
                
                $dstring = $oid.$vid;
                
                $key 		= "5fkjed65oj85epfe";
        
                $g_hash = hash_hmac('sha256',$dstring , $key);
        
                $f = [
                    'vid'	=>$vid,
                    'hash'	=>$g_hash,
                    'oid'   =>$oid
                    ];
            
                $ch1 = curl_init($url1);

                // Set the url, number of POST vars, POST data
        		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch1,CURLOPT_POSTFIELDS, $f);
                
        
                // Execute post
                $result1 = curl_exec($ch1);
                
                return $result1;

                
            }

			public function insert_to_mpesa_table()
			{

				$this->stmt = $this->dbase->prepare
				("INSERT INTO $this->mpesaTable(ServiceName,BusinessNumber,TransactionReference,InternalTransactionId,TransactionTimestamp,
					TransactionType,AccountNumber,SenderPhone,FirstName,MiddleName,LastName,Amount,Currency,Signature,PesaDate,PesaTime) 
					VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),now()) ");

				$this->stmt->bindParam(1,$this->service_name);
				$this->stmt->bindParam(2,$this->business_no);
				$this->stmt->bindParam(3,$this->transaction_ref);
				$this->stmt->bindParam(4,$this->internal_transId);
				$this->stmt->bindParam(5,$this->trans_timestamp);
				$this->stmt->bindParam(6,$this->trans_type);
				$this->stmt->bindParam(7,$this->account_no);
				$this->stmt->bindParam(8,$this->sender_phone);
				$this->stmt->bindParam(9,$this->first_name);
				$this->stmt->bindParam(10,$this->middle_name);
				$this->stmt->bindParam(11,$this->last_name);
				$this->stmt->bindParam(12,$this->amount);
				$this->stmt->bindParam(13,$this->currency);
				$this->stmt->bindParam(14,$this->signature);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}


			}

			public function insert_into_reserve_payment_table($transaction_ref,$sender_phone)
			{

				/*$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' AND SenderPhone='$sender_phone' ");

				if($sql->rowCount() > 0 ) {*/


					$this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->paymentTable(TransactionReference,PaymentId,ReserveId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,TableCapacity,AccDeposit,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,?,now(),now())");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_rs_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->table_capacity);
					$this->stmt->bindParam(10,$this->acc_deposit);

					if($this->stmt->execute()) {

						//echo "<script> alert('Successful pay'); </script>";
						return true;
					} else {
						//echo "<script> alert('Successful pay'); </script>";
						return false;
					}

			/*	} else {
					return false;
					echo "<script> alert('Incorrect Transaction Number or Phone Number or Both'); </script>";
				}*/

			}
			
			public function insert_into_drinks_payment_table($transaction_ref,$sender_phone)
			{

				/*$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' AND SenderPhone='$sender_phone' ");

				if($sql->rowCount() > 0 ) {*/


					$this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->drinksPaymentTable(TransactionReference,PaymentId,DrinksId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,AccDeposit,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,now(),now())");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_drk_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->acc_deposit);

					if($this->stmt->execute()) {

						//echo "<script> alert('Successful pay'); </script>";
						return true;
					} else {
						//echo "<script> alert('Successful pay'); </script>";
						return false;
					}

			/*	} else {
					return false;
					echo "<script> alert('Incorrect Transaction Number or Phone Number or Both'); </script>";
				}*/

			}

			public function fetch_specific_mpesa_details($transaction_ref)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' ");

				return $sql;

			}
			
			public function fetch_specific_reserve_mpesa_details($reserve_id)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$reserve_id' ");

				return $sql;

			}

			public function get_total_reserve_amount_per_payer($rs_id,$payer_phone)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$rs_id' AND PayerPhone='$payer_phone' ");

				return $sql;

			}

			public function insert_to_balance_table($rs_id,$user_phone,$balance)
			{

					$sql = $this->dbase->query("SELECT * FROM $this->balanceTable WHERE ReserveId='$rs_id' AND PayerPhone='$user_phone' ");

					$date = date("Y-d-m");
					$time = date("hh:mm:s");

					//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

						if($sql->rowCount() > 0 ) {


								$this->stmt = $this->dbase->prepare(
												"UPDATE $this->balanceTable SET ReserveId=? ,PayerPhone=?,AmountBalance=?,UpdateDate=now(),UpdateTime=now() WHERE ReserveId=? AND PayerPhone=? ");
								
								$this->stmt->bindParam(1,$rs_id);
								$this->stmt->bindParam(2,$user_phone);
								$this->stmt->bindParam(3,$balance);
								$this->stmt->bindParam(4,$rs_id);
								$this->stmt->bindParam(5,$user_phone);

								if($this->stmt->execute()) {
									return true;
								} else {
									return false;
								}

						} else {

							$this->stmt = $this->dbase->prepare("INSERT INTO $this->balanceTable(ReserveId,PayerPhone,AmountBalance,UpdateDate,UpdateTime) 
																VALUES(?,?,?,now(),now())");
							$this->stmt->bindParam(1,$rs_id);
							$this->stmt->bindParam(2,$user_phone);
							$this->stmt->bindParam(3,$balance);

							if($this->stmt->execute()) {
								return true;
							} else {
								return false;
							}

						}

			}


			public function fetch_by_payer_from_balance_table($payer_phone,$ReserveId)
			{


				$sql = $this->dbase->query("SELECT * FROM $this->balanceTable WHERE PayerPhone='$payer_phone' AND ReserveId='$ReserveId' ");

				if($sql->rowCount() > 0 ) {
					
					foreach($sql as $row) {

						$this->fetched_bal_reserve_id = $row["ReserveId"];
						$this->fetched_bal_payer_phone = $row["PayerPhone"];
						$this->fetched_bal_payer_balance = $row["AmountBalance"];
						$this->fetched_bal_update_date = $row["UpdateDate"];
						$this->fetched_bal_update_time = $row["UpdateTime"];

						return true;

					}
					
				} else {
					return false;
				}

			}

			public function fetch_user_payment_details($P_Phone,$ReserveId)
			{

				$sql = $this->dbase->query("SELECT DISTINCT ReserveId,PayerPhone,PayerEmail FROM $this->paymentTable WHERE PayerPhone='$P_Phone' AND ReserveId='$ReserveId' ");

				foreach($sql as $row) {

					return $row["PayerEmail"];

				}

			}


			public function fetch_reserve_payment_details($OwnerId)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,a.PayerPhone,a.PayerName,b.ReserveId,b.ReserveName 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND b.OwnerId='$OwnerId' AND a.AccDeposit='0' ");

				return $sql;

			}

			public function fetch_all_reserve_payment_details()
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.ReserveId,b.ReserveName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c WHERE a.ReserveId=b.ReserveId AND b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_specific_reserve_all_details($reserve_id)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.ReserveId,b.Category,b.ReserveLocation,b.ReservePrice,b.PicPath,b.StartDate,b.StartTime,b.StopTime,b.StopDate,b.ReserveName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c WHERE a.ReserveId=b.ReserveId AND b.ReserveId='$reserve_id' AND b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_reserve_payment_details_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.TransactionReference,a.PayerPhone,a.PayerName,b.Category,b.ReserveId,b.ReserveName 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND b.OwnerId='$OwnerId' AND b.Category='$Category' ");

				return $sql;

			}

			public function count_reserves_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable WHERE OwnerId='$OwnerId' ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function sum_of_reserves_total_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->paymentTable WHERE OwnerId='$OwnerId' AND AccDeposit<2 ");

				foreach($sql as $row) {
					return $row["TotalAmount"];
				}

			}

			public function sum_of_specific_reserves_total($ReserveId)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$ReserveId' AND AccDeposit<2 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function count_all_reserves()
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable  ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function sum_of_all_reserves_total()
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->paymentTable WHERE AccDeposit<2 ");

				foreach($sql as $row) {
					return $row["TotalAmount"];
				}

			}

			public function count_reserves_by_owner_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable WHERE OwnerId='$OwnerId' AND Category='$Category' ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function sum_of_reserves_total_by_owner_cat($OwnerId,$Category)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT a.OwnerId,a.ReserveId,a.AccDeposit,b.ReserveId,b.Category,a.AmountPaid FROM $this->paymentTable a,$this->reserveTable b WHERE b.Category='$Category' 
					AND a.ReserveId=b.ReserveId AND a.OwnerId='$OwnerId' AND  a.AccDeposit=1 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function fetch_of_specific_reserves_payment_details($ReserveId)
			{
				$total = 0;

				$sql = $this->dbase->query("SELECT DISTINCT a.ReserveId,a.AmountPaid,b.ReserveId,b.ReserveName,a.AccDeposit 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND a.ReserveId='$ReserveId' 
					AND a.AccDeposit='0'");

				foreach($sql as $row) {
					$this->rs_name = $row["ReserveName"];
					$this->rs_id = $row["ReserveId"];
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function update_pay_withdrawal_status($ReserveId)
			{

				$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET AccDeposit='1',PayDate=now(),PayTime=now() WHERE ReserveId=? AND AccDeposit='0'");

				$this->stmt->bindParam(1,$ReserveId);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function update_cashout_withdrawal_status($ReserveId)
			{

				$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET AccDeposit='2',PayDate=now(),PayTime=now() WHERE ReserveId=? AND AccDeposit='1'");

				$this->stmt->bindParam(1,$ReserveId);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function fetch_withdrawal_status_checkout()
			{

				$sql = $this->dbase->query("SELECT DISTINCT a.ReserveId,a.OwnerId,a.PayDate,a.PayTime,b.ReserveId,b.ReserveName,a.AccDeposit,
					c.UserId,c.Username,c.Email,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c 
					WHERE a.ReserveId=b.ReserveId AND a.OwnerId=c.UserId AND a.AccDeposit='1'");

				return $sql;

			}
			
			public function fetch_total_tables_booked($reserveId)
			{
				
				$sql = $this->dbase->query("SELECT SUM(TableCapacity) as TotalBookedTables FROM $this->paymentTable WHERE ReserveId='$reserveId' ");
				
				foreach($sql as $row){
					return $row['TotalBookedTables'];
				}
				
			}


	}

	$payment = new paymentAPI();


?>