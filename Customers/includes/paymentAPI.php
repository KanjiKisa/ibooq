<?php

	require_once("initialise.php");

	class paymentAPI extends database {

		public $service_name,$business_no,$transaction_ref,$internal_transId,$trans_timestamp,
				$trans_type,$account_no,$sender_phone,$first_name,$middle_name,$last_name,$full_name,$amount,$table_capacity,$currency,$acc_deposit,$signature;
		public $pay_id,$pay_rs_id,$pay_drk_id,$pay_owner_id,$pay_user_email,$pay_amount;

		public $rs_name,$rs_id,$rs_total_amount;

		public  $fetched_service_name,
				$fetched_business_no,
				$fetched_transaction_ref,
				$fetched_internal_transId,
				$fetched_trans_timestamp,
				$fetched_trans_type,
				$fetched_account_no,
				$fetched_sender_phone,
				$fetched_first_name,
				$fetched_middle_name,
				$fetched_last_name,
				$fetched_amount,
				$fetched_currency,
				$fetched_signature,
				$fetched_pesa_date,
				$fetched_pesa_time;

		public $fetched_bal_reserve_id,
				$fetched_bal_payer_phone,
				$fetched_bal_payer_balance,
				$fetched_bal_update_date,
				$fetched_bal_update_time,$fetched_bal_table_capacity;

		public $account_balance,$pay_now_date_only,$pay_now_time_only;

        public $mpesa_merchant_id,$mpesa_checkout_id,$mpesa_nopay,$mpesa_phone,$mpesa_pay_date,$mpesa_receipt,$mpesa_pay_name,$mpesa_error_desc,
				$fetched_c2b_TransactionDate,$fetched_c2b_Receipt,$fetched_c2b_Amount,$fetched_stk_Receipt,$fetched_stk_Amount,$pay_date,$pay_time,$arrival_date,$arrival_time,$ending_time;
				
		private $mpesaTable = "mpesa_data";
		private $mpesaErrorsTable = "mpesa_errors";
		private $mpesaC2BTable = "mpesa_c2b_data";
		
		private $payLaterTable = "poppinreservelater";
		private $paymentTable = "poppinreservepayment";
		private $receiptTable = "poppintickets_res";
		public $reserveTable = "poppinreserve";
		public $drinksTable = "poppindrinks";
		private $drinksPaymentTable = "poppindrinkspayment";
		private $regTable = "poppinreg_res";
		private $balanceTable = "poppinpaymentbalances_res";
		
		public $oid,$phone,$email,$sid;

			/*public function __construct()
			{
				date_default_timezone_set('Africa/Nairobi');
			
				$this->pay_now_date_only = date("Y-m-d");
				
				$this->pay_now_time_only = date("H:i:s");
			}*/
		
		    public function set_order_id($oid)
            {
                $this->oid = $oid;
            }
            
            
            public function set_phone($phone)
            {
                $this->phone = $phone;
            }
            
            public function set_email($email)
            {
                $this->email = $email;
            }
            
            public function set_sid($sid)
            {
                $this->sid = $sid;
            }

			public function set_service_name($attr) 
			{
					$this->service_name = $attr;
			}

			public function set_business_no($attr) 
			{
					$this->business_no = $attr;
			}

			public function set_transaction_ref($attr) 
			{
					$this->transaction_ref = $attr;
			}

			public function set_internal_transId($attr) 
			{
					$this->internal_transId = $attr;
			}

			public function set_trans_timestamp($attr) 
			{
					$this->trans_timestamp = $attr;
			}

			public function set_trans_type($attr) 
			{
					$this->trans_type = $attr;
			}

			public function set_account_no($attr) 
			{
					$this->account_no = $attr;
			}
			
			public function set_table_capacity($tkt_capacity)
			{
				$this->table_capacity = $tkt_capacity;
			}

			public function set_sender_phone($attr) 
			{
					$this->sender_phone = $attr;
			}

			public function set_first_name($attr) 
			{
					$this->first_name = $attr;
			}

			public function set_middle_name($attr) 
			{
					$this->middle_name = $attr;
			}

			public function set_last_name($attr) 
			{
					$this->last_name = $attr;
			}
			
			public function set_full_name($f,$l) 
			{
					$this->full_name = $f." ".$l;
			}

			public function set_amount($attr) 
			{
					$this->amount = $attr;
			}

			public function set_currency($attr) 
			{
					$this->currency = $attr;
			}

			public function set_acc_deposit($acc)
			{
				$this->acc_deposit = $acc;
			}

			public function set_signature($attr) 
			{
					$this->signature = $attr;
			}

			public function set_pay_id($pay_id)
			{
				$this->pay_id = $pay_id;
			}

			public function set_pay_rs_id($rs_id)
			{
				$this->pay_rs_id = $rs_id;
			}
			
			public function set_pay_drk_id($drk_id)
			{
				$this->pay_drk_id = $drk_id;
			}

			public function set_pay_owner_id($owner_id)
			{
				$this->pay_owner_id = $owner_id;
			}
			
			public function set_pay_user_email($user_email)
			{
				$this->pay_user_email = $user_email;
			}
			
			
			//mpesa daraja setters
			public function set_pay_MerchantRequestID($mpesa_merchant_id)
    		{
    		    $this->mpesa_merchant_id = $mpesa_merchant_id;
    		}
    
    		public function set_pay_CheckoutRequestID($mpesa_checkout_id)
    		{
    		    $this->mpesa_checkout_id = $mpesa_checkout_id;
    		}
    
    		public function set_pay_amount($mpesa_pay)
    		{
    		    $this->mpesa_pay = $mpesa_pay;
    		}
    
    		public function set_pay_name($mpesa_pay_name)
    		{
    		    $this->mpesa_pay_name = $mpesa_pay_name;
    		}
    
            public function set_pay_receipt($mpesa_receipt)
    		{
    		    $this->mpesa_receipt = $mpesa_receipt;
    		}
    
    		public function set_pay_date($mpesa_pay_date)
    		{
    		    $this->mpesa_pay_date = $mpesa_pay_date;
    		}

    		public function set_payment_date($pay_date)
    		{
    		    $this->pay_date = $pay_date;
    		}

    		public function set_payment_time($pay_time)
    		{
    		    $this->pay_time = $pay_time;
    		}
    
    		public function set_pay_phone($mpesa_phone)
    		{
    		    $this->mpesa_phone = $mpesa_phone;
    		}

    		public function set_arrival_date($arrival_date)
    		{
    		    $this->arrival_date = $arrival_date;
    		}

    		public function set_arrival_time($arrival_time)
    		{
    		    $this->arrival_time = $arrival_time;
			}
			
			public function set_ending_time($ending_time)
    		{
    		    $this->ending_time = $ending_time;
    		}


            public function get_mpesa_token()
    		{
    		    $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
      
    			$curl = curl_init();
    			curl_setopt($curl, CURLOPT_URL, $url);
    			$credentials = base64_encode('nMU1jskB2rKlUbzqbza0iRBsydGcrxFs:l6gsWcMAdxw33ojQ');
    			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
    			/*curl_setopt($curl, CURLOPT_HEADER, false);*/
    			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    
    			$curl_response = curl_exec($curl);
                
                  return json_decode($curl_response,true)['access_token'];
    		}
    		
    		public function LNMP_Query($check)
    		{
    		     $url = 'https://api.safaricom.co.ke/mpesa/stkpushquery/v1/query';
    		     
    		     $t=time();
                 $code = "336729";
      			$passkey = "eaadafd19826867d385fee6f3b2c999f968096c954431d5d32a40059b3c76110";
                  $stamp = date("YmdHis",$t);
                  $enco = $code.$passkey.$stamp;
                    $pass = base64_encode($enco);
      
                  $curl = curl_init();
                  curl_setopt($curl, CURLOPT_URL, $url);
                  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$this->get_mpesa_token())); //setting custom header
                  
                  
                  $curl_post_data = array(
                    //Fill in the request parameters with valid values
                    'BusinessShortCode' =>$code,
                    'Password' =>$pass,
                    'Timestamp' =>$stamp,
                    'CheckoutRequestID' =>$check
                  );
                  
                  $data_string = json_encode($curl_post_data);
                  
                  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($curl, CURLOPT_POST, true);
                  curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
                  
                 return $curl_response = curl_exec($curl);
                 
    		}
    
    		public function insert_to_mpesa_table()
    		{
    			$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE Receipt='$this->mpesa_receipt'");
    
    			if($sql->rowCount() > 0 ) {
    
    				$this->stmt = $this->dbase->prepare("UPDATE $this->mpesaTable SET MerchantRequestID=?,CheckoutRequestID=?,Receipt=?,Phone=?,Amount=?,TransactionDate=? WHERE Receipt=?");
    
    		            $this->stmt->bindParam(1,$this->mpesa_merchant_id);
    		            $this->stmt->bindParam(2,$this->mpesa_checkout_id);
    					$this->stmt->bindParam(3,$this->mpesa_receipt);
    					$this->stmt->bindParam(4,$this->mpesa_phone);
    					$this->stmt->bindParam(5,$this->mpesa_pay);
    					$this->stmt->bindParam(6,$this->mpesa_pay_date);
    					$this->stmt->bindParam(7,$this->mpesa_receipt);
    					$this->stmt->execute();
    
    					if($this->stmt) {
    						return true;
    					} else {
    						return false;
    					}
    
    			} else {
    
    					$this->stmt = $this->dbase->prepare("INSERT INTO $this->mpesaTable(MerchantRequestID,CheckoutRequestID,Receipt,Phone,Amount,TransactionDate)
    					VALUES(?,?,?,?,?,?)");
    
    		            $this->stmt->bindParam(1,$this->mpesa_merchant_id);
    		            $this->stmt->bindParam(2,$this->mpesa_checkout_id);
    					$this->stmt->bindParam(3,$this->mpesa_receipt);
    					$this->stmt->bindParam(4,$this->mpesa_phone);
    					$this->stmt->bindParam(5,$this->mpesa_pay);
    					$this->stmt->bindParam(6,$this->mpesa_pay_date);
    					$this->stmt->execute();
    
    					if($this->stmt) {
    						return true;
    					} else {
    						return false;
    					}
    
    			}
    		}
    
    		public function insert_to_mpesa_errors_table($mpesa_merchant_id,$mpesa_checkout_id,$mpesa_result_code,$mpesa_result_desc)
    		{
    			$this->stmt = $this->dbase->prepare("INSERT INTO $this->mpesaErrorsTable(MerchantRequestID,CheckoutRequestID,ResultCode,ResultDesc)
    			VALUES(?,?,?,?)");
    
                $this->stmt->bindParam(1,$mpesa_merchant_id);
                $this->stmt->bindParam(2,$mpesa_checkout_id);
    			$this->stmt->bindParam(3,$mpesa_result_code);
    			$this->stmt->bindParam(4,$mpesa_result_desc);
    			$this->stmt->execute();
    
    			if($this->stmt) {
    				return true;
    			} else {
    				return false;
    			}
    		}
    
    		public function is_error_available($mpesa_merchant_id)
    		{
    		    $sql = $this->dbase->query("SELECT * FROM $this->mpesaErrorsTable WHERE MerchantRequestID='$mpesa_merchant_id'");
    
    			if($sql->rowCount() > 0 ) {
    
    			        foreach($sql as $row) {
    			            $this->mpesa_error_desc = $row["ResultDesc"];
    			        }
    
    			        return true;
    
    			} else {
    			    return false;
    			}
    		}
    
    		public function insert_to_mpesa_c2b_table()
    		{
    			$sql = $this->dbase->query("SELECT * FROM $this->mpesaC2BTable WHERE Receipt='$this->mpesa_receipt'");
    
    			if($sql->rowCount() > 0 ) {
    
    				$this->stmt = $this->dbase->prepare("UPDATE $this->mpesaC2BTable SET Receipt=?,Phone=?,Amount=?,Name,TransactionDate=? WHERE Receipt=?");
    
    					$this->stmt->bindParam(1,$this->mpesa_receipt);
    					$this->stmt->bindParam(2,$this->mpesa_phone);
    					$this->stmt->bindParam(3,$this->mpesa_pay);
    					$this->stmt->bindParam(4,$this->mpesa_pay_name);
    					$this->stmt->bindParam(5,$this->mpesa_pay_date);
    					$this->stmt->bindParam(6,$this->mpesa_receipt);
    					$this->stmt->execute();
    
    					if($this->stmt) {
    						return true;
    					} else {
    						return false;
    					}
    
    			} else {
    
    					$this->stmt = $this->dbase->prepare("INSERT INTO $this->mpesaC2BTable(Receipt,Phone,Amount,Name,TransactionDate)
    					VALUES(?,?,?,?,?)");
    
    					$this->stmt->bindParam(1,$this->mpesa_receipt);
    					$this->stmt->bindParam(2,$this->mpesa_phone);
    					$this->stmt->bindParam(3,$this->mpesa_pay);
    					$this->stmt->bindParam(4,$this->mpesa_pay_name);
    					$this->stmt->bindParam(5,$this->mpesa_pay_date);
    					$this->stmt->execute();
    
    					if($this->stmt) {
    						return true;
    					} else {
    						return false;
    					}
    
    			}
    		}
    		
    	public function fetch_c2b_details($mpesa_code)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->mpesaC2BTable WHERE Receipt='$mpesa_code' ");

			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {

							$this->fetched_c2b_Receipt = $row["Receipt"];
							$this->fetched_c2b_Amount = $row["Amount"];
							$this->fetched_c2b_TransactionDate = $row["TransactionDate"];

				}

			} else {

				return false;

			}

		}

		public function fetch_stk_details($MerchantId)
		{

			$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE MerchantRequestID='$MerchantId' ");

			if($sql->rowCount() > 0 ) {

				foreach($sql as $row) {

							$this->fetched_stk_Receipt = $row["Receipt"];
							$this->fetched_stk_Amount = $row["Amount"];

				}

			} else {

				return false;

			}

		}

		public function is_payment_done($MerchantId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE MerchantRequestID='$MerchantId'");

			if($sql->rowCount() > 0 ) {

    			$sql2 = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE TransactionReference='$this->fetched_stk_Receipt'");

    			if($sql2->rowCount() > 0 ) {

    			    $this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET TransactionReference=?,PaymentId=?,ReserveId=?,OwnerId=?,PayerPhone=?,
    			                                        PayerName=?,PayerEmail=?,AmountPaid=?,TableCapacity=?,AccDeposit=?,ArrivalDate=?,ArrivalTime=?,PayDate=?,PayTime=? WHERE TransactionReference=? ");

                    $this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_rs_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->table_capacity);
					$this->stmt->bindParam(10,$this->acc_deposit);
					$this->stmt->bindParam(11,$this->arrival_date);
					$this->stmt->bindParam(12,$this->arrival_time);
					$this->stmt->bindParam(13,$this->pay_date);
					$this->stmt->bindParam(14,$this->pay_time);
					$this->stmt->bindParam(15,$this->transaction_ref);
        			$this->stmt->execute();

        			if($this->stmt) {
        				return true;
        			} else {
        				return false;
        			}

    			} else {

			        $this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->paymentTable(TransactionReference,PaymentId,ReserveId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,TableCapacity,AccDeposit,ArrivalDate,ArrivalTime,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_rs_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->table_capacity);
					$this->stmt->bindParam(10,$this->acc_deposit);
					$this->stmt->bindParam(11,$this->arrival_date);
					$this->stmt->bindParam(12,$this->arrival_time);
					$this->stmt->bindParam(13,$this->pay_date);
					$this->stmt->bindParam(14,$this->pay_time);

					if($this->stmt->execute()) {
						return true;
					} else {
						return false;
					}

    			}

			} else {
			    return false;
			}
		}

		public function is_pay_later_done()
		{

			        $this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->payLaterTable(TransactionReference,PaymentId,ReserveId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,TableCapacity,AccDeposit,ArrivalDate,ArrivalTime,EndingTime,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_rs_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->table_capacity);
					$this->stmt->bindParam(10,$this->acc_deposit);
					$this->stmt->bindParam(11,$this->arrival_date);
					$this->stmt->bindParam(12,$this->arrival_time);
					$this->stmt->bindParam(13,$this->ending_time);
					$this->stmt->bindParam(14,$this->pay_date);
					$this->stmt->bindParam(15,$this->pay_time);

					if($this->stmt->execute()) {
						return true;
					} else {
						return false;
					}

    		
		}
		
		public function is_drink_payment_done($MerchantId)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE MerchantRequestID='$MerchantId'");

			if($sql->rowCount() > 0 ) {

    			$sql2 = $this->dbase->query("SELECT * FROM $this->drinksPaymentTable WHERE TransactionReference='$this->fetched_stk_Receipt'");

    			if($sql2->rowCount() > 0 ) {

    			    $this->stmt = $this->dbase->prepare("UPDATE $this->drinksPaymentTable SET TransactionReference=?,PaymentId=?,DrinksId=?,OwnerId=?,PayerPhone=?,
    			                                        PayerName=?,PayerEmail=?,AmountPaid=?,AccDeposit=?,PayDate=?,PayTime=? WHERE TransactionReference=? ");

                    $this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_drk_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->acc_deposit);
					$this->stmt->bindParam(10,$this->pay_date);
					$this->stmt->bindParam(11,$this->pay_time);
					$this->stmt->bindParam(12,$this->transaction_ref);
        			$this->stmt->execute();

        			if($this->stmt) {
        				return true;
        			} else {
        				return false;
        			}

    			} else {

			        $this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->drinksPaymentTable(TransactionReference,PaymentId,DrinksId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,AccDeposit,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,?,?)");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_drk_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->acc_deposit);
					$this->stmt->bindParam(10,$this->pay_date);
					$this->stmt->bindParam(11,$this->pay_time);

					if($this->stmt->execute()) {
						return true;
					} else {
						return false;
					}

    			}

			} else {
			    return false;
			}
		}

		public function is_c2b_payment_done($MpesaCode)
		{
			$sql = $this->dbase->query("SELECT * FROM $this->mpesaC2BTable WHERE Receipt='$MpesaCode'");

			if($sql->rowCount() > 0 ) {

			        //update if already saved
    			$sql2 = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE MerchantRequestID='$MpesaCode'");

    			if($sql2->rowCount() > 0 ) {


        			return false;

    			} else {

			         $this->stmt = $this->dbase->prepare("INSERT INTO $this->paymentTable(MerchantRequestID,SongId,ArtistId,Phone,SessionStatus,PayDate)
			                                        VALUES(?,?,?,?,?,?)");

                    $this->stmt->bindParam(1,$MpesaCode);
                    $this->stmt->bindParam(2,$this->mv_id);
        			$this->stmt->bindParam(3,$this->artist_id);
        			$this->stmt->bindParam(4,$this->mpesa_phone);
        			$this->stmt->bindParam(5,$this->session_status);
        			$this->stmt->bindParam(6,$this->now_date);
        			$this->stmt->execute();

        			if($this->stmt) {
        				return true;
        			} else {
        				return false;
        			}

    			}

			} else {
			    return false;
			}
		}
		
		public function startsWith($string, $startString)
        {
            $len = strlen($startString);
            return (substr($string, 0, $len) === $startString);
        }

		public function has_user_paid()
		{
		    $sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE Phone='$this->mpesa_phone' AND SongId='$this->mv_id' ORDER BY PayDate ASC");

			if($sql->rowCount() > 0 ) {

			         return $sql;

			} else {
			    return false;
			}
		}

			
			public function insert_into_drinks_payment_table($transaction_ref,$sender_phone)
			{

				/*$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' AND SenderPhone='$sender_phone' ");

				if($sql->rowCount() > 0 ) {*/


					$this->stmt = $this->dbase->prepare(
									"INSERT INTO $this->drinksPaymentTable(TransactionReference,PaymentId,DrinksId,OwnerId,PayerPhone,PayerName,PayerEmail,AmountPaid,AccDeposit,PayDate,PayTime) 
									VALUES(?,?,?,?,?,?,?,?,?,now(),now())");

					$this->stmt->bindParam(1,$this->transaction_ref);
					$this->stmt->bindParam(2,$this->pay_id);
					$this->stmt->bindParam(3,$this->pay_drk_id);
					$this->stmt->bindParam(4,$this->pay_owner_id);
					$this->stmt->bindParam(5,$this->sender_phone);
					$this->stmt->bindParam(6,$this->full_name);
					$this->stmt->bindParam(7,$this->pay_user_email);
					$this->stmt->bindParam(8,$this->amount);
					$this->stmt->bindParam(9,$this->acc_deposit);

					if($this->stmt->execute()) {

						//echo "<script> alert('Successful pay'); </script>";
						return true;
					} else {
						//echo "<script> alert('Successful pay'); </script>";
						return false;
					}

			/*	} else {
					return false;
					echo "<script> alert('Incorrect Transaction Number or Phone Number or Both'); </script>";
				}*/

			}

			public function fetch_specific_mpesa_details($transaction_ref)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->mpesaTable WHERE TransactionReference='$transaction_ref' ");

				return $sql;

			}
			
			public function fetch_specific_reserve_mpesa_details($reserve_id)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$reserve_id' ");

				return $sql;

			}

			public function get_total_reserve_amount_per_payer($rs_id,$payer_phone)
			{

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$rs_id' AND PayerPhone='$payer_phone' ");

				return $sql;

			}

			public function insert_to_balance_table($rs_id,$user_phone,$balance)
			{

					$sql = $this->dbase->query("SELECT * FROM $this->balanceTable WHERE ReserveId='$rs_id' AND PayerPhone='$user_phone' ");

					$date = date("Y-d-m");
					$time = date("hh:mm:s");

					//if($balance != 0 ) // if the balance status of the buyer is 0 for any reserve, delete that record 

						if($sql->rowCount() > 0 ) {


								$this->stmt = $this->dbase->prepare(
												"UPDATE $this->balanceTable SET ReserveId=? ,PayerPhone=?,AmountBalance=?,UpdateDate=now(),UpdateTime=now() WHERE ReserveId=? AND PayerPhone=? ");
								
								$this->stmt->bindParam(1,$rs_id);
								$this->stmt->bindParam(2,$user_phone);
								$this->stmt->bindParam(3,$balance);
								$this->stmt->bindParam(4,$rs_id);
								$this->stmt->bindParam(5,$user_phone);

								if($this->stmt->execute()) {
									return true;
								} else {
									return false;
								}

						} else {

							$this->stmt = $this->dbase->prepare("INSERT INTO $this->balanceTable(ReserveId,PayerPhone,AmountBalance,UpdateDate,UpdateTime) 
																VALUES(?,?,?,now(),now())");
							$this->stmt->bindParam(1,$rs_id);
							$this->stmt->bindParam(2,$user_phone);
							$this->stmt->bindParam(3,$balance);

							if($this->stmt->execute()) {
								return true;
							} else {
								return false;
							}

						}

			}


			public function fetch_by_payer_from_payment_table($payer_phone,$ReserveId)
			{


				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE PayerPhone='$payer_phone' AND ReserveId='$ReserveId' ");

				if($sql->rowCount() > 0 ) {
					
					foreach($sql as $row) {

						$this->fetched_bal_reserve_id = $row["ReserveId"];
						$this->fetched_bal_payer_phone = $row["PayerPhone"];
						$this->fetched_bal_table_capacity = $row["TableCapacity"];

						return true;

					}
					
				} else {
					return false;
				}

			}

			public function fetch_user_payment_details($P_Phone,$ReserveId)
			{

				$sql = $this->dbase->query("SELECT DISTINCT ReserveId,PayerPhone,PayerEmail FROM $this->paymentTable WHERE PayerPhone='$P_Phone' AND ReserveId='$ReserveId' ");

				foreach($sql as $row) {

					return $row["PayerEmail"];

				}

			}


			public function fetch_reserve_payment_details($OwnerId)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,a.PayerPhone,a.PayerName,b.ReserveId,b.ReserveName 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND b.OwnerId='$OwnerId' AND a.AccDeposit='0' ");

				return $sql;

			}

			public function fetch_all_reserve_payment_details()
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.ReserveId,b.ReserveName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c WHERE a.ReserveId=b.ReserveId AND b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_specific_reserve_all_details($reserve_id)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.AccDeposit,a.TransactionReference,
					a.PayerPhone,a.PayerName,b.OwnerId,b.ReserveId,b.Category,b.ReserveLocation,b.ReservePrice,b.PicPath,b.StartDate,b.StartTime,b.StopTime,b.StopDate,b.ReserveName,c.UserId,c.Username,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c WHERE a.ReserveId=b.ReserveId AND b.ReserveId='$reserve_id' AND b.OwnerId=c.UserId ");

				return $sql;

			}

			public function fetch_reserve_payment_details_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT a.ReserveId,a.AmountPaid,a.TransactionReference,a.PayerPhone,a.PayerName,b.Category,b.ReserveId,b.ReserveName 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND b.OwnerId='$OwnerId' AND b.Category='$Category' ");

				return $sql;

			}

			public function count_reserves_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable WHERE OwnerId='$OwnerId' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReserves"];
					}
				} else {
					return "0";
				}

			}
			
			public function count_total_reserved_tables_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(DISTINCT c.ReserveId) as TotalReservations FROM $this->paymentTable b,$this->receiptTable c 
				                            WHERE b.ReserveId=c.ReserveId AND  b.OwnerId='$OwnerId' AND b.AccDeposit='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReservations"];
					}
				} else {
					return "0";
				}

			}
			
			public function count_table_reservations_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT Count(DISTINCT c.TicketId) as TotalReservations FROM $this->paymentTable b,$this->receiptTable c 
				                            WHERE b.PaymentId=c.PaymentId AND  b.OwnerId='$OwnerId' AND c.TicketStatus='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalReservations"];
					}
				} else {
					return "0";
				}

			}

			public function sum_of_reserves_total_by_owner($OwnerId)
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->paymentTable WHERE OwnerId='$OwnerId' AND AccDeposit='0' ");

				if($sql->rowCount() > 0 ) {
					foreach($sql as $row) {
						return $row["TotalAmount"];
					}
				} else {
					return "0";
				}

			}

			public function sum_of_specific_reserves_total($ReserveId)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT * FROM $this->paymentTable WHERE ReserveId='$ReserveId' AND AccDeposit<2 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function count_all_reserves()
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable  ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function sum_of_all_reserves_total()
			{

				$sql = $this->dbase->query("SELECT SUM(AmountPaid) as TotalAmount FROM $this->paymentTable WHERE AccDeposit<2 ");

				foreach($sql as $row) {
					return $row["TotalAmount"];
				}

			}

			public function count_reserves_by_owner_cat($OwnerId,$Category)
			{

				$sql = $this->dbase->query("SELECT Count(*) as TotalReserves FROM $this->reserveTable WHERE OwnerId='$OwnerId' AND Category='$Category' ");

				foreach($sql as $row) {
					return $row["TotalReserves"];
				}

			}

			public function sum_of_reserves_total_by_owner_cat($OwnerId,$Category)
			{

				$total = 0;

				$sql = $this->dbase->query("SELECT a.OwnerId,a.ReserveId,a.AccDeposit,b.ReserveId,b.Category,a.AmountPaid FROM $this->paymentTable a,$this->reserveTable b WHERE b.Category='$Category' 
					AND a.ReserveId=b.ReserveId AND a.OwnerId='$OwnerId' AND  a.AccDeposit=1 ");

				foreach($sql as $row) {
					 $total = $total + $row["AmountPaid"];
				}

				return $total;

			}

			public function fetch_of_specific_reserves_payment_details($ReserveId)
			{

				$sql = $this->dbase->query("SELECT DISTINCT a.ReserveId,a.AmountPaid,b.ReserveId,b.ReserveName,a.AccDeposit 
					FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND a.ReserveId='$ReserveId' 
					AND a.AccDeposit='0'");

				foreach($sql as $row) {
					$this->rs_name = $row["ReserveName"];
					$this->rs_id = $row["ReserveId"];
					 //$total = $total + $row["AmountPaid"];
				}

				//return $total;

			}
			
			public function fetch_specific_reserves_table_total_amount($ReserveId)
			{

				$sql = $this->dbase->query("SELECT SUM(a.AmountPaid) as total_amount FROM $this->paymentTable a,$this->reserveTable b WHERE a.ReserveId=b.ReserveId AND a.ReserveId='$ReserveId' 
					AND a.AccDeposit='0'");

				foreach($sql as $row) {
					$this->rs_total_amount = $row["total_amount"];
				}

				//return $total;

			}

			public function update_pay_withdrawal_status()
			{
				date_default_timezone_set('Africa/Nairobi');
			
				$this->pay_now_date_only = date("Y-m-d");
				
				$this->pay_now_time_only = date("H:i:s");

				$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET AccDeposit='1',PayDate=?,PayTime=? WHERE AccDeposit='0'");

				$this->stmt->bindParam(1,$this->pay_now_date_only);
				$this->stmt->bindParam(2,$this->pay_now_time_only);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function update_cashout_withdrawal_status($ReserveId)
			{

				$this->stmt = $this->dbase->prepare("UPDATE $this->paymentTable SET AccDeposit='2',PayDate=now(),PayTime=now() WHERE ReserveId=? AND AccDeposit='1'");

				$this->stmt->bindParam(1,$ReserveId);

				if($this->stmt->execute()) {
					return true;
				} else {
					return false;
				}

			}

			public function fetch_withdrawal_status_checkout()
			{

				$sql = $this->dbase->query("SELECT DISTINCT a.ReserveId,a.OwnerId,a.PayDate,a.PayTime,b.ReserveId,b.ReserveName,a.AccDeposit,
					c.UserId,c.Username,c.Email,c.PhoneNo 
					FROM $this->paymentTable a,$this->reserveTable b,$this->regTable c 
					WHERE a.ReserveId=b.ReserveId AND a.OwnerId=c.UserId AND a.AccDeposit='1'");

				return $sql;

			}
			
			public function fetch_total_tables_booked($reserveId)
			{
				
				$sql = $this->dbase->query("SELECT SUM(TableCapacity) as TotalBookedTables FROM $this->paymentTable WHERE ReserveId='$reserveId' ");
				
				foreach($sql as $row){
					return $row['TotalBookedTables'];
				}
				
			}


	}

	$payment = new paymentAPI();


?>