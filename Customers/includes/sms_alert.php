<?php

require_once("initialise.php");

use AfricasTalking\SDK\AfricasTalking;

class sms_alert {
    
    public $to_number,$message,$sms,$result;
   public $username = 'Pop-In'; // use 'sandbox' for development in the test environment
   public $apiKey   = 'c5df807ffff3074d5b9f3aa746fe1e45b6215738d1bd18afbe3226dc92bfeee0'; // use your sandbox app API key for development in the test environment
    public $AT;

    public function __construct()
    {
        $this->AT  = new AfricasTalking($this->username, $this->apiKey);
        // Get one of the services
        $this->sms      = $this->AT->sms();
    }
    
    
    public function set_number($number)
    {
        
        $this->to_number = $number;
        
    }
    
    public function set_message($message)
    {
        $this->message = $message;
    }
    
    public function send_sms()
    {
        
        // Use the service
            $this->result   = $this->sms->send([
                'to'      => $this->to_number,
                'from' => 'Pop-In',
                'message' => $this->message.' Your ultimate partying companion!'
            ]);
            
            $decoded = json_decode(json_encode($this->result));


            //$decoded = '{"status":"success","data":{"SMSMessageData":{"Message":"Sent to 1\/1 Total Cost: KES 0.8000",
            //				"Recipients":[{"cost":"KES 0.8000","messageId":"ATXid_080677696d56aa3be585d2a02cdc755b","messageParts":1,"number":"+254737893694","status":"Success","statusCode":101}]}}}';
            
            
            //$stat = $decoded->status;
            
            if($decoded->data->SMSMessageData->Recipients[0]->status == "Success") {
            
            	//echo $decoded->data->SMSMessageData->Recipients[0]->messageId;
            	
            	return true;
            
            } else {
            
            	//echo "Not successful";
            	return false;
            
            }
    
    
    }
        
}



$sms_alert = new sms_alert();

?>