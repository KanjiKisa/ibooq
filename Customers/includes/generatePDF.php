<?php

require_once("initialise.php");

	class generatePDF extends FPDF
	{

		/*public function __construct($orientation = 'P', $unit = 'pt', $format = 'A4', $margin= 40)
		{

			$this->FDF($orientation,$unit,$format);
			$this->SetTopMargin($margin);
			$this->SetLeftMargin($margin);
			$this->SetRightMarging($margin);
			$this->SetAutoPageBreak(true,$margin);

		}
*/

		//$this->get_total_meetings();

		/*public function set_ticket_poster_pic($url) 
		{

			$this->Image('Customers/Front End/'.$url,60,23,80,50);

		}*/

		public function Header()
		{

			$this->AddFont('Calligrapher','','calligra.php');
			$this->Image('Front End/Images/PopinSlideGreen.png',60,23,80,50);
			$this->SetFont('Calligrapher','',20);
			$this->SetFillColor(54,54,51);
			$this->SetTextColor(255);
			$this->SetX(0);
			$this->Cell(220,10,"POP-IN",0,1,'C',true);
			$this->Ln(20);

		}
		
		

		public function create_ticket($ticket_id,$ticket_capacity,$ev_category,$ev_name,$ev_location,$paid,$overpay,$name,$phone,$email)
		{
		    $db = new database();
		    
		    $db->fetch_reservation_user_reg_info($_SESSION['owner']['id']);
		    
		    $this->SetMargins(10,10,10);
			$this->AddPage();

			$this->AddFont('Calligrapher','','calligra.php');

			// addresses
			$this->Ln(20);
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);


			$this->Ln();
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->username,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->SetFillColor(255);
			$this->Cell(190,10,$db->city.", ".$db->country,0,"","L",true);

			$this->Ln(20);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(190,10,"BILLED TO",0,0,"L",true);
			
			$this->Ln(10);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Name of buyer",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$name,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Phone Number ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$phone,0,"","L",true);

			$this->Ln();
			$this->setFont("Arial","I",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,10,"Email Address ",0,"","L",true);
			$this->setFont("Arial","",12);
			$this->SetTextColor(0);
			$this->Cell(143,10,$email,0,"","L",true);

			$this->Ln(20);
			$this->setFont("Arial","",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(49,8,"Category",0,0,"C",true);
			$this->Cell(47,8,"Price",0,0,"C",true);
			$this->Cell(47,8,"Capacity",0,0,"C",true);
			$this->Cell(47,8,"Total",0,0,"C",true);



				# code...
				$this->Ln();
				$this->setFont("Arial","",12);
				$this->SetTextColor(0);
				$this->SetFillColor(255);
				$this->Cell(49,15,$ev_category,0,0,"C",true);
				$this->Cell(47,15,$paid,0,0,"C",true);
				$this->Cell(47,15,$ticket_capacity,0,0,"C",true);
				$this->Cell(47,15,$paid*$ticket_capacity,0,0,"C",true);

			$this->Ln(25);
			$this->SetX(107);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(225,153,0);
			$this->SetFillColor(255);
			$this->Cell(47,8,"Total",0,0,"C",true);
			$this->SetTextColor(0);
			$this->Cell(47,8,$paid*$ticket_capacity,0,"","C",true);

			$this->SetAutoPageBreak(true,2);

		}
		
		public function create_receipt($receipt_id)
		{
		    $db = new database();
		    
		    $db->fetch_reservation_user_reg_info($_SESSION['owner']['id']);

			$this->SetMargins(10,10,10);
			$this->AddPage();

			/*$this->Ln();
			$this->setFont("Courier","B",15);
			$this->Cell(40,7,"Invoice #",0,"","L");
			$this->setFont("Arial","",12);
			$this->Cell(40,7,uniqid(),0,"","L");

			$this->Ln();
			$this->setFont("Courier","B",15);
			$this->Cell(40,7,"Ticket Date",0,"","L");
			$this->setFont("Arial","",12);
			$this->Cell(40,7,date("Y-m-d"),0,"","L");*/

			$this->AddFont('Calligrapher','','calligra.php');

			// addresses
			$this->Ln(40);
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(255);
			$this->SetFillColor(150,152,15);
			$this->Cell(60,20,"Receipt ID : ".$receipt_id,1,0,"L",true);
			
			//$this->Image('Customers/Front End/'.$url,60,23,80,50);
			$this->setFont("Arial","B",12);
			$this->SetTextColor(255);
			$this->SetFillColor(150,152,15);
			$this->Cell(60,20,$db->username,1,0,"L",true);
			
			$this->setFont("Arial","B",12);
			$this->SetTextColor(255);
			$this->SetFillColor(150,152,15);
			$this->Cell(60,20,$db->city.", ".$db->country,1,0,"L",true);
			
			$this->Ln(30);
			$this->SetTextColor(0);
			$this->setFont("Courier","B",12);
			$this->Cell(60,8,"Category",1,0,"C");
			$this->Cell(35,8,"Name",1,0,"C");
			$this->Cell(35,8,"Quantity",1,0,"C");
			$this->Cell(35,8,"Price",1,0,"C");
			

			$query = $db->fetch_bought_drinks(); 
			

            foreach($query as $row) {
                
				# code...
				$this->Ln();
				$this->setFont("Arial","",12);

				$this->Cell(60,15,$row['Category'],1,0,"C");
				$this->Cell(35,15,$row['DrinkName'],1,0,"C");
				$this->Cell(35,15,$_SESSION['carts'][$row['DrinkId']]['quantity'],1,0,"C");
				$this->Cell(35,15,$row['DrinkPrice'],1,0,"C");
			}
			
			$this->Ln(25);
			$this->SetX(110);
			$this->Cell(50,8,"Total",1,0,"C");
			$this->Cell(50,8,$_SESSION['total']['amount'],1,"","C");

			$this->Ln(30);

			$this->setFont("Courier","B",12);
			$this->Write(5,"Keep your contact number online for efficient delivery.");
			$this->Ln();
			$this->Write(5,"If delivery is not made within 2 days of purchase, contact the admin on ".$db->phone_no);

			$this->SetAutoPageBreak(true,2);


		}

		public function Footer()
		{

			$this->SetFont('Arial','',20);
			$this->SetTextColor(255);
			$this->SetFillColor(54,54,51);
			$this->SetXY(10,-15);
			$this->Cell(0,15,"www.pop-in.com",'T',0,'C',true);

		}

		/*public function print_minutes()
		{

			
		}*/

	}

	$pdf = new generatePDF('P','mm','A4');

?>