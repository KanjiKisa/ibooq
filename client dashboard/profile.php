<?php

    require_once("../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 
      
      		  if(isset($_POST["submit_club_pics"])) {
			  
				  $upload->set_photo_name($_FILES["file_upload_profile_pic"]["name"]);
                  $upload->set_file_type($_FILES["file_upload_profile_pic"]["type"]);
                  $upload->set_tmp_loc($_FILES["file_upload_profile_pic"]["tmp_name"]);
                  $upload->set_file_error($_FILES["file_upload_profile_pic"]["error"]);
				  
				  $upload->set_cover_photo_name($_FILES["file_upload_cover_pic"]["name"]);
                  $upload->set_cover_file_type($_FILES["file_upload_cover_pic"]["type"]);
                  $upload->set_cover_tmp_loc($_FILES["file_upload_cover_pic"]["tmp_name"]);
                  $upload->set_cover_file_error($_FILES["file_upload_cover_pic"]["error"]);
                  
                  if($upload->validate_image() && $upload->validate_cover_image()) {
			  
					$database->set_profile_pic($upload->Object_path);
					$database->set_cover_pic($upload->Object_cover_path);
					
					if($database->update_user_pics($database->session_user_id)) {
						$alert->message("Profile and Cover pic updated","Success");
					} else {
						$alert->message("Profile and Cover pic failed to update","Fail");
					}
					
				  }
			  
		  }
		  
		 if(isset($_POST["submit_club_time"])) {
			  
				$open_days = $_POST["club_open_days"];
				$open_time = $_POST["club_open_time"];
				$close_time = $_POST["club_close_time"];
			  
				$database->set_open_days($open_days);
				$database->set_open_time($open_time);
				$database->set_close_time($close_time);
				
				if($database->update_club_time($database->session_user_id))
				{
					$alert->message("Time Status Updated","Success");					
				} else {
					$alert->message("Time Status Failed to update","Fail");
				}
			  
		  }
		  
		  if(isset($_POST["submit_happy_hour"])) {
			  
				$happy_hour_start = $_POST["happy_hour_start"];
				$happy_hour_stop = $_POST["happy_hour_stop"];
			  
				$database->set_happy_hour_start($happy_hour_start);
				$database->set_happy_hour_stop($happy_hour_stop);
				
				if($database->update_happy_hour_time($database->session_user_id))
				{
					$alert->message("Happy hour time status updated","Success");					
				} else {
					$alert->message("Happy hour time status failed to update","Fail");
				}
			  
		  }
      
    } else {

        header("Location: ../index.php");
  
    }
      
?>
      
      
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>iBooq</title>
    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 


  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<style>
  .upload-pic{
    border: 1px solid #ddd;
    border-style: dashed;
    padding: 1rem 1rem;
  }
</style>

<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebarBO" >

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item ">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Reservations List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome Back</span>
                  
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Settings
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Activity Log
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item text-white" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                  </a>
                </div>
              </li>
          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">Update your profile</h1>
            </div>
            <a href="create-service.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>Create Service</a>
          </div>


          <!-- Ticket list table -->
        <form action="" method="POST" enctype="multipart/form-data">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group upload-pic">
                <label for="exampleFormControlFile1">The best profile pic is a 1500 x 2100 dimension.</label>
                <input type="file" class="form-control-file" name="file_upload_profile_pic">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group upload-pic">
                <label for="exampleFormControlFile1">The best profile pic is a 2100 * 800 dimension.</label>
                <input type="file" class="form-control-file" name="file_upload_cover_pic">
              </div>
            </div>

          </div>
          <input type="submit" class="btn btn-primary" value="Update" name="submit_club_pics">
        </form>
        
        <form id="uploadProfileForm" action="" method="POST" enctype="multipart/form-data">
                          <div class="form-row">
                            <div class="col-md-4 col-lg-4">
                              <label id="club_open_days" class="form-text">Open Days</label>
                              <select name="club_open_days" class="form-control form-control-lg">
                                  <option>Everyday</option>
                                  <option>Weekdays</option>
                                  <option>Weekends</option>
        						  <option>Monday</option>
                                  <option>Tuesday</option>
                                  <option>Wednesday</option>
        						  <option>Thursday</option>
                                  <option>Friday</option>
                                  <option>Saturday</option>
        						  <option>Sunday</option>
                              </select>
                            </div>
                            <div class="col-md-4 col-lg-4">
                              <label id="club_close_time" class="form-text">Opening Time</label>
                              <input class="form-control form-control-lg" type="time" name="club_open_time" placeholder="Enter Open Time" required>
                            </div>
                            <div class="col-md-4 col-lg-4">
                              <label id="club_close_time" class="form-text">Closing Time</label>
                              <input class="form-control form-control-lg" type="time" name="club_close_time" placeholder="Enter Close Time" required>
                            </div>
                          </div><br>
                          <input type="submit" value="Update" name="submit_club_time" class="btn btn-primary">
        </form>

<!--        <form class="mt-5" action="" method="POST">
          <h6>Upload Happy Hour details</h6>
          <div class="row">
            <div class="form-group col-md-6">
                 <label>Happy hour start time</label>
                 <input type="time" class="form-control" name="happy_hour_start"> 
            </div>

            <div class="form-group col-md-6">
                 <label>Happy hour end time</label>
                 <input type="time" class="form-control" name="happy_hour_stop"> 
            </div>
          </div>
          <input type="submit" class="btn btn-primary" name="submit_happy_hour">
        </form>-->
          <!----end of ticket list table-->

         

           
              
              

            
              

              

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <!-- Modal: modalPoll -->
<div class="modal fade right" id="modalPoll-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Feedback request
        </p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
          <p>
            <strong>Your opinion matters</strong>
          </p>
          <p>Have some ideas how to improve our product?
            <strong>Give us your feedback.</strong>
          </p>
        </div>

        <hr>

        <!-- Radio -->
        <p class="text-center">
          <strong>Your rating</strong>
        </p>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-179" value="option1" checked>
          <label class="form-check-label" for="radio-179">Very good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-279" value="option2">
          <label class="form-check-label" for="radio-279">Good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-379" value="option3">
          <label class="form-check-label" for="radio-379">Mediocre</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-479" value="option4">
          <label class="form-check-label" for="radio-479">Bad</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-579" value="option5">
          <label class="form-check-label" for="radio-579">Very bad</label>
        </div>
        <!-- Radio -->

        <p class="text-center">
          <strong>What could we improve?</strong>
        </p>
        <!--Basic textarea-->
        <div class="md-form">
          <textarea type="text" id="form79textarea" class="md-textarea form-control" rows="3"></textarea>
          <label for="form79textarea">Your message</label>
        </div>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-primary waves-effect waves-light">Send
          <i class="fa fa-paper-plane ml-1"></i>
        </a>
        <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cancel</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->

</body>

</html>
