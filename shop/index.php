<?php 
    session_start(); 
    require_once("../Customers/includes/initialise.php");

   if(isset($_REQUEST['drk'])) {
       $owner_id = $_REQUEST['drk'];
       $_SESSION['owner']['id'] = $_REQUEST['drk'];
   } else {
       $owner_id =  $_SESSION['owner']['id'];
   }
   
    if(isset($_POST['addCart'])){ 
          
        $id=intval($_POST['DrinkId']); 
          
        if(isset($_SESSION['carts'][$id])){ 
              
            $_SESSION['carts'][$id]['quantity']++; 
              
        }else{ 
            
              
            $query_s=$database->fetch_drinks_data($id); 

            $automation->generate_cart_id();
            $cart_id = $automation->cart_id;

                foreach($query_s as $row_s){ 
                  
                $_SESSION['carts'][$row_s['DrinkId']]=array( 
                        "quantity" => 1, 
                        "cart_id" => $cart_id,
                        "drink_id" => $row_s['DrinkId'],
                        "price" => $row_s['DrinkPrice'] 
                    ); 
                    
                
                  
                  }

                  $_SESSION['c']['cid'] = $cart_id;
                 
              
        } 
          
    }
    
 
 ?>
 
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>iBooq Products</title>

    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

    <!-- Bootstrap core CSS -->
    <link href="../Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open Sans|Coda|Montserrat|Raleway|Josefin Sans|Varela Round|EB Garamond|Passion One|Archivo Black|Oswald|Coiny|Catamaran|Julius Sans One">

    <!--font awesome cdn-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!--animate css cdn-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!-- Custom styles for this template -->
    <link href="Front End/style.css" rel="stylesheet">
  </head>

  <body id="page-top-drinks">
      
      <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="Front End/img/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#projects">Service Providers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#signup">Business Owners</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="cart.php"><i class="fas fa-shopping-bag pr-2"></i>My Cart <span>[<?php echo count($_SESSION['carts']); ?> Items]</span></a>
          </li>
        </ul>
       
      </div>
    </div>
  </nav>

<!-- Navigation --
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
  <div class="container">
  <a class="navbar-brand" href="index.php"><img  src="Front End/Images/pop-in.png"></a>
    <ul class="navbar-nav ml-auto">
      <a href="cart.php"><i class="fas fa-shopping-bag fa-2x"></i>[<?php echo count($_SESSION['carts']); ?> Items]</a>
    </ul>
  </div>
</div>
</nav>--

<br><br><br>
<!--search section-->
<section id="search">
  <div class="search container">  
   <form action="" method="" id="searchForm">
        <div class="col-md-8 col-lg-8">
            
            <h6>Sample our selection of products and make your order </h6>
            <div class="form-group">
              <input type="text" id="searchReserveText" class="form-control" placeholder="Search by Name / Category / Location " name="p">
              <input id="searchReserveBtn" style=" color: #fff; " type="submit"  class="btn btn-primary mx-auto" value="Search" name="SearchReserve">
            </div>
        </div>
     </form>
</section>

<section id="drinks">
    <div class="container">
        
        <div class="row">
            
                <!--start of products-->
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <h5>ALL PRODUCTS</h5>
                                    <?php 
                          
                           $sql = $database->fetch_all_drinks_by_owner($owner_id);
                              
                            foreach($sql as $row){
                                  
                    ?>
                    <div class="drink wines-spirits d-flex justify-content-between">
                        <div class="name">
                          <strong><span class="mr-2"><?php echo $row['Category'] ?></span></strong>
                          <br/>
                            <i style="color:white;"><span class="mr-2"><?php echo $row['DrinkSize'] ?></span><?php echo $row['DrinkName'] ?></i>
                            <span class="d-block"><small>Ksh </small><?php echo $row['DrinkPrice'] ?></span>
                        </div>
                        <div class="add-to-cart">
                            <p><form action="" method="post">
                               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
                               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
                            </form></p>
                        </div>
                        
                    </div>

                    <hr/>
                    
                    
                    
                                      <?php 
                                  
                            } 
                          
                    ?>
                    
                </div>
                <!--end of products section-->
                
                
            
        </div>
        
    
        
        
        
    </div>
</section>
<!--end of search section-->

<!--drinks section--
<div class="drinks-wrapper container" id="wrapper">
  <!--row--
 <div class="row">
    <!--section one beers--


<div class="col-md-4 col-lg-4 col-xl-4">
      <section id="beer">
        <div class="product-title">
        <h5>BEER</h5>
        </div>
        <hr>

    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Beer");
              
            foreach($sql as $row){
                  
    ?> 
    
        <ul class="list">
         <li>
             <div class="drink-name">
                 <h6><i class="fas fa-beer"></i><?php echo $row['DrinkName'] ?></h6>
                 <span class="drink-size"><?php echo $row['DrinkSize'] ?></span>
                 <p><form action="" method="post">
                   <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
                   <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
                 </form></p>
             </div>
             <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
         </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?>
        
  </section>
   </div>
     
<!--end of section one--



<!--section two--
    
     
    

<div class="col-md-4 col-lg-4 col-xl-4">
    <section id="wines">
         <div class="product-title">
        <h5>WINES & SPIRITS</h5>
        </div>
        <hr>
        
    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Wines & Spirits");
              
            foreach($sql as $row){
                  
    ?>
    
        <ul class="list">
        <li>
            <div class="drink-name">
            <h6><i class="fas fa-wine-glass-alt"></i><?php echo $row['DrinkName'] ?></h6>
            <span class="drink-size"><?php echo $row['DrinkSize'] ?></span>
            <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
          </div>
          <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
        </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?>
    
</div>
</section>


<!--end of section two--

<!--section three--


<div class="col-md-4 col-lg-4 col-xl-4">
 <section id="gin">
        <div class="product-title ">
        <h5>GIN</h5>
        </div>
        <hr>
        
    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Gin");
              
            foreach($sql as $row){
                  
    ?> 
    
        <ul class="list">
        <li>
          <div class="drink-name">
            <h6><i class="fas fa-cocktail"></i><?php echo $row['DrinkName'] ?></h6>
            <span class="drink-size"><?php echo $row['DrinkSize'] ?></span>
            <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
          </div>
          <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
        </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?> 
    
</div>
</section>

        <!--<div class="product-content">
           <h5><?php echo $row['DrinkName'] ?></h5>
           <p><?php echo $row['DrinkSize'] ?> @ <?php echo $row['DrinkPrice'] ?></p>
           <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
        </div>

      </section>
    </div>-->

    
<!--end of section three--

<!--section four--
     

<div class="col-md-4 col-lg-4 col-xl-4">
<section id="rum">
        <div class="product-title">
        <h5>RUM</h5>
        </div>
        <hr>
        
    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Rum");
              
            foreach($sql as $row){
                  
    ?>
        <ul class="list">
        <li>
          <div class="drink-name">
            <h6><i class="fas fa-glass-martini-alt"></i><?php echo $row['DrinkName'] ?></h6>
            <span class="drink-size"><?php echo $row['DrinkSize'] ?></span>
            <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
          </div>

          <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
        </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?>
    
</section>
</div>

       <!-- <div class="product-content">
           <h6><?php echo $row['DrinkName'] ?></h6>
           <p><?php echo $row['DrinkSize'] ?> @ <?php echo $row['DrinkPrice'] ?></p>
           <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
        </div>

      </section>
    </div>--

<!--end of section four--

<!--section five--

<div class="col-md-4 col-lg-4 col-xl-4">
<section id="brandy">
        <div class="product-title">
        <h5>BRANDYS</h5>
        </div>
        <hr>
        
    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Brandys");
              
            foreach($sql as $row){
                  
    ?> 
    
        <ul class="list">
        <li>
          <div class="drink-name">
            <h6><i class="fas fa-glass-martini"></i><?php echo $row['DrinkName'] ?></h6>
            <span class="drink-size"><?php echo $row['DrinkSize'] ?>Ml</span>
            <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
          </div>

          <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
        </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?> 
    
</section>
</div>

        <!--<div class="product-content">
           <h6><?php echo $row['DrinkName'] ?></h6>
           <p><?php echo $row['DrinkSize'] ?>ml @ <?php echo $row['DrinkPrice'] ?></p>
           <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
        </div>

      </section>
    </div>-->


<!--end of section five--


<!--section six---

    

<div class="col-md-4 col-lg-4 col-xl-4">
<section id="soft-drinks">
        <div class="product-title">
        <h5>SOFT DRINKS</h5>
        </div>
        <hr>
        
    <?php 
          
           $sql = $database->fetch_all_drinks_by_owners_category($owner_id,"Soft Drinks");
              
            foreach($sql as $row){
                  
    ?> 
        <ul class="list">
        <li>
          <div class="drink-name">
            <h6><i class="fas fa-glass-cheers"></i><?php echo $row['DrinkName'] ?></h6>
            <span><?php echo $row['DrinkSize'] ?></span>
            <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
          </div>

          <div class="price"><sup>Ksh</sup><?php echo $row['DrinkPrice'] ?></div>
        </li>
        </ul>
        
    <?php 
                  
            } 
          
    ?>
    
</section>
</div>

        <!--<div class="product-content">
           <h6><?php //echo $row['DrinkName'] ?></h6>
           <p><?php //echo $row['DrinkSize'] ?> @ <?php ///echo $row['DrinkPrice'] ?></p>
           <p><form action="" method="post">
               <input type="hidden" name="DrinkId" value="<?php //echo $row['DrinkId'] ?>">
               <input type="submit" name="addCart" value="Add To Cart" class="btn btn-warning">
           </form></p>
        </div>

      </section>
    </div>-->

     
<!--end of section six--
</div>
<!--end of row--
</div>
<!--end of drinks wrapper--


  

   

    <!-- Bootstrap core JavaScript -->
    <script src="../Front End/vendor/jquery/jquery.min.js"></script>
    <script src="../Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../Front End/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../Front End/vendor/scrollreveal/scrollreveal.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="../Front End/js/jqBootstrapValidation.js"></script>
    <script src="../Front End/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../Front End/js/agency.js"></script>
    
    <script>

         $(document).ready(function(){
          $('.modal').on('show.bs.modal', function(e) {

            var payId = $(e.relatedTarget).data('book-id');

            $(e.currentTarget).find('input[name="drink_id"]').val(payId);

          });

        });

    </script>

  </body>

</html>
