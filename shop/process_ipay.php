<?php

require_once("../Customers/includes/initialise.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tables to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tables from the comfort of your device.
                                Book reserve in kenya, get table to reserve kenya, get table for reserves, table reserves, online reserve and table system, 
                                buy table using mpesa, easy way to buy table kenya, easy and cheap way to post an reserve for free kenya">
                                
  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tables to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tables from the comfort of your device.
                                Book reserve in kenya, get table to reserve kenya, get table for reserves, table reserves, online reserve and table system, 
                                buy table using mpesa, easy way to buy table kenya, easy and cheap way to post an reserve for free kenya">

  <meta http-equiv="Cache-control" content="no-cache">
  <meta http-equiv="Expires" content="-1">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />
    <link rel="stylesheet" href="../Front End/node_modules/material-design-lite/material.min.css">
    <script src="../Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="../Front End/custom.css" rel="stylesheet">

</head>
<body >
     
      <div class="navbar navbar-expand-lg bg-dark navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="index.php" class="pull-left"><img class="img-responsive2"       
                    src="../Front End/Images/pop-in.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar navbar-right">

                <a class="nav-item menuItem" href="index.php" style="text-decoration: none; font-family: 'Varela Round'; color : #96980f; font-size: 1.2rem; padding: 0.8rem 0;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

      </div>


    <div class="d flex flex-column" id="shama_section">


                  <div class="my-flex-item">


               <div class="mdl-grid" style="justify-content: center; ">
              
               		<?php
                        
                        if(isset($_POST["ConfirmPay"])) {

                                $data_result = json_decode($payment->search_pay_orders($_POST["oid1"]));
                                
                                $status = $data_result->header_status;
                                
                                if($status == 200 ){
                                
                                    $session_id = $data_result->data->sid;
                                    $order_id = $data_result->data->oid;
                                    $transaction_amount = $data_result->data->transaction_amount;
                                    $transaction_code = $data_result->data->transaction_code;
                                    $telephone = $data_result->data->telephone;
                                    $firstname = $data_result->data->firstname;
                                    $lastname = $data_result->data->lastname;
                                    $payment_mode = $data_result->data->payment_mode;
       
        
        
        
        						  // $Mobile_No = $_POST["user_phone"];
        						    $Email = $_POST["email"];
                                    $DrinksId = $_POST["drink_id"];
                                    //$TableCapacity = $_POST["capacity"];
    
        						    //$database->fetch_specific_reserve_data($ReserveId);
    
        						    $DrinksOwner = $_SESSION['owner']['id'];
        						    
        						    $DrinksPrice = $_SESSION['total']['amount'];
    
    
        						    $payment->set_transaction_ref($transaction_code);
        							
        							
        							$payment->set_sender_phone($telephone);
    
        							//$automation->generate_pay_id();
    
        							$payment->set_pay_id($_SESSION['c']['cid']);
        							$payment->set_pay_drk_id($DrinksId);
        							$payment->set_pay_owner_id($DrinksOwner);
        							$payment->set_full_name($firstname,$lastname);
        							$payment->set_pay_user_email($Email);
        							$payment->set_amount($transaction_amount);
                                    $payment->set_acc_deposit("0");
        							
        							//echo $payment->fetched_amount;
        							$database->set_full_name($firstname,$lastname);
        							$database->set_phone_no($telephone);
        							$database->set_email($Email);
        							$database->set_password($transaction_code);
    
    
        								if($payment->insert_into_drinks_payment_table($transaction_code,$telephone)) {

    
                                                      echo "<h2>Your Payment was successful.</h2> <br/>";
                            
                                				 	echo "<form method='post' action='generateTickets.php'>
                            
                                                              <input type='hidden' value=".$telephone." name='payer_phone'>
                                                              <input type='hidden' value=".$Email." name='user_email'>
                                                              <input type='hidden' value=".$DrinksId." name='drink_id'>
                                                              <input type='submit' value='Generate Receipt' name='GenTables' class='btn btn-primary' style='background: #96980f;' >
                            
                                                          </form>";
    
    
        										/*} else {
                                                         $alert->message("An error occured","Fail");
        										}*/
    
        									} else {
                                                        $alert->message("Incorrect Credentials","Fail");
    
        									}

                                } else {
                                    
                                     $alert->message("You have no paid. Pay and reload.","Fail");
                                    
                                }
    
    
    
                            } else {
                                header("Location: ev_index.php");
                            }
                                
                    ?>

                </div><!-- close of mdl-grid class div--> 

          </div>

    </div>

<footer class="bg-dark" id="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Pop-In</h2>
                  <hr>
                  <p>Pop-in to buy advance tables to upcoming reserves</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Contact us</h2>
                  <hr>
                 <ul>
                     <li style="list-style: none;"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Navigation</h2>
                  <hr>
                 <ul>
                     <a href="login.php"><li style="list-style: none;">Login</li></a>
                     <a href="register.php"><li style="list-style: none;">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p>&copy copyright Pop-In all Rights Reserved: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="../Front End/styling/script.js"></script>
    <script src="../Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->
      
</body>
</html>