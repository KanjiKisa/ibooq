<?php 
  
    require_once("../Customers/includes/initialise.php");
    
    if(isset($_POST["subLogin"])) {
      
      $email = $_POST["loginEmail"];
      $pass = $_POST["loginPass"];
      $database->set_email($email);
      $database->set_password($pass);
      
      if($database->insert_to_login_table()) {
        
        $session->login($database->fetched_user_id);

        if($session->is_logged) {


            header("Location: manager.php");


        } else {

          $alert->message("Failed to Login","Fail");

        }
        
      } else {

        $alert->message("Failed to Login","Fail");

      }
    }
  
  ?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>ibooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela Round|Oswald|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style2.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/styling/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/e-shama.css" />
    <link rel="stylesheet" href="../Front End/node_modules/material-design-lite/material.min.css">
    <script src="../Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="../Front End/custom.css" rel="stylesheet">

</head>
<body >
     
      <div class="navbar navbar-expand-lg bg-dark navbar-static-top" id="navigator" style= "border-bottom: 2px solid #df3d82; color: f2f2f2; padding-bottom: 0;" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="index.php" class="pull-left"><img class="img-responsive2"       
                    src="../Front End/Images/popin.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar navbar-right">

                <a class="nav-item menuItem" href="../index.php" style="text-decoration: none; font-family: 'Varela Round'; color : #df3d82; font-size: 1.2rem;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

  </div>

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

          <div class="my-flex-item loginPanel">

              <form action="login.php" method="POST" id="loginForm">
                <p><input type="text" placeholder="Email" name="loginEmail" required></p>
                <p><input type="password" placeholder="Password" name="loginPass" required></p>
                <p><input type="submit" class="btn btn-primary" value="Login" name="subLogin"></p>
              </form>

              <p><b>Don't have an account? <a href="register.php">Register</a></b></p>

          </div>

        </div>


<footer class="bg-dark" id="footer" style="background:#000;">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Pop-In</h2>
                  <hr style="border: 1px solid #fff;">
                  <p style="font-family:'Raleway';">Pop-in to buy advance tickets to upcoming events</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Contact us</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <li style="list-style: none; font-family:'Raleway';"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;font-family:'Raleway';"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Navigation</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <a href="login.php"><li style="list-style: none;font-family:'Raleway';">Login</li></a>
                     <a href="register.php"><li style="list-style: none;font-family:'Raleway';">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;font-family:'Raleway';">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p style="font-family:'Raleway';">&copy copyright Pop-In all Rights Reserved: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i style="font-size:40px; color:#3b5998" class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i style="font-size:40px; color:#c32aa3"class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>
<!--<div class="mdl-grid" id="footer">
    
    <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">About Pop-In</h2>
      <p><a href="index.php" style="text-align : center; color : #fff; text-decoration: none; ">Pop-in is a reservation system for clubs, restaurant and other entertainment facilities. With this system you can manage your guestlist, offer discounts on drinks and other services</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">We are social</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>

  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Quick Links</h2>
    
    <ul class="links">
          <li><a href="">Support</a></li>
          <li><a href="">Privacy Policy</a></li>
          <li><a href="">Terms & Conditions</a></li>
        </ul>

  </div>


</div>-->

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="../Front End/styling/script.js"></script>
    <script src="../Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->

</body>
</html>