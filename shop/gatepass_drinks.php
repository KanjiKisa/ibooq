<?php 

require_once("../Customers/includes/initialise.php");

if(isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['drink_id']) && isset($_POST['merchant_id'])) {
 
    $PartyA = $_POST['phone'];
    $Email = $_POST['email'];
    $DrinksId = $_POST['drink_id'];
    $MerchantId = $_POST['merchant_id'];
    $DrinksOwner = $_SESSION['owner']['id'];
    $DrinksPrice = $_SESSION['total']['amount'];
    

        $payment->fetch_stk_details($MerchantId);
	    $payment->set_transaction_ref($payment->fetched_stk_Receipt);

        $payment->set_sender_phone($PartyA);

		$payment->set_pay_id($_SESSION['c']['cid']);
		$payment->set_pay_drk_id($DrinksId);
		$payment->set_pay_owner_id($DrinksOwner);
		$payment->set_full_name("trial","stk");
		$payment->set_pay_user_email($Email);
		$payment->set_amount($DrinksPrice);
        $payment->set_acc_deposit("0");
        $payment->set_payment_date($database->now_date_only);
        $payment->set_payment_time($database->now_time_only);
		

if($payment->is_error_available($MerchantId)) {
    $alert->message($payment->mpesa_error_desc,"Fail");
} else {

    if($payment->is_drink_payment_done($MerchantId)) {

      // $alert->message("Thankyou for downloading this song","Success");
      $alert->message("Thank you for buying drinks. <form method='post' action='generate_receipt.php'>
                            
                                                              <input type='hidden' value=".$PartyA." name='payer_phone'>
                                                              <input type='hidden' value=".$Email." name='user_email'>
                                                              <input type='hidden' value=".$DrinksId." name='drink_id'>
                                                              <input type='submit' value='Generate Receipt' name='GenReceipt' class='btn btn-primary' style='background: #96980f;' >
                            
                                                          </form>","Success");
      // $session->client_login($PartyA);
       
    } else {

        //echo "<script>clearPayment();</script>";

        $alert->message("Check your phone for an MPesa prompt, enter pin to pay, then wait as we confirm your payment.","Fail");
    }

}
    
} else {
       echo "<div class='d-flex justify-content-center'>
                <div class='spinner-border text-light' role='status'>
                  <span class='sr-only'>Loading...</span>
                </div>
                </div>";
}
      
      
      
?>