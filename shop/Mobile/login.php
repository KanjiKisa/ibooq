<?php 
  
    require_once("../../Customers/includes/initialise.php");

    $response = array();
    
    if(isset($_POST["loginEmail"]) && isset($_POST["loginPass"])) {
      
      $email = $_POST["loginEmail"];
      $pass = $_POST["loginPass"];
      $database->set_email($email);
      $database->set_password($pass);
      
      if($database->insert_to_login_table()) {
        
          $response["success"] = 1;
          $response["user_id"] = $database->fetched_user_id;
        
      } else {

          $response["success"] = 0;
          $response["user_id"] = "0";

      }

    } else {

            $response["success"] = 0;
            $response["message"] = "Missing some information...";

    }

    echo json_encode($response);
  
  ?>