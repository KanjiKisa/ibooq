<?php

require_once("../../Customers/includes/initialise.php");

$response = array();

	if(isset($_POST["drk_category"])&&isset($_POST["drk_name"])&&isset($_POST["drk_desc"])&&isset($_POST["drk_price"])&&isset($_POST["drk_size"])) {

          $drk_category = $_POST["drk_category"];
          $drk_name = $_POST["drk_name"];
          $drk_description = $_POST["drk_desc"];
          $drk_price = $_POST["drk_price"];
          $drk_size = $_POST["drk_size"];

			  
            $automation->generate_drink_id();
            
           $database->set_drk_category($drk_category);
            $database->set_drk_id($automation->drk_id);
            $database->set_user_id($_POST["UserId"]);
            $database->set_drk_name($drk_name);
            $database->set_drk_description($drk_description);
            $database->set_drk_size($drk_size);
            $database->set_drk_price($drk_price);

            
            if($database->insert_to_drinks_table()) {
               //$alert->message("Drink Uploaded","Success");
            	$response["success"] = 1;
            	$response["message"] = "Drink Uploaded";
            } else {
               //$alert->message("Failed to upload drink","Fail");
            	$response["success"] = 0;
            	$response["message"] = "Failed to upload drink...";
            }


    } else {
    		$response["success"] = 0;
            $response["message"] = "Missing some information...";
    }

    echo json_encode($response);

?>