<?php

require_once("../../Customers/includes/initialise.php");

$response = array();

  if(isset($_POST["session_id"])) {


    $data = $ticket->get_receipt_details($_POST["session_id"]);

    foreach($data as $row) {

        $payer_phone = $row["PayerPhone"];
        $receipt_no = $row["ReceiptId"];
        $receipt_url = $row["ReceiptUrl"];
        $payer_email = $row["PayerEmail"];
        $payer_name = $row["PayerName"];
        $pay_date = $row["PayDate"];
        $pay_time = $row["PayTime"];

        $temp = [
                  'success'=>1,
                  'payer_phone'=>$payer_phone,
                  'receipt_id'=>$receipt_no,
                  'receipt_url'=>$receipt_url,
                  'payer_email'=>$payer_email,
                  'payer_name'=>$payer_name,
                  'pay_date'=>$pay_date,
                  'pay_time'=>$pay_time
        ];

        array_push($response,$temp);

      }

      echo json_encode($response);


?>