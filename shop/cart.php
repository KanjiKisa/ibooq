<?php
    session_start(); 
    require_once("../Customers/includes/initialise.php");
    
    if(isset($_POST['submit'])){ 
          
        foreach($_POST['quantity'] as $key => $val) { 
            if($val==0) { 
                unset($_SESSION['carts'][$key]); 
            }else{ 
                $_SESSION['carts'][$key]['quantity']=$val; 
            } 
        } 
          
    }
    
    $token = $payment->get_mpesa_token();
    
?>
                
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>iBooq Cart</title>

    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

    <!-- Bootstrap core CSS -->
    <link href="../Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Josefin Sans|Open Sans|Montserrat|Raleway|Merriweather|Varela Round|EB Garamond|Passion One|Archivo Black|Oswald|Coiny|Catamaran|Julius Sans One">

    <!--font awesome cdn-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

    <!--animate css cdn-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <!-- Custom styles for this template -->
    <link href="Front End/style.css" rel="stylesheet">

  </head>

  <body id="page-top-drinks">
      
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="Front End/img/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#projects">Service Providers</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#signup">Business Owners</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="cart.php"><i class="fas fa-shopping-bag pr-2"></i>My Cart <span>[<?php echo count($_SESSION['carts']); ?> Items]</span></a>
          </li>
        </ul>
       
      </div>
    </div>
  </nav>

    <!-- Navigation --
<nav class="navbar navbar-expand-lg navbar-light fixed-top">
  <div class="container">
  <a class="navbar-brand" href="index.php"><img  src="Front End/Images/pop-in.png"></a>
    <ul class="navbar-nav ml-auto">
      <a href=""><i class="fas fa-shopping-bag fa-2x"></i>[<?php echo count($_SESSION['carts']); ?> Items]</a>
    </ul>
  </div>
</div>
</nav>

<br><br><br><br>
<!--search section-->


<!--drinks section-->
<div class="container" id="wrapper">
  
<h2>Products Cart</h2>

<form method="post" action=""> 
    
 <table class="table table-hover"> 
          
        <tr> 
            <th>Name</th> 
            <th>Quantity</th> 
            <th>Price</th> 
            <th>Total Price</th> 
            <!--<th>Cart Id</th>-->
        </tr> 
            <?php 
                
                //print_r($_SESSION['carts']);
                  
                    if(isset($_SESSION['carts'])){ 
                          
                        $sql="SELECT * FROM poppindrinks WHERE DrinkId IN ("; 
                          
                        foreach($_SESSION['carts'] as $id => $value) { 
                            $sql.=$id.","; 
                        } 
                          
                        $sql=substr($sql, 0, -1).") ORDER BY category ASC"; 
                        $query=$database->dbase->query($sql); 

                        $totalprice=0; 

                    $drinks_cart_id = array();

                    foreach($query as $row){ 
                        $subtotal=$_SESSION['carts'][$row['DrinkId']]['quantity']*$row['DrinkPrice']; 
                        $totalprice+=$subtotal; 
                    ?> 
                        <tr> 
                            <td><?php echo $row['DrinkName'] ?></td> 
                            <td><input class="form-input" type="text" name="quantity[<?php echo $row['DrinkId'] ?>]" size="5" value="<?php echo $_SESSION['carts'][$row['DrinkId']]['quantity'] ?>" /></td> 
                            <td>Ksh <?php echo $row['DrinkPrice'] ?></td> 
                            <td>Ksh <?php echo $_SESSION['carts'][$row['DrinkId']]['quantity']*$row['DrinkPrice'] ?></td> 
                            <!--<td><?php echo $_SESSION['carts'][$row['DrinkId']]['cart_id'] ?></td>-->
                        </tr> 
                    <?php 

                        $drink_id_array = array_push($drinks_cart_id, $_SESSION['carts'][$row['DrinkId']]['drink_id']);
                          
                    } 
                     ?> 
                    <tr> 
                        <td colspan="4">Total Price: <?php echo $totalprice ?></td> 
                    </tr> 

                    <?php  

                        $_SESSION['total']['amount'] = $totalprice;

                            $reserveId = implode(",",$drinks_cart_id);
                            
            ?>
                    
        </table>
        
    <br /> 
    <button type="submit" name="submit">Update Cart</button> 

    <a href=".modal" data-toggle="modal" data-book-id="<?php echo $reserveId; ?>" class="btn btn-primary">Buy Products</a>
</form> 
<br /> 
<p>To remove an item set its quantity to 0. </p>
                            
            <?php 
                  
                }else{ 
                      
                    echo "<p>Your Cart is empty. Please add some products.</p>"; 
                      
                } 
          
            ?>

  <div class="row">


  </div>
  <!--end of row-->
  
 <div class="modal" tabindex="-1" role="dialog" id="payModal" >

      <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content" >

          <div class="header">
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body" >
                        <div class="text-center">
                            
                            <h5 class="pt-4">Pay for product</h5>
                        </div>

                      <form action="confirm_payment.php" method="post" >

                         <div class="form-group">
                             <label>Phone Number</label>
                            <input type="number" class="form-control" placeholder="Enter a valid phone number" name="user_phone">
                         </div>     

                         <div class="form-group">
                           <input type="hidden" class="form-control" id="drink_id" name="drink_id" required="required">
                         </div>  
                         
                         <div class="form-group" >
                             <label>Email Address</label>
                           <input type="text" class="form-control" id="buyer_email" name="buyer_email" placeholder="Enter a valid e-mail" required="required">
                         </div>
                         
                         <div class="form-group">
                           <input type="hidden" class="form-control" id="token_id" name="token_id" required="required" value='<?php echo $token; ?>'>
                         </div> 
                         
                         <div class="custom-control custom-checkbox mb-4">
                            <input type="checkbox" class="custom-control-input" id="customControlAutosizing">
                            <label class="custom-control-label" for="customControlAutosizing">Agree to <a href="terms.html">Terms & conditions</label>
                          </div>
                            
                         <div class="form-group">
                           <button type="submit" class="btn btn-primary" id="subMpesa" name="subMpesa">Complete</button>
                    
                           
                         </div>

                     </form>
          </div>

          <div class="modal-footer">

          </div>

        </div>

      </div>

</div>


</div>

<!--end of drinks section--
<style>
@media(max-width:576px){
    .navbar-nav a, #payModal{
        margin-right: 4rem;
    }
    
}
    
</style>-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    <script>

         $(document).ready(function(){
          $('.modal').on('show.bs.modal', function(e) {

            var payId = $(e.relatedTarget).data('book-id');

            $(e.currentTarget).find('input[name="drink_id"]').val(payId);

          });

        });

    </script>


    <!-- Bootstrap core JavaScript -->
    <script src="../Front End/vendor/jquery/jquery.min.js"></script>
    <script src="../Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../Front End/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../Front End/vendor/scrollreveal/scrollreveal.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="../Front End/js/jqBootstrapValidation.js"></script>
    <script src="../Front End/js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="../Front End/js/agency.js"></script>

  </body>

</html>
