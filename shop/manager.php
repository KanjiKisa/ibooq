<?php

    require_once("../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      //echo $database->session_user_id;

        if(isset($_POST["submit_drink"])) {
                  $drk_category = $_POST["drk_category"];
                  $drk_name = $_POST["drk_name"];
                  $drk_description = $_POST["drk_desc"];
                  $drk_price = $_POST["drk_price"];
                  $drk_size = $_POST["drk_size"];

					  
                    $automation->generate_drink_id();
                    
                   $database->set_drk_category($drk_category);
                    $database->set_drk_id($automation->drk_id);
                    $database->set_user_id($database->session_user_id);
                    $database->set_drk_name($drk_name);
                    $database->set_drk_description($drk_description);
                    $database->set_drk_size($drk_size);
                    $database->set_drk_price($drk_price);

                    
                    if($database->insert_to_drinks_table()) {
                       $alert->message("Drink Uploaded","Success");
                    } else {
                       $alert->message("Failed to upload drink","Fail");
                    }


          }
		  

          if(isset($_POST["confirmTicket"])) {
   
            $ticketN = $_POST["ticketNo"]; 

            if($ticket->update_receipt_status($ticketN)) {
              $alert->message("Ticket Confirmed","Success");
            } else {
              $alert->message("Ticket Confirmation Failed","Fail");
            }

          }

          if(isset($_POST["withdrawPay"])) {
   
            //$drk_id = $_POST["drinksId"]; 

            if($drinks_payment->update_pay_withdrawal_status($database->session_user_id)) {
              $alert->message("Amount withdrawal successful. You will receive payment in 24 hours","Success");
            } else {
              $alert->message("Amount withdrawal failed.","Fail");
            }

          }

      
    } else {

        header("Location: login.php");
  
    }

?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>ibooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="../Front End/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="../Front End/e-shama.css" />
	
	 <!-- Bootstrap core CSS -->
    <link href="../Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../Front End/fontawesome-free-5.2.0-web/css/all.css">

</head>
<body >
     
      <div class="navbar navbar-expand-lg bg-dark navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="manager.php" class="pull-left"><img class="img-responsive2"       
                    src="../Front End/Images/pop-in.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->

            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar">

                <a class="nav-item menuItem" href="index.php" style="text-decoration: none; font-family: 'Varela Round'; color : #df3d82; font-size: 1.2rem; padding: 0.8rem 0;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

      </div>

    <!--End Js slide-->
<br>
      <div class="d flex flex-column" id="shama_section">

        <h1 style="text-align : center; color : #96980f; font-family: 'Oswald';">MANAGER <?php echo strtoupper($database->session_username); ?></h1>

          <div class="my-flex-item managementContent">



              <div id="sidebar">
                <ul id="managementTab">
                  <li style="font-family:'Varela Round';" onclick="show_ticket_panel()">Reservations</li>
                  <li style="font-family:'Varela Round';" onclick="show_reserve_panel()">Upload</li>
                  <li style="font-family:'Varela Round';" onclick="show_teller_panel()">Account</li>
				  <li style="font-family:'Varela Round';" onclick="show_profile_panel()">Profile</li>
                </ul><!-- end of managementTab ul -->
              </div><!-- end of sidebar div-->

              <div id="ticketPanel">
                <h2 style="text-align : center; color : #96980f; font-family: 'Oswald';">ONLINE RESERVATIONS</h2>

                <div id="userManagement"></div>

                <input type="text" class="form-control" id="searchTicket" onkeyup="searchAllTickets()" placeholder="Search by Drinks Name / Ticket No / Phone Number.." >

                  <br>

                    <table class="table table-striped" id="ticketTable">
                     <thead>
                        <tr>
                          <th scope="col">Receipt No</th>
                          <th scope="col">Receipt</th>
                          <th scope="col">Phone No</th>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Pay Date</th>
                          <th scope="col">Pay Time</th>
                        </tr>
                      </thead>
                      <tbody>

                          <?php

                              $data = $ticket->get_receipt_details($database->session_user_id);

                              foreach($data as $row) {

                                  $payer_phone = $row["PayerPhone"];
                                  $receipt_no = $row["ReceiptId"];
                                  $receipt_url = $row["ReceiptUrl"];
                                  $payer_email = $row["PayerEmail"];
                                  $payer_name = $row["PayerName"];
                                  $pay_date = $row["PayDate"];
                                  $pay_time = $row["PayTime"];


                          ?>

                        <tr>
                          <td><?php echo $receipt_no; ?></td>
                          <td> <a href="<?php echo $receipt_url; ?>">
                                <img id="uploadDocIcon" 
                                  src="<?php echo 'Front End/Images/pdf.png';?>" style="width:20%; height: 70px;">
                              </a>
                          </td>
                          <td><?php echo $payer_phone; ?></td>
                          <td><?php echo $payer_name; ?></td>
                          <td><?php echo $payer_email; ?></td>
                          <td><?php echo $pay_date; ?></td>
                          <td><?php echo $pay_time; ?></td>
                          <td><form method="post" action="manager.php">

                                  <input type="hidden" value="<?php echo $receipt_no;?>" name="ticketNo">
                                  <input type="submit" value="Confirm Receipt" name="confirmTicket" class="btn btn-primary" style="background: #96980f;">

                              </form>
                          </td>
                        </tr>

                        <?php

                            }

                        ?>

                      </tbody>
                    </table>



              </div><!-- end of ticketPanel div-->

              <div id="reservePanel">


                    <h2 style="text-align : center; color : #000; font-family: 'Oswald';">UPLOAD DRINKS</h2>

                      <form id="uploadProductForm" action="manager.php" method="POST" enctype="multipart/form-data">
                        <p><select name="drk_category" class="form-control">
                          <option>Beer</option>
                          <option>Wines & Spirits</option>
                          <option>Gin</option>
                          <option>Rum</option>
                          <option>Brandys</option>
                          <option>Whisky</option>
                          <option>Soft Drinks</option>
                         
                        </select></p>

                        <p><input class="form-control" type="text" name="drk_name" placeholder="Drinks Name" required></p>
                        
                        <p><input class="form-control" type="number" name="drk_size" placeholder="Enter drink size" required></p>
						
                        <p><input class="form-control" type="number" name="drk_price" placeholder="Enter price per drink" required></p>
						
                        <p><textarea class="form-control" rows="4" cols="20" name="drk_desc" placeholder="description"></textarea></p>

                        <p><input type="submit" value="Upload Drink" name="submit_drink" class="btn btn-primary" ></p>

                      </form>
                      
                      <form>
                          <p><input class="form-control" type="text" name="happy_hour_drk_name" placeholder="Drinks Name" required></p>
                          <p><input class="form-control" type="number" name="happy_hour_drk_size" placeholder="Enter drink size" required></p>
						
                          <p><input class="form-control" type="number" name="happy_hour_drk_price" placeholder="Enter price per drink" required></p>
                          <p><input type="submit" value="Upload Drink" name="happy_hour_submit_drink" class="btn btn-primary" ></p>
                          
                      </form>

                  
                  <div id="managerProducts">
                    
                         <div class="mdl-grid" style="justify-content: center; ">
              

                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/beers.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">BEERS</div></a>

                      </div><!-- close of card class div-->
                      
                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/wine.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">WINE & SPIRITS</div></a>

                      </div><!-- close of card class div-->
                                       
                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/gin.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">GIN</div></a>

                      </div><!-- close of card class div-->
                      
                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/rum.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">RUM</div></a>

                      </div><!-- close of card class div-->
                      
                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/brandys.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">BRANDYS</div></a>

                      </div><!-- close of card class div-->
                      
                      <div class="mdl-cell--4-col mdl-cell--2-col-tablet mdl-cell--3-col-phone" style="margin-left: 10px; margin-top: 10px;">

                            <a href="manage/soft.php" style="text-decoration: none; font-family: 'Varela Round';"><div class="category">SOFT DRINKS</div></a>

                      </div><!-- close of card class div-->


                </div><!-- close of mdl-grid class div--> 
                      

                  </div><!-- end of managerProducts div -->


              </div><!-- end of drinksPanel div -->

              <div id="tellerPanel">

                <div class="d flex flex-row">

                   <div class="my-flex-item managerAccounting">

                   <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">Data</th>
                        </tr>
                      </thead>

                      <tbody>

                        <?php

                            $totalDrinks = $drinks_payment->count_drinks_by_owner($database->session_user_id);
                            $totalAmount = $drinks_payment->sum_of_drinks_total_by_owner($database->session_user_id);

                            $percantage_total = $totalAmount * 0.15;

                            $final_pay = $totalAmount - $percantage_total;

                        ?>

                        <tr>
                            <th>Total Drinks</th>
                            <td><?php echo $totalDrinks; ?></td>
                        </tr>

                        <tr>
                            <th>Total Amount</th>
                            <td><?php echo $totalAmount; ?></td>
                        </tr>

                        <tr>
                            <th>Price Pay Per Drinks</th>
                            <td><b>15%</b></td>
                        </tr>

                        <tr>
                            <th>% Pay Total</th>
                            <td><?php echo $percantage_total; ?></td>
                        </tr>

                        <tr>
                            <th>Your Amount After % Pay</th>
                            <td><?php echo $final_pay; ?></td>
                        </tr>

                      </tbody>

                    </table>

                    

                    <table class="table table-striped" id="withdrawTable">
                      <thead>
                        <tr>
                          <th scope="col">Drinks Name</th>
                          <th scope="col">Amount</th>
                        </tr>
                      </thead>

                      <tbody>

                          <?php

                              $all_drinks_id = $database->fetch_all_reserves_by_owners($database->session_user_id);

                              foreach($all_drinks_id as $all_drinks) {

                                 $data = $drinks_payment->fetch_of_specific_drinks_payment_details($all_drinks["DrinkId"]);




                          ?>

                        <tr>
                          <td><?php //echo $drinks_payment->drk_name; ?></td>
                          <td><?php //echo $data; ?></td>
                          <td><form method="post" action="manager.php">

                                  <input type="submit" value="Withdraw" name="withdrawPay" class="btn btn-primary" style="background: #96980f;">

                              </form>
                          </td>
                        </tr>

                        <?php

                           // }

                        ?>

                       <tr><td><?php //echo $total; ?></td></tr>

                      </tbody>
                  
                   </table>

                  </div>

                 <div class="my-flex-item managerAccountingHistory" >

                     <h2 style="text-align : center; color : #000; font-family: 'Oswald';">RECORDS</h2>

                    <input type="text" class="form-control" id="searchPay" onkeyup="searchPayments()" placeholder="Search by Drinks Name / Transaction Code / Payer Phone.." >

                  <table class="table table-striped" id="payTable">
                      <thead>
                        <tr>
                          <th scope="col">Drinks Name</th>
                          <th scope="col">Transaction Code</th>
                          <th scope="col">Payer Phone</th>
                          <th scope="col">Total</th>
                        </tr>
                      </thead>

                      <tbody>

                          <?php

                              /*$data = $drinks_payment->fetch_drinks_payment_details($database->session_user_id);

                              $total = 0;

                              foreach($data as $row) {


                                  $drinks_name = $row["DrinksName"];
                                  $trans_code = $row["TransactionReference"];
                                  $payer_phone = $row["PayerPhone"];
                                  $amount = $row["AmountPaid"];*/


                          ?>

                        <tr>
                          <td><?php //echo $drinks_name; ?></td>
                          <td><?php //echo $trans_code; ?></td>
                          <td><?php //echo $payer_phone; ?></td>
                          <td><?php //echo $amount; ?></td>

                          </td>
                        </tr>

                        <?php

                            //}

                        ?>

                        <tr><td><?php //echo $total; ?></td></tr>

                      </tbody>
                  
                   </table>

                 </div>

              </div><!-- end of tellerPanel div -->
			  
		 </div>
			  
			  <div id="profilePanel">

					<h2>Sampler Drinks<h2>
					    
					    <form>
					        <div class="form-group">
					            <label>Happy Hour start time</label>
					            <input type="time" class="fomr-control">
					            
					            <label>Happy Hour end time</label>
					            <input type="time" class="fomr-control">
					        </div>
					    </form>


              </div><!-- end of profilePanel div -->

          </div>

    </div>

<footer class="bg-dark" id="footer" style="background:#000;">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Pop-In</h2>
                  <hr style="border: 1px solid #fff;">
                  <p style="font-family:'Raleway';">Pop-in to buy advance tickets to upcoming events</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Contact us</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <li style="list-style: none; font-family:'Raleway';"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;font-family:'Raleway';"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Navigation</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <a href="login.php"><li style="list-style: none;font-family:'Raleway';">Login</li></a>
                     <a href="register.php"><li style="list-style: none;font-family:'Raleway';">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;font-family:'Raleway';">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p style="font-family:'Raleway';">&copy copyright Pop-In all Rights Drinksd: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i style="font-size:40px; color:#3b5998" class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i style="font-size:40px; color:#c32aa3"class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>

<!--<div class="mdl-grid" id="footer">
    
    <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">About Pop-In</h2>
      <p><a href="index.php" style="text-align : center; color : #fff; text-decoration: none; ">Pop-in is a reservation system for clubs, restaurant and other entertainment facilities. With this system you can manage your guestlist, offer discounts on drinks and other services</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">We are social</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>

  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Quick Links</h2>
    
    <ul class="links">
          <li><a href="">Support</a></li>
          <li><a href="">Privacy Policy</a></li>
          <li><a href="">Terms & Conditions</a></li>
        </ul>

  </div>


</div>-->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="../Front End/styling/script.js"></script>
    <script src="Front End/e-shama.js"></script>
    <script src="../Front End/ShamaScript.js"></script>
	
	<!-- Bootstrap core JavaScript -->
    <script src="../Front End/vendor/jquery/jquery.min.js"></script>
    <script src="../Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    </script>

    <script>
      function searchAllTickets() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchTicket");
        filter = input.value.toUpperCase();
        table = document.getElementById("ticketTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          td2 = tr[i].getElementsByTagName("td")[1];
          td3 = tr[i].getElementsByTagName("td")[2];
          if (td || td2 || td3) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
      }

      function searchPayments() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("searchPay");
        filter = input.value.toUpperCase();
        table = document.getElementById("payTable");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
          td = tr[i].getElementsByTagName("td")[0];
          td2 = tr[i].getElementsByTagName("td")[1];
          td3 = tr[i].getElementsByTagName("td")[2];
          if (td || td2 || td3) {
            if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
              tr[i].style.display = "";
            } else {
              tr[i].style.display = "none";
            }
          }       
        }
      }

    </script>
  <!--side bars-->
      
</body>
</html>




