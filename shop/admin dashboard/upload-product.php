<?php

     require_once("../../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      //echo $database->session_user_id;

        if(isset($_POST["submit_product"])) {
                  $product_category = $_POST["product_category"];
                  $product_name = $_POST["product_name"];
                  $product_description = $_POST["product_desc"];
                  $product_price = $_POST["product_price"];
                  $product_size = $_POST["product_size"];

					  
                    $automation->generate_drink_id();
                    
                   $database->set_drk_category($product_category);
                    $database->set_drk_id($automation->drk_id);
                    $database->set_user_id($database->session_user_id);
                    $database->set_drk_name($product_name);
                    $database->set_drk_description($product_description);
                    $database->set_drk_size($product_size);
                    $database->set_drk_price($product_price);
                    $database->set_drk_happy_hour("0");

                    
                    if($database->insert_to_drinks_table()) {
                       $alert->message("Product Uploaded","Success");
                    } else {
                       $alert->message("Failed to upload product","Fail");
                    }


          }
          
	  

          if(isset($_POST["confirmTicket"])) {
   
            $ticketN = $_POST["ticketNo"]; 

            if($ticket->update_receipt_status($ticketN)) {
              $alert->message("Ticket Confirmed","Success");
            } else {
              $alert->message("Ticket Confirmation Failed","Fail");
            }

          }

          if(isset($_POST["withdrawPay"])) {
   
            //$drk_id = $_POST["drinksId"]; 

            if($drinks_payment->update_pay_withdrawal_status($database->session_user_id)) {
              $alert->message("Amount withdrawal successful. You will receive payment in 24 hours","Success");
            } else {
              $alert->message("Amount withdrawal failed.","Fail");
            }

          }

      
    } else {

        header("Location: ../login.php");
  
    }

?>

<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>iBooq</title>
    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="../../favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../favicons/favicon-16x16.png">
    <link rel="manifest" href="../../favicons/site.webmanifest">
    <link rel="mask-icon" href="../../favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="../../favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="../../favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<style>
  .upload-pic{
    border: 1px solid #ddd;
    border-style: dashed;
    padding: 1rem 1rem;
  }
</style>



<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="../Front End/img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item ">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Purchase List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="all-products.php">
         <i class="fas fa-users"></i>
          <span>My Products</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      
    <li class="nav-item">
        <a class="nav-link collapsed" href="../../admin dashboard/index.php">
         <i class="fas fa-users"></i>
          <span>My Services</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome Back</span>
                  
                </a>
                <!-- Dropdown - User Information -->
                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Profile
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Settings
                  </a>
                  <a class="dropdown-item" href="#">
                    <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                    Activity Log
                  </a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item text-white" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                  </a>
                </div>
              </li>
          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">Upload Products</h1>
            </div>
            
          </div>

          <!-- Content Row -->
          <div class="row no-gutters">

          </div>

          <!-- Ticket list table -->
         <div class="row">
             <div class="col-md-9">
                  <form id="uploadProductForm" action="" method="POST" enctype="multipart/form-data">
                        <p><select name="product_category" class="form-control">
                          <option>Lotions</option>
                          <option>Oils</option>
                          <option>Powders</option>
                          <option>Creams</option>
                          <option>Eyebrow Pencil</option>
                          <option>Hair Spray</option>
                          <option>Lipstick</option>
                         
                        </select></p>

                        <p><input class="form-control" type="text" name="product_name" placeholder="Product Name" required></p>
                        
                        <p><input class="form-control" type="text" name="product_size" placeholder="Enter product size" required></p>
						
                        <p><input class="form-control" type="number" name="product_price" placeholder="Enter price of product" required></p>
						
                        <p><textarea class="form-control" rows="4" cols="20" name="product_desc" placeholder="description"></textarea></p>

                        <p><input type="submit" value="Upload Product" name="submit_product" class="btn btn-primary" ></p>

            </form>
             </div>
             
         </div>
            
            
                      
                      
          
          <!----end of ticket list table-->

         

           
              
              

            
              

              

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="js/form.js"></script>

  <!-- Modal: modalPoll -->
<div class="modal fade right" id="modalPoll-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Feedback request
        </p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
          <p>
            <strong>Your opinion matters</strong>
          </p>
          <p>Have some ideas how to improve our product?
            <strong>Give us your feedback.</strong>
          </p>
        </div>

        <hr>

        <!-- Radio -->
        <p class="text-center">
          <strong>Your rating</strong>
        </p>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-179" value="option1" checked>
          <label class="form-check-label" for="radio-179">Very good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-279" value="option2">
          <label class="form-check-label" for="radio-279">Good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-379" value="option3">
          <label class="form-check-label" for="radio-379">Mediocre</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-479" value="option4">
          <label class="form-check-label" for="radio-479">Bad</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-579" value="option5">
          <label class="form-check-label" for="radio-579">Very bad</label>
        </div>
        <!-- Radio -->

        <p class="text-center">
          <strong>What could we improve?</strong>
        </p>
        <!--Basic textarea-->
        <div class="md-form">
          <textarea type="text" id="form79textarea" class="md-textarea form-control" rows="3"></textarea>
          <label for="form79textarea">Your message</label>
        </div>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-primary waves-effect waves-light">Send
          <i class="fa fa-paper-plane ml-1"></i>
        </a>
        <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cancel</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->

</body>

</html>
