<?php 

require_once("Customers/includes/initialise.php");

?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>ibooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Hind|Poppins|Nunito|Archivo Narrow">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Authentication/styles/css/font-awesome.min.css" />-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="Front End/css/themify-icons/themify-icons.css">
   <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#8AD879">  
    
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	
	 <!-- Bootstrap core CSS -->
   <!--<link href="Front End/mdlront End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css">-->


</head>
<body id="reserve-page">
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>



<main class="about-section" >
       
<!--venues card section--> 
<section id="events">
    <!--<div class="container-fluid">-->
<div class="container">
   
<div class="row">
<div class="col-lg-12 col-md-12 ftco-animate d-flex ">
<br/><br/><br/><br/>
            <div class="text" style="width:100%;">
              <br/><br/><br/><br/>
              <h4 class"ibooq-title" style="color:#8AD879; text-align:center;">CATEGORIES</h4>
              <p style="color:#000; text-align:center;">Select a service category to get your favourite service provider or shop</p>
              
              <div class="row">
                <div class="col-sm padding-0">
                    <form action="searching.php" method="get" class="">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Hairdresser" id="Hairdresser"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Hairdresser" id="HairdresserBtn"/> 
                                  
                        </div>
                      </form>

                      <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Model" id="Model"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Model" id=""/> 
                                  
                        </div>
                      </form>
                </div>

                <div class="col-sm padding-0">
                  <form action="searching.php" method="get" class="btn-switch-form">
                      <div class="form-group">
                                    <input type="hidden" class="btn-switch" name="p" value="Colorist" id="Colorist"/> 
                                    <input type="submit" class="btn-switch btn" name="search" value="Colorist" id="Colorist"/> 
                                
                      </div>
                    </form>

                    <form action="searching.php" method="get" class="btn-switch-form">
                      <div class="form-group">
                                    <input type="hidden" class="btn-switch" name="p" value="Makeup artist" id="Makeup_artist"/> 
                                    <input type="submit" class="btn-switch btn" name="search" value="Makeup artist" id="Makeup_artist"/> 
                                
                      </div>
                    </form>

                </div>

                <div class="col-sm padding-0">

                    <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Barber" id="Barber"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Barber" id="Barber"/> 
                                  
                        </div>
                      </form>


                      <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Masseur" id="Masseur"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Masseur" id="Masseur"/> 
                                  
                        </div>
                      </form>

                </div>

                <div class="col-sm padding-0">
                     <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Wellness instructor" id="Wellness_instructor"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Wellness instructor" id="Wellness_instructor"/> 
                                  
                        </div>
                      </form>


                      <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Public health educator" id="Public_health_educator"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Public health educator" id="Public_health_educator"/> 
                                  
                        </div>
                      </form>

                </div>

                <div class="col-sm padding-0">
                    <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Health services manager" id="Health_services_manager"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Health services manager" id="Health_services_manager"/> 
                                  
                        </div>
                      </form>

                      <form action="searching.php" method="get" class="btn-switch-form">
                        <div class="form-group">
                                      <input type="hidden" class="btn-switch" name="p" value="Health coach" id="Health_coach"/> 
                                      <input type="submit" class="btn-switch btn" name="search" value="Health coach" id="Health_coach"/> 
                                  
                        </div>
                      </form>

                </div>


              </div>

            </div>
          </div>
</row>
    
<div class="row" id="result">

              
                    <?php 

                      $sql = $database->get_all_BO();

                      foreach($sql as $row) {

                          $userId = $row["UserId"];
                          $clubName = $row["Username"];
                          $profilePic = $row["ProfilePic"];
                          $coverPic = $row["CoverPic"]; 
                          $open_days = $row["OpeningDays"];
                          $open_time = $row["OpeningTime"];
                          $close_time = $row["ClosingTime"];
                          $city = $row["City"];

                    ?>

                            <div class="col-sm-3 provider-card" style="margin-bottom:5px;">
                            <a href="ibooq.php?rs_id=<?php echo $userId; ?>" style="text-decoration:none;">
                                <div class="card">

                                <div class="club-poster">
                                    <img class="card-img-top sample-project" src="<?php echo $profilePic; ?>" alt="<?php echo $clubName; ?>">
                                    
                                </div>

                                  <div class="card-body">
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $clubName; ?></strong></p>
                                  </div>

                                </div><!-- close of card class div-->

                                </a>

                          </div>



                    <?php 


                       }


                    ?>


</div>
</div>
</section>

</main>



    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    <script>  
        $('.services_search').click(function() {

       //if(this.checked || this.unchecked){
        
            var ids = [];  
           $('.services_search').each(function(){  
                if($(this).is(":checked"))  
                {  
                     ids.push($(this).val());  
                }  
           });  
           ids = ids.toString();

            $.ajax({
                type: "POST",
                url: 'search_BO.php',
                data: {id:ids}, //--> send id of checked checkbox on other page
                success: function(data) {
                    $('#result').html(data);
                },
                 error: function() {
                    console.log('it broke');
                },
                complete: function() {
                    console.log('it completed');
                }
            });

          //  }
      });

 </script>  
	
	 <!-- Bootstrap core JavaScript 
    <script src="Front End/vendor/jquery/jquery.min.js"></script>
    <script src="Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> 

    </script>-->
  <!--side bars-->
       
    
    <script language="javascript">
        function fbshareCurrentPage(var url)
        {
            window.open('https://www.facebook.com/sharer/sharer.php?u=&t='+document.title, '','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false; }
    </script>

</body>
</html>