<?php
require 'Customers/includes/businessDays.php';
$businessDays = $_POST['open_days'];
$openTime = $_POST['open_time'];
$closeTime = $_POST['close_time'];
?>
<!DOCTYPE html>
<html>
<head>
  
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta charset='utf-8' />
<meta name="viewport" content="width=device-width,initial-scale=1.0">  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Hind|Poppins|Nunito|Archivo Narrow">
     
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
<link href='Front End/packages/core/main.css' rel='stylesheet' />
<link href='Front End/packages/daygrid/main.css' rel='stylesheet' />
<link href='Front End/packages/bootstrap/main.css' rel='stylesheet' />
<script src='Front End/packages/core/main.js'></script>
<script src='Front End/packages/interaction/main.js'></script>
<script src='Front End/packages/daygrid/main.js'></script>
<script src='Front End/packages/bootstrap/main.js'></script>
<script src='Front End/vendor/rrule.js'></script>
<script src='Front End/packages/moment/main.js'></script>
<script src='Front End/packages/moment-timezone/main.js'></script>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>


   <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500">  
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'momentTimezone', 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
      timeZone: 'local',
      weekLabel: "Week",
      weekNumbers: true,
      weekNumbersWithinDays: false,
      themeSystem: 'bootstrap',
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      defaultDate: "<?php $t=time(); echo(date("Y-m-d",$t)); ?>",
      selectable: true,
      showNonCurrentDates: true,
      validRange: {
        start: Date.now()
      },
      //pass business days below
      hiddenDays: [ <?php businessDay($businessDays) ?> ],
      header: {
        left: 'title prev,next',
        center: '',
        right: 'today'
      },
      dateClick: function(info) {
        var service_id = "<?php echo $_POST['service_id'];?>";
        var user_id = "<?php echo $_POST['user_id'];?>";
        var rs_id = "<?php echo $_POST['rs_id'];?>";
        var open_time = "<?php echo $openTime;?>";
        var close_time = "<?php echo $closeTime;?>";
        var business_days = "<?php businessDay($businessDays) ?>";
        var event_date = Date.parse(arg.start); 
        document.calendarForm.service_id.value = service_id;
        document.calendarForm.user_id.value = user_id;
        document.calendarForm.rs_id.value = rs_id;
        document.calendarForm.open_time.value = open_time;
        document.calendarForm.close_time.value = close_time;
        document.calendarForm.business_days.value = business_days;
        document.calendarForm.event_date.value = event_date;
        document.forms["calendarForm"].submit();
      },
      select: function(arg) {
        var service_id = "<?php echo $_POST['service_id'];?>";
        var user_id = "<?php echo $_POST['user_id'];?>";
        var rs_id = "<?php echo $_POST['rs_id'];?>";
        var open_time = "<?php echo $openTime;?>";
        var close_time = "<?php echo $closeTime;?>";
        var business_days = "<?php businessDay($businessDays) ?>";
        var event_date = Date.parse(arg.start); 
        document.calendarForm.service_id.value = service_id;
        document.calendarForm.user_id.value = user_id;
        document.calendarForm.rs_id.value = rs_id;
        document.calendarForm.open_time.value = open_time;
        document.calendarForm.close_time.value = close_time;
        document.calendarForm.business_days.value = business_days;
        document.calendarForm.event_date.value = event_date;
        document.forms["calendarForm"].submit();
      }
    });

    calendar.render();
  });

  

</script>
<style>

  body {
    padding: 0;
  }

  .btn {
    max-width: auto;
    padding: 0;
    max-height: 15px;
    margin-left: -1px;
  }
  .br {
    background-color: #ffffff;
    height: 10px;
  }
  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  
  div.fc-view {
    width: 100%;
    max-height: 70vh;
    overflow: auto;
  }
  .fc-head-container {
    background-color: #ffa500;
  }

</style>
</head>
<body class="bg-light">

<!--    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>
          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="register.php"><i class="fas fa-sign-in-alt pr-2"></i>Register</a>
          </li>
        </ul>
      </div>
    </div>

  </nav>

  <hr class="br">
  <hr class="br">
  -->
  <div class="bg-light" id='calendar'></div>
   
  <form id="calendarForm" name="calendarForm" method="post" action="booqtime.php">
    <input type="hidden" name="service_id" id="service_id">
    <input type="hidden" name="user_id" id="user_id">
    <input type="hidden" name="rs_id" id="rs_id">
    <input type="hidden" name="open_time" id="open_time">
    <input type="hidden" name="close_time" id="close_time">
    <input type="hidden" name="event_date" id="event_date">
    <input type="hidden" name="business_days" id="business_days">
  </form> 
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
   

 
</body>
</html>
