<?php

require_once("Customers/includes/initialise.php");

 if(isset($_POST["subMpesaLater"])) {
        
        //initialisation of variables
        
        $PartyA = $_POST['user_phone'];
        
        if($payment->startsWith($PartyA,"07")) {
            $mob = ltrim($PartyA,"0");
            $PartyA = "254".$mob;
        } else if($payment->startsWith($PartyA,"+254")) {
            $PartyA = ltrim($PartyA,"+");
        } else if($payment->startsWith($PartyA,"Pay Using: ")) {
            $PartyA = ltrim($PartyA,"Pay Using: ");
        }
        
        $Service_Id = $_POST['service_id'];
                            
        $database->fetch_specific_reserve_data($Service_Id);
        
        $ReserveOwner = $database->fetched_user_id;
        
        $Service_Price = $database->fetched_rs_price;
        
        //payment id
        $automation->generate_pay_id();
		
		$Payment_Id = $automation->pay_id;
        
    
 }  else {
     header("Location: index.php");
 }


?>

<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

  <meta http-equiv="Cache-control" content="no-cache">
  <meta http-equiv="Expires" content="-1">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

</head>
<body >
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>
      
<br><br>

    <div class="d flex flex-column" id="shama_section">

<br/><br/><br/>

                  <div class="my-flex-item">


               <div class="mdl-grid" style="justify-content: center; ">
                   
                   <div class="mdl-cell--10-col mdl-cell--10-col-tablet mdl-cell--10-col-phone" style="margin-left: 10px; margin-top: 10px;">
              
                            <div class="row">

                                          <div class="col-md-8">
                                              
                                        <?php
                                          
                                                    $user_phone = $PartyA;
                                                    $user_email = $_POST["buyer_email"];
                                                    $pay_id = $Payment_Id;
                                                    $service_id = $Service_Id;
                                                    $Arrival_Date = $_POST['Arrival_Date'];
                                                    $Arrival_Time = $_POST['Arrival_Time'];
                                                    $Ending_Time = $_POST['Ending_Time'];
                                              

                                                        $reserve_mpesa = $payment->fetch_by_payer_from_payment_table($user_phone,$service_id);
                                              
                                              
                                                        //$service_id = $payment->fetched_bal_reserve_id;
                                                        $payer_phone = $payment->fetched_bal_payer_phone;
                                                        //$table_capacity = $payment->fetched_bal_table_capacity;
                                                        $payBal = 0;
                                              
                                                        //data from reserves Table 
                                                        $database->fetch_specific_reserve_data($service_id);
                                              
                                                        $rs_category = $database->fetched_rs_category;
                                                        $rs_name = $database->fetched_rs_name;
                                                        $rs_photo_path = $database->fetched_rs_photo_path;
                                                        $rs_price = $database->fetched_rs_price;
                                                        $rs_location = $database->fetched_rs_location;
                                                        $reserve_owner = $database->fetched_user_id;
                                              
                                              
                                                            $automation->generate_ticked_id();
                                                            $receipt_id = $automation->ticket_id;
                                                            
                                                            //$user_email = $payment->fetch_user_payment_details($payer_phone,$service_id);
                                              
                                                          //create qr code of the Ticket Id with the code below
                                                            
                                                            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'Tickets'.DIRECTORY_SEPARATOR.'QR'.DIRECTORY_SEPARATOR;
                                                            
                                                            $filename = $PNG_TEMP_DIR.$receipt_id.".png";
                                                            
                                                            $append_path = 'Tickets'.DIRECTORY_SEPARATOR.'QR'.DIRECTORY_SEPARATOR;
                                                            
                                                            QRcode::png($receipt_id, $filename, 'L', $matrixPointSize = '9', 2); 

                                                            // benchmark
                                                            //QRtools::timeBenchmark();  
                                              
                                                            $pdf->set_append_path($append_path);
                                                            
                                                            $pdf->set_qr_code($receipt_id);
                                                            
                                                            $pdf->create_ticket($reserve_owner,"1",$rs_category,$rs_name,$rs_price,$user_phone,$user_phone,$user_email,$Arrival_Date,$Arrival_Time);
                                              
                                                            $pdf->Output("Tickets/Paid/".$receipt_id.".pdf","F");
                                              
                                                            //insert into tickets table
                                                            $ticket->set_ticket_id($receipt_id);
                                                            $ticket->set_pay_id($pay_id);
                                                            $ticket->set_reserve_id($service_id);
                                                            $ticket->set_payer_phone($PartyA);
                                                            $ticket->set_ticket_url("Tables/Paid/".$receipt_id.".pdf");
                                                            $ticket->set_ticket_status("0");

                                                            //insert into payment table
                                                            $payment->set_transaction_ref("LATER");
                                                            $payment->set_sender_phone($PartyA);
                                                            $payment->set_pay_id($pay_id);
                                                            $payment->set_pay_rs_id($service_id);
                                                            $payment->set_pay_owner_id($reserve_owner);
                                                            $payment->set_pay_user_email($user_email);
                                                            $payment->set_full_name("trial","stk");
                                                            $payment->set_amount("0");
                                                            $payment->set_table_capacity("1");
                                                            $payment->set_acc_deposit("0");
                                                            $payment->set_payment_date($database->now_date_only);
                                                            $payment->set_payment_time($database->now_time_only);
                                                            $payment->set_arrival_date($Arrival_Date);
                                                            $payment->set_arrival_time($Arrival_Time);
                                                            $payment->set_ending_time($Ending_Time);
                                                            
                                                        if($payment->is_pay_later_done()) {
                                                            if($ticket->insert_into_unpaid_tickets_table()) {
                                                            
                                                              
                                                                  $mailSending->set_from_address("sizzys@shamalandscapes.com");
                                                                $mailSending->set_from_name("iBooq Service Reservations");
                                                                $mailSending->set_to_address($user_email);
                                                                $mailSending->set_to_name("iBooq User");
                                                                $mailSending->set_subject("Receipt");
                                                                $mailSending->set_message("Thank you for booking a service with ibooq. Your receipt is attached below. ");
                                                                $mailSending->add_attachment("Tickets/Paid/".$receipt_id.".pdf");
                                                                $mailSending->set_variables();
                                              
                                                                if($mailSending->send_email()) {
                                                                  // header("Location: success.php");
                                                                    $alert->message("Receipt sent to email. Thank you","Success");
                                                                } else {
                                                                  $alert->message("Receipt Sending Failed. Use ID sent through text","Fail");
                                                                }
                                              
                                                            } else {
                                                              $database->delete_from_balance_table($PayerPhone);
                                                                $alert->message("Receipt Issue Unsuccessful. Contact Admin","Fail");
                                                            }

                                                          } else {

                                                            $alert->message("Pay Later was unsuccessful. Try again later.","Fail");
                                                        }
                                            
                                        ?>
                                               
                                            
                                            
                                          </div>
                                    
                                          
                                          
                             </div>
                    
                    </div>

                </div><!-- close of mdl-grid class div--> 

          </div>

    </div>

    <footer class="footer">
      <div class="foo-top">
        <div class="container text-center">
          <div class="row no-gutters align-items-center">
            <div class="col-md-12">
              <div class="widget widget-insta-feed">
                <ul class="list-unstyled mt-4">
                  <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" class="tweet"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" class="send"><i class="fab fa-whatsapp"></i></a></li>
                  <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                  
                </ul>
              </div>
              <div class="widget widget-address">
                <address>
                  
                  <a href="tel:"><i class="ti-mobile mr-2"></i>0700...</a>
                  <a href="mailto:" class="d-block"><i class="ti-email mr-2"></i> info@ibooq.co.ke</a>
                </address>
              </div>
            </div>

          </div>
        </div>
      </div>

    </footer>
    
     <style>
        h2{
            font-family: 'Oswald';
            
        }
        
        .btn{
            background: #ad67ea;
            background: #ad67ea;
  font-size: 0.9rem;
  padding: 15px 25px;
  color: #fff;
  text-transform: uppercase;
  font-family: 'Varela Round';
  border: 1px solid #fff;
  border-radius: 5px;
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
        }
        
        ul{
            list-style: none;
        }
        
        ul li{
            font-family: 'Raleway';
        }
    </style>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>
    <script src="Front End/automate_pay.js"></script>

    </script>
  <!--side bars-->
      
</body>
</html>