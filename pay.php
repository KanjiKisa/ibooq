<?php
require 'Customers/includes/initialise.php';
require 'Customers/includes/businessDays.php';

$referrer = $_SERVER['HTTP_REFERER'];
$page = "http://ibooq.co.ke/booqtime.php";

//if ($page === $referrer) {
    
  $datetime = $_POST['service_time'];

  //Divide by 1,0000
  $timestampSeconds = $datetime / 1000 + 10800;
   
  //Format it into a human-readable datetime.
  $service_datetime = gmdate("d/m/Y H:i:s", $timestampSeconds);


  $service_date = gmdate("Y-m-d", $timestampSeconds);


  $service_time = gmdate("H:i:s", $timestampSeconds);

//Print it out()
 $service_id = $_POST['service_id'];
 $user_id = $_POST['user_id'];

 $database->fetch_specific_reserve_data($service_id);

  //get the ending time in seconds

  $end_time_seconds = strtotime($service_time)+$database->fetched_rs_stock;

  $ending_time =  date("H:i",$end_time_seconds);
 
$token = $payment->get_mpesa_token();

// } else {
//       header('Location: http://ibooq.co.ke/booqing.php');
// }

?>

<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <!--<link href="https://fonts.googleapis.com/css?family=Roboto|Oswald" rel="stylesheet">-->
     <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="Authentication/styles/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
	
	 <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins|Inconsolata|Droid+Sans|Raleway|Merriweather|">

    <!-- Bootstrap core CSS -->
    <link href="Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />


	<!-- ClockPicker Stylesheet -->
	<link rel="stylesheet" type="text/css" href="js/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

</head>
<body id="inner-page" >
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>
  
  <br>
  <section id="club-inner">
      <div class="fluid pt-5">
          <div class="row no-gutters">

              <div class="col-md-10">
        
                     <a href="#modal-pay-now" data-service_id="<?php echo $service_id; ?>" data-Arrival_Date="<?php echo $service_date; ?>" data-Arrival_Time="<?php echo $service_time; ?>" data-token_id="<?php echo $token; ?>" class="openEditDialog btn btn-primary" data-toggle="modal">Pay Now</a>
                     <a href="#modal-pay-later" data-service_id="<?php echo $service_id; ?>" data-Arrival_Date="<?php echo $service_date; ?>" data-Arrival_Time="<?php echo $service_time; ?>" data-token_id="<?php echo $token; ?>" class="openEditDialog btn btn-primary" data-toggle="modal">Pay Later</a>

                  
              </div>
          </div>
      </div>
  </section>
            		  
  <div class="modal fade" id="modal-pay-now" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-dialog-centered" role="document">
                                <div class="modal-content bg-gradient-secondary">
                                    <div class="modal-header">
                                        <p class="modal-title" id="modal-title-notification">Pay Now.</p>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    <form action="confirm_payment.php" method="post" >

                                        <div id="display"></div>
                                          
                                          <div class="form-group">
                                          <input type="hidden" class="form-control" id="service_id" name="service_id" value="<?php echo $service_id; ?>" required="required">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label for="Arrival_Date">Service will start on</label>
                                            <input type="text" class="form-control" id="Arrival_Date" placeholder="When do you want to reserve for?" name="Arrival_Date" value="<?php echo $service_date; ?>" required readonly>
                                        </div>
                                        
                                        <div class="form-group">
                                        <label for="Arrival_Time">Service will start at</label>
                                            <input type="time" class="form-control" id="Arrival_Time" placeholder="What time do you want to reserve from?" name="Arrival_Time" value="<?php echo $service_time; ?>" required readonly>
                                        </div>

                                        <div class="form-group">
                                        <label for="Ending_Time">Service will stop at</label>
                                            <input type="time" class="form-control" id="Ending_Time" placeholder="Ending time of service?" name="Ending_Time" value="<?php echo $ending_time; ?>" required readonly>
                                        </div>

                                          <div class="form-group">
                                          <input type="number" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Mobile Number" required="required">
                                          </div>

                                        
                                        <div class="form-group" >
                                          <input type="text" class="form-control" id="buyer_email" name="buyer_email" placeholder="Enter a valid e-mail" required="required">
                                        </div>
                                        
                                        <div class="form-group">
                                          <input type="hidden" class="form-control" id="token_id" name="token_id" required="required" value='<?php echo $token; ?>'>
                                        </div> 

                                        <div class="form-group">
                                        <label for="bo_service_providers">Choose Service Provider</label>
                                          <select name="bo_service_providers"  class="form-control form-control-lg" required>
                                        <?php

                                            $sp = $database->get_all_BO_SP($user_id);

                                            foreach ($sp as $key => $row) {
                                                

                                        ?>

                                            <option value="<?php echo $row['UserId'];?>"><?php echo $row['Username'];?></option>

                                        <?php } ?>
                                        </select>
                                        </div>

                                        <div class="form-group">

                                          <button type="submit" class="btn btn-primary" id="subMpesa" name="subMpesa">Pay Now</button>

                                        </div>

                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="modal-pay-later" tabindex="-1" role="dialog" aria-labelledby="modal-notification" aria-hidden="true">
                            <div class="modal-dialog modal-info modal-dialog-centered" role="document">
                                <div class="modal-content bg-gradient-secondary">
                                    <div class="modal-header">
                                        <p class="modal-title" id="modal-title-notification">Pay Later.</p>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                    <form action="confirm_later_payment.php" method="post" >

                                        <div id="display"></div>
                                          
                                          <div class="form-group">
                                          <input type="hidden" class="form-control" id="service_id" name="service_id" value="<?php echo $service_id; ?>" required="required">
                                        </div>
                                        
                                        <div class="form-group">
                                        <label for="Arrival_Date">Service will start on</label>
                                            <input type="text" class="form-control" id="Arrival_Date" placeholder="When do you want to reserve for?" name="Arrival_Date" value="<?php echo $service_date; ?>" required readonly>
                                        </div>
                                        
                                        <div class="form-group">
                                        <label for="Arrival_Time">Service will start at</label>
                                            <input type="time" class="form-control" id="Arrival_Time" placeholder="What time do you want to reserve from?" name="Arrival_Time" value="<?php echo $service_time; ?>" required readonly>
                                        </div>

                                        <div class="form-group">
                                        <label for="Ending_Time">Service will stop at</label>
                                            <input type="time" class="form-control" id="Ending_Time" placeholder="Ending time of service?" name="Ending_Time" value="<?php echo $ending_time; ?>" required readonly>
                                        </div>

                                          <div class="form-group">
                                          <input type="number" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Mobile Number" required="required">
                                          </div>

                                        
                                        <div class="form-group" >
                                          <input type="text" class="form-control" id="buyer_email" name="buyer_email" placeholder="Enter a valid e-mail" required="required">
                                        </div>
                                        
                                        <div class="form-group">
                                          <input type="hidden" class="form-control" id="token_id" name="token_id" required="required" value='<?php echo $token; ?>'>
                                        </div> 

                                        <div class="form-group">
                                        <label for="bo_service_providers">Choose Service Provider</label>
                                          <select name="bo_service_providers"  class="form-control form-control-lg" required>
                                        <?php

                                            $sp = $database->get_all_BO_SP($user_id);

                                            foreach ($sp as $key => $row) {
                                                

                                        ?>

                                            <option value="<?php echo $row['UserId'];?>"><?php echo $row['Username'];?></option>

                                        <?php } ?>
                                        </select>
                                        </div>

                                        <div class="form-group">

                                          <button type="submit" class="btn btn-primary" id="subMpesaLater" name="subMpesaLater">Pay Later</button>

                                        </div>

                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
            </div>

    
   <!-- Footer strat -->
   <footer class="footer">
      <div class="foo-top">
        <div class="container text-center">
          <div class="row no-gutters align-items-center">
            <div class="col-md-12">
              <div class="widget widget-insta-feed">
                <ul class="list-unstyled mt-4">
                  <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" class="tweet"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" class="send"><i class="fab fa-whatsapp"></i></a></li>
                  <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                  
                </ul>
              </div>
              <div class="widget widget-address">
                <address>
                  
                  <a href="tel:"><i class="ti-mobile mr-2"></i>0700...</a>
                  <a href="mailto:" class="d-block"><i class="ti-email mr-2"></i> info@ibooq.co.ke</a>
                </address>
              </div>
            </div>

          </div>
        </div>
      </div>

    </footer>
    <!-- Footer end -->





</div>

<!--<style>
    .navigator .link a{
        border: 1pX solid #df3d82;
        padding: 0.4rem 0.8rem;
        color: #000;
        text-decoration: none;
        font-family: 'Inconsolata';
        font-size: 1.4rem;
    }
</style>-->

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
     
	 <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<script src="Front End/styling/script.js"></script>
    <script src="Front End/scripting/e-shama.js"></script>

    <script src="js/bootstrap-clockpicker.min.js"></script>
	
    </script>
  <!--side bars-->
       
    <script>

      $(document).on("click", ".openEditDialog", function () {
            var service_id = $(this).data('service_id');
            var Arrival_Date = $(this).data('Arrival_Date');
            var Arrival_Time = $(this).data('Arrival_Time');
            var token_id = $(this).data('token_id');

            $("#service_id").attr("value", service_id);
            $("#Arrival_Date").attr("value", Arrival_Date);
            $("#Arrival_Time").attr("value", Arrival_Time);
            $("#token_id").attr("value", token_id);

        });

    </script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $(function() {
    
            $( "#Arrival_Date" ).datepicker({
               changeMonth:true,
               changeYear:true,
               dateFormat:"yy-mm-dd",
               showAnim: "slide"
            });
         });
  </script>
		<script type="text/javascript">
		$('#Arrival_Time').clockpicker({
			autoclose: true
		});
		</script>
    
     <script language="javascript">
        function fbshareCurrentPage()
        {
            window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 
        'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false; }
    </script>

</body>
</html>
