<?php

	require_once("Customers/includes/initialise.php");


?>

<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">
                                
  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Front End/custom.css" rel="stylesheet">

</head>
<body >
     
      <div class="navbar navbar-expand-lg navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="index.php" class="pull-left"><img class="img-responsive2"       
                    src="Front End/Images/logo.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->


            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar navbar-right">

                <a class="nav-item menuItem" href="index.php" style="text-decoration: none; font-family: 'Josefin Slab'; color : #fff; font-size: 40px; padding: 30px;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

      </div>


    <div class="d flex flex-column" id="shama_section">


                  <div class="my-flex-item">


               <div class="mdl-grid" style="justify-content: center; ">
              
               		<?php


               		if(isset($_POST["subMpesa"])) {

    						    $Ref_No = $_POST["mpesa_code"];
    						   // $Mobile_No = $_POST["mobile_no"];
    						    $Email = $_POST["buyer_email"];
    						    $ReserveId = $_POST["reserve_id"];
    						    $TableCapacity = $_POST["table_capacity"];

    						    $database->fetch_specific_reserve_data($ReserveId);

    						    $ReserveOwner = $database->fetched_user_id;
    						    
    						    $ReservePrice = $database->fetched_rs_price * $TableCapacity;

    						    //echo $Ref_No.$Mobile_No.$Email.$ReserveOwner.$ReserveId;

    						    $payment->set_transaction_ref(trim($Ref_No));
    							

    							$data = $payment->fetch_specific_mpesa_details(trim($Ref_No));

    							foreach($data as $row) {

    								$payment->fetched_service_name = $row["ServiceName"];
    								$payment->fetched_business_no = $row["BusinessNumber"];
    								$payment->fetched_transaction_ref = $row["TransactionReference"];
    								$payment->fetched_internal_transId = $row["InternalTransactionId"];
    								$payment->fetched_trans_timestamp = $row["TransactionTimestamp"];
    								$payment->fetched_trans_type = $row["TransactionType"];
    								$payment->fetched_account_no = $row["AccountNumber"];
    								$payment->fetched_sender_phone = $row["SenderPhone"];
    								$payment->fetched_first_name = $row["FirstName"];
    								$payment->fetched_middle_name = $row["MiddleName"];
    								$payment->fetched_last_name = $row["LastName"];
    								$payment->fetched_amount = $row["Amount"];
    								$payment->fetched_currency = $row["Currency"];
    								$payment->fetched_signature = $row["Signature"];
    								$payment->fetched_pesa_date = $row["PesaDate"];
    								$payment->fetched_pesa_time = $row["PesaTime"];

    							}
    							
    							$payment->set_sender_phone($payment->fetched_sender_phone);

    							$automation->generate_pay_id();

    							$payment->set_pay_id($automation->pay_id);
    							$payment->set_pay_rs_id($ReserveId);
    							$payment->set_pay_owner_id($ReserveOwner);
    							$payment->set_pay_user_email($Email);
    							$payment->set_amount($payment->fetched_amount);
    							$payment->set_table_capacity($TableCapacity);
                                $payment->set_acc_deposit("0");
    							
    							//echo $payment->fetched_amount;
    							$database->set_full_name($payment->fetched_first_name,$payment->fetched_last_name);
    							$database->set_phone_no($payment->fetched_sender_phone);
    							$database->set_email($Email);
    							$database->set_password($payment->fetched_transaction_ref);

    								//if($database->insert_to_user_reg_table()) {

    									if($payment->insert_into_reserve_payment_table(trim($Ref_No),$payment->fetched_sender_phone)) {

    										//get total amount paid by user for a certain reserve, and calculate his balance for that reserve

    										$paid_amounts = $payment->get_total_reserve_amount_per_payer($ReserveId,$payment->fetched_sender_phone);

    										$total = 0;

    										foreach($paid_amounts as $row) {

    											$amount_paid = $row["AmountPaid"];

    											$total = $amount_paid + $total;

    										}

    										$balance = $ReservePrice  - $total; // look into this if error occurs due 10/18/2018 1200hrs

    										if($payment->insert_to_balance_table($ReserveId,$payment->fetched_sender_phone,$balance)) {

                          echo "<h2>Your Payment was successful. Confirm Pay to generate ticket.</h2>";

    											echo "<form method='post' action='generateTickets.php'>

                                  <input type='hidden' value=".$payment->fetched_sender_phone." name='payer_phone'>
                                  <input type='hidden' value=".trim($ReserveId)." name='reserve_id'>
                                  <input type='submit' value='Confirm Pay' name='ConfirmPay' class='btn btn-primary' style='background: #96980f;' >

                              </form>";
    											/*header("Location : 'generateTickets.php?Mobile=\".trim($Mobile_No)\" ");*/

    										} else {
                          $alert->message("An error occured","Fail");
    										}

    										//echo $total." and balance = ".$balance;

    									} else {
                        header("Location: index.php");
                        $alert->message("Incorrect Credentials","Fail");

    									}

    								/*} else {

    									echo "Failed to register. ";

    								}*/

  						} else {
  							header("Location: index.php");
  						}

					?>


                </div><!-- close of mdl-grid class div--> 

          </div>

    </div>


<div class="mdl-grid" id="footer">

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Tickets</h2>
      <p><a href="../index.php" style="text-align : center; color : #fff; text-decoration: none; ">Confirm Ticket</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Contact Us</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>


  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Our Products</h2>

  </div>

</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->
      
</body>
</html>