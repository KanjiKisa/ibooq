<?php

  require_once("Customers/includes/initialise.php");

  if($session->is_logged) {

      $database->get_session_details($session->session_id); 

        $OwnerId = $database->session_user_id;

        if(isset($_POST['withdraw_stat'])) {

            $event_id = $_POST['event_id'];

            $event_details =  $payment->fetch_specific_event_all_details($event_id);

            foreach($event_details as $row) {

              $fetched_ev_category = $row["Category"];
              $fetched_ev_name = $row["EventName"];
              $fetched_ev_price = $row["EventPrice"];
              $fetched_ev_location = $row["EventLocation"];
              $fetched_ev_photo_path = $row["PicPath"];
              $fetched_ev_start_date = $row["StartDate"];
              $fetched_ev_start_time = $row["StartTime"];
              $fetched_ev_stop_date = $row["StopDate"];
              $fetched_ev_stop_time = $row["StopTime"];

            }

            $amount = $payment->sum_of_specific_events_total($event_id);

            $withdrawn_amount = $amount * 0.94;

            

        } else {

            header("Location: super_admin.php");


        }
        
        if(isset($_POST['delete'])) {

            $event_id = $_POST["event_id"];

            if($database->delete_event($event_id)) {
              echo "<script>alert('Event Deleted'); </script>";
            } else {
              echo "<script>alert('Failed to delete Event'); </script>";
            }
          
        }

      } else {

          header("Location: ../index.php");

      }


?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an event organizer or promoter looking for an easy way to sell advance tickets to your event, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of events from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book event in kenya, get ticket to event kenya, get ticket for events, ticket events, online event and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an event for free kenya">

  <meta name="keywords" content="Are you an event organizer or promoter looking for an easy way to sell advance tickets to your event, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of events from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book event in kenya, get ticket to event kenya, get ticket for events, ticket events, online event and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an event for free kenya">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style2.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">   
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Front End/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />

</head>
<body >
     
      <div class="navbar navbar-expand-lg navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="super_admin.php" class="pull-left"><img class="img-responsive2"       
                    src="Front End/Images/logo.png"> </a>
              </h2>

            <!-- <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> -->

                       <div id="showMenu" onclick="showmenu()">&#9776;</div>

            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar">

                <a class="nav-item menuItem" href="super_admin.php" style="text-decoration: none; font-family: 'Josefin Slab'; color : #fff; font-size: 40px; padding: 30px;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>

            <div id="tModalmenu" class="teamModalmenu">
                <div class="modalContent">
                  <span class="closemenu" onclick="hidemenu()">&times;</span>

                  <div id="menuone">

                    <ol>
                       <li><a href="super_admin.php">Home</a></li>
                    </ol>

                    </div> 

                </div> 

            </div>

        </div>

      </div>

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

        <h1 style="text-align: center; color: #000; font-size: 70px; ">SUPER MANAGER <?php echo strtoupper($database->session_username); ?></h1>

          <div class="my-flex-item managementContent">



              <div id="sidebar">
                <ul id="managementTab">
                  <li class="specificManagementTab" onclick="show_specific_event_panel()">Movie Events</li>
                  <li onclick="show_specific_teller_panel()">Accounting</li>
                </ul><!-- end of managementTab ul -->
              </div><!-- end of sidebar div-->


              <div id="specificEventPanel">


                  <div class="mdl-grid" style="justify-content: center; ">              


                  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone event-data" id="event_poster">

                    <img src="<?php echo $fetched_ev_photo_path; ?>" >

                  </div>

                  <div class="mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--1-col-phone event-data" id="event_poster_details">

                    <h5 style="font-family: 'Josefin Slab'; color: #96980f; ">
                      CATEGORY 
                      <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_category; ?></span>
                    </h5>

                    <h5 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      EVENT NAME <span style="margin-left:5px; color: #000;  "><?php echo $fetched_ev_name; ?></span>
                    </h5>

                    <h5 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      EVENT PRICE 
                      <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_price; ?></span>
                    </h5>

                    <h5 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      EVENT LOCATION <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_location; ?></span>
                    </h5>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      START DATE <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_start_date; ?></span>
                    </h6>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f; ">
                      START TIME <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_start_date; ?></span>
                    </h6>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      STOP DATE <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_stop_date; ?></span>
                    </h6>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      STOP TIME <span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $fetched_ev_stop_time; ?></span>
                    </h6>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f; ">
                      AMOUNT (<i>KSH</i>)<span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $amount; ?></span>
                    </h6>

                    <h6 style="font-family: 'Josefin Slab'; color: #96980f; ">
                      WITHDRAWN AMOUNT (<i>KSH</i>)<span style="margin-left:5px; color: #000; font-size: 20px; "><?php echo $withdrawn_amount; ?></span>
                    </h6>

                    <form method="post" action="update_withdraw.php">

                        <input type="hidden" value="<?php echo $event_id;?>" name="event_id">
                        <input type="submit" value="Cash Out" name="cashOut" class="btn btn-primary" style="left: 80%; background: #96980f;">

                    </form>

                    
                  </div>


                </div><!-- close of mdl-grid class div--> 

            </div><!-- end of productPanel div -->

              <div id="specificTellerPanel">

                  <div class="my-flex-item managerAccounting" >

                    <input type="text" class="form-control" id="searchPay" onkeyup="searchPayments()" placeholder="Search by Event Name / Transaction Code / Payer Phone.." >

                  <table class="table table-striped" id="payTable">
                      <thead>
                        <tr>
                          <th scope="col">Event Name</th>
                          <th scope="col">Transaction Code</th>
                          <th scope="col">Payer Phone</th>
                          <th scope="col">Total</th>
                        </tr>
                      </thead>

                      <tbody>

                          <?php

                              $data = $payment->fetch_event_payment_details_cat($OwnerId,"Movies");

                              $total = 0;

                              foreach($data as $row) {


                                  $event_name = $row["EventName"];
                                  $trans_code = $row["TransactionReference"];
                                  $payer_phone = $row["PayerPhone"];
                                  $amount = $row["AmountPaid"];


                          ?>

                        <tr>
                          <td><?php echo $event_name; ?></td>
                          <td><?php echo $trans_code; ?></td>
                          <td><?php echo $payer_phone; ?></td>
                          <td><?php echo $amount; ?></td>

                          </td>
                        </tr>

                        <?php

                            }

                        ?>

<!--                         <tr><td><?php //echo $total; ?></td></tr> -->

                      </tbody>
                  
                   </table>

                 </div>

                 <div class="my-flex-item managerAccounting">

                   <table class="table table-striped">
                      <thead>
                        <tr>
                          <th scope="col"></th>
                          <th scope="col">Data</th>
                        </tr>
                      </thead>

                      <tbody>

                        <?php

                            $totalEvents = $payment->count_events_by_owner_cat($OwnerId,"Movies");
                            $totalAmount = $payment->sum_of_events_total_by_owner_cat($OwnerId,"Movies");

                            $percantage_total = $totalAmount * 0.06;

                            $final_pay = $totalAmount - $percantage_total;

                        ?>

                        <tr>
                            <th>Total Events</th>
                            <td><?php echo $totalEvents; ?></td>
                        </tr>

                        <tr>
                            <th>Total Amount</th>
                            <td><?php echo $totalAmount; ?></td>
                        </tr>

                        <tr>
                            <th>% Pay Per Event</th>
                            <td><b>6%</b></td>
                        </tr>

                        <tr>
                            <th>% Pay Total</th>
                            <td><?php echo $percantage_total; ?></td>
                        </tr>

                        <tr>
                            <th>Your Amount After % Pay</th>
                            <td><?php echo $final_pay; ?></td>
                        </tr>

                      </tbody>

                    </table>

                  </div>

              </div><!-- end of tellerPanel div -->


          </div>

    </div>


<div class="mdl-grid" id="footer">

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Tickets</h2>
      <p><a href="index.php" style="text-align : center; color : #fff; text-decoration: none; ">Confirm Ticket</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Contact Us</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>


  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Our Products</h2>

  </div>

</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/scripting/e-shama.js"></script>
    <script src="Front End/ShamaScript.js"></script>

  <!--side bars-->
      
</body>
</html>




