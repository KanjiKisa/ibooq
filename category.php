<?php 
  
    require_once("Customers/includes/initialise.php");

     if(!isset($_GET["type"])) {

          header("Location: index.php");

      } else {

          $keyword = $_GET["type"];

      }
    

  
  ?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tickets to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book reserve in kenya, get ticket to reserve kenya, get ticket for reserves, ticket reserves, online reserve and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an reserve for free kenya">

     <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway|Montserrat" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style2.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/styling/style.css" />-->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />
	 <!-- Bootstrap core CSS -->    <link href="Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">    <link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css"> 
</head>
<body >
<div class="navigation fixed-top">
        <div class="logo">
            <a href="index.php"><img src="Front End/Images/pop-in.png"></a>
        </div>
        <div class="link">
            <a href="index.php">Home</a>
        </div>
</div>
      <br><br><br><br>
    <!--End Js slide-->
<section id="reserve">
      <!--<div class="d flex flex-column" id="shama_section">

            <div class="my-flex-item">-->
              <hr>
              <h2 style=" color : #96980f; font-family: 'Montserrat';">Category : <?php echo $keyword; ?></h2>
              <hr>


               <!--<div class="mdl-grid" style="justify-content: center; ">-->
              

                    <?php 

                      $sql = $database->search_reserves_category($keyword);

                      foreach($sql as $row) {

                          $category = $row["Category"];
                          $reserveId = $row["ReserveId"];
                          $ownerId = $row["OwnerId"];
                          $reserveName = $row["ReserveName"];
                          $reserveDesc = $row["ReserveDesc"];
                          $picPath = $row["PicPath"]; 
                          $reserveStock = $row["ReserveStock"];						  						  
                          $reserveTotalSeats = $row["ReserveTablesTotal"];
                          $reservePrice = $row["ReservePrice"];
                          $reserveLocation = $row["ReserveLocation"];
                          $reserveDate = $row["ReserveDate"];
                          $reserveTime = $row["ReserveTime"];						  						  
                          $total_seats_booked = $payment->fetch_total_tables_booked($reserveId);						 							 
                          $remaining_seats = ($reserveTotalSeats * $reserveStock) - $total_seats_booked;							 							
                          if($remaining_seats <= 0 ){								 								
                              $database->update_rs_vacancy_status($reserveId);		 								 							 }

                    ?>
                    
                     <div class="card">
                        <div class="card-header"><h5 class="card-title" style="font-family:'Oswald';"><?php echo $category; ?></h5></div>
                           <div class="card-body">
                              <h5 class="card-subtitle"><span class="span">Capacity:</span><?php echo $reserveStock; ?> Seats @ Ksh <?php echo $reservePrice; ?> per seat</h5>
                              <h6 class="card-subtitle-two"><i><?php echo $remaining_seats; ?> Seat(s) Remaining</i></h6>
                              <a href=".modal" data-toggle="modal" data-book-id="<?php echo $reserveId; ?>" class="btn btn-primary">Reserve this table</a>
                            </div>
                    </div>
       

                    <!-- <div class="card mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone">					   
                     <div class="card-body">					   
                     <h5 class="card-title"><?php echo $category; ?></h5>					  
                     <span class="reserve-rem"><i><?php echo $remaining_seats; ?> Seat(s) Remaining</i></span>					   
                     <img class="card-img-top" src="<?php echo $picPath; ?>" alt="" height="80px">					   
                     <br/>					   
                     <h6 class="card-title"><i><?php echo $reserveName; ?></i></h6>						
                     <h6 class="card-subtitle mb-2"><?php echo $reserveStock; ?> Seater| <?php echo $reservePrice; ?> per person</h6>						
                     <a href=".modal" data-toggle="modal" data-book-id="<?php echo $reserveId; ?>" class="btn btn-primary">Reserve</a>-->					   </div>					</div>

                    <?php 


                       }


                    ?>


               <!-- </div>

          </div>-->

</section>
        
   <div class="modal" tabindex="-1" role="dialog" id="payModal" >

      <div class="modal-dialog" role="document">

        <div class="modal-content" >

          <div class="modal-header">
            <h5 class="modal-title" style="text-align : center; color : #ff3c4e; font-family: 'Josefin Slab';">PAY FOR EVENT</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body" >
                    <!-- <ul id="m-pesa_guide">
                         <li> Go to M-Pesa on your phone</li>
                         <li> Select Buy Goods & Services option</li>
                         <li> Enter Till no. 576804</li>
                         <li> Enter the right amount and finalize</li>
                         <li> You will receive a confirmation sms from M-Pesa</li>
                         <li> <i>THREE DOTS FINANCIAL</i></li>
                         <li> Fill the fields below with correct information.</li>
                         <li> Click on Complete</li>

                       </ul>-->

                      <form action="ipay.php" method="post" >

                        <div id="display"></div>
                          
                          <div class="form-group">
                           <input type="hidden" class="form-control" id="reserve_id" name="reserve_id" required="required">
                         </div>

                         <div class="form-group">
                            <input type="number" class="form-control" id="capacity" placeholder="Number of tables to reserve" name="table_capacity" onkeyup="calculate_price('display','capacity','reserve_id')">
                         </div>


                          <div class="form-group">
                           <input type="text" class="form-control" id="user_phone" name="user_phone" placeholder="Enter Mobile Number" required="required">
                          </div>
       
                         
                         <div class="form-group" >
                           <input type="text" class="form-control" id="buyer_email" name="buyer_email" placeholder="Enter a valid e-mail" required="required">
                         </div>
             
                         <div class="form-group">
                           <button type="submit" class="btn btn-primary" id="subMpesa" name="subMpesa">Complete</button>
                            <br>
                            <input type="checkbox" required="required"> Agree to <a href="terms.html" style="color: #ff3c4e; text-decoration: none; ">Terms & conditions</a>
                         </div>

                     </form>
          </div>

          <div class="modal-footer">

          </div>

        </div>

      </div>

    </div>
    
<!--footer section--->

 <footer class="bg-dark" id="footer" style="background:#000; padding-top:1.2rem">
       <div class="container">
           <!--footer row-->
           <div class="row">
               <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Pop-In</h2>
                  <hr style="border: 1px solid #fff;">
                  <p style="font-family:'Raleway';">Pop-in to buy advance tickets to upcoming events</p>
              </div>
              
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Contact us</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <li style="list-style: none; font-family:'Raleway';"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;font-family:'Raleway';"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2 style="font-family:'Oswald'; color:#df3d82;">Navigation</h2>
                  <hr style="border: 1px solid #fff;">
                 <ul>
                     <a href="login.php"><li style="list-style: none;font-family:'Raleway';">Login</li></a>
                     <a href="register.php"><li style="list-style: none;font-family:'Raleway';">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;font-family:'Raleway';">Terms & Conditions</li></a>
                 </ul>
              </div>
           </div>
           <!--end of footer row-->
       </div>
       
       <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p style="font-family:'Raleway';">&copy copyright Pop-In all Rights Reserved: Developed with <i class="fas fa-heart"></i> by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i style="font-size:40px; color:#3b5998" class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i style="font-size:40px; color:#c32aa3"class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
   </footer>

<!--<div class="mdl-grid" id="footer">
    
    <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">About Pop-In</h2>
      <p><a href="index.php" style="text-align : center; color : #fff; text-decoration: none; ">Pop-in is a reservation system for clubs, restaurant and other entertainment facilities. With this system you can manage your guestlist, offer discounts on drinks and other services</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">We are social</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>

  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Quick Links</h2>
    
    <ul class="links">
          <li><a href="">Support</a></li>
          <li><a href="">Privacy Policy</a></li>
          <li><a href="">Terms & Conditions</a></li>
        </ul>

  </div>


</div>-->

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>		<!-- Bootstrap core JavaScript -->    <script src="Front End/vendor/jquery/jquery.min.js"></script>    <script src="Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> 		<script>         $(document).ready(function(){          $('.modal').on('show.bs.modal', function(e) {            var payId = $(e.relatedTarget).data('book-id');            $(e.currentTarget).find('input[name="reserve_id"]').val(payId);          });        });    </script>
  <!--side bars-->

</body>
</html>