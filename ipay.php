<?php

	require_once("Customers/includes/initialise.php");



?>

<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tables to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tables from the comfort of your device.
                                Book reserve in kenya, get table to reserve kenya, get table for reserves, table reserves, online reserve and table system, 
                                buy table using mpesa, easy way to buy table kenya, easy and cheap way to post an reserve for free kenya">
                                
  <meta name="keywords" content="Are you an reserve organizer or promoter looking for an easy way to sell advance tables to your reserve, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of reserves from parties, launches e.t.c and buy advance tables from the comfort of your device.
                                Book reserve in kenya, get table to reserve kenya, get table for reserves, table reserves, online reserve and table system, 
                                buy table using mpesa, easy way to buy table kenya, easy and cheap way to post an reserve for free kenya">

  <meta http-equiv="Cache-control" content="no-cache">
  <meta http-equiv="Expires" content="-1">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

</head>
<body >
<nav class="navbar navbar-expand-lg navbar-light  fixed-top" id="navigator" >
  <a class="navbar-brand" href="index.php"><img  src="Front End/Images/pop-in.png"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    Menu <i class="fas fa-bars"></i>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a  href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a  href="login.php">Log-In</a>
      </li>
      <li class="nav-item">
        <a href="register.php">Register</a>
      </li>
    </ul>
  </div>
</nav>
      
<br><br>

    <div class="d flex flex-column" id="shama_section">


                  <div class="my-flex-item">


               <div class="mdl-grid" style="justify-content: center; ">
                   
                   <div class="mdl-cell--10-col mdl-cell--10-col-tablet mdl-cell--10-col-phone" style="margin-left: 10px; margin-top: 10px;">
              
               		<?php
                        
                        if(isset($_POST["subMpesa"])) {
                            $Mobile_No = $_POST["user_phone"];
                            $Email = $_POST["buyer_email"];
                            $ReserveId = $_POST["reserve_id"];
                            $TableCapacity = $_POST["table_capacity"];
                            
                            $automation->generate_cart_id();
                            
                            $OrderId = $automation->cart_id;
                            
                            $database->fetch_specific_reserve_data($ReserveId);
                            
                            $ReserveOwner = $database->fetched_user_id;
                            
                            $ReservePrice = $database->fetched_rs_price * $TableCapacity;
                            
                            $payment->set_order_id($OrderId);
                            $payment->set_phone($Mobile_No);
                            $payment->set_email($Email);
                            $payment->set_amount($ReservePrice);
                            
                            //$payment->set_service_name("mpesa");
                            
                            $result = $payment->initiator_request();
                            
                            //print_r($result);
                            
                            $sid = json_decode($result)->data->sid;
                            
                                   // print_r($sid);
                                   
                               // echo "Wait as we configure you";
                                    
                            /*echo '<h2></h2>';
                              
                            echo '
                            <br><br>
                            <form method="post" action="">
                                
                                <input type="hidden" name="sid" value="'.$sid.'">
                                
                                 <input type="hidden" name="mobile" value="'.$Mobile_No.'">
                                
                                <input type="hidden" name="oid" value="'.$payment->oid.'">
                                
                                <input type="hidden" name="email" value="'.$Email.'">
                                
                                <input type="hidden" name="reserve_id" value="'.$ReserveId.'">
                                
                                <input type="hidden" name="capacity" value="'.$TableCapacity.'">
                                
                                <input type="submit" name="Pay" value="Pay">
                                
                            </form>';*/  
                            
                              $fields = [
                                        'sid1' => $sid,
                                        'mobile1' => $Mobile_No,
                                        'oid1' => $payment->oid,
                                        'email1' => $Email,
                                        'reserve_id1' => $ReserveId,
                                        'capacity1' => $TableCapacity
                                      ];

                            $url = 'http://reservations.popin.co.ke/ipay_values.php';
                            $data_string = json_encode($fields);
                            
                            $curl = curl_init($url);
                            curl_setopt($curl, CURLOPT_POST, 1);
                            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
                            curl_setopt($curl, CURLOPT_POSTFIELDS, urlencode($data_string));
                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                            $curl_response = curl_exec($curl);
                            
                            //echo $curl_response;
                            
                            // after initiating the request, you need to trigger stk push by the code below
                            
                            $oid2 = json_decode($curl_response)->oid1;
                            
                            $Mobile_No2 = json_decode($curl_response)->mobile1;
                            
                            $Email2 = json_decode($curl_response)->email1;
                            $ReserveId2 = json_decode($curl_response)->reserve_id1;
                            $TableCapacity2 = json_decode($curl_response)->capacity1;
                            
                            $payment->set_sid(json_decode($curl_response)->sid1);
                            
                           $payment->set_phone($Mobile_No2);
                           
                           $status = json_decode($payment->process_initiator_stk())->header_status;
                           
                            //print_r(json_decode($payment->process_initiator_stk()));
                            
                            if($status == 200){
                                
                                echo "<h2>Confirm Payment</h2>";
                                
                                echo "<ul>
                                        <li>You will be redirected to your M-Pesa menu shortly...</li>
                                        <li>After paying, click the button below.</li>
                                    </ul>";
                                
                               // echo $oid2;
                                
                                echo '<form method="post" action="process_ipay.php">
                                    
                                    <input type="hidden" name="oid1" value="'.$oid2.'">
                                    
                                    <input type="hidden" name="email" value="'.$Email2.'">
                                
                                    <input type="hidden" name="reserve_id" value="'.$ReserveId2.'">
                                
                                    <input type="hidden" name="capacity" value="'.$TableCapacity2.'">
                                    
                                    <input type="submit" class="btn" name="ConfirmPay" value="Confirm Your Payment">
                                    
                                </form>';
                                
                                
                            } else { // if stk push was unsuccessful
                                 $alert->message("Transaction could not be completed","Fail");
                                echo '<a href="index.php"><button>Return Home</button></a>';
                            }
                            
                        } else {
                            header("Location: index.php");
                        }
                        
                      
                                
                    ?>
                    
                    </div>

                </div><!-- close of mdl-grid class div--> 

          </div>

    </div>

<footer class="bg-dark" id="footer">
      <div class="container">
          <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Pop-In</h2>
                  <hr>
                  <p>Pop-in to buy advance tables to upcoming reserves</p>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Contact us</h2>
                  <hr>
                 <ul>
                     <li style="list-style: none;"><i class="fas fa-at"></i>support@popin.co.ke</li>
                     <li style=" list-style: none;"><i class="fas fa-phone"></i>+254737714245</li>
                 </ul>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-12">
                  <h2>Navigation</h2>
                  <hr>
                 <ul>
                     <a href="login.php"><li style="list-style: none;">Login</li></a>
                     <a href="register.php"><li style="list-style: none;">Register</li></a>
                     <a href="terms.html"><li style="list-style: none;">Terms & Conditions</li></a>
                 </ul>
              </div>
          </div>
      </div>
      <div class="container-fluid">
          <div class="row">
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <p>&copy copyright Pop-In all Rights Reserved: Powered by <span><a href="">Konectify Technologies</a></span></p>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-12">
                  <ul clas="ml-auto">
                      <li style="display: inline-block; list-style: none;"><a href="https://www.facebook.com/popinke"><i class="fab fa-facebook-square"></i></a></li>
                      <li style="display: inline-block; list-style: none;"><a href="https://www.instagram.com/popin254/"><i class="fab fa-instagram"></i></a></li>
                  </ul>
              </div>
          </div>
      </div>
    </footer>
    
     <style>
        h2{
            font-family: 'Oswald';
            
        }
        
        .btn{
            background: #ad67ea;
            background: #ad67ea;
  font-size: 0.9rem;
  padding: 15px 25px;
  color: #fff;
  text-transform: uppercase;
  font-family: 'Varela Round';
  border: 1px solid #fff;
  border-radius: 5px;
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
        }
        
        ul{
            list-style: none;
        }
        
        ul li{
            font-family: 'Raleway';
        }
    </style>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->
      
</body>
</html>