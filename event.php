  <?php 
  
    require_once("Customers/includes/initialise.php");
    
    if(isset($_GET['ev_id'])) {

            $event_id = $_GET['ev_id'];

            $database->fetch_specific_event_data($event_id);


            $amount = $payment->sum_of_specific_events_total($event_id);

            $withdrawn_amount = $amount * 0.96;

            

        } else {

            header("Location: index.php");


        }
  
  ?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>POP-IN</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="Are you an event organizer or promoter looking for an easy way to sell advance tickets to your event, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of events from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book event in kenya, get ticket to event kenya, get ticket for events, ticket events, online event and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an event for free kenya">

  <meta name="keywords" content="Are you an event organizer or promoter looking for an easy way to sell advance tickets to your event, sign-up for free to pop-in to 
                                get started. Pop-in to browse hundreds of events from parties, launches e.t.c and buy advance tickets from the comfort of your device.
                                Book event in kenya, get ticket to event kenya, get ticket for events, ticket events, online event and ticket system, 
                                buy ticket using mpesa, easy way to buy ticket kenya, easy and cheap way to post an event for free kenya">

    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Front End/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />

</head>
<body >
    

     
     <!-- <div class="navbar navbar-expand-lg navbar-static-top" id="navigator" >
          <div class="container">

              <h2 class="navbar-brand brand-name">
                    <a href="index.php" class="pull-left"><img class="img-responsive2"       
                    src="Front End/Images/logo.png"> </a>
              </h2>

            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
              <span class="navbar-toggle-icon"></span>
            </button> 

            <div class="collapse navbar-collapse" id="navbarSupportedContent" style="justify-content: center;">
              <ul class="navbar-nav ml-auto menuBar navbar-right">

                <a class="nav-item menuItem" href="index.php" style="text-decoration: none; font-family: 'Josefin Slab'; color : #fff; font-size: 40px; padding: 30px;">
                  <li class="nav-link">HOME</li>
                </a>

              </ul>
              
            </div>


        </div>

  </div>-->

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

          <div class="my-flex-item mdl-grid" style="justify-content: center; ">


                  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone event-data" id="event_poster">

                    <img src="<?php echo $database->fetched_ev_photo_path; ?>" >

                  </div>

                  <div class="mdl-cell--8-col mdl-cell--8-col-tablet mdl-cell--4-col-phone event-data" id="event_poster_details" style="margin-bottom: 15px; margin-top: 5px;">

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f; ">
                      CATEGORY 
                      <span style="margin-left:5px; color: #000;  "><?php echo $database->fetched_ev_category; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      EVENT NAME <span style="margin-left:5px; color: #000;"><?php echo $database->fetched_ev_name; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      EVENT PRICE 
                      <span style="margin-left:5px; color: #000; "><?php echo $database->fetched_ev_price; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f; ">
                      EVENT LOCATION <span style="margin-left:5px; color: #000;  "><?php echo $database->fetched_ev_location; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      START DATE <span style="margin-left:5px; color: #000;  "><?php echo $database->fetched_ev_start_date; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      START TIME <span style="margin-left:5px; color: #000;  "><?php echo $database->fetched_ev_start_date; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f; ">
                      STOP DATE <span style="margin-left:5px; color: #000; "><?php echo $database->fetched_ev_stop_date; ?></span>
                    </span>

                    <br/><br/>

                    <span class="event_poster_details" style="font-family: 'Josefin Slab'; color: #96980f;  ">
                      STOP TIME <span style="margin-left:5px; color: #000; "><?php echo $database->fetched_ev_stop_time; ?></span>
                    </span>

                    <div class="nav-toggle image view view-first" style="background:#ff3c4e; cursor: pointer; left:60%; border: 1px solid #ff3c4e; width: 20%; border-radius: 5px; ">
                                <a href=".modal" data-toggle="modal" data-book-id="<?php echo $event_id; ?>" style="text-decoration: none; color:#fff; ">Buy Ticket</a>
                              </div>

                    
                  </div>


          </div>

        </div>
        
<div class="modal" tabindex="-1" role="dialog" id="payModal" >

      <div class="modal-dialog" role="document">

        <div class="modal-content" >

          <div class="modal-header">
            <h5 class="modal-title" style="text-align : center; color : #ff3c4e; font-family: 'Josefin Slab';">PAY FOR EVENT</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body" >
                     <ul id="m-pesa_guide">
                         <li> Go to M-Pesa on your phone</li>
                         <li> Select Buy Goods & Services option</li>
                         <li> Enter Till no. 576804</li>
                         <li> Enter the right amount and finalize</li>
                         <li> You will receive a confirmation sms from M-Pesa</li>
                         <li> <i>THREE DOTS FINANCIAL</i></li>
                         <li> Fill the fields below with correct information.</li>
                         <li> Click on Complete</li>

                       </ul>

                      <form action="reserve_pay.php" method="post" >
                          
                           <div id="display"></div>
                          
                          <div class="form-group">
                           <input type="hidden" class="form-control" id="reserve_id" name="reserve_id" required="required">
                         </div>

                         <div class="form-group">
                            <input type="number" class="form-control" id="capacity" placeholder="Number Of Tickets" name="ticket_capacity" onkeyup="calculate_price('display','capacity','reserve_id')">
                         </div>

                          <div class="form-group">
                           <input type="text" class="form-control" id="mpesa_code" name="mpesa_code" placeholder="Enter Transaction ID" required="required">
                          </div>
                          
                         <div class="form-group" >
                           <input type="email" class="form-control" id="buyer_email" name="buyer_email" placeholder="Enter a valid e-mail" required="required">
                         </div>
             
                         <div class="form-group">
                           <button type="submit" class="btn btn-primary" id="subMpesa" name="subMpesa">Complete</button>
                            <br>
                            <input type="checkbox" required="required"> Agree to <a href="terms.html" style="color: #ff3c4e; text-decoration: none; ">Terms & conditions</a>
                         </div>

                     </form>
          </div>

          <div class="modal-footer">

          </div>

        </div>

      </div>

    </div>

<div class="mdl-grid" id="footer">

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-menu">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Tickets</h2>
      <p><a href="index.php" style="text-align : center; color : #fff; text-decoration: none; ">Confirm Ticket</a></p>
  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footer-contact">

    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Contact Us</h2>
      <p style="text-align : center; color : #fff;">Location : Limuru.</p>

  </div>

  <div class="mdl-cell--4-col mdl-cell--8-col-tablet mdl-cell--4-col-phone footer-data" id="footerMap">
  
    <h2 style="text-align : center; color : #96980f; font-family: 'Josefin Slab';">Our Products</h2>

  </div>

</div>

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>

     <script>

         $(document).ready(function(){
          $('.modal').on('show.bs.modal', function(e) {

            var payId = $(e.relatedTarget).data('book-id');

            $(e.currentTarget).find('input[name="event_id"]').val(payId);

          });

        });

    </script>

  <!--side bars-->

</body>
</html>




