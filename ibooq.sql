-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2020 at 04:46 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ibooq`
--

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_c2b_data`
--

CREATE TABLE `mpesa_c2b_data` (
  `Receipt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` int(11) NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TransactionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_data`
--

CREATE TABLE `mpesa_data` (
  `MerchantRequestID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CheckoutRequestID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Receipt` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `Amount` int(11) NOT NULL,
  `TransactionDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mpesa_data`
--

INSERT INTO `mpesa_data` (`MerchantRequestID`, `CheckoutRequestID`, `Receipt`, `Phone`, `Amount`, `TransactionDate`) VALUES
('20421-8059609-1', 'ws_CO_180220201311196064', 'OBI064CZRG', '254705972361', 1, '2020-02-18 13:11:38'),
('20412-8173095-1', 'ws_CO_180220201423269587', 'OBI066G98A', '254705972361', 1, '2020-02-18 14:23:41'),
('20413-7987330-1', 'ws_CO_180220201216064107', 'OBI162R3JT', '254705972361', 1, '2020-02-18 12:16:15'),
('31553-1543030-1', 'ws_CO_180220201356242264', 'OBI165OGKP', '254705972361', 1, '2020-02-18 13:56:40'),
('30189-1421028-1', 'ws_CO_180220201222076554', 'OBI362XBQ5', '254705972361', 1, '2020-02-18 12:22:21'),
('31552-1550171-1', 'ws_CO_180220201400547819', 'OBI465TBX0', '254705972361', 1, '2020-02-18 14:01:22'),
('17118-8139465-1', 'ws_CO_180220201352019800', 'OBI765JZCP', '254705972361', 1, '2020-02-18 13:52:24'),
('30175-1584082-1', 'ws_CO_180220201414250722', 'OBI9666WLX', '254705972361', 1, '2020-02-18 14:14:41'),
('31553-3423168-1', 'ws_CO_190220201314090349', 'OBJ36Y7RFJ', '254705972361', 1, '2020-02-19 13:14:23'),
('17059-34568927-1', 'ws_CO_190220201318487485', 'OBJ86YCQ4Q', '254705972361', 1, '2020-02-19 13:19:01'),
('30178-7944634-1', 'ws_CO_210220201822307762', 'OBL08WQUYK', '254705180969', 1, '2020-02-21 18:22:28'),
('7453-6090652-1', 'ws_CO_280220201600055345', 'OBS1EILP49', '254705972361', 1, '2020-02-28 16:00:22'),
('7459-6512358-1', 'ws_CO_280220202013272856', 'OBS1ESXJUH', '254705180969', 1, '2020-02-28 20:13:37'),
('4869-6445693-1', 'ws_CO_280220201927429334', 'OBS3EQRKU1', '254705972361', 1, '2020-02-28 19:27:57'),
('25562-6510078-1', 'ws_CO_280220202000521196', 'OBS3ESDM5V', '254705972361', 1, '2020-02-28 20:01:11'),
('25566-6458423-1', 'ws_CO_280220201934041014', 'OBS5ER3BNH', '254705972361', 1, '2020-02-28 19:34:19'),
('11220-6468734-1', 'ws_CO_280220201936581443', 'OBS7ER8J2H', '254705972361', 1, '2020-02-28 19:37:17'),
('7447-6451063-1', 'ws_CO_280220201942167569', 'OBS7ERHXDX', '254705972361', 1, '2020-02-28 19:42:32'),
('25568-6495490-1', 'ws_CO_280220201953137202', 'OBS7ES194V', '254705972361', 1, '2020-02-28 19:53:34'),
('4863-5920377-1', 'ws_CO_280220201358097927', 'OBS9EEY7FP', '254705972361', 1, '2020-02-28 13:57:44'),
('25561-12126335-1', 'ws_CO_020320201632269960', 'OC28H2TZNC', '254705972361', 1, '2020-03-02 16:32:46'),
('14681-388553-1', 'ws_CO_050320202241391046', 'OC51K32DHV', '254705972361', 1, '2020-03-05 22:40:51'),
('14676-38189-1', 'ws_CO_050320201928539310', 'OC55JW8BDR', '254705972361', 1, '2020-03-05 19:29:05'),
('1998-19107805-1', 'ws_CO_060320200957255693', 'OC60K9640Q', '254705972361', 1, '2020-03-06 09:57:40'),
('2000-19487801-1', 'ws_CO_060320201418435675', 'OC61KH2E4J', '254729074249', 1, '2020-03-06 14:18:58'),
('8386-1035856-1', 'ws_CO_060320201018325566', 'OC62K9SOUG', '254705972361', 1, '2020-03-06 10:18:46'),
('2004-19238845-1', 'ws_CO_060320201122063017', 'OC62KBOXAY', '254705972361', 1, '2020-03-06 11:21:12'),
('8393-1173021-1', 'ws_CO_060320201144101024', 'OC62KCE6F0', '254705972361', 2, '2020-03-06 11:44:24'),
('9844-19338011-1', 'ws_CO_060320201202403893', 'OC63KCY72N', '254705972361', 1, '2020-03-06 12:02:51'),
('14680-1298569-1', 'ws_CO_060320201235307643', 'OC63KDWFH1', '254705972361', 1, '2020-03-06 12:34:34'),
('2000-19335949-1', 'ws_CO_060320201231027267', 'OC67KDSWPJ', '254705972361', 1, '2020-03-06 12:31:16'),
('9838-19573375-1', 'ws_CO_060320201451462136', 'OC67KI1CYR', '254705972361', 1, '2020-03-06 14:50:49'),
('1993-19144590-1', 'ws_CO_060320201022115058', 'OC69K9VFC7', '254705972361', 1, '2020-03-06 10:21:16'),
('19948-781470-1', 'ws_CO_090320200805329282', 'OC91MRBE61', '254705972361', 1, '2020-03-09 08:05:48'),
('31591-794653-1', 'ws_CO_090320200835086731', 'OC91MRYR6Z', '254705972361', 2, '2020-03-09 08:34:00'),
('11615-791284-1', 'ws_CO_090320200822407553', 'OC92MRPE7M', '254705972361', 1, '2020-03-09 08:22:54'),
('28927-601135-1', 'ws_CO_090320202250008358', 'OC92NJ2YAY', '254705972361', 1, '2020-03-09 22:48:47'),
('4862-24655471-1', 'ws_CO_090320200814170553', 'OC93MRICW7', '254705972361', 1, '2020-03-09 08:14:31'),
('9843-26066661-1', 'ws_CO_090320202256023428', 'OC93NJ5JT3', '254705180969', 1, '2020-03-09 22:56:14'),
('9838-26098082-1', 'ws_CO_090320202337221811', 'OC93NJGWDZ', '254705972361', 1, '2020-03-09 23:36:12'),
('29782-652540-1', 'ws_CO_090320202353258155', 'OC93NJJMJV', '254705972361', 1, '2020-03-09 23:52:16'),
('28925-619215-1', 'ws_CO_090320202303420971', 'OC95NJ7QCN', '254705972361', 1, '2020-03-09 23:02:37'),
('31583-781790-1', 'ws_CO_090320200824457271', 'OC96MRR46K', '254705972361', 1, '2020-03-09 08:25:01'),
('19954-791091-1', 'ws_CO_090320200812117493', 'OC97MRGUGV', '254705972361', 1, '2020-03-09 08:12:29'),
('7050-2708161-1', 'ws_CO_100320200833420061', 'OCA0NMC35A', '254705972361', 1, '2020-03-10 08:33:56'),
('28937-1148656-1', 'ws_CO_100320200850032640', 'OCA0NMP0PU', '254705972361', 1, '2020-03-10 08:48:51'),
('29786-1066830-1', 'ws_CO_100320200753409093', 'OCA1NLGIAN', '254705972361', 1, '2020-03-10 07:54:02'),
('9844-26596919-1', 'ws_CO_100320200846456944', 'OCA1NMNFXZ', '254705972361', 1, '2020-03-10 08:46:59'),
('28933-1442323-1', 'ws_CO_100320201223582809', 'OCA1NSPQDV', '254705972361', 1, '2020-03-10 12:22:56'),
('9842-26569870-1', 'ws_CO_100320200826551455', 'OCA3NM69IP', '254705972361', 1, '2020-03-10 08:27:11'),
('28925-1104702-1', 'ws_CO_100320200819481380', 'OCA4NM0CFM', '254705972361', 1, '2020-03-10 08:20:02'),
('5518-8436022-1', 'ws_CO_100320200831030905', 'OCA4NM9YGU', '254705972361', 1, '2020-03-10 08:31:20'),
('7047-2692074-1', 'ws_CO_100320200823556018', 'OCA5NM2H3N', '254705972361', 1, '2020-03-10 08:22:42'),
('5512-8443796-1', 'ws_CO_100320200835495600', 'OCA6NMDUT4', '254705972361', 1, '2020-03-10 08:36:06'),
('28925-1446629-1', 'ws_CO_100320201225084922', 'OCA6NSSIXG', '254705972361', 1, '2020-03-10 12:25:29'),
('29786-1149777-1', 'ws_CO_100320200850429914', 'OCA7NMQU65', '254705972361', 1, '2020-03-10 08:50:56'),
('7049-2630075-1', 'ws_CO_100320200739365836', 'OCA8NL7FJI', '254705972361', 1, '2020-03-10 07:40:01'),
('28929-1134504-1', 'ws_CO_100320200839228456', 'OCA9NMH1V5', '254705972361', 1, '2020-03-10 08:39:36'),
('29782-3918799-1', 'ws_CO_110320201823344507', 'OCB0OYUPIE', '254705972361', 1, '2020-03-11 18:23:43'),
('5517-11246047-1', 'ws_CO_110320201830298449', 'OCB1OZ2Q81', '254705972361', 1, '2020-03-11 18:29:10'),
('4863-29405901-1', 'ws_CO_110320201827182136', 'OCB5OYXY69', '254705972361', 1, '2020-03-11 18:25:56'),
('4873-29420524-1', 'ws_CO_110320201834364622', 'OCB8OZB8LO', '254705972361', 1, '2020-03-11 18:34:46'),
('18312-2087446-1', 'ws_CO_190320200736384569', 'OCJ0V12JKW', '254705972361', 1, '2020-03-19 07:36:52'),
('19227-1647589-1', 'ws_CO_190320201107057458', 'OCJ1V1BX2N', '254705972361', 1, '2020-03-19 07:53:33'),
('24950-2102308-1', 'ws_CO_190320201102051535', 'OCJ4V193G4', '254705972361', 1, '2020-03-19 07:48:33'),
('18311-2097597-1', 'ws_CO_190320200751155571', 'OCJ5V1APJT', '254705972361', 1, '2020-03-19 07:51:26'),
('24939-2068593-1', 'ws_CO_190320201011043142', 'OCJ6V0MDTQ', '254705972361', 1, '2020-03-19 06:57:30'),
('17757-1666582-1', 'ws_CO_190320200817399210', 'OCJ6V1T5D2', '254705972361', 1, '2020-03-19 08:17:59'),
('18315-2133854-1', 'ws_CO_190320200840477268', 'OCJ9V29R9V', '254705972361', 1, '2020-03-19 08:39:00'),
('14327-3416334-1', 'ws_CO_200320201017370428', 'OCK0VZ5VY4', '254705972361', 1, '2020-03-20 10:15:43'),
('14327-3366248-1', 'ws_CO_200320200922517938', 'OCK1VXQZUL', '254705972361', 1, '2020-03-20 09:23:05'),
('10206-3432047-1', 'ws_CO_200320201033288263', 'OCK2VZM30Q', '254705972361', 1, '2020-03-20 10:31:35'),
('14323-3363750-1', 'ws_CO_200320200920112400', 'OCK5VXOGYR', '254705972361', 1, '2020-03-20 09:20:24'),
('12709-2966688-1', 'ws_CO_200320201023572983', 'OCK5VZEF41', '254705972361', 1, '2020-03-20 10:24:11'),
('10205-3479068-1', 'ws_CO_200320201116201248', 'OCK5W0WK0B', '254705972361', 1, '2020-03-20 11:16:35'),
('12711-3682253-1', 'ws_CO_200320202145437287', 'OCK5WM79E1', '254727683082', 5, '2020-03-20 21:45:51'),
('14320-3360012-1', 'ws_CO_200320200918247369', 'OCK6VXKUU2', '254705972361', 1, '2020-03-20 09:16:32'),
('28989-3466511-1', 'ws_CO_200320201114331911', 'OCK6W0SF5Q', '254705972361', 1, '2020-03-20 11:12:40'),
('12705-3023790-1', 'ws_CO_200320201120192293', 'OCK6W0YAVY', '254705972361', 1, '2020-03-20 11:18:25'),
('12706-2880308-1', 'ws_CO_200320200853322101', 'OCK7VWZ3HT', '254705972361', 1, '2020-03-20 08:51:38'),
('10208-3401261-1', 'ws_CO_200320201001550035', 'OCK7VYSGKX', '254705972361', 1, '2020-03-20 10:02:12'),
('28990-3459606-1', 'ws_CO_200320201106312529', 'OCK7W0KA23', '254705972361', 1, '2020-03-20 11:04:52'),
('28376-2997958-1', 'ws_CO_200320201106043813', 'OCK7W0LV0X', '254705972361', 1, '2020-03-20 11:06:19'),
('29518-3667454-1', 'ws_CO_200320202127067474', 'OCK7WLOCWB', '254727683082', 2, '2020-03-20 21:25:07'),
('5957-4764119-1', 'ws_CO_210320201348273528', 'OCL2WZ0QEA', '254705180969', 1, '2020-03-21 13:48:37'),
('10201-6998606-1', 'ws_CO_230320201503333474', 'OCN2YIPKXC', '254705180969', 1, '2020-03-23 15:03:57'),
('32584-13193456-1', 'ws_CO_290320202330181820', 'OCT84KSIO4', '254705972361', 1, '2020-03-29 23:30:32'),
('12609-13960455-1', 'ws_CO_300320201330350402', 'OCU14VD01R', '254705972361', 1, '2020-03-30 13:28:03'),
('25067-4125454-1', 'ws_CO_300320201340293910', 'OCU24VR5SM', '254705972361', 1, '2020-03-30 13:40:42'),
('25067-4129210-1', 'ws_CO_300320201349218914', 'OCU24VXY1Y', '254705972361', 1, '2020-03-30 13:46:52'),
('25062-4123942-1', 'ws_CO_300320201338040708', 'OCU44VOD1Q', '254705972361', 1, '2020-03-30 13:38:15'),
('5734-5036714-1', 'ws_CO_300320201335111765', 'OCU54VI3W7', '254705972361', 1, '2020-03-30 13:32:39'),
('16941-13968398-1', 'ws_CO_300320201249484715', 'OCU64U7CSK', '254705972361', 1, '2020-03-30 12:50:04'),
('32579-13582337-1', 'ws_CO_300320201321298804', 'OCU64V5W7C', '254705972361', 1, '2020-03-30 13:21:43'),
('12618-13963815-1', 'ws_CO_300320201333232286', 'OCU74VJ7C9', '254705972361', 1, '2020-03-30 13:33:40'),
('12617-13962556-1', 'ws_CO_300320201331164638', 'OCU84VGQMO', '254705972361', 1, '2020-03-30 13:31:28'),
('32589-13599406-1', 'ws_CO_300320201349231713', 'OCU84W10G0', '254705972361', 1, '2020-03-30 13:49:37'),
('17890-5062538-1', 'ws_CO_300320201321071437', 'OCU94V2LK1', '254705972361', 1, '2020-03-30 13:18:34'),
('16943-14669965-1', 'ws_CO_310320201110035976', 'OCV05J2LWQ', '254705972361', 1, '2020-03-31 11:07:28'),
('12621-14821430-1', 'ws_CO_310320201437410430', 'OCV15PLBGX', '254705180969', 1, '2020-03-31 14:37:56'),
('17890-5749553-1', 'ws_CO_310320201105280104', 'OCV25J0V70', '254705972361', 1, '2020-03-31 11:05:47'),
('32575-14270324-1', 'ws_CO_310320201112128297', 'OCV25J815E', '254705972361', 1, '2020-03-31 11:12:32'),
('17886-5713958-1', 'ws_CO_310320201028044070', 'OCV35HV4XX', '254705972361', 1, '2020-03-31 10:25:32'),
('16943-14833107-1', 'ws_CO_310320201415096547', 'OCV65OVISK', '254705180969', 1, '2020-03-31 14:15:17'),
('12610-15742587-1', 'ws_CO_010420201229381709', 'OD136F9VOD', '254705972361', 1, '2020-04-01 12:29:51'),
('32587-15377739-1', 'ws_CO_010420201231044552', 'OD176F86DJ', '254705972361', 1, '2020-04-01 12:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `mpesa_errors`
--

CREATE TABLE `mpesa_errors` (
  `MerchantRequestID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `CheckoutRequestID` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ResultCode` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ResultDesc` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mpesa_errors`
--

INSERT INTO `mpesa_errors` (`MerchantRequestID`, `CheckoutRequestID`, `ResultCode`, `ResultDesc`) VALUES
('10208-3334575-1', 'ws_CO_200320200847195909', '1031', 'Request cancelled by user'),
('10209-3470086-1', 'ws_CO_200320201109094648', '17', 'Rule limited.'),
('12623-13958056-1', 'ws_CO_300320201326281005', '1037', 'DS timeout.'),
('12709-3653635-1', 'ws_CO_200320202122468153', '1037', 'DS timeout.'),
('12716-2879131-1', 'ws_CO_200320200850045038', '1031', 'Request cancelled by user'),
('12716-3657472-1', 'ws_CO_200320202125537358', '1032', 'Request cancelled by user'),
('13405-2066952-1', 'ws_CO_190320201010254906', '1032', 'Request cancelled by user'),
('14327-3467285-1', 'ws_CO_200320201104565031', '1031', 'Request cancelled by user'),
('14681-333567-1', 'ws_CO_050320202206207237', '1031', 'Request cancelled by user'),
('14681-35358-1', 'ws_CO_050320201927321453', '1031', 'Request cancelled by user'),
('16939-13993323-1', 'ws_CO_300320201331183241', '17', 'Rule limited.'),
('16939-14673200-1', 'ws_CO_310320201113300752', '1037', 'DS timeout.'),
('16943-14670165-1', 'ws_CO_310320201107410960', '1037', 'DS timeout.'),
('16947-14003341-1', 'ws_CO_300320201345029565', '1032', 'Request cancelled by user'),
('17757-1787563-1', 'ws_CO_190320201033034435', '1031', 'Request cancelled by user'),
('18317-2239588-1', 'ws_CO_190320201034310530', '1032', 'Request cancelled by user'),
('1998-18409151-1', 'ws_CO_050320202226011013', '1031', 'Request cancelled by user'),
('2004-18412654-1', 'ws_CO_050320202227558903', '1001', 'Unable to lock subscriber, a transaction is already in process for the current subscriber'),
('25059-4119334-1', 'ws_CO_300320201330356834', '1031', 'Request cancelled by user'),
('25067-4127356-1', 'ws_CO_300320201346241741', '1032', 'Request cancelled by user'),
('28385-5491039-1', 'ws_CO_220320201346321773', '1031', 'Request cancelled by user'),
('28927-630918-1', 'ws_CO_090320202318205301', '1031', 'Request cancelled by user'),
('28928-637624-1', 'ws_CO_090320202328572227', '1031', 'Request cancelled by user'),
('28931-595231-1', 'ws_CO_090320202244321834', '1031', 'Request cancelled by user'),
('28936-594077-1', 'ws_CO_090320202245102211', '1031', 'Request cancelled by user'),
('28937-524198-1', 'ws_CO_090320202157222627', '1037', 'DS timeout.'),
('29517-5833023-1', 'ws_CO_220320201907535165', '1', 'The balance is insufficient for the transaction'),
('29518-3693089-1', 'ws_CO_200320202148034890', '1032', 'Request cancelled by user'),
('29773-591873-1', 'ws_CO_090320202242170554', '1031', 'Request cancelled by user'),
('29774-597876-1', 'ws_CO_090320202246207990', '1031', 'Request cancelled by user'),
('29774-646929-1', 'ws_CO_090320202341499792', '1037', 'DS timeout.'),
('29779-512799-1', 'ws_CO_090320202151341530', '1031', 'Request cancelled by user'),
('29783-3309425-1', 'ws_CO_110320201155388159', '1', 'The balance is insufficient for the transaction'),
('29783-648163-1', 'ws_CO_090320202344490498', '1031', 'Request cancelled by user'),
('29785-1061892-1', 'ws_CO_100320200750243352', '1031', 'Request cancelled by user'),
('29786-3918010-1', 'ws_CO_110320201823068159', '1001', 'Unable to lock subscriber, a transaction is already in process for the current subscriber'),
('29786-632915-1', 'ws_CO_090320202320292837', '1031', 'Request cancelled by user'),
('29786-642098-1', 'ws_CO_090320202333517043', '1031', 'Request cancelled by user'),
('32583-14458678-1', 'ws_CO_310320201442112844', '1037', 'DS timeout.'),
('4864-26019087-1', 'ws_CO_090320202211019238', '1037', 'DS timeout.'),
('4865-18434671-1', 'ws_CO_050320202206580605', '1036', 'SMSC ACK timeout.'),
('4870-26069471-1', 'ws_CO_090320202245591370', '1031', 'Request cancelled by user'),
('4871-18432328-1', 'ws_CO_050320202206331477', '1031', 'Request cancelled by user'),
('4872-26099663-1', 'ws_CO_090320202312277826', '1031', 'Request cancelled by user'),
('4872-26620863-1', 'ws_CO_100320200850422382', '17', 'Rule limited.'),
('5501-8379602-1', 'ws_CO_100320200752339857', '1031', 'Request cancelled by user'),
('5502-8900164-1', 'ws_CO_100320201350555807', '1031', 'Request cancelled by user'),
('5504-9558048-1', 'ws_CO_100320202027345395', '1', 'The balance is insufficient for the transaction'),
('5513-11223788-1', 'ws_CO_110320201816088318', '1031', 'Request cancelled by user'),
('5513-11227444-1', 'ws_CO_110320201819414349', '1031', 'Request cancelled by user'),
('5513-8356234-1', 'ws_CO_100320200736413678', '1031', 'Request cancelled by user'),
('5513-8409780-1', 'ws_CO_100320200813415533', '1037', 'DS timeout.'),
('5515-7915644-1', 'ws_CO_090320202248020299', '1037', 'DS timeout.'),
('5517-11233608-1', 'ws_CO_110320201821478555', '1031', 'Request cancelled by user'),
('5518-7867562-1', 'ws_CO_090320202216526385', '1031', 'Request cancelled by user'),
('5731-5042465-1', 'ws_CO_300320201341583588', '1037', 'DS timeout.'),
('5736-5713443-1', 'ws_CO_310320201109246716', '1037', 'DS timeout.'),
('5738-5039750-1', 'ws_CO_300320201337274875', '1032', 'Request cancelled by user'),
('5961-6269765-1', 'ws_CO_220320201905599037', '1037', 'DS timeout.'),
('5961-6907051-1', 'ws_CO_230320201320258952', '1031', 'Request cancelled by user'),
('6877-2964807-1', 'ws_CO_200320201016288334', '1031', 'Request cancelled by user'),
('7041-2105345-1', 'ws_CO_090320202156478626', '1037', 'DS timeout.'),
('7042-2229084-1', 'ws_CO_090320202335101580', '1031', 'Request cancelled by user'),
('7042-5505365-1', 'ws_CO_110320201822131303', '1036', 'SMSC ACK timeout.'),
('7053-2225233-1', 'ws_CO_090320202330292050', '1031', 'Request cancelled by user'),
('7053-2234861-1', 'ws_CO_090320202345136366', '1037', 'DS timeout.'),
('7053-2675158-1', 'ws_CO_100320200810536083', '1031', 'Request cancelled by user'),
('8389-1200510-1', 'ws_CO_060320201201492485', '1031', 'Request cancelled by user'),
('8393-302505-1', 'ws_CO_050320202227569553', '1036', 'SMSC ACK timeout.'),
('8397-6473114-1', 'ws_CO_090320200752262366', '1031', 'Request cancelled by user'),
('9219-4099583-1', 'ws_CO_200320202122499036', '1032', 'Request cancelled by user'),
('9835-18392405-1', 'ws_CO_050320202158406373', '1031', 'Request cancelled by user'),
('9837-19521629-1', 'ws_CO_060320201418525709', '1', 'The balance is insufficient for the transaction'),
('9837-29376260-1', 'ws_CO_110320201819285603', '1037', 'DS timeout.'),
('9838-18409185-1', 'ws_CO_050320202208563674', '1001', 'Unable to lock subscriber, a transaction is already in process for the current subscriber'),
('9838-18443101-1', 'ws_CO_050320202228407563', '1031', 'Request cancelled by user'),
('9838-26544740-1', 'ws_CO_100320200809399737', '1031', 'Request cancelled by user'),
('9842-18434151-1', 'ws_CO_050320202223020099', '1037', 'DS timeout.'),
('9842-26594089-1', 'ws_CO_100320200844518349', '1037', 'DS timeout.');

-- --------------------------------------------------------

--
-- Table structure for table `music_payment`
--

CREATE TABLE `music_payment` (
  `MerchantRequestID` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `SongId` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ArtistId` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `SessionStatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `PayDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `poppindrinks`
--

CREATE TABLE `poppindrinks` (
  `Category` varchar(20) NOT NULL,
  `DrinkId` varchar(20) NOT NULL,
  `OwnerId` varchar(20) NOT NULL,
  `DrinkName` varchar(50) NOT NULL,
  `DrinkSize` varchar(20) NOT NULL,
  `DrinkPrice` int(11) NOT NULL,
  `DrinkDesc` varchar(300) NOT NULL,
  `HappyHour` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppindrinks`
--

INSERT INTO `poppindrinks` (`Category`, `DrinkId`, `OwnerId`, `DrinkName`, `DrinkSize`, `DrinkPrice`, `DrinkDesc`, `HappyHour`) VALUES
('Lotions', '584219', 'AdLz336916POP-INN', 'Amara', '500 ml', 200, 'Nice and Lovely', 0);

-- --------------------------------------------------------

--
-- Table structure for table `poppindrinkspayment`
--

CREATE TABLE `poppindrinkspayment` (
  `TransactionReference` varchar(15) NOT NULL,
  `PaymentId` varchar(100) NOT NULL,
  `DrinksId` varchar(100) NOT NULL,
  `OwnerId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `PayerName` varchar(200) NOT NULL,
  `PayerEmail` varchar(255) NOT NULL,
  `AmountPaid` int(10) UNSIGNED NOT NULL,
  `AccDeposit` int(10) NOT NULL,
  `PayDate` date NOT NULL,
  `PayTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `poppindrinksreceipt`
--

CREATE TABLE `poppindrinksreceipt` (
  `ReceiptId` varchar(100) NOT NULL,
  `DrinkId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `ReceiptUrl` varchar(255) NOT NULL,
  `ReceiptStatus` varchar(5) NOT NULL,
  `ReceiptDate` date NOT NULL,
  `ReceiptTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `poppinevent`
--

CREATE TABLE `poppinevent` (
  `Category` varchar(100) NOT NULL,
  `EventId` varchar(100) NOT NULL,
  `OwnerId` varchar(100) NOT NULL,
  `ApiKey` varchar(255) DEFAULT NULL,
  `EventName` varchar(300) NOT NULL,
  `EventDesc` varchar(3000) NOT NULL,
  `PicPath` varchar(500) NOT NULL,
  `EventStock` varchar(15) NOT NULL,
  `EventPrice` int(10) UNSIGNED NOT NULL,
  `EventLocation` varchar(1000) NOT NULL,
  `StartDate` date NOT NULL,
  `StartTime` time NOT NULL,
  `StopDate` date NOT NULL,
  `StopTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinevent`
--

INSERT INTO `poppinevent` (`Category`, `EventId`, `OwnerId`, `ApiKey`, `EventName`, `EventDesc`, `PicPath`, `EventStock`, `EventPrice`, `EventLocation`, `StartDate`, `StartTime`, `StopDate`, `StopTime`) VALUES
('Music', '431277AYo', 'ANtl243146POP-INN', '', 'Takeover', 'It\'s a wabebe party', 'Uploaded/Images/Takeover.jpg', 'full', 1, 'Mojito Lounge', '2019-09-04', '08:30:00', '2019-09-05', '08:30:00'),
('Music', '12283AvPc', 'ANtl243146POP-INN', '', 'Hart Unplugged', 'Gwaan Hot', 'Uploaded/Images/Takeover.jpg', '20', 1, 'Nairobi', '2019-07-02', '09:00:00', '2019-07-03', '06:00:00'),
('Music', '108065AdFy', '2', 'kisa.co.ke', 'Musically', 'Reggae Legends', 'Uploaded/Images/Takeover.jpg', '20', 1, 'Limuru', '2019-12-25', '14:00:00', '2019-12-25', '15:00:00'),
('Business & Professional', '157435AxJE', 'APAx880725POP-INN', NULL, 'my event', 'testing 123', 'Uploaded/Images/ell mobile.PNG', 'unlimited', 1, 'limuru', '2020-02-18', '06:00:00', '2020-02-18', '19:00:00'),
('dummy', '404849AsJxFYo', 'Atru877311POP-INN', NULL, 'Bob', 'Nice', 'Uploaded/Images/bob.PNG', '20', 0, 'Limuru', '2020-02-20', '20:00:00', '2020-02-21', '08:00:00'),
('dummy', '549014AUtR', 'Atru877311POP-INN', NULL, 'Genje', 'Wagwaan', 'Uploaded/Images/01-02.jpg', '20', 0, 'Limuru', '2020-02-20', '08:00:00', '2020-02-21', '20:00:00'),
('dummy', '884901ADXi', 'APAx880725POP-INN', NULL, 'test event', 'free event', 'Uploaded/Images/vehicle.jpg', 'unlimited', 0, 'nairobi', '2020-02-20', '06:00:00', '2020-02-20', '20:00:00'),
('dummy', '715961AjGs', 'APAx880725POP-INN', NULL, 'test event', 'test event', 'Uploaded/Images/app.PNG', 'unlimited', 0, 'limuru', '2020-02-21', '07:00:00', '2020-02-22', '18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `poppineventpayment`
--

CREATE TABLE `poppineventpayment` (
  `TransactionReference` varchar(15) NOT NULL,
  `PaymentId` varchar(100) NOT NULL,
  `EventId` varchar(100) NOT NULL,
  `EventTicketTypeId` varchar(255) NOT NULL,
  `OwnerId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `PayerEmail` varchar(255) NOT NULL,
  `AmountPaid` int(10) UNSIGNED NOT NULL,
  `TicketCapacity` int(11) NOT NULL,
  `AccDeposit` int(10) NOT NULL,
  `PayDate` date NOT NULL,
  `PayTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppineventpayment`
--

INSERT INTO `poppineventpayment` (`TransactionReference`, `PaymentId`, `EventId`, `EventTicketTypeId`, `OwnerId`, `PayerPhone`, `PayerEmail`, `AmountPaid`, `TicketCapacity`, `AccDeposit`, `PayDate`, `PayTime`) VALUES
('OD136F9VOD', 'POPINNPAY413570', '549014AUtR', 'TKT47724PGMLXKNXIJBI', 'Atru877311POP-INN', '254705972361', 'kanjianto@gmail.com', 1, 1, 0, '2020-04-01', '12:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `poppinlogin`
--

CREATE TABLE `poppinlogin` (
  `LoginTimes` int(11) NOT NULL,
  `UserId` varchar(100) NOT NULL,
  `LoginDate` date NOT NULL,
  `LoginTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinlogin`
--

INSERT INTO `poppinlogin` (`LoginTimes`, `UserId`, `LoginDate`, `LoginTime`) VALUES
(492, 'AnYi952953POP-INN', '2020-02-25', '05:26:21'),
(491, 'AdLz336916POP-INN', '2020-02-25', '01:58:18'),
(490, 'Atru877311POP-INN', '2020-02-24', '09:51:41'),
(489, 'APAx880725POP-INN', '2020-02-21', '11:39:28'),
(488, 'APAx880725POP-INN', '2020-02-21', '08:13:37'),
(487, 'APAx880725POP-INN', '2020-02-21', '06:45:29'),
(486, 'APAx880725POP-INN', '2020-02-20', '10:46:37'),
(485, 'APAx880725POP-INN', '2020-02-20', '10:36:41'),
(484, 'Atru877311POP-INN', '2020-02-20', '06:11:33'),
(483, 'AdLz336916POP-INN', '2020-02-19', '05:39:18'),
(482, 'AnYi952953POP-INN', '2020-02-19', '01:47:30'),
(481, 'APAx880725POP-INN', '2020-02-19', '01:00:52'),
(480, 'AdLz336916POP-INN', '2020-02-18', '06:30:34'),
(479, 'AnYi952953POP-INN', '2020-02-18', '05:47:01'),
(478, 'APAx880725POP-INN', '2020-02-18', '04:37:31'),
(477, 'APAx880725POP-INN', '2020-02-18', '04:31:04'),
(476, 'APAx880725POP-INN', '2020-02-18', '03:13:51'),
(475, 'AdLz336916POP-INN', '2020-02-18', '02:39:15'),
(474, 'APAx880725POP-INN', '2020-02-17', '11:45:15'),
(473, 'AdLz336916POP-INN', '2020-02-17', '11:03:25'),
(472, 'AKYW585483POP-INN', '2020-02-17', '10:46:29'),
(471, 'Atru877311POP-INN', '2020-02-17', '07:04:54'),
(493, 'Atru877311POP-INN', '2020-02-29', '23:39:16'),
(494, 'Atru877311POP-INN', '2020-03-01', '01:07:51'),
(495, 'AdLz336916POP-INN', '2020-03-05', '03:28:13'),
(496, 'Atru877311POP-INN', '2020-03-05', '05:54:50'),
(497, 'AdLz336916POP-INN', '2020-03-05', '07:06:28'),
(498, 'Atru877311POP-INN', '2020-03-06', '01:28:01'),
(499, 'Atru877311POP-INN', '2020-03-06', '03:55:42'),
(500, 'AdLz336916POP-INN', '2020-03-06', '03:58:55'),
(501, 'Atru877311POP-INN', '2020-03-06', '04:12:03'),
(502, 'Atru877311POP-INN', '2020-03-06', '04:12:11'),
(503, 'Atru877311POP-INN', '2020-03-06', '04:12:20'),
(504, 'Atru877311POP-INN', '2020-03-06', '04:12:21'),
(505, 'Atru877311POP-INN', '2020-03-06', '07:08:49'),
(506, 'AdLz336916POP-INN', '2020-03-06', '07:15:48'),
(507, 'AnYi952953POP-INN', '2020-03-06', '07:51:13'),
(508, 'AdLz336916POP-INN', '2020-03-09', '09:32:51'),
(509, 'AdLz336916POP-INN', '2020-03-13', '03:47:27'),
(510, 'Atru877311POP-INN', '2020-03-18', '17:12:22'),
(511, 'APAx880725POP-INN', '2020-03-19', '17:00:00'),
(512, 'AdLz336916POP-INN', '2020-03-26', '15:09:01'),
(513, 'Atru877311POP-INN', '2020-03-29', '19:47:00'),
(514, 'AdLz336916POP-INN', '2020-03-29', '19:48:10'),
(515, 'AdLz336916POP-INN', '2020-03-30', '06:36:17'),
(516, 'Atru877311POP-INN', '2020-03-31', '04:56:05'),
(517, 'AdLz336916POP-INN', '2020-03-31', '05:04:04'),
(518, 'AdLz336916POP-INN', '2020-03-31', '08:04:25'),
(519, 'AdLz336916POP-INN', '2020-03-31', '11:03:21'),
(520, 'AdLz336916POP-INN', '2020-03-31', '11:45:41'),
(521, 'Atru877311POP-INN', '2020-04-01', '07:02:01'),
(522, 'AdLz336916POP-INN', '2020-04-02', '00:20:29'),
(523, 'AlYJ973209POP-INN', '2020-04-02', '12:25:58'),
(524, 'AlYJ973209POP-INN', '2020-04-02', '12:37:40'),
(525, 'AdLz336916POP-INN', '2020-04-02', '16:17:04'),
(526, 'AdLz336916POP-INN', '2020-04-02', '16:57:46'),
(527, 'AdLz336916POP-INN', '2020-04-02', '16:58:48'),
(528, 'Admin1', '2020-04-02', '17:00:46'),
(529, 'Admin1', '2020-04-02', '17:01:31'),
(530, 'Admin1', '2020-04-02', '17:02:23'),
(531, 'Admin1', '2020-04-02', '17:02:51'),
(532, 'AlYJ973209POP-INN', '2020-04-02', '17:29:35'),
(533, 'APgZ713049POP-INN', '2020-04-02', '17:37:26'),
(534, 'Admin1', '2020-04-02', '17:39:01'),
(535, 'APgZ713049POP-INN', '2020-04-02', '17:39:42'),
(536, 'APgZ713049POP-INN', '2020-04-02', '17:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `poppinmpesa`
--

CREATE TABLE `poppinmpesa` (
  `ServiceName` varchar(7) NOT NULL,
  `BusinessNumber` int(8) UNSIGNED NOT NULL,
  `TransactionReference` varchar(15) NOT NULL,
  `InternalTransactionId` varchar(20) NOT NULL,
  `TransactionTimestamp` varchar(100) NOT NULL,
  `TransactionType` varchar(20) NOT NULL,
  `AccountNumber` varchar(20) NOT NULL,
  `SenderPhone` varchar(13) NOT NULL,
  `FirstName` varchar(20) NOT NULL,
  `MiddleName` varchar(20) DEFAULT NULL,
  `LastName` varchar(20) NOT NULL,
  `Amount` int(10) UNSIGNED NOT NULL,
  `Currency` varchar(3) NOT NULL,
  `Signature` varchar(50) NOT NULL,
  `PesaDate` date NOT NULL,
  `PesaTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinmpesa`
--

INSERT INTO `poppinmpesa` (`ServiceName`, `BusinessNumber`, `TransactionReference`, `InternalTransactionId`, `TransactionTimestamp`, `TransactionType`, `AccountNumber`, `SenderPhone`, `FirstName`, `MiddleName`, `LastName`, `Amount`, `Currency`, `Signature`, `PesaDate`, `PesaTime`) VALUES
('M-PESA', 576804, 'MIA4KT7KJ4', '43618557', '2018-09-10T15:17:12Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 5, 'Ksh', 'rfWlap281UsciWdHVpT7SQxtjJk=', '2018-09-17', '00:46:37'),
('M-PESA', 576804, 'MIH0P8U8KO', '44448789', '2018-09-17T08:37:26Z', 'buygoods', 'N/A', '+254705972361', 'ANTONY', 'ONDERE', 'KANJI', 2000, 'Ksh', 'hQ+aN/NaK/2wqYOhEGcPBsBiv30=', '2018-09-17', '04:37:29'),
('M-PESA', 576804, 'MIH5PCCURJ', '44464173', '2018-09-17T11:08:16Z', 'buygoods', 'N/A', '+254723145729', 'JANE', '', 'MBUGUA', 5, 'Ksh', 'VOTJd5gWodEnYXSBiuKJC1pvwzY=', '2018-09-17', '07:08:18'),
('M-PESA', 576804, 'MIH5PPL3XN', '44522378', '2018-09-17T19:09:01Z', 'buygoods', 'N/A', '+254727683082', 'DENNIS', 'WACIRA', 'MICERE', 5, 'Ksh', 'g3t/AbZ3PInpyHW3CMxNEAekMpM=', '2018-09-17', '15:09:04'),
('M-PESA', 576804, 'MII7PQPYKH', '44540036', '2018-09-18T02:40:16Z', 'buygoods', 'N/A', '+254705972361', 'ANTONY', 'ONDERE', 'KANJI', 5, 'Ksh', '5NTqTg3KKUawtwULPKJOuPRH8Ss=', '2018-09-17', '22:40:19'),
('M-PESA', 576804, 'MII3PSWAUX', '44546190', '2018-09-18T05:53:49Z', 'buygoods', 'N/A', '+254740223794', 'Dennis', 'Kinuthia', 'Wanjiku', 10, 'Ksh', 'MqAPyNZZfAFrsfnlSMNzmlvKXQA=', '2018-09-18', '01:53:58'),
('M-PESA', 576804, 'MJH9ATCEU9', '48269047', '2018-10-17T10:27:03Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 5, 'Ksh', 'DIO/lxKxBmoJOT7iMhXqjWoAlQY=', '2018-10-17', '06:27:07'),
('M-PESA', 576804, 'MJO5FSM8KH', '49195750', '2018-10-24T14:51:56Z', 'buygoods', 'N/A', '+254727683082', 'DENNIS', 'WACIRA', 'MICERE', 5, 'Ksh', 'ETgRb4/quz/VBZJ//+6UUK2yxJo=', '2018-10-24', '10:52:00'),
('M-PESA', 576804, 'MJV4KT0N98', '50101172', '2018-10-31T13:01:54Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 5, 'Ksh', 'vwfiNtm5CU3Gd+WlcnXOH3O+M5Q=', '2018-10-31', '09:02:00'),
('M-PESA', 576804, 'MJV0KUPSQ6', '50107790', '2018-10-31T13:57:38Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 5, 'Ksh', 'ftxU/Jz/1wvi7aWWQ/4BcT3Dz8w=', '2018-10-31', '09:57:42'),
('M-PESA', 576804, 'MJV1KZJ8S1', '50127067', '2018-10-31T16:01:00Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 5, 'Ksh', '8qWpRFKDwd2bJA+UBcpguS9cxYY=', '2018-10-31', '12:01:05'),
('M-PESA', 576804, 'MK29M9J9TV', '50358425', '2018-11-02T10:11:56Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 10, 'Ksh', 'pc+W/goAhlRy/2xf6oyiMhNGSks=', '2018-11-02', '06:11:58'),
('M-PESA', 576804, 'MKQ74WBEGP', '53659438', '2018-11-26T08:57:03Z', 'buygoods', 'N/A', '+254705180969', 'PETER', 'MWANGI', 'MBUGUA', 10, 'Ksh', 'bPdUMH/WfTMGXDRXQ767sShGxwk=', '2018-11-26', '03:57:04'),
('M-PESA', 576804, 'MKQ74WBEGPA', '53659438', '2018-11-26T08:57:03Z', 'buygoods', 'N/A', '+254705180965', 'PETER', 'MWANGI', 'MBUGUA', 20, 'Ksh', 'bPdUMH/WfTMGXDRXQ767sShGxwk=', '2018-11-26', '03:57:04'),
('M-PESA', 576804, 'MKQ74WBEGPW', '53659438', '2018-11-26T08:57:03Z', 'buygoods', 'N/A', '+254705180960', 'PETER', 'MWANGI', 'MBUGUA', 10, 'Ksh', 'bPdUMH/WfTMGXDRXQ767sShGxwk=', '2018-11-26', '03:57:04'),
('M-PESA', 576804, 'MKS46MADY4', '53969380', '2018-11-28T14:37:37Z', 'buygoods', 'N/A', '+254705972361', 'ANTONY', 'ONDERE', 'KANJI', 5, 'Ksh', 'khF7aaaf/WEcDFtLtaLu37ZdFqE=', '2018-11-28', '09:37:39'),
('M-PESA', 576804, 'MLQ3RXRYYX', '58066173', '2018-12-26T14:19:21Z', 'buygoods', 'N/A', '+254705972361', 'ANTONY', 'ONDERE', 'KANJI', 1, 'Ksh', '6s7J2eyeqrE8Y+RjuIlFs3xdrAU=', '2018-12-26', '09:19:23');

-- --------------------------------------------------------

--
-- Table structure for table `poppinpaymentbalances`
--

CREATE TABLE `poppinpaymentbalances` (
  `EventId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `AmountBalance` int(10) NOT NULL,
  `UpdateDate` date NOT NULL,
  `UpdateTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinpaymentbalances`
--

INSERT INTO `poppinpaymentbalances` (`EventId`, `PayerPhone`, `AmountBalance`, `UpdateDate`, `UpdateTime`) VALUES
('919097AAN', '254705180969', -7, '2019-04-11', '01:58:08'),
('312849AWGd', '254701204261', 0, '2019-06-22', '15:05:08');

-- --------------------------------------------------------

--
-- Table structure for table `poppinpaymentbalances_res`
--

CREATE TABLE `poppinpaymentbalances_res` (
  `ReserveId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `AmountBalance` int(10) NOT NULL,
  `UpdateDate` date NOT NULL,
  `UpdateTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinpaymentbalances_res`
--

INSERT INTO `poppinpaymentbalances_res` (`ReserveId`, `PayerPhone`, `AmountBalance`, `UpdateDate`, `UpdateTime`) VALUES
('346174Aumc', '+254701002541', -500, '2018-08-13', '10:37:39'),
('915002AiXb', '+254701002544', -5000, '2018-10-17', '19:57:57'),
('915002AiXb', '+254706087108', -23000, '2018-10-18', '10:49:50'),
('362574AKkV', '+254705972361', 2000, '2018-10-24', '16:08:07'),
('359130AvzN', '+254705180969', 15, '2018-10-31', '12:01:52'),
('474275ALsI', '+254705180969', 10, '2018-11-02', '06:13:14'),
('584936AIcL', '+254705180969', 10, '2018-11-26', '07:18:44'),
('584936AIcL', '+254705180965', 60, '2018-11-26', '07:22:16'),
('359130AvzN', '+254705180960', -5, '2018-11-26', '07:43:36'),
('842446AaZJ', '254705180969', -5, '2019-07-08', '08:35:41'),
('842446AaZJ', '254727683082', -10, '2019-07-25', '12:00:31'),
('110309AeAv', '254705180969', -1, '2019-07-30', '14:16:17');

-- --------------------------------------------------------

--
-- Table structure for table `poppinreg`
--

CREATE TABLE `poppinreg` (
  `UserId` varchar(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `UserType` varchar(10) NOT NULL,
  `Email` varchar(250) NOT NULL,
  `PhoneNo` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  `City` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinreg`
--

INSERT INTO `poppinreg` (`UserId`, `Username`, `UserType`, `Email`, `PhoneNo`, `Address`, `City`, `Country`, `Password`, `RegDate`, `RegTime`) VALUES
('Atru877311POP-INN', 'Kanji Antony', 'Admin', 'kanjianto@gmail.com', '2547889', '12', 'Limuru', 'Kenya', '$2y$10$M9fJOpPoNsFPwDeRYlTk6uIyDlF/CJSrOI1p/qS3QVqKWnemKAKf.', '2020-02-17', '06:37:27'),
('APAx880725POP-INN', 'peter', 'Admin', 'swazicreatives@gmail.com', '0712473322', '104780', 'nairobi', 'Kenya', '$2y$10$xtiBUGL9I2Im6PlzaUI65unPgXNU.5WaBNIwIutADfjDxKyzmSJHi', '2020-02-17', '11:44:39'),
('AKYW585483POP-INN', 'cetrick', 'Admin', 'cetricka@gmail.com', '0705972361', 'nai', 'NAI', 'Kenya', '$2y$10$OnwvvmFsbcR1uqJz60d0T.86c9jIGUlK7kfPtCjhEsnGemrifMfDK', '2020-02-17', '10:45:55'),
('AGAn635701POP-INN', 'Mitch', 'Admin', 'oharry0535@gmail.com', '254705851241', 'Nairobi', 'Nairobi', 'Kenya', '8cfa93e5daf8cd2ce5b409d48c53ff53', '2018-08-07', '01:50:05'),
('Afqj695398POP-INN', 'Bianca', 'Admin', 'maureenchebett@gmail.com', '254712270519', '4282', 'Nairobi', 'Kenya', 'de8d5cf3709508bb5fea6ab060bab3aa', '2018-08-28', '03:29:09'),
('AXUV835652POP-INN', 'DENNIS WACHIRA', 'Admin', 'denniswachira87@gmail.com', '0727683082', '60100-205 Embu', 'Nairobi', 'Kenya', 'd3eb05a3d5bb7e4901f739286ba8eee9', '2018-11-04', '13:05:27'),
('AWcp842116POP-INN', 'popin', 'Admin', 'KENYAPOPIN@GMAIL.COM', '254737714245', '60100-205 Embu', 'Nairobi', 'Kenya', '32c11bbe2d0edde86b552d0c6c75c9d2', '2018-11-04', '13:27:26'),
('AsNH531347POP-INN', 'fpsnkdvb', 'Admin', 'sample@email.tst', '555-666-0606', '3137 Laguna Street', 'San Francisco', 'Kenya', '32cc5886dc1fa8c106a02056292c4654', '2018-11-20', '03:34:56'),
('ATjl946723POP-INN', 'Mc Raw', 'Admin', 'rotyeno12@gmail.com', '0758391500', '70 ', 'Sindo', 'Kenya', 'd71c464dd097aa5566ddda12ad176156', '2018-11-28', '09:29:08'),
('AsQH236208POP-INN', 'Burndown Babylon Reggae', 'Admin', 'bbreggaekenya@gmail.com', '254705972361', '23 ', 'Nairobi', 'Kenya', '22b6d45f231ddf4eb7f30b9f686fe03d', '2019-01-04', '15:36:21'),
('AYcX410132POP-INN', 'Victor', 'Admin', 'Victormusyoki13@gmail.com', '254758664405', '2050-machakos', 'Nairobi', 'Kenya', '5a551131e99ed8644cb461af76e6ff2f', '2019-03-02', '11:39:37'),
('Awqv197906POP-INN', 'popin', 'Admin', 'denniswachira87@yahoo.com', '254727683082', '60100-205 Embu', 'Nairobi', 'Kenya', 'b56522cb95aa89c207e129509362cce3', '2019-03-09', '11:07:16'),
('AUkd114367POP-INN', 'swazicreatives@gmail.com', 'Admin', 'mwangi.peter36@yahoo.com', '0705180969', '104780', 'swazicreatives@gmail.com', 'Kenya', 'db673a49ec1cbc9b0c87882e0741ff11', '2019-03-09', '12:10:07'),
('AWGH126015POP-INN', 'Joseph Rembe', 'Admin', 'mbuguarembe@yahoo.com', '0722408892', '102415', 'Nairobi', 'Kenya', '97067da16c36ae9e4c41488bf74b149a', '2019-03-20', '12:09:57'),
('AWbz770097POP-INN', 'Mojito Lounge', 'Admin', 'cetricka1@gmail.com', '254703478182', '23', 'Kitui', 'Kenya', 'a94a3f7f325f7e78dd5cff7befc2a67a', '2019-04-12', '12:14:00'),
('APer879245POP-INN', 'XenEvedy', 'Admin', '4e7dfdg@yandex.com', '87313966151', 'NY', 'NY', 'Kenya', 'a38cd93be03e642aa4c78829735ad2b9', '2019-04-22', '14:16:48'),
('AVfP44535POP-INN', 'Bree Mwangi', 'Admin', 'trenisbrage@gmail.com', '', 'karen', 'Nairobi ', 'Kenya', '137005be6f10c721ed780919aa0d7266', '2019-05-19', '10:17:18'),
('AvWu480478POP-INN', 'jdoe', 'Admin', 'frankipham@gmail.com', '722000000', '123', 'Mombasa', 'Kenya', '75491edad9c72286347796c5bc5a3be3', '2019-05-24', '10:29:06'),
('AUUR838093POP-INN', 'popin', 'Admin', 'dennis@popin.co.ke', '254727683082', '60100-205', 'Nairobi', 'Kenya', 'b56522cb95aa89c207e129509362cce3', '2019-06-01', '14:44:51'),
('AjmW203592POP-INN', 'Keivin Kay', 'Admin', 'urbandynastyke@gmail.com', '254701070319', 'P.O.Box 4540-40100', 'Kisumu', 'Kenya', 'fbe1a2c2f0c2c078af8a911c426bdc84', '2019-06-03', '17:59:12'),
('Acue986326POP-INN', 'Keivin Kay', 'Admin', 'keivinkuria336@gmail.com', '0701070319', 'P.O.Box 4540-40100', 'Kisumu', 'Kenya', 'f20eabc50323f184e5f9d97ab28756d0', '2019-06-11', '03:50:41'),
('AYeo897226POP-INN', 'Dennis Kinuthia', 'Admin', 'dkinuthiaw@gmail.com', '0726604439', '770', 'Thika', 'Kenya', '$2y$10$ezDnR6ehiHzBsZph4B4d7ueRDtsLpgjm2ew1qjLIMT/bHHRNYsu2i', '2019-07-01', '10:31:31'),
('Axkl946959POP-INRSV', 'Rastyle Lounge', 'Admin', 'kanjiantonio@gmail.com', '0737893694', '23', 'Eldoret', 'Kenya', '$2y$10$dgILtzFPVVeAH7.OoLD4jeUdffFQcZMMNtYDRwXzOzAW0BL/l4/Qe', '2019-07-28', '10:03:47'),
('ARBU769732POP-INRSV', 'Mario', 'Admin', 'jeanmario35@gmail.com', '+254790560382', '456', 'Limuru', 'Kenya', '48a47ae80cbaa4369f0776c6860b98ad', '2019-07-29', '04:25:04'),
('AtNm469235POP-INN', 'awlawjnr@gmail.com', 'Admin', 'awlawjnr@gmail.com', '0700000000', 'awlawjnr@gmail.com', 'awlawjnr@gmail.com', 'Kenya', '$2y$10$KgPbQ5h1DhDLjAqWwbDa4.iMIFz4PkhSeoWzlxT9Vso5IMtpGkHKy', '2019-08-10', '12:00:57');

-- --------------------------------------------------------

--
-- Table structure for table `poppinreg_res`
--

CREATE TABLE `poppinreg_res` (
  `UserId` varchar(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `UserType` varchar(10) NOT NULL,
  `ProfilePic` varchar(600) NOT NULL,
  `CoverPic` varchar(600) NOT NULL,
  `OpeningDays` varchar(15) NOT NULL,
  `OpeningTime` time NOT NULL,
  `ClosingTime` time NOT NULL,
  `HappyHourStart` time DEFAULT '22:00:00',
  `HappyHourStop` time DEFAULT '23:00:00',
  `FrontIDPic` varchar(255) DEFAULT NULL,
  `BackIDPic` varchar(255) DEFAULT NULL,
  `IDStatus` int(1) NOT NULL,
  `BusinessLicencePic` varchar(255) DEFAULT NULL,
  `BusinessLicenceStatus` int(1) DEFAULT NULL,
  `SettlementAccount` varchar(10) NOT NULL,
  `SettlementAccountStatus` int(1) DEFAULT NULL,
  `Email` varchar(250) NOT NULL,
  `PhoneNo` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  `City` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Approval` int(1) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinreg_res`
--

INSERT INTO `poppinreg_res` (`UserId`, `Username`, `UserType`, `ProfilePic`, `CoverPic`, `OpeningDays`, `OpeningTime`, `ClosingTime`, `HappyHourStart`, `HappyHourStop`, `FrontIDPic`, `BackIDPic`, `IDStatus`, `BusinessLicencePic`, `BusinessLicenceStatus`, `SettlementAccount`, `SettlementAccountStatus`, `Email`, `PhoneNo`, `Address`, `City`, `Country`, `Password`, `Approval`, `RegDate`, `RegTime`) VALUES
('Admin1', 'Kanji Antony', 'Admin', 'Uploaded/Images/profileAdLz336916POP-INN.png', 'Uploaded/Images/coverAdLz336916POP-INN.png', 'Weekends', '17:00:00', '23:00:00', '17:50:00', '18:55:00', 'Uploaded\\Images\\cover_kisa2.png', 'Uploaded\\Images\\cover_kisa1.png', 1, NULL, 1, 'Mpesa', 1, 'kanjianto@gmail.com', '0705972361', 'Kenya', 'Nairobi', 'Kenya', '$2y$10$CK0.tCKV4TRW3ZfSppBBTul.W1WBzfrQ5y5SezWXa6TszfRsvMwO6', 1, '2020-02-17', '11:03:13'),
('AlYJ973209POP-INN', 'iSpa', 'Admin', 'Uploaded\\Images\\defaultP.jpg', 'Uploaded\\Images\\defaultC.jpg', 'Weekends', '18:00:00', '06:00:00', '22:00:00', '23:00:00', 'Uploaded\\Images\\IMG_20200228_112117_0.jpg', 'Uploaded\\Images\\IMG_20200228_112126_1.jpg', 1, NULL, 1, 'Mpesa', 1, 'kanjiantonio@gmail.com', '254711', '23', 'Limuru', 'Kenya', '$2y$10$.aW/sgYdWgalFST1p/UcIOTk8VwCTi.iqhI18WLEWjKj3asoztDMu', 1, '2020-04-02', '12:25:38'),
('APgZ713049POP-INN', 'Marto', 'Admin', 'Uploaded\\Images\\defaultP.jpg', 'Uploaded\\Images\\defaultC.jpg', 'Weekends', '18:00:00', '06:00:00', '22:00:00', '23:00:00', 'Uploaded\\Images\\cover_kisa1.png', 'Uploaded\\Images\\cover_kisa2.png', 1, 'Uploaded\\Images\\Capture2.PNG', 1, 'Mpesa', 1, 'kanji@gmail.com', '2546', '23', 'Limuru', 'Kenya', '$2y$10$KaBQGruXZv98Ps5FB/tO3OFddjqEBJDIbA4bByBJ0lEJDLB4FlfXK', 1, '2020-04-02', '17:37:15');

-- --------------------------------------------------------

--
-- Table structure for table `poppinreg_res_superadmin`
--

CREATE TABLE `poppinreg_res_superadmin` (
  `UserId` varchar(100) NOT NULL,
  `Username` varchar(100) NOT NULL,
  `UserType` varchar(10) NOT NULL,
  `ProfilePic` varchar(600) NOT NULL,
  `CoverPic` varchar(600) NOT NULL,
  `OpeningDays` varchar(15) NOT NULL,
  `OpeningTime` time NOT NULL,
  `ClosingTime` time NOT NULL,
  `Email` varchar(250) NOT NULL,
  `PhoneNo` varchar(20) NOT NULL,
  `Address` varchar(20) NOT NULL,
  `City` varchar(50) NOT NULL,
  `Country` varchar(50) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinreg_res_superadmin`
--

INSERT INTO `poppinreg_res_superadmin` (`UserId`, `Username`, `UserType`, `ProfilePic`, `CoverPic`, `OpeningDays`, `OpeningTime`, `ClosingTime`, `Email`, `PhoneNo`, `Address`, `City`, `Country`, `Password`, `RegDate`, `RegTime`) VALUES
('ACxX738909POP-INN', 'Kanji Antony, CTO', 'Admin', 'Uploaded\\Images\\15.jpg', 'Uploaded\\Images\\Banner.jpg', 'Everyday', '20:00:00', '06:00:00', 'kanjianto@gmail.com', '254705972361', '23', 'Nairobi', 'Kenya', '689f862e4aefa2bc9b673010aedc3d6e', '2018-10-15', '13:11:02'),
('AQKl47461POP-INN', 'peter mwangi', 'Admin', 'Uploaded/Images/index.jpg', 'Uploaded/Images/index.jpg', 'Everyday', '18:00:00', '00:00:00', 'swazicreatives@gmail.com', '254705180969', '104780', 'Nairobi', 'Kenya', 'db673a49ec1cbc9b0c87882e0741ff11', '2018-11-01', '05:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `poppinreserve`
--

CREATE TABLE `poppinreserve` (
  `Category` varchar(100) NOT NULL,
  `ReserveId` varchar(100) NOT NULL,
  `OwnerId` varchar(100) NOT NULL,
  `ReserveName` varchar(300) NOT NULL,
  `ReserveDesc` varchar(3000) NOT NULL,
  `PicPath` varchar(500) NOT NULL,
  `ReserveStock` varchar(15) NOT NULL,
  `ReserveTablesTotal` int(10) NOT NULL,
  `ReservePrice` int(10) UNSIGNED NOT NULL,
  `ReserveLocation` varchar(1000) NOT NULL,
  `ReserveVacancy` varchar(10) NOT NULL,
  `ReserveDate` date NOT NULL,
  `ReserveTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinreserve`
--

INSERT INTO `poppinreserve` (`Category`, `ReserveId`, `OwnerId`, `ReserveName`, `ReserveDesc`, `PicPath`, `ReserveStock`, `ReserveTablesTotal`, `ReservePrice`, `ReserveLocation`, `ReserveVacancy`, `ReserveDate`, `ReserveTime`) VALUES
('Wellness', '901243ApIx', 'APgZ713049POP-INN', 'Weight Lifting', 'Beba weight', 'Uploaded\\Images\\sample.png', '60', 0, 1, 'kenya', 'Vacant', '2020-04-02', '17:41:51'),
('VIP Table', '578589AQkr', 'AdLz336916POP-INN', 'Genje', 'Nice table for vipse', 'Uploaded/Images/gr.jpg', '20', 2, 1, 'Club', 'Vacant', '2020-03-05', '07:07:05'),
('VIP Table', '529561ADyq', 'AdLz336916POP-INN', 'Yutman', 'Closer to selecta santa', 'Uploaded/Images/bob.PNG', '5', 5, 1, 'Club', 'Vacant', '2020-03-31', '11:18:05'),
('Beauty', '138861Acgo', 'AdLz336916POP-INN', 'Shaving', 'Nice shave', 'Uploaded\\Images\\sample.png', '30', 0, 1, 'kenya', 'Vacant', '2020-04-02', '10:06:28');

-- --------------------------------------------------------

--
-- Table structure for table `poppinreservepayment`
--

CREATE TABLE `poppinreservepayment` (
  `TransactionReference` varchar(15) NOT NULL,
  `PaymentId` varchar(100) NOT NULL,
  `ReserveId` varchar(100) NOT NULL,
  `OwnerId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `PayerName` varchar(200) NOT NULL,
  `PayerEmail` varchar(255) NOT NULL,
  `AmountPaid` int(10) UNSIGNED NOT NULL,
  `TableCapacity` int(11) NOT NULL,
  `AccDeposit` int(10) NOT NULL,
  `ArrivalDate` date DEFAULT NULL,
  `ArrivalTime` time DEFAULT NULL,
  `PayDate` date NOT NULL,
  `PayTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppinreservepayment`
--

INSERT INTO `poppinreservepayment` (`TransactionReference`, `PaymentId`, `ReserveId`, `OwnerId`, `PayerPhone`, `PayerName`, `PayerEmail`, `AmountPaid`, `TableCapacity`, `AccDeposit`, `ArrivalDate`, `ArrivalTime`, `PayDate`, `PayTime`) VALUES
('OBI362XBQ5', 'POPINNPAY386616', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBI765JZCP', 'POPINNPAY938773', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBI165OGKP', 'POPINNPAY627555', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBI465TBX0', 'POPINNPAY300270', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBI9666WLX', 'POPINNPAY556666', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBI066G98A', 'POPINNPAY833522', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBJ36Y7RFJ', 'POPINNPAY376646', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBJ86YCQ4Q', 'POPINNPAY762309', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBL08WQUYK', 'POPINNPAY834532', '578589AQkr', 'AdLz336916POP-INN', '254705180969', 'trial stk', 'swazicreatives@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBS1EILP49', 'POPINNPAY244137', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'k@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBS3EQRKU1', 'POPINNPAY43548', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'cetricka1@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBS7ES194V', 'POPINNPAY353598', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'cetricka1@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBS3ESDM5V', 'POPINNPAY497425', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'cetricka1@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OBS1ESXJUH', 'POPINNPAY904785', '578589AQkr', 'AdLz336916POP-INN', '254705180969', 'trial stk', 'swazicreatives@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OC28H2TZNC', 'POPINNPAY464077', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OC93NJ5JT3', 'POPINNPAY916173', '578589AQkr', 'AdLz336916POP-INN', '0705180969', 'trial stk', 'swazicreatives@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCB8OZB8LO', 'POPINNPAY351270', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK7VWZ3HT', 'POPINNPAY708569', '529561ADyq', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK6VXKUU2', 'POPINNPAY420828', '529561ADyq', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK5VXOGYR', 'POPINNPAY374732', '578589AQkr', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK0VZ5VY4', 'POPINNPAY981312', '529561ADyq', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK5VZEF41', 'POPINNPAY807202', '529561ADyq', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCK5WM79E1', 'POPINNPAY74384', '529561ADyq', 'AdLz336916POP-INN', '254727683082', 'trial stk', 'denniswachira87@gmail.com', 5, 5, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCN2YIPKXC', 'POPINNPAY398133', '529561ADyq', 'AdLz336916POP-INN', '0705180969', 'trial stk', 'swazicreatives@gmail.com', 1, 1, 1, NULL, NULL, '2020-03-29', '23:24:50'),
('OCT84KSIO4', 'POPINNPAY987919', '529561ADyq', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 0, NULL, NULL, '2020-03-29', '23:30:44'),
('OCU64U7CSK', 'POPINNPAY617876', '529561ADyq', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 0, NULL, NULL, '2020-03-30', '12:50:12'),
('OCU84W10G0', 'POPINNPAY917220', '578589AQkr', 'AdLz336916POP-INN', '0705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 0, '2020-03-30', '01:48:00', '2020-03-30', '13:49:45'),
('OCV35HV4XX', 'POPINNPAY151389', '529561ADyq', 'AdLz336916POP-INN', '254705972361', 'trial stk', 'kanjianto@gmail.com', 1, 1, 0, '2020-04-01', '16:00:00', '2020-03-31', '10:26:17'),
('OCV65OVISK', 'POPINNPAY306918', '529561ADyq', 'AdLz336916POP-INN', '0705180969', 'trial stk', 'swazicreatives@gmail.com', 1, 1, 0, '2020-03-12', '06:30:00', '2020-03-31', '14:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `poppintickets`
--

CREATE TABLE `poppintickets` (
  `TicketId` varchar(100) NOT NULL,
  `EventId` varchar(100) NOT NULL,
  `EventTicketTypeId` varchar(200) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `TicketUrl` varchar(255) NOT NULL,
  `TicketStatus` varchar(5) NOT NULL,
  `TicketDate` date NOT NULL,
  `TicketTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppintickets`
--

INSERT INTO `poppintickets` (`TicketId`, `EventId`, `EventTicketTypeId`, `PayerPhone`, `TicketUrl`, `TicketStatus`, `TicketDate`, `TicketTime`) VALUES
('TKT293494PYZK', '549014AUtR', 'TKT47724PGMLXKNXIJBI', '254705972361', 'Tickets/Paid/TKT293494PYZK.pdf', '0', '2020-04-01', '09:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `poppintickets_res`
--

CREATE TABLE `poppintickets_res` (
  `TicketId` varchar(100) NOT NULL,
  `ReserveId` varchar(100) NOT NULL,
  `PayerPhone` varchar(13) NOT NULL,
  `TicketUrl` varchar(255) NOT NULL,
  `TicketStatus` varchar(5) NOT NULL,
  `TicketDate` date NOT NULL,
  `TicketTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppintickets_res`
--

INSERT INTO `poppintickets_res` (`TicketId`, `ReserveId`, `PayerPhone`, `TicketUrl`, `TicketStatus`, `TicketDate`, `TicketTime`) VALUES
('TKT872961PGKAQ', '986983ANdQ', '+254701002544', 'Tickets/Paid/TKT872961PGKAQ.pdf', '0', '2018-08-07', '12:59:32'),
('TKT863083PTOJ', '346174Aumc', '+254701002541', 'Tickets/Paid/TKT863083PTOJ.pdf', '0', '2018-08-13', '10:40:18'),
('TKT374619PAUOW', '986983ANdQ', '+254701002541', 'Tickets/Paid/TKT374619PAUOW.pdf', '0', '2018-08-13', '10:14:36'),
('TKT833114PMTDD', '915002AiXb', '+254701002544', 'Tickets/Paid/TKT833114PMTDD.pdf', '0', '2018-10-17', '19:58:01'),
('TKT179166PAQ', '915002AiXb', '+254706087108', 'Tickets/Paid/TKT179166PAQ.pdf', '0', '2018-10-18', '10:49:53'),
('TKT459378PSZNS', '359130AvzN', '+254727683082', 'Tickets/Paid/TKT459378PSZNS.pdf', '0', '2018-10-25', '07:12:54'),
('TKT743510PTSTH', '584936AIcL', '+254705180960', 'Tickets/Paid/TKT743510PTSTH.pdf', '0', '2018-11-26', '07:33:18'),
('TKT930842PRMSR', '359130AvzN', '+254705180960', 'Tickets/Paid/TKT930842PRMSR.pdf', '0', '2018-11-26', '07:43:40'),
('TKT395198PAVVF', '474275ALsI', '254705972361', 'Tables/Paid/TKT395198PAVVF.pdf', '0', '2019-02-09', '06:13:12'),
('TKT795640PWSQO', '735462AVoB', '254705972361', '../Tickets/Paid/TKT795640PWSQO.pdf', '0', '2019-02-10', '15:56:11'),
('TKT276649PKLCK', '825795Axue', '254791365550', '../Tickets/Paid/TKT276649PKLCK.pdf', '0', '2019-02-14', '05:55:17'),
('TKT281072PLOFL', '362574AKkV', '254705972361', 'Tables/Paid/TKT281072PLOFL.pdf', '0', '2019-03-30', '03:42:54'),
('TKT729754PHKEX', '474275ALsI', '254705180969', 'Tables/Paid/TKT729754PHKEX.pdf', '0', '2019-04-09', '08:57:46'),
('TKT896039PEFTR', '842446AaZJ', '254705180969', 'Tables/Paid/TKT896039PEFTR.pdf', '0', '2019-07-08', '08:35:43'),
('TKT659018PUJYD', '241641ASTp', '254705180969', 'Tables/Paid/TKT659018PUJYD.pdf', '0', '2019-07-16', '05:58:54'),
('TKT675877PAGSK', '842446AaZJ', '254727683082', 'Tables/Paid/TKT675877PAGSK.pdf', '0', '2019-07-25', '12:00:45'),
('TKT637685PVUDE', '110309AeAv', '254705180969', 'Tables/Paid/TKT637685PVUDE.pdf', '0', '2019-07-30', '14:16:31'),
('TKT533228PIDWH', '277234AyGj', '254727683082', 'Tables/Paid/TKT533228PIDWH.pdf', '1', '2019-07-28', '14:40:09'),
('TKT226915PCSET', '578589AQkr', '254705972361', 'Tables/Paid/TKT226915PCSET.pdf', '1', '2020-03-20', '06:20:34'),
('TKT459834PUNJN', '578589AQkr', '254705180969', 'Tables/Paid/TKT459834PUNJN.pdf', '0', '2020-02-28', '11:13:41'),
('TKT694828PLILE', '578589AQkr', '0705972361', '../../reserve/Tickets/Paid/TKT694828PLILE.pdf', '0', '2020-03-30', '10:49:45'),
('TKT648587PAJOX', '578589AQkr', '0705180969', 'Tables/Paid/TKT648587PAJOX.pdf', '0', '2020-03-09', '14:56:18'),
('TKT277373PWGCH', '529561ADyq', '254705972361', 'Tables/Paid/TKT277373PWGCH.pdf', '1', '2020-03-31', '07:28:25'),
('TKT338114PDCIJ', '529561ADyq', '0705972361', '../../reserve/Tickets/Paid/TKT338114PDCIJ.pdf', '0', '2020-03-30', '09:50:12'),
('TKT33783PVXYG', '529561ADyq', '254727683082', '../../reserve/Tickets/Paid/TKT33783PVXYG.pdf', '0', '2020-03-20', '18:45:56'),
('TKT446404PUNWZ', '529561ADyq', '0705180969', '../../reserve/Tickets/Paid/TKT446404PUNWZ.pdf', '1', '2020-03-31', '11:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `poppinuser`
--

CREATE TABLE `poppinuser` (
  `Name` varchar(255) NOT NULL,
  `PhoneNo` varchar(20) NOT NULL,
  `Email` varchar(300) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `poppin_reg_admin`
--

CREATE TABLE `poppin_reg_admin` (
  `Email` varchar(500) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Unique_Code` varchar(255) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppin_reg_admin`
--

INSERT INTO `poppin_reg_admin` (`Email`, `Phone`, `Unique_Code`, `RegDate`, `RegTime`) VALUES
('kanjianto@gmail.com', '', 'AxHK584797POP-INRSV', '2018-10-15', '12:41:43'),
('kanji@shamalandscapes.com', '', 'AeRN180264POP-INRSV', '2018-10-17', '20:05:41'),
('cetricka1@gmail.com', '', 'Aclg473027POP-INRSV', '2019-04-12', '10:51:09'),
('info@popin.co.ke', '', 'AdFy865767POP-INRSV', '2019-04-13', '02:33:04'),
('trenisbrage@gmail.com', '', 'AFWU189706POP-INRSV', '2019-05-19', '11:36:14'),
('kanjiantonio@gmail.com', '0737893694', 'Axkl946959POP-INRSV', '2019-07-28', '09:36:12'),
('cetricka@gmail.com', '0704289994', 'Acyu669225POP-INRSV', '2019-07-28', '08:30:53'),
('dennis@popin.co.ke', '0727683082', 'AzBr243803POP-INRSV', '2019-07-28', '10:27:46'),
('jeanmario35@gmail.com', '0790560382', 'ARBU769732POP-INRSV', '2019-07-29', '04:01:53'),
('swazicreatives@gmail.com', '0705180969', 'Atqz848352POP-INRSV', '2019-08-17', '13:20:08');

-- --------------------------------------------------------

--
-- Table structure for table `poppin_reg_superadmin`
--

CREATE TABLE `poppin_reg_superadmin` (
  `Email` varchar(500) NOT NULL,
  `Unique_Code` varchar(255) NOT NULL,
  `RegDate` date NOT NULL,
  `RegTime` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppin_reg_superadmin`
--

INSERT INTO `poppin_reg_superadmin` (`Email`, `Unique_Code`, `RegDate`, `RegTime`) VALUES
('kanjianto@gmail.com', 'AxHK584797POP-INRSV', '2018-10-15', '12:41:43'),
('kanji@shamalandscapes.com', 'AeRN180264POP-INRSV', '2018-10-17', '20:05:41'),
('kanjiantonio@gmail.com', 'AJKx602103POP-INRSV', '2018-10-25', '05:39:29'),
('swazicreatives@gmail.com', 'AOsB619051POP-INRSV', '2018-11-01', '04:45:02');

-- --------------------------------------------------------

--
-- Table structure for table `poppin_ticket_type`
--

CREATE TABLE `poppin_ticket_type` (
  `EventId` varchar(200) NOT NULL,
  `TicketTypeId` varchar(200) NOT NULL,
  `TicketType` varchar(200) NOT NULL,
  `TicketPrice` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poppin_ticket_type`
--

INSERT INTO `poppin_ticket_type` (`EventId`, `TicketTypeId`, `TicketType`, `TicketPrice`) VALUES
('143133ATfH', 'TKT684362PYAWSZYFENJABVUC', 'at the gate', 1200),
('143133ATfH', 'TKT196535PYAWSZYFENJA', 'couples', 800),
('143133ATfH', 'TKT75470PYAWSZYFE', 'advance', 500),
('143133ATfH', 'TKT236806PYAWS', 'regular ', 1000),
('549014AUtR', 'TKT47724PGMLXKNXIJBI', 'Wabebe Couples', 1),
('549014AUtR', 'TKT834266PGMLXKNXI', 'Wabebe Gate', 200),
('549014AUtR', 'TKT208545PGMLX', 'Wabebe Advance', 100),
('544595AsJx', 'TKT385358PKDSEJUMU', 'Adva 2', 100),
('544595AsJx', 'TKT376945PKDSE', 'Adva Santa', 100),
('143133ATfH', 'TKT61988PYAWSZYFENJABVUCMMVR', 'vip', 2000),
('116407AOIP', 'TKT168968PPSFS', 'regular ', 1000),
('116407AOIP', 'TKT250457PPSFSJISZ', 'advance', 500),
('116407AOIP', 'TKT564005PPSFSJISZXSQW', 'couples', 800),
('116407AOIP', 'TKT455009PPSFSJISZXSQWBEFM', 'at the gate', 1200),
('116407AOIP', 'TKT331763PPSFSJISZXSQWBEFMRPOL', 'vip', 2000),
('884901ADXi', 'TKT671795PADPG', 'regular', 1000),
('884901ADXi', 'TKT133365PADPGLQGE', 'advance', 500),
('884901ADXi', 'TKT321145PADPGLQGEHCBN', 'couples', 800),
('884901ADXi', 'TKT187790PADPGLQGEHCBNOGON', 'at the gate', 1200),
('884901ADXi', 'TKT512438PADPGLQGEHCBNOGONZU', 'vip', 2000),
('715961AjGs', 'TKT627802PBFKJ', 'general', 1000),
('549014AUtR', 'TKT47724GANJI', 'Wabebe Ganji', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mpesa_c2b_data`
--
ALTER TABLE `mpesa_c2b_data`
  ADD PRIMARY KEY (`Receipt`);

--
-- Indexes for table `mpesa_data`
--
ALTER TABLE `mpesa_data`
  ADD PRIMARY KEY (`Receipt`),
  ADD KEY `MerchantRequestID` (`MerchantRequestID`);

--
-- Indexes for table `mpesa_errors`
--
ALTER TABLE `mpesa_errors`
  ADD PRIMARY KEY (`MerchantRequestID`);

--
-- Indexes for table `music_payment`
--
ALTER TABLE `music_payment`
  ADD PRIMARY KEY (`MerchantRequestID`);

--
-- Indexes for table `poppindrinks`
--
ALTER TABLE `poppindrinks`
  ADD PRIMARY KEY (`DrinkId`);

--
-- Indexes for table `poppindrinkspayment`
--
ALTER TABLE `poppindrinkspayment`
  ADD PRIMARY KEY (`PaymentId`),
  ADD UNIQUE KEY `TransactionReference` (`TransactionReference`),
  ADD KEY `TransactionReference_2` (`TransactionReference`),
  ADD KEY `PaymentId` (`PaymentId`);

--
-- Indexes for table `poppindrinksreceipt`
--
ALTER TABLE `poppindrinksreceipt`
  ADD PRIMARY KEY (`ReceiptId`);

--
-- Indexes for table `poppinevent`
--
ALTER TABLE `poppinevent`
  ADD PRIMARY KEY (`EventId`);

--
-- Indexes for table `poppineventpayment`
--
ALTER TABLE `poppineventpayment`
  ADD PRIMARY KEY (`PaymentId`),
  ADD UNIQUE KEY `TransactionReference` (`TransactionReference`),
  ADD KEY `TransactionReference_2` (`TransactionReference`);

--
-- Indexes for table `poppinlogin`
--
ALTER TABLE `poppinlogin`
  ADD PRIMARY KEY (`LoginTimes`);

--
-- Indexes for table `poppinmpesa`
--
ALTER TABLE `poppinmpesa`
  ADD PRIMARY KEY (`TransactionReference`);

--
-- Indexes for table `poppinreg`
--
ALTER TABLE `poppinreg`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `poppinreg_res`
--
ALTER TABLE `poppinreg_res`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `poppinreg_res_superadmin`
--
ALTER TABLE `poppinreg_res_superadmin`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `poppinreserve`
--
ALTER TABLE `poppinreserve`
  ADD PRIMARY KEY (`ReserveId`);

--
-- Indexes for table `poppinreservepayment`
--
ALTER TABLE `poppinreservepayment`
  ADD PRIMARY KEY (`PaymentId`),
  ADD UNIQUE KEY `TransactionReference` (`TransactionReference`),
  ADD KEY `TransactionReference_2` (`TransactionReference`);

--
-- Indexes for table `poppintickets`
--
ALTER TABLE `poppintickets`
  ADD PRIMARY KEY (`TicketId`);

--
-- Indexes for table `poppintickets_res`
--
ALTER TABLE `poppintickets_res`
  ADD PRIMARY KEY (`TicketId`);

--
-- Indexes for table `poppin_reg_admin`
--
ALTER TABLE `poppin_reg_admin`
  ADD PRIMARY KEY (`Unique_Code`);

--
-- Indexes for table `poppin_ticket_type`
--
ALTER TABLE `poppin_ticket_type`
  ADD PRIMARY KEY (`TicketTypeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `poppinlogin`
--
ALTER TABLE `poppinlogin`
  MODIFY `LoginTimes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=537;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
