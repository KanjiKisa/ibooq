<?php 

require_once("Customers/includes/initialise.php");

if(!isset($_GET["search"])) {

  header("Location: index.php");

} else {

  $keyword = $_GET["p"];

}

?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>ibooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Hind|Poppins|Nunito|Archivo Narrow">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Authentication/styles/css/font-awesome.min.css" />-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="Front End/css/themify-icons/themify-icons.css">
   <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500">  
    
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	
	 <!-- Bootstrap core CSS -->
   <!--<link href="Front End/mdlront End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css">-->


</head>
<body id="reserve-page">
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>

<main class="about-section" >
    
    
<!--venues card section--> 
<section id="events">
    <!--<div class="container-fluid">-->
<div class="container">
   
<div class="row">
<div class="col-lg-12 col-md-12 ftco-animate d-flex ">
            <div class="text">
              <br/><br/><br/><br/>
              <h4 class"ibooq-title" style="color:#8AD879; text-align:center;">SEARCH : <?php echo $keyword;?></h4>
              <form action="" method="post" id="select_service_form">
              <div class="row">
                <div class="col-sm padding-0">
                  <div class="form-group">
                          <label for="Hairdresser" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Hairdresser" value="Hairdresser" id="Hairdresser"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Hairdresser</strong>
                            </label>
                            
                  </div>
                  <div class="form-group">
                          <label for="Model" class="btn-switch-search">
                                <input type="checkbox" class="services_search" class="services_search" name="Model" value="Model" id="Model"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Model</strong>
                            </label>    
                  </div>
                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Colorist" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Colorist" value="Colorist" id="Colorist"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Colorist</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Makeup_artist" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Makeup_artist" value="Makeup artist" id="Makeup_artist"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Makeup artist</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Barber" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Barber" value="Barber" id="Barber"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Barber</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Masseur" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Masseur" value="Masseur" id="Masseur"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Masseur</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Beautician" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Beautician" value="Beautician" id="Beautician"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Beautician</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Wig_master" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Wig_master" value="Wig master" id="Wig_master"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Wig master</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Wellness_instructor" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Wellness_instructor" value="Wellness instructor" id="Wellness_instructor"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Wellness instructor</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Public_health_educator" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Public_health_educator" value="Public health educator" id="Public_health_educator"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Public health educator</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Corporate_wellness_instructor" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Corporate_wellness_instructor" value="Corporate wellness instructor" id="Corporate_wellness_instructor"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Corporate wellness instructor</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Community_health_worker" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Community_health_worker" value="Community health worker" id="Community_health_worker"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Community health worker</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Health_services_manager" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Health_services_manager" value="Health services manager" id="Health_services_manager"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Health services manager</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Health_coach" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Longevity_wellness_specialist" value="Longevity wellness specialist" id="Longevity_wellness_specialist"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Longevity wellness specialist</strong>
                            </label>   
                  </div>

                </div>

                <div class="col-sm padding-0">
                  <div class="form-group">

                                <label for="Back_injury_prevention_specialist" class="btn-switch-search">
                                    <input type="checkbox" class="services_search" name="Back_injury_prevention_specialist" value="Back injury prevention specialist" id="Back_injury_prevention_specialist"/> 
                                      
                                      <em class="glyphicon glyphicon-ok"></em>
                                      <strong>Back injury prevention specialist</strong>
                                </label>
                            
                  </div>
                  <div class="form-group">
                            <label for="Golf_conditioning_specialist" class="btn-switch-search">
                                <input type="checkbox" class="services_search" name="Golf_conditioning_specialist" value="Golf conditioning specialist" id="Golf_conditioning_specialist"/> 
                                  
                                  <em class="glyphicon glyphicon-ok"></em>
                                  <strong>Golf conditioning specialist</strong>
                            </label>   
                  </div>

                </div>

              </div>

              </form>
            </div>
          </div>
</div>
    
<div class="row" id="result">
              
             
                    <?php 

                    if($database->search_owners($keyword) !== False) {
                      $sql = $database->search_owners($keyword);

                      foreach($sql as $row) {

                          $userId = $row["UserId"];
                          $clubName = $row["Username"];
                          $profilePic = $row["ProfilePic"];
                          $coverPic = $row["CoverPic"]; 
                          $open_days = $row["OpeningDays"];
                          $open_time = $row["OpeningTime"];
                          $close_time = $row["ClosingTime"];
                          $city = $row["City"];

                    ?>

                            <div class="col-sm-3 provider-card" style="margin-bottom:5px;">
                            <a href="ibooq.php?rs_id=<?php echo $userId; ?>" style="text-decoration:none;">
                                <div class="card">

                                <div class="club-poster">
                                    <img class="card-img-top sample-project" src="<?php echo $profilePic; ?>" alt="<?php echo $clubName; ?>">
                                    
                                </div>

                                  <div class="card-body">
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $clubName; ?></strong></p>
                                  </div>

                                </div><!-- close of card class div-->

                                </a>

                          </div>
                      


                    <?php 


                       }

                      } else if($database->search_services($keyword) !== False) {
                          $sql = $database->search_services($keyword);
    
                          foreach($sql as $row) {
    
                              $ReserveId = $row["ReserveId"];
                              $OwnerId = $row["OwnerId"];
                              $ReserveCategory = $row["Category"];
                              $ReserveName = $row["ReserveName"];
                              $profilePic = $row["PicPath"];

                              $database->fetch_reservation_user_reg_info($OwnerId);
                      
                    ?>

                        <div class="col-sm-3 provider-card" style="margin-bottom:5px;">
                            <a href="ibooq.php?rs_id=<?php echo $OwnerId; ?>" style="text-decoration:none;">
                                <div class="card">

                                <div class="club-poster">
                                    <img class="card-img-top sample-project" src="Uploaded/Images/defaultP.jpg" alt="<?php echo $ReserveCategory; ?>">
                                    
                                </div>

                                  <div class="card-body">
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $ReserveName; ?> (<?php echo $ReserveCategory; ?>)</strong></p>
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $database->username; ?></strong></p>
                                  </div>

                                </div><!-- close of card class div-->

                                </a>

                          </div>

                      <?php
                      
                          }

                        } else {
                            echo "No results";
                        }
                      
                      ?>


</div>
</div>
</section>

</main>



    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    <script>  
        $('.services_search').click(function() {

       //if(this.checked || this.unchecked){
        
            var ids = [];  
           $('.services_search').each(function(){  
                if($(this).is(":checked"))  
                {  
                     ids.push($(this).val());  
                }  
           });  
           ids = ids.toString();

            $.ajax({
                type: "POST",
                url: 'search.php',
                data: {id:ids}, //--> send id of checked checkbox on other page
                success: function(data) {
                    $('#result').html(data);
                },
                 error: function() {
                    console.log('it broke');
                },
                complete: function() {
                    console.log('it completed');
                }
            });

          //  }
      });

 </script>  
	
	 <!-- Bootstrap core JavaScript 
    <script src="Front End/vendor/jquery/jquery.min.js"></script>
    <script src="Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script> 

    </script>-->
  <!--side bars-->
       
    
    <script language="javascript">
        function fbshareCurrentPage(var url)
        {
            window.open('https://www.facebook.com/sharer/sharer.php?u=&t='+document.title, '','menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false; }
    </script>

</body>
</html>