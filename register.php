<?php

    require_once("Customers/includes/initialise.php");
  
  if(isset($_POST['subReg'])) {
    $automation->generate_user_id();

    $userId = $automation->random_user_id;
    $fname = $_POST["firstName"];
    $lname = $_POST["lastName"];
    $username = $fname." ".$lname;
    $userType = $_POST["userType"];
    $email = $_POST["userEmail"];
    $phoneNo = $_POST["phone"];
    $address = $_POST["userAddress"];
    $city = $_POST["userCity"];
    $country = $_POST["userCountry"];
    $pass = $_POST["pass"];
    $conPass = $_POST["confirmPass"];

	
		
    $database->set_user_id($userId);
    $database->set_username($username);
    $database->set_user_type($userType);
	$database->set_profile_pic("Uploaded\Images\defaultP.jpg");
	$database->set_cover_pic("Uploaded\Images\defaultC.jpg");
	$database->set_open_days("Weekends");
	$database->set_open_time("18:00:00");
	$database->set_close_time("06:00:00");
    $database->set_email($email);
    $database->set_phone_no($phoneNo);

    if($userType === "Client") {
      $database->set_account_approval("1");
    } else {
      $database->set_account_approval("0");
    }
    
    $database->set_address($address);
    $database->set_city($city);
    $database->set_country($country);

    if($pass == $conPass) {
      $database->set_password($pass);
      
      if($database->insert_to_reg_table("0","0","0")) {

         /*$mailSending->set_from_address("market@shamalandscapes.com");
         $mailSending->set_from_name("Shama Landscape Architects Limited POS");
         $mailSending->set_to_address($email);
         $mailSending->set_to_name($username);
         $mailSending->set_subject("Welcome");
         $mailSending->set_message("Thank you for choosing Shama Landscape Architects LTD Market Point Of Sale as your trusted Market place. 
          You can now purchase different products and we will do the delivery.");
         $mailSending->add_attachment("Invoices/Undelivered/".$invoice_no.".pdf");
         $mailSending->set_variables();

         if($mailSending->send_email()) {*/
            
            header("Location: login.php");

        /* } else {

            echo "Error Occured";

         }*/

      } else {
        
        $alert->message("Failed to register","Fail");
 

      }
      
    } else {

      $alert->message("Passwords do not match","Fail");

    } 
    
  }

?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela Round|Oswald|Raleway|Coiny|Montserrat" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/style2.css" />-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/styling/style.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
   <!-- <link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Front End/style.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 


</head>
<body >

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>

<div id="head">
<br/>
   <div class="container">

   <div class="row no-gutters ">
        <div class="col-md-6 padding-0" style='background: url("Front End/Images/nairobi.jpg");' id="login_left_panel">
          <h4 style="text-align:center; color:#fff;" id="login_left_panel_text">iBook</h4>
        </div>
        <div class="col-md-6 ">
          
          <form action="register.php" method="POST" id="regForm">
          <h4 style="color:#8AD879;text-align:center;">Create Account</h4>
                <input type="text"  class="form-control form-control-lg" placeholder="First Name" name="firstName" required>
                <input type="text"  class="form-control form-control-lg" placeholder="Last Name" name="lastName" required>
                <input type="text"  class="form-control form-control-lg" placeholder="Email" name="userEmail" required>
                <input type="number"  class="form-control form-control-lg" placeholder="Phone Number." name="phone" required>
                <input type="hidden"  class="form-control form-control-lg" placeholder="Address" name="userAddress" value="23" required>
                <select name="userCity"  class="form-control form-control-lg" required>
                  <option value="Athi River (Mavoko)">Athi River (Mavoko)</option>
                  <option value="Awendo">Awendo</option>
                  <option value="Banisa (Banissa)">Banisa (Banissa)</option>
                  <option value="Bondo">Bondo</option>
                  <option value="Bungoma">Bungoma</option>
                  <option value="Busia">Busia</option>
                  <option value="Chuka">Chuka</option>
                  <option value="Elburgon">Elburgon</option>
                  <option value="Eldama Ravine">Eldama Ravine</option>
                  <option value="Eldoret">Eldoret</option>
                  <option value="Elwak">Elwak</option>
                  <option value="Emali">Emali</option>
                  <option value="Embu">Embu</option>
                  <option value="Garbatula">Garbatula</option>
                  <option value="Garissa">Garissa</option>
                  <option value="Gilgil">Gilgil</option>
                  <option value="Githunguri">Githunguri</option>
                  <option value="Habaswein">Habaswein</option>
                  <option value="Hola">Hola</option>
                  <option value="Homa Bay">Homa Bay</option>
                  <option value="Isebania">Isebania</option>
                  <option value="Isiolo">Isiolo</option>
                  <option value="Juja">Juja</option>
                  <option value="Kabarnet">Kabarnet</option>
                  <option value="Kajiado">Kajiado</option>
                  <option value="Kakamega">Kakamega</option>
                  <option value="Kakuma">Kakuma</option>
                  <option value="Kapsabet">Kapsabet</option>
                  <option value="Karatina">Karatina</option>
                  <option value="Karuri">Karuri</option>
                  <option value="Kehancha">Kehancha</option>
                  <option value="Kenol">Kenol</option>
                  <option value="Kericho">Kericho</option>
                  <option value="Kerugoya">Kerugoya</option>
                  <option value="Kiambu">Kiambu</option>
                  <option value="Kikuyu">Kikuyu</option>
                  <option value="Kilifi">Kilifi</option>
                  <option value="Kimilili">Kimilili</option>
                  <option value="Kiminini">Kiminini</option>
                  <option value="Kiserian">Kiserian</option>
                  <option value="Kisii">Kisii</option>
                  <option value="Kisumu">Kisumu</option>
                  <option value="Kitale">Kitale</option>
                  <option value="Kitengela">Kitengela</option>
                  <option value="Kitui">Kitui</option>
                  <option value="Lafey">Lafey</option>
                  <option value="Lamu">Lamu</option>
                  <option value="Limuru">Limuru</option>
                  <option value="Lodwar">Lodwar</option>
                  <option value="Machakos">Machakos</option>
                  <option value="Mai Mahiu">Mai Mahiu</option>
                  <option value="Mairo Inya">Mairo Inya</option>
                  <option value="Makindu">Makindu</option>
                  <option value="Makutano (Kapenguria)">Makutano (Kapenguria)</option>
                  <option value="Malaba">Malaba</option>
                  <option value="Malindi">Malindi</option>
                  <option value="Mandera">Mandera</option>
                  <option value="Maralal">Maralal</option>
                  <option value="Mariakani">Mariakani</option>
                  <option value="Marsabit">Marsabit</option>
                  <option value="Masalani">Masalani</option>
                  <option value="Maua">Maua</option> 
                  <option value="Mazeras">Mazeras</option>
                  <option value="Mbale (Vihiga)">Mbale (Vihiga)</option>
                  <option value="Mbita (Mbita Point)">Mbita (Mbita Point)</option>
                  <option value="Meru">Meru</option>
                  <option value="Migori">Migori</option>
                  <option value="Mlolongo">Mlolongo</option>
                  <option value="Moi's Bridge">Moi's Bridge</option>
                  <option value="Molo (Turi)">Molo (Turi)</option>
                  <option value="Mombasa">Mombasa</option>
                  <option value="Moyale">Moyale</option>
                  <option value="Msambweni">Msambweni</option>
                  <option value="Mtwapa">Mtwapa</option>
                  <option value="Mumias">Mumias</option>
                  <option value="Murang'a">Murang'a</option>
                  <option value="Mwingi">Mwingi</option>
                  <option value="Nairobi">Nairobi</option>
                  <option value="Naivasha">Naivasha</option>
                  <option value="Nakuru">Nakuru</option>
                  <option value="Namanga">Namanga</option>
                  <option value="Nanyuki">Nanyuki</option>
                  <option value="Narok">Narok</option>
                  <option value="Ngong">Ngong</option>
                  <option value="Njoro">Njoro</option>
                  <option value="Nyahururu">Nyahururu</option>
                  <option value="Nyamira">Nyamira</option>
                  <option value="Nyeri">Nyeri</option>
                  <option value="Ongata Rongai">Ongata Rongai</option>
                  <option value="Oyugis">Oyugis</option>
                  <option value="Rhamu">Rhamu</option>
                  <option value="Rongo">Rongo</option>
                  <option value="Ruiru">Ruiru</option>
                  <option value="Siaya">Siaya</option>
                  <option value="Takaba">Takaba</option>
                  <option value="Taveta">Taveta</option>
                  <option value="Thika">Thika</option>
                  <option value="Ukunda">Ukunda</option>
                  <option value="Voi">Voi</option>
                  <option value="Wajir">Wajir</option>
                  <option value="Wanguru">Wanguru</option>
                  <option value="Watamu">Watamu</option>
                  <option value="Webuye">Webuye</option>
                  <option value="Wote">Wote</option>
                </select>
                <select name="userCountry"  class="form-control form-control-lg" required>
                  <option value="Kenya">Kenya</option>
                </select>
                <label for="userType">Register As</label>
                <select name="userType"  class="form-control form-control-lg" required>
                  <option value="Client">Client</option>
                  <option value="BO">Business Owner</option>
                  <option value="SP">Service Provider</option>
                </select>
                <input type="password"  class="form-control form-control-lg" placeholder="Password" name="pass" required>
                <input type="password"  class="form-control form-control-lg" placeholder="Confirm Password" name="confirmPass" required>
                <input type="submit" class="btn btn-primary" value="Register" name="subReg"><br><br><br>
              <div class="switch-login">
                <a href="login.php">Already have an account? <span>Login</span></a>
                <a href="index.php"><i class="fas fa-home"></i>Home</a>
              </div>
          </form>
        </div>
      </div>
     
    </div>
 
</div>

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->

</body>
</html>