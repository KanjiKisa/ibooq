<?php 

require_once("Customers/includes/initialise.php");

$ReserveId = $_GET['rs_id'];

$sql = $database->fetch_from_reg_by_owner_id($ReserveId);

foreach($sql as $row) {

  $userId = $row["UserId"];
  $clubName = $row["Username"];
  $profilePic = $row["ProfilePic"];
  $coverPic = $row["CoverPic"]; 
  $open_days = $row["OpeningDays"];
  $open_time = $row["OpeningTime"];
  $close_time = $row["ClosingTime"];
  $city = $row["City"];
  
}

?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <!--<link href="https://fonts.googleapis.com/css?family=Roboto|Oswald" rel="stylesheet">-->
     <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="Authentication/styles/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />
	
	 <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins|Inconsolata|Droid+Sans|Raleway|Merriweather|">

    <!-- Bootstrap core CSS -->
    <link href="Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css">
   <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500">   
    

	

</head>
<body id="inner-page" >
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>
  
  
  <section id="club-inner">
  <br/> <br/> <br/> <br/>
  <div class="container">    
   <div class="row no-gutters">
              <div class="col-md-3">
                  <img src="<?php echo $profilePic; ?>" class="img-circle" style="">
                  
              </div>
              <div class="col-md-8">
                  <div class="club-details featured-title-box">
                       <h3 class="event-name"><?php echo $clubName; ?></h3>
                      <ul class="list-unstyled">
                          <li style="color:#000000;"><i class="fas fa-map-marked"></i><?php echo $city; ?></li>
                          <li style="color:#000000;"><i class="fas fa-calendar-week"></i><?php echo $open_days; ?></li>
                          <li style="color:#000000;"><i class="fas fa-clock"></i><?php echo $open_time; ?></li>
                      </ul>
                  </div>
                  
                  <div class="happy-hour-offers">
                      <a href="shop/index.php?drk=<?php echo $ReserveId; ?>" class="btn-primary mb-5" > <?php echo $clubName; ?>'s Shop</a>
                  </div>
                  
                  
              </div>
          </div>
      </div>
  </section>
  
  <main class="about-section" >
    
    
<!--venues card section--> 
<section id="events">
    <!--<div class="container-fluid">-->
<div class="container">
   
    
<div class="row">
              
                    <?php 

                      $sql = $database->fetch_from_reserve_table_by_club($ReserveId);

                      foreach($sql as $row) {

                          $category = $row["Category"];
                          $reserveId = $row["ReserveId"];
                          $ownerId = $row["OwnerId"];
                          $reserveName = $row["ReserveName"];
                          $reserveDesc = $row["ReserveDesc"];
                          $reserveStock = $row["ReserveStock"];
                          $reservePrice = $row["ReservePrice"];
                          $pic = $row["PicPath"];

                          $hours = floor($reserveStock / 3600);
                          $minutes = floor(($reserveStock / 60) % 60);
                          $seconds = $reserveStock % 60;

                          
                          $duration = $hours." hours ".$minutes." minutes";
                         

                    ?>


                            <div class="col-sm-3 provider-card" style="margin-bottom:5px;">
                                <div class="card">

                                <form action="booqing.php" method="post" >

                                <button type="submit" id="subMpesa" name="subMpesa" style="background:#fff;border:0px;">
                                <div class="club-poster"><a href="ibooq.php?rs_id=<?php echo $userId; ?>">
                                    <img class="card-img-top sample-project" src="<?php echo $pic; ?>" alt="<?php echo $clubName; ?>">
                                    </a>
                                </div>

                                  <div class="card-body">
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $reserveName; ?> (<?php echo $category; ?>)</strong></p>
                                    <p class="card-text" style="color:#000000;font-size:12px;"><strong>Ksh <?php echo $reservePrice; ?></strong></p>
                                    <p class="card-text" style="color:#000000;font-size:12px;"><strong>Duration: <?php echo $duration; ?></strong></p>

                                    

                                        <div id="display"></div>
                                          
                                          <div class="form-group">
                                           <input type="hidden" class="form-control" id="service_id" name="service_id" value="<?php echo $reserveId; ?>" required="required">
                                         </div>
                                          <div class="form-group">
                                           <input type="hidden" class="form-control" id="rs_id" name="rs_id" value="<?php echo $ReserveId; ?>" required="required">
                                         </div>
                                         <div class="form-group">
                                           <input type="hidden" class="form-control" id="open_days" name="open_days" value="<?php echo $open_days; ?>" required="required">
                                         </div>
                                         <div class="form-group">
                                           <input type="hidden" class="form-control" id="open_time" name="open_time" value="<?php echo $open_time; ?>" required="required">
                                         </div>
                                         <div class="form-group">
                                           <input type="hidden" class="form-control" id="close_time" name="close_time" value="<?php echo $close_time; ?>" required="required">
                                         </div>

                                         <div class="form-group">
                                           <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $userId; ?>" required="required">
                                         </div>
                                         

                                    
                                  </div>
                                  </button>
                                  </form>
                                </div><!-- close of card class div-->

                          </div>



                    <?php 


                       }


                    ?>


</div>
</div>
</section>

</main>

    
   <!-- Footer strat -->
    <footer class="footer">
      <div class="foo-top">
        <div class="container text-center">
          <div class="row no-gutters align-items-center">
            <div class="col-md-12">
              <div class="widget widget-insta-feed">
                <ul class="list-unstyled mt-4">
                  <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" class="tweet"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" class="send"><i class="fab fa-whatsapp"></i></a></li>
                  <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                  
                </ul>
              </div>
              <div class="widget widget-address">
                <address>
                  
                  <a href="tel:"><i class="ti-mobile mr-2"></i>0700...</a>
                  <a href="mailto:" class="d-block"><i class="ti-email mr-2"></i> info@ibooq.co.ke</a>
                </address>
              </div>
            </div>

          </div>
        </div>
      </div>

    </footer>
    <!-- Footer end -->

</div>

<!--<style>
    .navigator .link a{
        border: 1pX solid #df3d82;
        padding: 0.4rem 0.8rem;
        color: #000;
        text-decoration: none;
        font-family: 'Inconsolata';
        font-size: 1.4rem;
    }
</style>-->

    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
     
	 <!-- Bootstrap core JavaScript -->
    <script src="Front End/vendor/jquery/jquery.min.js"></script>
    <script src="Front End/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<script src="Front End/styling/script.js"></script>
    <script src="Front End/scripting/e-shama.js"></script>

    </script>
  <!--side bars-->
       
    <script>

         $(document).ready(function(){
          $('.modal').on('show.bs.modal', function(e) {

            var payId = $(e.relatedTarget).data('book-id');

            $(e.currentTarget).find('input[name="service_id"]').val(payId);

          });

        });

    </script>
    
     <script language="javascript">
        function fbshareCurrentPage()
        {
            window.open("https://www.facebook.com/sharer/sharer.php?u="+escape(window.location.href)+"&t="+document.title, '', 
        'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
        return false; }
    </script>

</body>
</html>