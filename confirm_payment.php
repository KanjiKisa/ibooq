<?php

require_once("Customers/includes/initialise.php");

 if(isset($_POST["subMpesa"])) {
        
        //initialisation of variables
        
        $PartyA = $_POST['user_phone'];
        $email = $_POST['buyer_email'];
        $Arrival_Date = $_POST['Arrival_Date'];
        $Arrival_Time = $_POST['Arrival_Time'];
        
        if($payment->startsWith($PartyA,"07")) {
            $mob = ltrim($PartyA,"0");
            $PartyA = "254".$mob;
        } else if($payment->startsWith($PartyA,"+254")) {
            $PartyA = ltrim($PartyA,"+");
        } else if($payment->startsWith($PartyA,"Pay Using: ")) {
            $PartyA = ltrim($PartyA,"Pay Using: ");
        }
        
        $Service_Id = $_POST['service_id'];
        $Token_Id = $_POST['token_id'];

                            
        $database->fetch_specific_reserve_data($Service_Id);
        
        $ReserveOwner = $database->fetched_user_id;
        
        $Service_Price = $database->fetched_rs_price;
        
        //payment id
        $automation->generate_pay_id();
		
		$Payment_Id = $automation->pay_id;
        
        //initiate mpesa stk push
        
        $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
          
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_URL, $url);
          curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json','Authorization:Bearer '.$Token_Id)); //setting custom header
          
          $t=time();
          $code = "336729";
          $passkey = "eaadafd19826867d385fee6f3b2c999f968096c954431d5d32a40059b3c76110";
          $stamp = date("YmdHis",$t);
          $enco = $code.$passkey.$stamp;
            $pass = base64_encode($enco);
            
          $curl_post_data = array(
            //Fill in the request prameters with valid values
            'BusinessShortCode' => "336729",
            'Password' => $pass,
            'Timestamp' => $stamp,
            'TransactionType' => 'CustomerBuyGoodsOnline',
            'Amount' => $Service_Price,
            'PartyA' => $PartyA,//254712473322
            'PartyB' => "336016",//588797
            'PhoneNumber' => $PartyA,
            'CallBackURL' => 'https://ibooq.shamalandscapes.com/callback.php',
            'AccountReference' => 'ref',
            'TransactionDesc' => 'trial'
          );
          
          $data_string = json_encode($curl_post_data);
          
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_POST, true);
          curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
          
          $curl_response = curl_exec($curl);
          
          
        /*  $curl_response = '{
              "MerchantRequestID":"18632-3961752-1", 
              "CheckoutRequestID":"ws_CO_MER_29092019093927294", 
              "ResponseCode": "0", 
              "ResponseDescription":"Success. Request accepted for processing", 
              "CustomerMessage":"Success. Request accepted for processing" }';*/
          
        if(isset($curl_response['errorCode'])) {
              $code = false;
          } 

          //else if(isset($curl_response['ResponseCode'])) {
              
              $MerchantId = json_decode($curl_response,true)['MerchantRequestID'];
          
                $response_code = json_decode($curl_response,true)["ResponseCode"];
            
            if($response_code == 0) {
                $code = true;
                
            } else {
                $error_code = json_decode($curl_response,true)["errorCode"];
                $alert->message("Failure","Fail");
                $code = false;
            } 
            
          //}
    
 }  else {
     header("Location: index.php");
 }


?>

<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

  <meta http-equiv="Cache-control" content="no-cache">
  <meta http-equiv="Expires" content="-1">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway" rel="stylesheet">
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

</head>
<body >
    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>
      
<br><br>

    <div class="d flex flex-column" id="shama_section">

<br/><br/><br/>

                  <div class="my-flex-item">


               <div class="mdl-grid" style="justify-content: center; ">
                   
                   <div class="mdl-cell--10-col mdl-cell--10-col-tablet mdl-cell--10-col-phone" style="margin-left: 10px; margin-top: 10px;">
              
                            <div class="row">

                                          <div class="col-md-8">
                                              
                                        <?php
                                          
                                                    if($code) {
                                            
                                        ?>
                                               
                                               <input type="hidden" id="PartyA" value="<?php echo $PartyA; ?>">
                                               <input type="hidden" id="email" value="<?php echo $email; ?>">
                                               <input type="hidden" id="Arrival_Date" value="<?php echo $Arrival_Date; ?>">
                                               <input type="hidden" id="Arrival_Time" value="<?php echo $Arrival_Time; ?>">
                                               <input type="hidden" id="ServiceId" value="<?php echo $Service_Id; ?>">
                                               <input type="hidden" id="PaymentId" value="<?php echo $Payment_Id; ?>">
                                               <input type="hidden" id="MerchantId" value="<?php echo $MerchantId; ?>">
                                          
                                            <div id="spin">
                                                <!--<div class="pinner-border" role="status">
                                                  <span class="sr-only">Loading...</span>
                                                </div>-->
                                            </div>
                                    
                                            <p style="color:#000" id="countdown_message"></p>
                                            
                                            <?php
                                            
                                                } else {
                                            
                                            ?>
                                            
                                            
                                                
                                                <h6><?php //print_r(json_decode($curl_response));?></h6>
                                                
                                                <a href="index.php"><h6>Start Again</h6></a>
                                                
                                            
                                            <?php } ?>
                                            
                                          </div>
                                    
                                          
                                          
                             </div>
                    
                    </div>

                </div><!-- close of mdl-grid class div--> 

          </div>

    </div>

    <footer class="footer">
      <div class="foo-top">
        <div class="container text-center">
          <div class="row no-gutters align-items-center">
            <div class="col-md-12">
              <div class="widget widget-insta-feed">
                <ul class="list-unstyled mt-4">
                  <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" class="tweet"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" class="send"><i class="fab fa-whatsapp"></i></a></li>
                  <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                  
                </ul>
              </div>
              <div class="widget widget-address">
                <address>
                  
                  <a href="tel:"><i class="ti-mobile mr-2"></i>0700...</a>
                  <a href="mailto:" class="d-block"><i class="ti-email mr-2"></i> info@ibooq.co.ke</a>
                </address>
              </div>
            </div>

          </div>
        </div>
      </div>

    </footer>
    
     <style>
        h2{
            font-family: 'Oswald';
            
        }
        
        .btn{
            background: #ad67ea;
            background: #ad67ea;
  font-size: 0.9rem;
  padding: 15px 25px;
  color: #fff;
  text-transform: uppercase;
  font-family: 'Varela Round';
  border: 1px solid #fff;
  border-radius: 5px;
  -webkit-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out;
        }
        
        ul{
            list-style: none;
        }
        
        ul li{
            font-family: 'Raleway';
        }
    </style>



    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>
    <script src="Front End/automate_pay.js"></script>

    </script>
  <!--side bars-->
      
</body>
</html>