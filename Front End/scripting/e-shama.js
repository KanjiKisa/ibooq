
var accountMenu = document.getElementById("accountMenu");

var toggleAccountMenu = document.getElementById("profileMenu");

var ticketPanel = document.getElementById("ticketPanel");

var UserIDStatus = document.getElementById("show_id_status");

var UserSettlementAccountStatus = document.getElementById("show_settlement_account_status");

var UserBusinessLicenceStatus = document.getElementById("show_business_licence_status");

var reservePanel = document.getElementById("reservePanel");

var tellerPanel = document.getElementById("tellerPanel");

var show_id_status_tab = document.getElementById("show_id_status_tab");

var show_business_licence_status_tab = document.getElementById("show_business_licence_status_tab");

var show_settlement_account_status_tab = document.getElementById("show_settlement_account_status_tab");

var final_status_tab = document.getElementById("final_status_tab");

var profilePanel = document.getElementById("profilePanel");

var specificReservePanel = document.getElementById("specificReservePanel");

var specificTellerPanel = document.getElementById("specificTellerPanel");

var cartPanel = document.getElementById("cartPanel");

var accPanel = document.getElementById("accPanel");

var textField = document.getElementById("notesArea");

var menuBar = document.getElementById("menuBar");


//var mobileMenuBar = document.getElementById("menuBar");

function showmenu()
{
	if(menuBar.style.display == "none") {
		menuBar.style.display = "block"
	} else {
		menuBar.style.display = "none";
	}
}

function showLogin()
{
		if(accountMenu.style.display == "none") {
			accountMenu.style.display = "block";
		} else {
			accountMenu.style.display = "none";
		}
}

/*document.getElementById("accountMenu").onmouseout = function() 
{
		accountMenu.style.display = "none";
}*/

function startup_function()
{
	if((UserIDStatus.innerHTML.indexOf("0") != -1) && (UserBusinessLicenceStatus.innerHTML.indexOf("0") != -1)) {
		ticketPanel.style.display = "block";
		show_id_status_tab.style.background = "#8AD879";
		reservePanel.style.display = "none";
		tellerPanel.style.display = "none";
		profilePanel.style.display = "none";
	} else if((UserBusinessLicenceStatus.innerHTML.indexOf("0") != -1) && (UserSettlementAccountStatus.innerHTML.indexOf("0") != -1)) {
		ticketPanel.style.display = "none";
		reservePanel.style.display = "block";
		show_business_licence_status_tab.style.background = "#8AD879";
		tellerPanel.style.display = "none";
		profilePanel.style.display = "none";
	} else if((UserSettlementAccountStatus.innerHTML.indexOf("0") != -1)){
		ticketPanel.style.display = "none";
		reservePanel.style.display = "none";
		tellerPanel.style.display = "block";
		show_settlement_account_status_tab.style.background = "#8AD879";
		profilePanel.style.display = "none";
	} else {
		ticketPanel.style.display = "none";
		reservePanel.style.display = "none";
		tellerPanel.style.display = "none";
		profilePanel.style.display = "block";
		final_status_tab.style.background = "#8AD879";
	}
}

function show_ticket_panel()
{
		ticketPanel.style.display = "block";
		reservePanel.style.display = "none";
		tellerPanel.style.display = "none";
		profilePanel.style.display = "none";
}

function show_reserve_panel()
{
	ticketPanel.style.display = "none";
	reservePanel.style.display = "block";
	tellerPanel.style.display = "none";
	profilePanel.style.display = "none";
}

function show_teller_panel()
{
	ticketPanel.style.display = "none";
	reservePanel.style.display = "none";
	tellerPanel.style.display = "block";
	profilePanel.style.display = "none";
}

function show_profile_panel()
{
	ticketPanel.style.display = "none";
	reservePanel.style.display = "none";
	tellerPanel.style.display = "none";
	profilePanel.style.display = "block";
}

function show_specific_reserve_panel()
{
	specificReservePanel.style.display = "block";
	specificTellerPanel.style.display = "none";
}

function show_specific_teller_panel()
{
	specificReservePanel.style.display = "none";
	specificTellerPanel.style.display = "block";
}

function show_cart_panel()
{
	cartPanel.style.display = "block";
	accPanel.style.display = "none";
}

function show_acc_panel()
{
	cartPanel.style.display = "none";
	accPanel.style.display = "block";
}

function confirm_email(displayMonitor,email) 
{
	var emailConfirmRequest;

	var display = document.getElementById(displayMonitor);
	var emailCon = document.getElementById(email).value;

	var emailConQueryString = "Email=" + emailCon;
	var emailConExtensionUrl = "emailConfirmation.php";
	
	//alert(emailCon);

    if(emailCon !== null) {
			try{

				emailConfirmRequest = new XMLHttpRequest();

			} catch(e) {

				try {
					emailConfirmRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {

					try {
						emailConfirmRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						alert("Browser broke");
						return false;

					}

				}

			}

	emailConfirmRequest.onreadystatechange = function () {

			display.innerHTML = "Sending email...";

			if(emailConfirmRequest.readyState == 4 && emailConfirmRequest.status == 200) {

				display.innerHTML = emailConfirmRequest.responseText;

			}

	}
	
    } else {
        display.innerHTML = "Email field should not be empty";
    }

	emailConfirmRequest.open("POST","includes/ajax/"+emailConExtensionUrl,true);
	emailConfirmRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	emailConfirmRequest.send(emailConQueryString);

}

function reg_user(displayMonitor,username,userid,phoneno,password,confirmPass) 
{
	var regConfirmRequest;

	var display = document.getElementById(displayMonitor);
	var regUsername = document.getElementById(username).value;
	var regUserId = document.getElementById(userid).value;
	var regPhoneNo = document.getElementById(phoneno).value;
	var regPassword = document.getElementById(password).value;
	var regConFirmPass = document.getElementById(confirmPass).value;

	var regConQueryString = "Username=" + regUsername + "&UserId="+ regUserId + "&PhoneNo="+ regPhoneNo +"&Password=" + regPassword;
	var regConExtensionUrl = "userRegistration.php";


	if(regPassword == regConFirmPass) {

			try{

				regConfirmRequest = new XMLHttpRequest();

			} catch(e) {

				try {
					regConfirmRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {

					try {
						regConfirmRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						alert("Browser broke");
						return false;

					}

				}

			}

			regConfirmRequest.onreadystatechange = function () {
			    
			    display.innerHTML = "Wait as we validate your account...";

					if(regConfirmRequest.readyState = 4) {

						display.innerHTML = regConfirmRequest.responseText;

					}

			}

	} else {

		display.innerHTML = "Passwords do not match";

	}


	regConfirmRequest.open("POST","includes/ajax/"+regConExtensionUrl,true);
	regConfirmRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	regConfirmRequest.send(regConQueryString);

}

function calculate_price(displayMonitor,capacity,price) 
{
	var calculatePriceRequest;

	var display = document.getElementById(displayMonitor);
	var ticketCapacity = document.getElementById(capacity).value;
	var ticketPrice = document.getElementById(price).value;

	var calculatePriceQueryString = "Capacity=" + ticketCapacity + "&Price="+ ticketPrice ;
	var calculatePriceExtensionUrl = "calcPrice.php";


	if(ticketCapacity != null && ticketPrice != null) {

			try{

				calculatePriceRequest = new XMLHttpRequest();

			} catch(e) {

				try {
					calculatePriceRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {

					try {
						calculatePriceRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						alert("Browser broke");
						return false;

					}

				}

			}

			calculatePriceRequest.onreadystatechange = function () {
			    
			    display.innerHTML = "Wait ...";

					if(calculatePriceRequest.status === 200) {

						var response = calculatePriceRequest.responseText;
						display.innerHTML = response;


					}

			}

	} else {

		display.innerHTML = "You need to pay ...";

	}


	calculatePriceRequest.open("POST","Customers/includes/ajax/"+calculatePriceExtensionUrl,true);
	calculatePriceRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	calculatePriceRequest.send(calculatePriceQueryString);

}


function delete_photo(displayMonitor,gallery,photoid,key) 
{
	var deletePhotoRequest;

	var display = document.getElementById(displayMonitor);
	var photoGallery = document.getElementsByClassName(gallery)[key].value;

	var deletePhotoQueryString = "PhotoId=" + photoid;
	var deletePhotoExtensionUrl = "deletePhoto.php";

			try{

				deletePhotoRequest = new XMLHttpRequest();

			} catch(e) {

				try {
					deletePhotoRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {

					try {
						deletePhotoRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						alert("Browser broke");
						return false;

					}

				}

			}

	deletePhotoRequest.onreadystatechange = function () {
	    
	    display.innerHTML = "Deleting photo...";

			if(deletePhotoRequest.readyState = 4) {

				display.innerHTML = deletePhotoRequest.responseText;

			}

	}

	deletePhotoRequest.open("POST","includes/ajax/"+deletePhotoExtensionUrl,true);
	deletePhotoRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	deletePhotoRequest.send(deletePhotoQueryString);

}

function delete_user(displayMonitor,userid) 
{
	var deleteUserRequest;

	var display = document.getElementById(displayMonitor);

	var deleteUserQueryString = "UserId=" + userid;
	var deleteUserExtensionUrl = "deleteUser.php";

			try{

				deleteUserRequest = new XMLHttpRequest();

			} catch(e) {

				try {
					deleteUserRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(e) {

					try {
						deleteUserRequest = new ActiveXObject("Microsoft.XMLHTTP");
					} catch (e) {

						alert("Browser broke");
						return false;

					}

				}

			}

	deleteUserRequest.onreadystatechange = function () {
	    
	    display.innerHTML = "Deleting User...";

			if(deleteUserRequest.readyState = 4) {

				display.innerHTML = deleteUserRequest.responseText;

			}

	}

	deleteUserRequest.open("POST","includes/ajax/"+deleteUserExtensionUrl,true);
	deleteUserRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	deleteUserRequest.send(deleteUserQueryString);

}