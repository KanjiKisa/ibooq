<?php 
  
    require_once("Customers/includes/initialise.php");
    
    if(isset($_POST["subLogin"])) {
      
      $email = $_POST["loginEmail"];
      $pass = $_POST["loginPass"];
      $database->set_email($email);
      $database->set_raw_password($pass);
      
      if($database->insert_to_login_table()) {
        
        $session->login($database->fetched_user_id);

        if($session->is_logged) {

          if($database->fetched_user_id === "Admin1") {
              header("Location: super admin/index.php");
          } else {
              if($database->fetch_approval_status_from_reg_data($database->fetched_user_id) === "1") {
                
                  if($database->fetched_user_type === "BO") {
                    header("Location: business owner/index.php");
                  } else if($database->fetched_user_type === "SP") {
                    header("Location: admin dashboard/index.php");
                  } else {
                    header("Location: client dashboard/index.php");
                  }

               } else {

                  if($database->fetched_user_type === "BO") {
                    header("Location: business_manager.php");
                  } else if($database->fetched_user_type === "SP") {
                    header("Location: manager.php");
                  } else {
                    header("Location: client dashboard/index.php");
                  }

               }

             }


        } else {

          $alert->message("Failed to Login","Fail");

        }
        
      } else {

        $alert->message("Failed to Login","Fail");

      }
    }
  
  ?>


<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela Round|Oswald|Raleway|Coiny|Montserrat" rel="stylesheet">
     <!-- Bootstrap CSS -->
   <!-- <link rel="stylesheet" type="text/css" media="screen" href="Front End/style2.css" />-->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/styling/style.css" />-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="Front End/style.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 


</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="business owner.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="service provider.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="register.php"><i class="fas fa-user-circle pr-2"></i>Register</a>
          </li>

          <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
          </form>

        </ul>
      </div>
    </div>
  </nav>

<div id="head">
<br/>
   <div class="container">

   <div class="row no-gutters ">
     
        <div class="col-md-4 padding-0" style='background: url("Front End/Images/nairobi.jpg");' id="login_left_panel">
          <h4 style="text-align:center; color:#fff;" id="login_left_panel_text">iBook</h4>
        </div>
        <div class="col-md-7 padding-0">
          
          <form action="login.php" method="POST" id="loginForm">
          <h4 style="color:#8AD879;text-align:center;">Login</h4>
                  <input type="text" class="form-control form-control-lg" placeholder="Email" name="loginEmail" required>
                  <input type="password" class="form-control form-control-lg" placeholder="Password" name="loginPass" required>
                  <input type="submit" class="btn btn-primary" value="Login" name="subLogin"> <br><br><br>
                <div class="switch-login">
                  <a href="register.php">Don't have an account? <span>Sign-Up</span></a>
                  <a href="index.php"><i class="fas fa-home"></i>Home</a>
                </div>
              </form></div>
      </div>
     
    </div>
 
</div>
    
     

 



    <script>

      $('.carousel').carousel({
        interval: 2000
      })

    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/ShamaScript.js"></script>

    </script>
  <!--side bars-->

</body>
</html>