<?php

require_once("Customers/includes/initialise.php");

$ReserveId = $_POST['rs_id'];

$referrer = $_SERVER['HTTP_REFERER'];
$page = "http://ibooq.co.ke/ibooq/booqing.php";

// if ($page === $referrer) {
    
  $datetime = $_POST['event_date'];

  //Divide by 1,0000
  $timestampSeconds = $datetime / 1000 + 10800;
   
  //Format it into a human-readable datetime.
  $service_datetime = gmdate("d/m/Y H:i:s", $timestampSeconds);

  $service_date = gmdate("Y-m-d", $timestampSeconds);

// } else {
//       header('Location: http://localhost/ibooq/booqing.php');
// }

?>
<!DOCTYPE html>
<html>
<head>
  
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta charset='utf-8' />
<meta name="viewport" content="width=device-width,initial-scale=1.0">  
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
  <meta name="description" content="Book any service with your favourite business or service provider">

  <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Hind|Poppins|Nunito|Archivo Narrow">
     
<!-- Bootstrap CSS -->

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'>
<link href='Front End/packages/core/main.css' rel='stylesheet' />
<link href='Front End/packages/daygrid/main.css' rel='stylesheet' />
<link href='Front End/packages/timegrid/main.css' rel='stylesheet' />
<link href='Front End/packages/bootstrap/main.css' rel='stylesheet' />
<script src='Front End/packages/core/main.js'></script>
<script src='Front End/packages/interaction/main.js'></script>
<script src='Front End/packages/daygrid/main.js'></script>
<script src='Front End/packages/timegrid/main.js'></script>
<script src='Front End/packages/bootstrap/main.js'></script>
<script src='Front End/vendor/rrule.js'></script>
<script src='Front End/packages/moment/main.js'></script>
<script src='Front End/packages/moment-timezone/main.js'></script>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>


   <link rel="stylesheet" type="text/css" media="screen" href="Front End/style.css" />

    <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500">  
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'momentTimezone', 'bootstrap', 'interaction', 'timeGrid' ],
      defaultView: 'timeGridDay',
      timeZone: 'local',
      weekLabel: "Week",
      weekNumbers: true,
      weekNumbersWithinDays: false,
      themeSystem: 'bootstrap',
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      defaultDate: "<?php echo $service_date; ?>",
      selectable: true,
      showNonCurrentDates: true,
      slotDuration:'00:05:00',
      validRange: {
        start: Date.now()
      },
      allDaySlot: false,
      hiddenDays: [ <?php echo $_POST['business_days'] ?> ],
      minTime: '<?php echo $_POST['open_time'] ?>',
      maxTime: '<?php echo $_POST['close_time'] ?>',
      header: {
        left: 'title prev,next',
        center: '',
        right: 'today'
      },
      dateClick: function(info) {
        
        var id = "<?php echo $_POST['service_id'];?>";
        var user_id = "<?php echo $_POST['user_id'];?>";
        var eventdate = Date.parse(info.date); 
        document.sampleForm.service_id.value = id;
        document.sampleForm.user_id.value = user_id;
        document.sampleForm.service_time.value = eventdate;
        document.forms["sampleForm"].submit();
      },
      select: function(info) {
        var id = "<?php echo $_POST['service_id'];?>";
        var user_id = "<?php echo $_POST['user_id'];?>";
        var eventdate = Date.parse(arg.start); 
        document.sampleForm.service_id.value = id;
        document.sampleForm.user_id.value = user_id;
        document.sampleForm.service_time.value = eventdate;
        document.forms["sampleForm"].submit();
      },
      eventLimit: true, // allow "more" link when too many events

      eventSources: [

        // your event source
        {
          events: [ // put the array in the `events` property

            <?php
              
              $data = $ticket->get_unpaid_distinct_ticket_id($ReserveId);
              
                foreach($data as $row) {
                  $payment_id = $row["PaymentId"];
                  $ticket_id = $row["TicketId"];

                  $ticket->get_unpaid_ticket_id_details($payment_id);
            ?>
            {
              title: '<?php echo $ticket->reserve_name; ?> - Booked',
              start: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>',
              end: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->ending_time; ?>'
            },

            <?php
              }
            ?>

            <?php
              
              $data = $ticket->get_distinct_ticket_id($ReserveId);
              
                foreach($data as $row) {
                  $payment_id = $row["PaymentId"];
                  $ticket_id = $row["TicketId"];

                  $ticket->get_ticket_id_details($payment_id);
            ?>
            {
              title: '<?php echo $ticket->reserve_name; ?> - Booked',
              start: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>',
              end: '<?php echo $ticket->pay_date; ?> <?php echo "05:40:00"; ?>'
            },

            <?php
              }
            ?>
            
          ],
          color: 'black',     // an option!
          textColor: 'yellow' // an option!
        }

        // any other event sources...

      ]

    });
    
    calendar.render();
  });

  

</script>
<style>

  body {
    padding: 0;
  }

  .btn {
    max-width: auto;
    padding: 0;
    max-height: 15px;
    margin-left: -1px;
  }
  .br {
    background-color: #ffffff;
    height: 10px;
  }
  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  
  div.fc-view {
    width: 100%;
    max-height: 70vh;
    overflow: auto;
  }
  .fc-head-container {
    background-color: #ffa500;
  }

</style>
</head>
<body class="bg-light">

<!--    
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Business Owner</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="index.php">Service Provider</a>
          </li>

          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="login.php"><i class="fas fa-user-circle pr-2"></i>Login</a>
          </li>
          <li class="nav-item nav-badge">
            <a class="nav-link js-scroll-trigger" href="register.php"><i class="fas fa-sign-in-alt pr-2"></i>Register</a>
          </li>
        </ul>
      </div>
    </div>

  </nav>

  <hr class="br">
  <hr class="br">
  -->
  <div class="bg-light" id='calendar'></div>
   
  <form id="sampleForm" name="sampleForm" method="post" action="pay.php">
    <input type="hidden" name="service_id" id="serviceid">
    <input type="hidden" name="user_id" id="userid">
    <input type="hidden" name="service_time" id="service_time">
  </form> 
  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
   
</body>
</html>
