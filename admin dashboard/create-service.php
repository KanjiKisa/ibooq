<?php

    require_once("../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      //echo $database->session_user_id;

        if(isset($_POST["submit_rservice"])) {
                  $rs_category = $_POST["rs_category"];
                  $rs_name = $_POST["rs_name"];
                  //$rs_location = $_POST["rs_location"];
                  $rs_description = $_POST["rs_description"];
                  $rs_duration = $_POST["rs_duration"];
				  //$rs_no_tables = $_POST["rs_no_tables"];
                  $rs_price = $_POST["rs_price"];
					  
                    $automation->generate_reserve_id();
                    
                   $database->set_rs_category($rs_category);
                    $database->set_rs_id($automation->rs_id);
                    $database->set_user_id($database->session_user_id);
                    $database->set_rs_name($rs_name);
                    $database->set_rs_description($rs_description);
                    $database->set_rs_stock($rs_duration);
					         $database->set_rs_no_tables("0");
                    $database->set_rs_photo_path("Uploaded\Images\sample.png");
                    $database->set_rs_price($rs_price);
                    $database->set_rs_location("kenya");
					         $database->set_rs_vacancy("Vacant");
                    
                    if($database->insert_to_reserve_table()) {
                       $alert->message("Service Uploaded","Success");
                    } else {
                       $alert->message("Failed to Upload Service","Fail");
                    }
                    
                  

          }

      
    } else {

        header("Location: ../index.php");
  
    }

?>

<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>iBooq</title>

    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 


  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style.css" />
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<style>
  .upload-pic{
    border: 1px solid #ddd;
    border-style: dashed;
    padding: 1rem 1rem;
  }
</style>



<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar" style="background:#8AD879;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Reservations List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="all-services.php">
         <i class="fas fa-users"></i>
          <span>My Services</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      
    <li class="nav-item">
        <a class="nav-link collapsed" href="../shop/admin dashboard/index.php">
         <i class="fas fa-users"></i>
          <span>My Shop</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    
                <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="../Front End/Images/ibooq.png" style="height: 50px;"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="color:#000">
                      <a class="nav-link js-scroll-trigger" href="../index.php" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../business owner.php" target="_blank">Business Owner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../service provider.php" target="_blank">Service Provider</a>
                    </li>

                    <li class="nav-item dropdown nav-badge">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2  d-lg-inline text-gray-600 small"><?php echo $database->session_username; ?></span>
                        
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item text-white" href="../logout.php">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">Logout</i>
                          
                        </a>
                      </div>
                    </li>

                    <form class="form-inline my-2 my-lg-0" action="../searching.php" method="get">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    </form>

                  </ul>
                </div>
              
          </nav>

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">Create Service</h1>
            </div>
            
          </div>

          <!-- Content Row -->
          <div class="row no-gutters">


          </div>

          <!-- Ticket list table -->
          <form  action=""  method="POST" enctype="multipart/form-data">

             <div class="row">

              <div class="form-group col-md-6">
              <label for="service_category">Service Type</label>
                <select id="service_category" name="rs_category" class="form-control">
                <option value="0">- Select Service-</option>
                  <option>Beauty</option>
                  <option>Wellness</option>
                  <option>Health</option>
                </select>
              </div>

              <div class="form-group col-md-6">
              <label for="s">Service Name</label>
                <select id="sel_service" name="rs_name" class="form-control">
                  <option value="0">- Select Service-</option>
                </select>
              </div>

              <div class="form-group col-md-6">
              <label>Service Time</label>
                <select id="rs_duration" name="rs_duration" class="form-control">
                  <option value="10 Minutes">10 Minutes</option>
                  <option value="20 Minutes">20 Minutes</option>
                  <option value="30 Minutes">30 Minutes</option>
                  <option value="40 Minutes">40 Minutes</option>
                  <option value="50 Minutes">50 Minutes</option>
                  <option value="1 Hour">1 Hour</option>
                  <option value="1 Hour 30 Minutes">1 Hour 30 Minutes</option>
                  <option value="2 Hours">2 Hours</option>
                  <option value="2 Hours 30 Minutes">2 Hours 30 Minutes</option>
                  <option value="3 Hours">3 Hours</option>
                  <option value="3 Hours 30 Minutes">3 Hours 30 Minutes</option>
                  <option value="4 Hours">4 Hours</option>
                  <option value="5 Hours">5 Hours</option>
                  <option value="6 Hours">6 Hours</option>
                  <option value="7 Hours">7 Hours</option>
                  <option value="8 Hours">8 Hours</option>
                </select>
              </div>

              <div class="form-group col-md-6">
                <label>Price (Ksh)</label>
                <input type="text" class="form-control" name="rs_price">
              </div>

              <div class="form-group col-md-6">
                <label>Service Description</label>
                <textarea type="text" class="form-control" name="rs_description"></textarea>
              </div>
             </div>
              
              
              
              <input type="submit" class="btn btn-primary" value="Upload service" name="submit_rservice">
              

            
          </form>
          <!----end of ticket list table-->

         

           
              
              

            
              

              

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="js/form.js"></script>

  <!-- Modal: modalPoll -->
  <script>
  $(document).ready(function(){

$("#service_category").change(function(){
    var serviceid = $(this).val();

    $.ajax({
        url: 'getservices.php',
        type: 'post',
        data: {depart:serviceid},
        dataType: 'json',
        success:function(response){

            var len = response.length;

            $("#sel_service").empty();
            for( var i = 0; i<len; i++){
                var id = response[i]['id'];
                var name = response[i]['name'];
                
                $("#sel_service").append("<option value='"+id+"'>"+name+"</option>");

            }
        }
    });
});

});
</script>
<!-- Modal: modalPoll -->

</body>

</html>
