<?php

    require_once("../Customers/includes/initialise.php");
    require_once("../Customers/includes/businessDays.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      if($database->fetch_approval_status_from_reg_data($session->session_id) === "0") {
              header("Location: ../manager.php");
      }

      
    } else {

        header("Location: ../index.php");
  
    }

$businessDays = 'Weekdays';
$openTime = '10:00';
$closeTime = '17:00';
?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>iBooq</title>

    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style.css" />
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">




<link href='https://ibooq.shamalandscapes.com/Front End/packages/core/main.css' rel='stylesheet' />
<link href='https://ibooq.shamalandscapes.com/Front End/packages/daygrid/main.css' rel='stylesheet' />
<link href='https://ibooq.shamalandscapes.com/Front End/packages/timegrid/main.css' rel='stylesheet' />
<script src='https://ibooq.shamalandscapes.com/Front End/packages/core/main.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/packages/interaction/main.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/packages/daygrid/main.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/packages/timegrid/main.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/vendor/rrule.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/packages/moment/main.js'></script>
<script src='https://ibooq.shamalandscapes.com/Front End/packages/moment-timezone/main.js'></script>
<link href='https://ibooq.shamalandscapes.com/Front End/packages/bootstrap/main.css' rel='stylesheet' />
<script src='https://ibooq.shamalandscapes.com/Front End/packages/bootstrap/main.js'></script>


<style>

  body {
    max-height: 100vh;
    overflow: hidden;
    padding: 0;
  }

  .br {
    background-color: #ffffff;
    height: 10px;
  }
  
  div.fc-view {
    width: 100%;
    max-height: 70vh;
    overflow: auto;
  }
  .fc-head-container {
    background-color: #ffa500;
  }

  #calendar {
    max-width: auto;
    min-height: 100%;
    margin: 0 auto;
  }

  #calendar .btn {
    max-width: auto;
    padding: 0;
    max-height: 15px;
    margin-left: -1px;
  }
</style>
<script>

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'momentTimezone', 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
      timeZone: 'local',
      themeSystem: 'bootstrap',
      weekLabel: "Week",
      weekNumbers: true,
      weekNumbersWithinDays: false,
      themeSystem: 'bootstrap',
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      hiddenDays: [ <?php businessDay($businessDays) ?> ],
      minTime: '<?php echo $openTime; ?>',
      maxTime: '<?php closeTime($closeTime);?>',
      allDaySlot: false,
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },

      defaultDate: "<?php $t=time(); echo(date("Y-m-d",$t)); ?>",
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectMirror: true,
      unselectAuto: true,  
      height: 'auto',
      handleWindowResize: true,
      contentHeight: 'auto',
      
      selectMinDistance: 0.1,
      
      dateClick: function(info) {
        calendar.changeView('timeGridDay', info.date);

      },

      selectOverlap: function(event) {
        return event.rendering === 'background';
      },

      editable: false,
      eventLimit: true, // allow "more" link when too many events

      //events in db
        eventSources: [

        // your event source
        {

          events: [ // put the array in the `events` property
          
           <?php

                  $data = $ticket->get_distinct_ticket_id($session->session_id);

                  foreach($data as $row) {

                      $payment_id = $row["PaymentId"];
                      $ticket_id = $row["TicketId"];

                      $ticket->get_ticket_id_details($payment_id);


              ?>
                          
            {
              id: '<?php echo $ticket_id; ?>',
              title: '<?php echo $ticket->reserve_name; ?>',
              url: 'service.php?id=<?php echo $ticket_id; ?>',
              start: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>',
              end: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>'
            },
            
            <?php

                }

            ?>
                        
          ],
              color: 'green',     // an option!
              textColor: 'white' // an option!
            },{
        
                events: [ // put the array in the `events` property
          
                   <?php
        
                          $data = $ticket->get_distinct_confirmed_ticket_id($session->session_id);
        
                          foreach($data as $row) {
        
                              $payment_id = $row["PaymentId"];
                              $ticket_id = $row["TicketId"];
        
                              $ticket->get_ticket_id_details($payment_id);
        
        
                      ?>
                                  
                    {
                      id: '<?php echo $ticket_id; ?>',
                      title: '<?php echo $ticket->reserve_name; ?>',
                      url: 'service.php?id=<?php echo $ticket_id; ?>',
                      start: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>',
                      end: '<?php echo $ticket->pay_date; ?> <?php echo $ticket->pay_time; ?>'
                    },
                    
                    <?php
        
                        }
        
                    ?>
                                
                  ],
                      color: 'grey',     // an option!
                      textColor: 'white' // an option!
                    }
        
          ]

    });


    calendar.render();

  });

  

</script>

<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebarBO" style="background:#8AD879;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Reservations List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="all-services.php">
         <i class="fas fa-users"></i>
          <span>My Services</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      
    <li class="nav-item">
        <a class="nav-link collapsed" href="../shop/admin dashboard/index.php">
         <i class="fas fa-users"></i>
          <span>My Shop</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    
                <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="../Front End/Images/ibooq.png" style="height: 50px;"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="color:#000">
                      <a class="nav-link js-scroll-trigger" href="../index.php" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../business owner.php" target="_blank">Business Owner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../service provider.php" target="_blank">Service Provider</a>
                    </li>

                    <li class="nav-item dropdown nav-badge">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2  d-lg-inline text-gray-600 small"><?php echo $database->session_username; ?></span>
                        
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item text-white" href="../logout.php">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">Logout</i>
                          
                        </a>
                      </div>
                    </li>

                    <form class="form-inline my-2 my-lg-0" action="../searching.php" method="get">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    </form>

                  </ul>
                </div>
              
          </nav>

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">View all records</h1>
            </div>
            <a href="create-service.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>Create Service</a>
          </div>

        <div class="container-fluid"
          <div id='calendar'></div>
         </div>    

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="js/form.js"></script>

  <!-- Modal: modalPoll -->
<div class="modal fade right" id="modalPoll-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Feedback request
        </p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
        <div class="text-center">
          <i class="far fa-file-alt fa-4x mb-3 animated rotateIn"></i>
          <p>
            <strong>Your opinion matters</strong>
          </p>
          <p>Have some ideas how to improve our product?
            <strong>Give us your feedback.</strong>
          </p>
        </div>

        <hr>

        <!-- Radio -->
        <p class="text-center">
          <strong>Your rating</strong>
        </p>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-179" value="option1" checked>
          <label class="form-check-label" for="radio-179">Very good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-279" value="option2">
          <label class="form-check-label" for="radio-279">Good</label>
        </div>

        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-379" value="option3">
          <label class="form-check-label" for="radio-379">Mediocre</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-479" value="option4">
          <label class="form-check-label" for="radio-479">Bad</label>
        </div>
        <div class="form-check mb-4">
          <input class="form-check-input" name="group1" type="radio" id="radio-579" value="option5">
          <label class="form-check-label" for="radio-579">Very bad</label>
        </div>
        <!-- Radio -->

        <p class="text-center">
          <strong>What could we improve?</strong>
        </p>
        <!--Basic textarea-->
        <div class="md-form">
          <textarea type="text" id="form79textarea" class="md-textarea form-control" rows="3"></textarea>
          <label for="form79textarea">Your message</label>
        </div>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-primary waves-effect waves-light">Send
          <i class="fa fa-paper-plane ml-1"></i>
        </a>
        <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cancel</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->

</body>

</html>

