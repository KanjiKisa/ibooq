<?php

require_once("../Customers/includes/initialise.php");

$departid = $_POST['depart'];   // department id

$users_arr = array();

$result = $database->get_all_services($departid);

foreach($result as $row){
    $userid = $row['id'];
    $name = $row['name'];

    $users_arr[] = array("id" => $userid, "name" => $name);
}

// encoding array to json format
echo json_encode($users_arr);