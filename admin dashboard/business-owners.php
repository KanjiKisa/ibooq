<?php

    require_once("../Customers/includes/initialise.php");
 
 if($session->is_logged) {

      $database->get_session_details($session->session_id); 

          if(isset($_POST['subReg'])) {
            $automation->generate_user_id();

            $userId = $automation->random_user_id;
            $fname = $_POST["firstName"];
            $lname = $_POST["lastName"];
            $username = $fname." ".$lname;
            $email = $_POST["userEmail"];
            $phoneNo = $_POST["phone"];
            $address = $_POST["userAddress"];
            $city = $_POST["userCity"];
            $country = $_POST["userCountry"];
            $pass = $_POST["pass"];
            $conPass = $_POST["confirmPass"];

          
            
            $database->set_user_id($userId);
            $database->set_username($username);
            $database->set_user_type("SP");
          $database->set_profile_pic("Uploaded\Images\defaultP.jpg");
          $database->set_cover_pic("Uploaded\Images\defaultC.jpg");
          $database->set_open_days("Weekends");
          $database->set_open_time("18:00:00");
          $database->set_close_time("06:00:00");
            $database->set_email($email);
            $database->set_phone_no($phoneNo);
            $database->set_account_approval("0");
            $database->set_address($address);
            $database->set_city($city);
            $database->set_country($country);

            if($pass == $conPass) {
              $database->set_password($pass);
              
              if($database->insert_to_SP_to_BO_relation_table($userId,$username,$session->session_id,"1","1")) {

                if($database->BO_to_reg_SP_table($userId)) {

                      
                      $alert->message("Registered a service provider","Success");


                } else {
                  
                  $alert->message("Failed to register","Fail");
          

                }

              } else {
                  $alert->message("Failed to register","Fail");
              }
              
            } else {

              $alert->message("Passwords do not match","Fail");

            } 

          }

          if(isset($_POST["delete_data"])) {

            $mv_id = $_POST["del_service_id"];
              
              if($database->delete_BO_registered_SP($session->session_id,$mv_id)) {
                $alert->message("Service provider removed from business","Success");
              } else {
                $alert->message("Failed to delete service provider from business","Fail");
              }
              
            

          }

          if(isset($_POST["apply_sp_job"])) {

            $BOId = $_POST["BOId"];
            $username = $_POST["username"];
              
              if($database->insert_to_SP_to_BO_relation_table($session->session_id,$username,$BOId,"0","1")) {
                $alert->message("Request sent, await approval","Success");
              } else {
                $alert->message("Failed to sent application","Fail");
              }
              
            

          }

  } else {

        header("Location: ../index.php");
  
  }

?>

<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

  
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>iBooq</title>
    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 


  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style.css" />
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<style>
  .upload-pic{
    border: 1px solid #ddd;
    border-style: dashed;
    padding: 1rem 1rem;
  }
</style>



<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebarBO" style="background:#8AD879;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Reservations List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="all-services.php">
         <i class="fas fa-users"></i>
          <span>My Services</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      
    <li class="nav-item">
        <a class="nav-link collapsed" href="../shop/admin dashboard/index.php">
         <i class="fas fa-users"></i>
          <span>My Shop</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="create-sp.php">
         <i class="fas fa-users"></i>
          <span>My Service Providers</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->

        </nav>

        
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    
                <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="../Front End/Images/ibooq.png" style="height: 50px;"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="color:#000">
                      <a class="nav-link js-scroll-trigger" href="../index.php" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../business owner.php" target="_blank">Business Owner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../service provider.php" target="_blank">Service Provider</a>
                    </li>

                    <li class="nav-item dropdown nav-badge">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2  d-lg-inline text-gray-600 small"><?php echo $database->session_username; ?></span>
                        
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item text-white" href="../logout.php">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">Logout</i>
                          
                        </a>
                      </div>
                    </li>

                    <form class="form-inline my-2 my-lg-0" action="../searching.php" method="get">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    </form>

                  </ul>
                </div>
              
          </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">Apply as SP to business owners</h1>
            </div>
            
          </div>

          <!-- Content Row -->
          <div class="row">

              
                    <?php 

                      $sql = $database->get_all_BO();

                      foreach($sql as $row) {

                          $userId = $row["UserId"];
                          $clubName = $row["Username"];
                          $profilePic = $row["ProfilePic"];
                          $coverPic = $row["CoverPic"]; 
                          $open_days = $row["OpeningDays"];
                          $open_time = $row["OpeningTime"];
                          $close_time = $row["ClosingTime"];
                          $city = $row["City"];

                    ?>

                            <div class="col-sm-3 provider-card" style="margin-bottom:5px;">
                            <a href="ibooq.php?rs_id=<?php echo $userId; ?>" style="text-decoration:none;" target="_blank">
                                <div class="card">

                                <div class="club-poster">
                                    <img class="card-img-top sample-project" src="../<?php echo $profilePic; ?>" alt="<?php echo $clubName; ?>">
                                    
                                </div>

                                  <div class="card-body">
                                    <p class="card-text" style="color:#8AD879;font-size:12px;"><strong><?php echo $clubName; ?></strong></p>
                                  </div>

                                </div><!-- close of card class div-->

                                </a>
                                <form action="" method="post" class="form-profile" >

                                  <input type="hidden" id="BOId" class="form-control" name="BOId" placeholder="BOId" value="<?php echo $userId;?>">
                                  
                                  <input type="hidden" id="username" class="form-control" name="username" placeholder="BOId" value="<?php echo $clubName;?>">

                                  <input type="submit" style="float: right; " name="apply_sp_job" value="Apply" class="btn btn-primary">
                                </form>

                          </div>



                    <?php 


                       }


                    ?>


          </div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">All my business owners</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    
                    <thead>
                        <tr>
                          <th scope="col">Username</th>
                          <th scope="col">Email</th>
                          <th scope="col">Phone Number</th>
                          <th scope="col">City</th>
                        </tr>
                      </thead>
                      <tbody>

                          <?php

                              $data = $database->get_all_registered_BO_to_SP($session->session_id,"1");

                              foreach($data as $row) {

                                    $fetched_user_id = $row["BOId"];

                                    $database->fetch_reservation_user_reg_info($fetched_user_id);


                          ?>

                        <tr>
                          <td><?php echo $database->username; ?></td>
                          <td><?php echo $database->user_email; ?></td>
                          <td><?php echo $database->phone_no; ?></td>
                          <td><?php echo $database->city; ?></td>
                          <td>
                            <a href="#deleteDialog" id="mydel" data-delid="<?php echo $fetched_user_id; ?>" class="openDeleteDialog btn btn-danger" data-toggle="modal" style="float: right;">Delete</a>
                          </td>
                        </tr>

                        <?php

                            }

                        ?>

                      </tbody>
                      
                </table>
              </div>
            </div>
          </div>

           
              
              

            
              

              

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="js/form.js"></script>

  <!-- Modal: modalPoll -->
  <div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Confirm Deletion</h2>
                
                <p class="delete-p">Are you sure you want to delete this service provider?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myDelId" class="form-control" name="del_service_id" placeholder="Title"> 

                  <input type="submit" style="float: right; " name="delete_data" value="Delete" class="btn btn-primary">
                </form>
              
                <button type="button" class="btn btn-danger center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal: modalPoll -->

<script>
  $(document).on("click", ".openDeleteDialog", function () {
            var myId = $(this).data('delid');
            $("#myDelId").attr("value", myId);
        });
</script>

</body>

</html>
