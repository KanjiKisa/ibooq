<?php

    require_once("../Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      if($database->fetch_approval_status_from_reg_data($session->session_id) === "0") {
              header("Location: ../manager.php");
      }

          //echo $database->session_user_id;
          $totalReserves = $payment->count_reserves_by_owner($database->session_user_id);
            $totalAmount = $payment->sum_of_reserves_total_by_owner($database->session_user_id);
            $totalReservations = $payment->count_table_reservations_by_owner($database->session_user_id);
        
            $percantage_total = $totalAmount * 1;
        
            //$final_pay = $totalAmount - $percantage_total;
            
            $final_pay = $percantage_total;

            if(isset($_POST["update_service"])) {
              $servicetype = $_POST["servicetype"];
              $servicename = $_POST["servicename"];
              $serviceid = $_POST["serviceid"];
              $servicetime = $_POST["servicetime"];
              $servicedesc = $_POST["servicedesc"];
              $serviceprice = $_POST["serviceprice"];

                
              $database->set_rs_category($servicetype);
                $database->set_rs_id($serviceid);
                $database->set_user_id($database->session_user_id);
                $database->set_rs_name($servicename);
                $database->set_rs_description($servicedesc);
                $database->set_rs_stock($servicetime);
                $database->set_rs_no_tables("1");
                $database->set_rs_photo_path("defaultTable.png");
                $database->set_rs_price($serviceprice);
                $database->set_rs_location("kenya");
                $database->set_rs_vacancy("Vacant");
                
                if($database->update_reserve_table($database->session_user_id)) {
                  $alert->message("Service Updated","Success");
                } else {
                  $alert->message("Failed to Update Service","Fail");
                }
                
              

      }

      if(isset($_POST["delete_data"])) {

        $mv_id = $_POST["del_service_id"];
          
          if($database->delete_reserve($mv_id)) {
            $alert->message("Service data deleted","Success");
          } else {
            $alert->message("Failed to delete service data","Fail");
          }
          
        

}



             /* if(isset($_POST["confirmTicket"])) {
       
                $ticketN = $_POST["ticketNo"]; 

                if($update_ticket_status($ticketN)) {
                  $alert->message("Receipt Confirmed","Success");
                } else {
                  $alert->message("Receipt Confirmation Failed","Fail");
                }

              }*/

        /*} else {
              header("Location: ../manager.php");
        }*/

      
    } else {

        header("Location: ../index.php");
  
    }

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>iBooq</title>


    <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab & Cetrick Afundi,https://cetricka.co.ke">
    <meta name="description" content="Book any service with your favourite business or service provider">

    <meta name="keywords" content="Book any service with your favourite business or service provider">

    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">
    <link rel="manifest" href="/favicons/site.webmanifest">
    <link rel="mask-icon" href="favicons/safari-pinned-tab.svg" color="#000000">
    <link rel="shortcut icon" href="/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffa500"> 

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/themify-icons/themify-icons.css">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine|Staatliches|Montserrat|Raleway|Teko|Anton|Didact Gothic|Varela Round|Fugaz One|Abel|Fascinate Inline|Fahkwang|Lalezar|Open Sans|Josefin Sans|IBM Plex Sans|Oswald|Meera Inimai|Abril Fatface|Rubik|Poppins|Nunito|Archivo Narrow">

  <!-- Custom styles for this template -->
  <link href="css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="../Front End/style.css" />
   <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar" style="background:#8AD879;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        
        <div class="sidebar-brand-text "><img src="img/ibooq.png" style="height: 50px;"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Quick Overview
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="index.php">
          <i class="fas fa-money-bill-wave-alt"></i>
          <span>Reservations List</span>
        </a>
        
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="all-services.php">
         <i class="fas fa-users"></i>
          <span>My Services</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="my-calender.php">
         <i class="fas fa-users"></i>
          <span>My Calender</span>
        </a>
        
      </li>
      
    <li class="nav-item">
        <a class="nav-link collapsed" href="../shop/admin dashboard/index.php">
         <i class="fas fa-users"></i>
          <span>My Shop</span>
        </a>
        
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading text-white">
        Finance
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="payments.php">
          <i class="fas fa-file-invoice-dollar"></i>
          <span>My Wallet</span>
        </a>
      </li>

      <hr class="sidebar-divider">

      <div class="sidebar-heading text-white">
        Account
      </div>


      <!-- Nav Item my profile -->
      <li class="nav-item">
        <a class="nav-link" href="profile.php">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>My Profile</span></a>
      </li>
      
      <!-- Nav Item - Tables -->
     

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white  topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          

          <!-- Topbar Navbar -->

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <div class="title">
              <h6>Welcome Back <span></span></h6>
              <h1 class="h3 mb-0 text-gray-800">All Services</h1>
            </div>
            <a href="create-service.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i>Create Service</a>
          </div>

          <!-- Content Row -->
          <div class="row no-gutters">

          <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    
                <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="../Front End/Images/ibooq.png" style="height: 50px;"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="color:#000">
                      <a class="nav-link js-scroll-trigger" href="../index.php" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../business owner.php" target="_blank">Business Owner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="../service provider.php" target="_blank">Service Provider</a>
                    </li>

                    <li class="nav-item dropdown nav-badge">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2  d-lg-inline text-gray-600 small"><?php echo $database->session_username; ?></span>
                        
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item text-white" href="../logout.php">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">Logout</i>
                          
                        </a>
                      </div>
                    </li>

                    <form class="form-inline my-2 my-lg-0" action="../searching.php" method="get">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    </form>

                  </ul>
                </div>
              
          </nav>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card  shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total No of Services</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totalReserves;?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card  shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Upcoming Reservations</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $totalReservations;?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-4 col-md-6 mb-4">
              <div class="card  shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">My Wallet</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">Ksh <?php echo $final_pay;?></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-comments fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <!-- all events list table -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">All services uploaded</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    
                    <thead>
                        <tr>
                          <th scope="col">Service Type</th>
                          <th scope="col">Service Name</th>
                          <th scope="col">Service Time(in minutes)</th>
                          <th scope="col">Price per duration</th>
                          <th scope="col">Description</th>
                        </tr>
                      </thead>
                      <tbody>

                          <?php

                              $data = $database->fetch_reserve_by_owner($session->session_id);

                              foreach($data as $row) {


                                    $fetched_rs_category = $row["Category"];
        							$fetched_rs_id = $row["ReserveId"];
        							$fetched_rs_name = $row["ReserveName"];
        							$fetched_rs_description = $row["ReserveDesc"];
        							$fetched_rs_stock = $row["ReserveStock"];
        							$fetched_rs_price = $row["ReservePrice"];


                          ?>

                        <tr>
                          <td><?php echo $fetched_rs_category; ?></td>
                          <td><?php echo $fetched_rs_name; ?></td>
                          <td><?php echo $fetched_rs_stock; ?></td>
                          <td><?php echo $fetched_rs_price; ?></td>
                          <td><?php echo $fetched_rs_description; ?></td>
                          <td>
                          <a href="#modal-edit" data-toggle="modal" data-serviceid="<?php echo $fetched_rs_id; ?>" 
                              data-servicetype="<?php echo $fetched_rs_category; ?>" data-servicename="<?php echo $fetched_rs_name; ?>" data-servicetime="<?php echo $fetched_rs_stock; ?>" 
                              data-serviceprice="<?php echo $fetched_rs_price; ?>" data-servicedesc="<?php echo $fetched_rs_description; ?>" class="btn btn-primary openEditDialog">Edit Service</a>
                          </td>
                          <td>
                            <a href="#deleteDialog" id="mydel" data-delid="<?php echo $fetched_rs_id; ?>" class="openDeleteDialog btn btn-danger" data-toggle="modal" style="float: right;">Delete</a>
                          </td>
                        </tr>

                        <?php

                            }

                        ?>

                      </tbody>
                      
                </table>
              </div>
            </div>
          </div>
          <!----end of ticket list table-->

         

           
              
              

            
              

              

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
  

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  

  <script src="vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="js/demo/datatables-demo.js"></script>

  <script src="js/form.js"></script>

   <!-- Modal: modalPoll -->
   <div class="modal" tabindex="-1" role="dialog" id="modal-edit" >

<div class="modal-dialog" role="document">

  <div class="modal-content" >

    <div class="modal-header">
      <h5 class="modal-title" style="text-align : center; color : #ff3c4e; font-family: 'Josefin Slab';">Edit Table</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <div class="modal-body" >

                <form  action=""  method="POST" enctype="multipart/form-data">

                       <div class="row">

                        <div class="form-group col-md-6">
                          <label for="inputState">Table Type</label>
                          <select id="inputState" name="servicetype" class="form-control">
                            <option value="Beauty">Beauty</option>
                            <option value="Wellness">Wellness</option>
                          </select>
                        </div>

                        <div class="form-group col-md-6">
                          <label>Service Name</label>
                          <input type="text" class="form-control" name="servicename" id="servicename">
                        </div>

                          <input type="hidden" class="form-control" name="serviceid" id="serviceid">

                        <div class="form-group col-md-6">
                          <label>Service Time</label>
                          <input type="text" class="form-control" name="servicetime" id="servicetime">
                        </div>

                        <div class="form-group col-md-6">
                          <label>Price</label>
                          <input type="text" class="form-control" name="serviceprice" id="serviceprice">
                        </div>

                        <div class="form-group col-md-6">
                          <label>Description</label>
                          <textarea type="text" class="form-control" name="servicedesc" id="servicedesc"></textarea>
                        </div>
                       </div>
        
        
        
                  <input type="submit" class="btn btn-primary" value="Update Service" name="update_service">
                  

                
              </form>
    </div>

    <div class="modal-footer">

    </div>

  </div>

</div>

</div>

<div class="modal fade" id="deleteDialog" tabindex="-1" role="dialog" aria-labelledby="gardenImageLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                
                
                <h2 class="delete-title">Confirm Deletion</h2>
                
                <p class="delete-p">Are you sure you want to delete this service?</p>

            </div>
            <div class="modal-footer">
                
                <form action="" method="post" class="form-profile" >

                  <input type="hidden" id="myDelId" class="form-control" name="del_service_id" placeholder="Title"> 

                  <input type="submit" style="float: right; " name="delete_data" value="Delete" class="btn btn-primary">
                </form>
              
                <button type="button" class="btn btn-danger center-block" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<!-- Modal: modalPoll -->

<script>
  $(document).on("click", ".openEditDialog", function () {
            var serviceid = $(this).data('serviceid');
            var servicetype = $(this).data('servicetype');
            var servicename = $(this).data('servicename');
            var servicetime = $(this).data('servicetime');
            var serviceprice = $(this).data('serviceprice');
            var servicedesc = $(this).data('servicedesc');
        

            $("#serviceid").attr("value", serviceid);
            $("#inputState option[value='"+servicetype+"']").attr("selected","selected");
            $("#servicedesc").text(servicedesc);
            $("#servicename").attr("value", servicename);
            $("#servicetime").attr("value", servicetime);
            $("#serviceprice").attr("value", serviceprice);
        });

        $(document).on("click", ".openDeleteDialog", function () {
            var myId = $(this).data('delid');
            $("#myDelId").attr("value", myId);
        });
</script>

</body>

</html>
