<?php

    require_once("Customers/includes/initialise.php");

    /*if(file_exists("validate_log.txt")) {

        //Get input stream data and log it in a file 
        $payload = file_get_contents('php://input'); 
        $file = 'validate_log.txt'; //Please make sure that this file exists and is writable 
        $fh = fopen($file, 'a'); 
        fwrite($fh, "\n====".date("d-m-Y H:i:s")."====\n"); 
        fwrite($fh, $payload."\n"); 
        fclose($fh); 
  
        if (json_decode($payload)->sender_phone == +254705972361) {
             //Return a success response to m-pesa 
                $response = array( 'status' => 01, 'description' => 'Accepted', 'subscriber_message' => 'Thank you for the payments'); 
                echo json_encode($response); 
        } else {
             //Return a success response to m-pesa 
                $response = array(  'status' => 02, 'description' => 'Rejected', 'subscriber_message' => 'You didnt make the payment' ); 
                echo json_encode($response); 
        }

    } else {
        echo "No such file";
    }*/

    $mpesaDetails = file_get_contents("validate_log.json");

    $payment->set_service_name(json_decode($mpesaDetails)->service_name);
    $payment->set_business_no(json_decode($mpesaDetails)->business_number);
    $payment->set_transaction_ref(json_decode($mpesaDetails)->transaction_reference);
    $payment->set_internal_transId(json_decode($mpesaDetails)->internal_transaction_id);
    $payment->set_trans_timestamp(json_decode($mpesaDetails)->transaction_timestamp);
    $payment->set_trans_type(json_decode($mpesaDetails)->transaction_type);
    $payment->set_account_no(json_decode($mpesaDetails)->account_number);
    $payment->set_sender_phone(json_decode($mpesaDetails)->sender_phone);
    $payment->set_first_name(json_decode($mpesaDetails)->first_name);
    $payment->set_middle_name(json_decode($mpesaDetails)->middle_name);
    $payment->set_last_name(json_decode($mpesaDetails)->last_name);
    $payment->set_amount(json_decode($mpesaDetails)->amount);
    $payment->set_currency(json_decode($mpesaDetails)->currency);
    $payment->set_signature(json_decode($mpesaDetails)->signature);

    if($payment->insert_to_mpesa_table()) {
        echo "Payment Success";
    } else {
        echo "Payment Failed";
    }

?>
