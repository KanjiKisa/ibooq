<?php

    require_once("Customers/includes/initialise.php");

    if($session->is_logged) {

      $database->get_session_details($session->session_id); 

      //echo $database->session_user_id;
      if($database->fetch_approval_status_from_reg_data($session->session_id) === "1") {
                 header("Location: admin dashboard/index.php");
               } else {
		  
		  if(isset($_POST["profile_id_upload"])) {
			  
				  $upload->set_photo_name($_FILES["front_id"]["name"]);
                  $upload->set_file_type($_FILES["front_id"]["type"]);
                  $upload->set_tmp_loc($_FILES["front_id"]["tmp_name"]);
                  $upload->set_file_error($_FILES["front_id"]["error"]);
				  
				  $upload->set_cover_photo_name($_FILES["back_id"]["name"]);
                  $upload->set_cover_file_type($_FILES["back_id"]["type"]);
                  $upload->set_cover_tmp_loc($_FILES["back_id"]["tmp_name"]);
                  $upload->set_cover_file_error($_FILES["back_id"]["error"]);
                  
                  if($upload->validate_image() && $upload->validate_id_image()) {
			  
					$database->set_front_id_pic($upload->Object_path);
					$database->set_back_id_pic($upload->Object_cover_path);
          $database->set_user_id_status("1");
					
					if($database->update_user_id_pics($database->session_user_id)) {
						$alert->message("ID pic updated","Success");
					} else {
						$alert->message("ID pic failed to update","Fail");
					}
					
				  }
			  
		  }

      if(isset($_POST["submit_licence"])) {
        
          $upload->set_photo_name($_FILES["business_licence"]["name"]);
                  $upload->set_file_type($_FILES["business_licence"]["type"]);
                  $upload->set_tmp_loc($_FILES["business_licence"]["tmp_name"]);
                  $upload->set_file_error($_FILES["business_licence"]["error"]);
        
                  
                  if($upload->validate_image()) {
        
          $database->set_business_licence_pic($upload->Object_path);
          $database->set_business_licence_status("1");
          
          if($database->update_user_business_licence_pics($database->session_user_id)) {
            $alert->message("Business Licence updated","Success");
          } else {
            $alert->message("Business Licence failed to update","Fail");
          }
          
          }
        
      }

      if(isset($_POST["submit_settlement_account"])) {
        
          $settlement_account = $_POST["settlement_account"];

          $database->set_settlement_account($settlement_account);
          $database->set_settlement_account_status("1");
          
          if($database->update_user_settlement_account($database->session_user_id)) {
            $alert->message("Settlement Account updated","Success");
          } else {
            $alert->message("Settlement Account failed to update","Fail");
          }
          
        
      }

      if(isset($_POST["submit_club_pics"])) {
        
          $upload->set_photo_name($_FILES["file_upload_profile_pic"]["name"]);
                  $upload->set_file_type($_FILES["file_upload_profile_pic"]["type"]);
                  $upload->set_tmp_loc($_FILES["file_upload_profile_pic"]["tmp_name"]);
                  $upload->set_file_error($_FILES["file_upload_profile_pic"]["error"]);
          
          $upload->set_cover_photo_name($_FILES["file_upload_cover_pic"]["name"]);
                  $upload->set_cover_file_type($_FILES["file_upload_cover_pic"]["type"]);
                  $upload->set_cover_tmp_loc($_FILES["file_upload_cover_pic"]["tmp_name"]);
                  $upload->set_cover_file_error($_FILES["file_upload_cover_pic"]["error"]);
                  
                  if($upload->validate_image() && $upload->validate_cover_image()) {
        
          $database->set_profile_pic($upload->Object_path);
          $database->set_cover_pic($upload->Object_cover_path);
          
          if($database->update_user_pics($database->session_user_id)) {
            $alert->message("Profile and Cover pic updated","Success");
          } else {
            $alert->message("Profile and Cover pic failed to update","Fail");
          }
          
          }
        
      }
		  
		  if(isset($_POST["submit_club_time"])) {
			  
				$open_days = $_POST["club_open_days"];
				$open_time = $_POST["club_open_time"];
				$close_time = $_POST["club_close_time"];
			  
				$database->set_open_days($open_days);
				$database->set_open_time($open_time);
				$database->set_close_time($close_time);
				
				if($database->update_club_time($database->session_user_id))
				{
					$alert->message("Time Status Updated","Success");					
				} else {
					$alert->message("Time Status Failed to update","Fail");
				}
			  
		  }

    }

      
    } else {

        header("Location: index.php");
  
    }

?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <title>iBooq</title>

  <link rel="SHORTCUT ICON" href="icon.ico" type="image/x-icon" />
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="Author" content="Kanji Antony Ondere,Kanji Technology Lab">
  <meta name="description" content="">

  <meta name="keywords" content="">

    <link href="https://fonts.googleapis.com/css?family=Roboto|Oswald|Varela Round|Raleway|Montserrat|Coiny" rel="stylesheet">
     <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="Front End/node_modules/material-design-lite/material.min.css">
    <script src="Front End/node_modules/material-design-lite/material.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!--<link href="Front End/custom.css" rel="stylesheet">-->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="Front End/e-shama.css" />-->
    <link href="Front End/style.css" rel="stylesheet">
	
	 <!-- Bootstrap core CSS -->
    <link href="Front End/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="Front End/fontawesome-free-5.2.0-web/css/all.css">

</head>
<body id="manager-page" onload="startup_function();">
  <div class="nav-top" id="navigator">
      <div class="logo">
      <h1 class="text-center" style=" color : #fff; font-family: 'Montserrat';"><?php echo strtoupper($database->session_username); ?></h1>
      </div>
</div>

<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    
                <a class="navbar-brand js-scroll-trigger" href="index.php"><img src="Front End/Images/ibooq.png" style="height: 50px;"></a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                  Menu
                  <i class="fas fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                  <ul class="navbar-nav ml-auto">
                    <li class="nav-item" style="color:#000">
                      <a class="nav-link js-scroll-trigger" href="index.php" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="business owner.php" target="_blank">Business Owner</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger" href="service provider.php" target="_blank">Service Provider</a>
                    </li>

                    <li class="nav-item dropdown nav-badge">
                      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="mr-2  d-lg-inline text-gray-600 small"><?php echo $database->session_username; ?></span>
                        
                      </a>
                      <!-- Dropdown - User Information -->
                      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item text-black" href="logout.php">
                          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">Logout</i>
                          
                        </a>
                      </div>
                    </li>

                    <form class="form-inline my-2 my-lg-0" action="searching.php" method="get">
                      <input class="form-control mr-sm-2" type="search" placeholder="Search" name="p" aria-label="Search">
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search">Search</button>
                    </form>

                  </ul>
                </div>
              
          </nav>

     
  <br/></br></br>

    <!--End Js slide-->

      <div class="d flex flex-column" id="shama_section">

        <h6 style="text-align : center; font-family: 'Varela Round';">Welcome <?php echo strtoupper($database->session_username); ?>, please finish your registration in four eay steps</h6>

          <div class="my-flex-item managementContent">


              <div id="sidebar">
              <ul id="managementTab">
                  <li style="font-family:'Varela Round';" id="show_id_status_tab">Step 1 (Upload national id/ passport/ driving licence)</li><!-- onclick="show_ticket_panel()" -->
                  <li id="show_id_status" style="display:none;"><?php

                                              if($database->is_uploaded_id_status_true($database->session_user_id)) {
                                                echo "1";
                                              } else {
                                                echo "0";
                                              }

                                            ?>
                    </li>
                  <li style="font-family:'Varela Round';" id="show_business_licence_status_tab">Step 2 (Business Licence)</li><!-- onclick="show_reserve_panel()" -->
                  <li id="show_business_licence_status" style="display:none;"><?php

                                              if($database->is_uploaded_business_licence_status_true($database->session_user_id)) {
                                                echo "1";
                                              } else {
                                                echo "0";
                                              }

                                            ?>
                    </li>
                  <li style="font-family:'Varela Round';" id="show_settlement_account_status_tab">Step 3 (Settlement Account)</li><!-- onclick="show_teller_panel()" -->
                  <li id="show_settlement_account_status" style="display:none;"><?php

                                              if($database->is_uploaded_settlement_account_status_true($database->session_user_id)) {
                                                echo "1";
                                              } else {
                                                echo "0";
                                              }

                                            ?>
                    </li>

				  <li style="font-family:'Varela Round';" id="final_status_tab">Step 4 (Final)</li><!--onclick="show_profile_panel()"-->
                </ul><!-- end of managementTab ul -->
              </div>
              <!-- end of sidebar div-->
<br><br>
              <!-- <div id="ticketPanel"> -->

              <div id="ticketPanel" >

                  <div class="container table-responsive">
                <h2 style="text-align : center; color : #00000; font-family: 'Montserrat';">Upload national id/ passport/ driving licence</h2>

                  <form action="" method="post" enctype="multipart/form-data">

                      <label>Front Side Of Id</label>

                       <!-- Front Image preview -->
                      <div id="idFrontImagePreview"></div>

                      <div class="form-group">

                        <input id="front_id" type="file" onchange="ValidateSize(this); frontIdPreview();" name="front_id" class="form-control-file" accept="image/*">
                    
                      </div>

                      <label>Back Side Of Id</label>

                      <!-- Front Image preview -->
                      <div id="idBackImagePreview"></div>

                      <div class="form-group">

                            <input id="back_id" name="back_id" onchange="ValidateSize(this); backIdPreview();" type="file" class="form-control-file" accept="image/*">
                       
                            <span class="input-group-addon mt-2"><button type="submit" value="Upload ID" name="profile_id_upload" class="btn-sm btn  btn-info mt-2">Upload</button></span>
                      </div>

                      
                 </form>

                
              

               </div>
              </div>
              <!-- end of ticketPanel div-->

              <div id="reservePanel" style="display:none;">



                    <h2 style="text-align : center; color : #000; font-family: 'Montserrat';">Upload Business Licence</h2>
                    <form id="uploadProductForm" action="manager.php" method="POST" enctype="multipart/form-data">

                      <div class="form-row">
                        <div class="form-group col-md-6">
                          <label>Upload licence photo</label>

                          <!-- Front Image preview -->
                          <div id="businessLicenceImagePreview"></div>

                          <input class="input-group" type="file" onchange="ValidateSize(this); businessLicencePreview();" aria-describedby="business_licence" name="business_licence" id="business_licence" accept="image/*" required>
                        </div>
                      </div>

                      <input type="submit" value="Upload Licence" name="submit_licence" class="btn btn-primary" >
                    </form>



              </div><!-- end of reservePanel div -->

              <div class="container" id="tellerPanel">

                 <form id="uploadPicForm" action="manager.php" method="POST" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-12 col-md-6 col-lg-6">
                          <label id="settlementAccount" class="form-text">Select Settlement Account.</label>
                          <select id="inputState" name="settlement_account" class="form-control">
                            <option selected>Mpesa</option>
                          </select>
                        </div>
                      </div><br>
                      <input type="submit" value="Update" name="submit_settlement_account" class="btn btn-primary" >
                  </form>

              </div><!-- end of tellerPanel div -->
			  

			  
			  <div class="container" id="profilePanel">
			      
                <h4>Thank you for creating a business owner account with ibooq. Wait for account approval to proceed</h4>   

              </div><!-- end of profilePanel div -->

          </div>

    </div>

    <footer class="footer">
      <div class="foo-top">
        <div class="container text-center">
          <div class="row no-gutters align-items-center">
            <div class="col-md-12">
              <div class="widget widget-insta-feed">
                <ul class="list-unstyled mt-4">
                  <li><a href="#" class="fb"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#" class="tweet"><i class="fab fa-twitter"></i></a></li>
                  <li><a href="#" class="send"><i class="fab fa-whatsapp"></i></a></li>
                  <li><a href="#" class="insta"><i class="fab fa-instagram"></i></a></li>
                  
                </ul>
              </div>
              <div class="widget widget-address">
                <address>
                  
                  <a href="tel:"><i class="ti-mobile mr-2"></i>0700...</a>
                  <a href="mailto:" class="d-block"><i class="ti-email mr-2"></i> info@ibooq.co.ke</a>
                </address>
              </div>
            </div>

          </div>
        </div>
      </div>

    </footer>

<style>
@media(min-width:800px){
.navigator{
    padding: 0.8rem 1.2rem;
}

.navigator .logo a{
    
    text-transform: uppercase;
    font-family: 'Oswald';
}
    .navigator .link a{
        padding: 0.7rem 1.2rem;
        text-decoration: none;
        text-transform: uppercase;
        border: 1px solid #96980f;
      
    }
    
    .navigator .link{
        margin-bottom: 1.2rem;
    }
}

@media(max-width: 576px){
    .navigator .link{
        margin-bottom: 1rem;
        
    }
    
    .navigator .link a{
        padding: 0.7rem 0.8rem;
        text-decoration: none;
        text-transform: uppercase;
        border: 1px solid #96980f;
        
        
    }
    
    .navigator .logo{
        display:none;
    }
    
    h1{
        font-size: 1.4rem;
    }
}
</style>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="Front End/styling/script.js"></script>
    <script src="Front End/scripting/e-shama.js"></script>

	
	<!-- Bootstrap core JavaScript -->

    </script>

    <script>
        function ValidateSize(file) {
            var FileSize = file.files[0].size / 1024 / 1024; // in MB
            if (FileSize > 5) {
                alert('File size exceeds 5 MB. Choose file with a lesser size.');
               $(file).val(''); //for clearing with Jquery
            } else {
    
            }
        }
        
        function frontIdPreview(){
            var fileInput = document.getElementById('front_id');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            /*if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }else{*/
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('idFrontImagePreview').innerHTML = '<img class="img-fluid" style="min-height:150px;width:70%;" src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            //}
        }    

        function backIdPreview(){
            var fileInput = document.getElementById('back_id');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            /*if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }else{*/
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('idBackImagePreview').innerHTML = '<img class="img-fluid" style="min-height:150px;width:70%;" src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            //}
        }   

        function businessLicencePreview(){
            var fileInput = document.getElementById('business_licence');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            /*if(!allowedExtensions.exec(filePath)){
                alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
                fileInput.value = '';
                return false;
            }else{*/
                //Image preview
                if (fileInput.files && fileInput.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        document.getElementById('businessLicenceImagePreview').innerHTML = '<img class="img-fluid" style="min-height:150px;width:70%;" src="'+e.target.result+'"/>';
                    };
                    reader.readAsDataURL(fileInput.files[0]);
                }
            //}
        }    

    </script>
  <!--side bars-->
      
</body>
</html>




